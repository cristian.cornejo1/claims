package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "subCategory", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "subCategory", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Claim Subcategory identifier.
     */
    private String id;
    /**
     * Claim Subcategory name.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}