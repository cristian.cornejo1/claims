package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 29/10/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "transactionType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "transactionType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Transaction type identifier. DISCLAIMER: UNCATEGORIZED type will be accompanied by information that must be fulfilled on the attribute internalCode that belogs to transactionType.
     */
    private String id;
    private String name;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}