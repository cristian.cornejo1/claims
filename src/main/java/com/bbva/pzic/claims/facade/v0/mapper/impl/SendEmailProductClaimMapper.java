package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.DTOIntContactSendEmail;
import com.bbva.pzic.claims.business.dto.DTOIntDocument;
import com.bbva.pzic.claims.business.dto.InputSendEmailProductClaim;
import com.bbva.pzic.claims.facade.v0.dto.ContactSendEmail;
import com.bbva.pzic.claims.facade.v0.dto.Document;
import com.bbva.pzic.claims.facade.v0.dto.SendEmail;
import com.bbva.pzic.claims.facade.v0.mapper.ISendEmailProductClaimMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy
 */
@Component
public class SendEmailProductClaimMapper implements ISendEmailProductClaimMapper {

    private static final Log LOG = LogFactory.getLog(SendEmailProductClaimMapper.class);

    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }

    @Override
    public InputSendEmailProductClaim mapIn(final String productClaimId, final SendEmail sendEmail) {
        LOG.info("... called method SendEmailProductClaimMapper.mapIn ...");
        InputSendEmailProductClaim input = new InputSendEmailProductClaim();
        input.setProductClaimId(productClaimId);
        input.setAap(serviceInvocationContext.getProperty(BackendContext.AAP));
        input.setClientId(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID));
        input.setContact(mapOutContact(sendEmail.getContact()));
        input.setDocuments(mapInDocuments(sendEmail.getDocuments()));
        return input;
    }

    private DTOIntContactSendEmail mapOutContact(final ContactSendEmail contact) {
        if (contact == null) {
            return null;
        }
        DTOIntContactSendEmail dtoIntContactSendEmail = new DTOIntContactSendEmail();
        dtoIntContactSendEmail.setId(contact.getId());
        dtoIntContactSendEmail.setAddress(contact.getAddress());
        dtoIntContactSendEmail.setContactDetailType(contact.getContactDetailType());
        return dtoIntContactSendEmail;
    }

    private List<DTOIntDocument> mapInDocuments(final List<Document> documents) {
        if (CollectionUtils.isEmpty(documents)) {
            return null;
        }
        return documents.stream().filter(Objects::nonNull).map(this::mapInDocument).collect(Collectors.toList());
    }

    private DTOIntDocument mapInDocument(final Document document) {
        DTOIntDocument dtoIntDocument = new DTOIntDocument();
        dtoIntDocument.setId(document.getId());
        return dtoIntDocument;
    }
}
