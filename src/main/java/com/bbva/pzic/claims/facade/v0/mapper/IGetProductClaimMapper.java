package com.bbva.pzic.claims.facade.v0.mapper;

import com.bbva.pzic.claims.business.dto.InputGetProductClaim;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaimsData;

/**
 * Created on 28/11/2019.
 *
 * @author Entelgy
 */
public interface IGetProductClaimMapper {

    InputGetProductClaim mapIn(String productClaimId);

    ProductClaimsData mapOut(ProductClaims productClaims);
}
