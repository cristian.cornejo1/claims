package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "tracking", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "tracking", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tracking implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Tracking identifier.
     */
    private String id;
    /**
     * Tracking description.
     */
    private String description;
    /**
     * Tracking last update date.
     */
    private String lastUpdateDate;
    /**
     * Status of the claim of the tracking.
     */
    private StatusTracking status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public StatusTracking getStatus() {
        return status;
    }

    public void setStatus(StatusTracking status) {
        this.status = status;
    }
}
