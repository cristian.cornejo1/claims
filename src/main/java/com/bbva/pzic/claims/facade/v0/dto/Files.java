package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "files", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "files", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Files implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String group;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}