package com.bbva.pzic.claims.facade.v0.mapper;

import com.bbva.pzic.claims.business.dto.InputSendEmailProductClaim;
import com.bbva.pzic.claims.facade.v0.dto.SendEmail;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy
 */
public interface ISendEmailProductClaimMapper {

    InputSendEmailProductClaim mapIn(String productClaimId, SendEmail sendEmail);
}
