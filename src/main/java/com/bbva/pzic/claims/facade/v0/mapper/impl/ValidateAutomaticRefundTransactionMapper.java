package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.ContactDetail;
import com.bbva.pzic.claims.canonic.*;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.claims.facade.v0.mapper.IValidateAutomaticRefundTransactionMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class ValidateAutomaticRefundTransactionMapper implements IValidateAutomaticRefundTransactionMapper {

    private static final Log LOG = LogFactory.getLog(ValidateAutomaticRefundTransactionMapper.class);

    private Translator translator;
    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public ValidateAutomaticRefundTransactionMapper(Translator translator,
                                                    ServiceInvocationContext serviceInvocationContext) {
        this.translator = translator;
        this.serviceInvocationContext = serviceInvocationContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntAutomaticRefundTransaction mapIn(
            final AutomaticRefundTransaction automaticRefundTransaction) {
        LOG.info("... called method ValidateAutomaticRefundTransactionMapper.mapIn ...");
        DTOIntAutomaticRefundTransaction dtoInt = new DTOIntAutomaticRefundTransaction();
        dtoInt.setPetitioner(mapInPetitioners(automaticRefundTransaction.getPetitioner()));
        dtoInt.setRelatedClaim(mapInRelatedClaim(automaticRefundTransaction.getRelatedClaim()));
        dtoInt.setReason(mapInReason(automaticRefundTransaction.getReason()));
        dtoInt.setApp(serviceInvocationContext.getProperty(ServiceInvocationContext.AAP));
        dtoInt.setOperations(mapInOperations(automaticRefundTransaction.getOperations()));
        return dtoInt;
    }

    private DTOIntReasonRefund mapInReason(final ReasonRefund reason) {
        if (reason == null) {
            return null;
        }
        DTOIntReasonRefund dtoIn = new DTOIntReasonRefund();
        dtoIn.setId(reason.getId());
        return dtoIn;
    }

    private DTOIntRelatedClaim mapInRelatedClaim(final RelatedClaim relatedClaim) {
        if (relatedClaim == null) {
            return null;
        }
        DTOIntRelatedClaim dtoIn = new DTOIntRelatedClaim();
        dtoIn.setId(relatedClaim.getId());
        return dtoIn;
    }

    private DTOIntPetitioners mapInPetitioners(final Petitioner petitioner) {
        if (petitioner == null) {
            return null;
        }
        DTOIntPetitioners dtoIn = new DTOIntPetitioners();
        dtoIn.setId(petitioner.getId());
        dtoIn.setPetitioner(mapInPetitioner(petitioner));
        return dtoIn;
    }

    private DTOIntPetitioner mapInPetitioner(final Petitioner petitioner) {
        if (petitioner == null) {
            return null;
        }
        DTOIntPetitioner dtoIn = new DTOIntPetitioner();
        dtoIn.setId(petitioner.getId());
        dtoIn.setIdentityDocument(mapInIdentityDocument(petitioner.getIdentityDocument()));
        dtoIn.setSegment(mapInSegment(petitioner.getSegment()));
        dtoIn.setContactDetails(mapInContactDetails(petitioner.getContactDetails()));
        return dtoIn;
    }

    private DTOIntIdentityDocument mapInIdentityDocument(final IdentityDocument identityDocument) {
        if (identityDocument == null) {
            return null;
        }
        DTOIntIdentityDocument dtoIn = new DTOIntIdentityDocument();
        dtoIn.setDocumentType(mapInDocumentType(identityDocument.getDocumentType()));
        dtoIn.setDocumentNumber(identityDocument.getDocumentNumber());
        return dtoIn;
    }

    private DTOIntDocumentType mapInDocumentType(final DocumentType documentType) {
        if (documentType == null) {
            return null;
        }
        DTOIntDocumentType dtoIn = new DTOIntDocumentType();
        dtoIn.setId(documentType.getId());
        return dtoIn;
    }

    private DTOIntSegment mapInSegment(final Segment segment) {
        if (segment == null) {
            return null;
        }
        DTOIntSegment dtoIn = new DTOIntSegment();
        dtoIn.setId(segment.getId());
        return dtoIn;
    }

    private List<DTOIntContact> mapInContactDetails(final List<ContactDetail> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)) {
            return null;
        }
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContactDetail).collect(Collectors.toList());
    }

    private DTOIntContact mapInContactDetail(final ContactDetail contactDetail) {
        if (contactDetail == null) {
            return null;
        }
        DTOIntContact dtoIn = new DTOIntContact();
        dtoIn.setContacto(mapInContact(contactDetail.getContact()));
        return dtoIn;
    }

    private DTOIntBaseContact mapInContact(final BaseContract contact) {
        if (contact == null) {
            return null;
        }
        DTOIntBaseContact dtoIn = new DTOIntBaseContact();
        String contactDetailType = contact.getContactDetailType();
        if (contactDetailType != null) {
            dtoIn.setContactDetailType(translator.translateFrontendEnumValueStrictly("claims.petitioner.contactDetailType", contactDetailType));
            // FRONT: EMAIL -> BACK: EMAIL || FRONT: LANDLINE -> BACK: PHONE || FRONT:MOBILE -> BACK: MOBILE
            if (contactDetailType.equalsIgnoreCase("LANDLINE") || contactDetailType.equalsIgnoreCase("MOBILE")) {
                // in common
                dtoIn.setNumber(contact.getNumber());
                dtoIn.setCountryCode(contact.getCountryCode());
                if (contactDetailType.equalsIgnoreCase("LANDLINE")) {
                    if (contact.getPhoneType() != null) {
                        dtoIn.setPhoneType(translator.translateFrontendEnumValueStrictly("claims.petitioner.contact.phoneType", contact.getPhoneType()));
                    }
                    dtoIn.setCountry(mapInCountry(contact.getCountry()));
                    dtoIn.setRegionalCode(contact.getRegionalCode());
                    dtoIn.setExtension(contact.getExtension());
                }
                if (contactDetailType.equalsIgnoreCase("MOBILE")) {
                    dtoIn.setPhoneCompany(mapInPhoneCompany(contact.getPhoneCompany()));
                }
            }
            if (contactDetailType.equalsIgnoreCase("EMAIL")) {
                dtoIn.setAddress(contact.getAddress());
                dtoIn.setReceivesNotifications(contact.getReceivesNotifications());
            }
        }
        return dtoIn;
    }

    private DTOIntCountry mapInCountry(final Country country) {
        if (country == null) {
            return null;
        }
        DTOIntCountry dtoIn = new DTOIntCountry();
        dtoIn.setId(country.getId());
        return dtoIn;
    }

    private DTOIntPhoneCompany mapInPhoneCompany(final PhoneCompany phoneCompany) {
        if (phoneCompany == null) {
            return null;
        }
        DTOIntPhoneCompany dtoIn = new DTOIntPhoneCompany();
        dtoIn.setId(phoneCompany.getId());
        return dtoIn;
    }

    private List<DTOIntOperation> mapInOperations(final List<Operation> operations) {
        if (CollectionUtils.isEmpty(operations)) {
            return null;
        }
        return operations.stream().filter(Objects::nonNull).map(this::mapInOperation).collect(Collectors.toList());
    }

    private DTOIntOperation mapInOperation(final Operation operation) {
        if (operation == null) {
            return null;
        }
        DTOIntOperation dtoIn = new DTOIntOperation();
        dtoIn.setSubproductRefund(mapInSubProductRefund(operation.getSubproductRefund()));
        dtoIn.setDetail(operation.getDetail());
        if (operation.getOperationType() != null) {
            dtoIn.setOperationType(translator.translateFrontendEnumValueStrictly("automaticRefundTransactions.operationType", operation.getOperationType()));
        }
        dtoIn.setRequestedAmount(mapInRequestedAmount(operation.getRequestedAmount()));
        dtoIn.setBank(mapInBank(operation.getBank()));
        dtoIn.setRelatedContract(mapInRelatedContract(operation.getRelatedContract()));
        dtoIn.setClaimReason(mapInClaimReason(operation.getClaimReason()));
        return dtoIn;
    }

    private DTOIntClaimReason mapInClaimReason(final ClaimReason claimReason) {
        if (claimReason == null) {
            return null;
        }
        DTOIntClaimReason dtoIn = new DTOIntClaimReason();
        dtoIn.setContract(mapInContract(claimReason.getContract()));
        return dtoIn;
    }

    private DTOIntBaseContract mapInContract(final Contract contract) {
        if (contract == null) {
            return null;
        }
        DTOIntBaseContract dtoIn = new DTOIntBaseContract();
        String contractType = contract.getContractType();
        if (contractType != null) {
            dtoIn.setContractType(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.contractType", contractType));
            // FRONT: CARD -> BACK: TARJE || FRONT: ACCOUNT -> BACK: CUENT || FRONT:LOAN -> BACK: PREST
            if (contractType.equalsIgnoreCase("CARD") || contractType.equalsIgnoreCase("ACCOUNT") || contractType.equalsIgnoreCase("LOAN")) {
                //in common
                dtoIn.setId(contract.getId());
                dtoIn.setNumber(contract.getNumber());
                dtoIn.setNumberType(mapInNumberType(contract.getNumberType()));
                dtoIn.setTransactions(mapInTransactions(contract.getTransactions()));
                if (contractType.equalsIgnoreCase("CARD")) {
                    dtoIn.setCardAgreement(mapInCardAgreement(contract.getCardAgreement()));
                }
                if (contractType.equalsIgnoreCase("ACCOUNT")) {
                    dtoIn.setAccountType(mapInAccountType(contract.getAccountType()));
                }
                if (contractType.equalsIgnoreCase("LOAN")) {
                    dtoIn.setLoanType(mapInLoanType(contract.getLoanType()));
                }
            }
        }
        return dtoIn;
    }

    private DTOIntLoanType mapInLoanType(final LoanType loanType) {
        if (loanType == null) {
            return null;
        }
        DTOIntLoanType dtoIn = new DTOIntLoanType();
        dtoIn.setId(loanType.getId());
        return dtoIn;
    }

    private DTOIntCardAgreement mapInCardAgreement(final CardAgreement cardAgreement) {
        if (cardAgreement == null) {
            return null;
        }
        DTOIntCardAgreement dtoIn = new DTOIntCardAgreement();
        dtoIn.setId(cardAgreement.getId());
        dtoIn.setSubProduct(mapInSubProduct(cardAgreement.getSubProduct()));
        return dtoIn;
    }

    private DTOIntSubProduct mapInSubProduct(final SubProduct subProduct) {
        if (subProduct == null) {
            return null;
        }
        DTOIntSubProduct dtoIn = new DTOIntSubProduct();
        dtoIn.setId(subProduct.getId());
        return dtoIn;
    }

    private List<DTOIntTransaction> mapInTransactions(final List<Transaction> transactions) {
        if (CollectionUtils.isEmpty(transactions)) {
            return null;
        }
        return transactions.stream().filter(Objects::nonNull).map(this::mapInTransaction).collect(Collectors.toList());
    }

    private DTOIntTransaction mapInTransaction(final Transaction transaction) {
        if (transaction == null) {
            return null;
        }
        DTOIntTransaction dtoIn = new DTOIntTransaction();
        dtoIn.setId(transaction.getId());
        dtoIn.setOperationDate(transaction.getOperationDate());
        dtoIn.setConcept(transaction.getConcept());
        dtoIn.setTransactionType(mapInTransactionType(transaction.getTransactionType()));
        dtoIn.setLocalAmount(mapInLocalAmount(transaction.getLocalAmount()));
        dtoIn.setStore(mapInStore(transaction.getStore()));
        return dtoIn;
    }

    private DTOIntStore mapInStore(final Store store) {
        if (store == null) {
            return null;
        }
        DTOIntStore dtoIn = new DTOIntStore();
        dtoIn.setId(store.getId());
        dtoIn.setName(store.getName());
        return dtoIn;
    }

    private DTOIntLocalAmount mapInLocalAmount(final LocalAmount localAmount) {
        if (localAmount == null) {
            return null;
        }
        DTOIntLocalAmount dtoIn = new DTOIntLocalAmount();
        dtoIn.setAmount(localAmount.getAmount());
        dtoIn.setCurrency(localAmount.getCurrency());
        return dtoIn;
    }

    private DTOIntTransactionType mapInTransactionType(final TransactionType transactionType) {
        if (transactionType == null) {
            return null;
        }
        DTOIntTransactionType dtoIn = new DTOIntTransactionType();
        if (transactionType.getId() != null) {
            dtoIn.setId(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.transactionType", transactionType.getId()));
        }
        return dtoIn;
    }

    private DTOIntNumberType mapInNumberType(final NumberType numberType) {
        if (numberType == null) {
            return null;
        }
        DTOIntNumberType dtoIn = new DTOIntNumberType();
        if (numberType.getId() != null) {
            dtoIn.setId(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.numberType", numberType.getId()));
        }
        return dtoIn;
    }

    private DTOIntAccountType mapInAccountType(final AccountType accountType) {
        if (accountType == null) {
            return null;
        }
        DTOIntAccountType dtoIn = new DTOIntAccountType();
        dtoIn.setId(accountType.getId());
        return dtoIn;
    }

    private DTOIntRelatedContract mapInRelatedContract(final RelatedContract relatedContract) {
        if (relatedContract == null) {
            return null;
        }
        DTOIntRelatedContract dtoIn = new DTOIntRelatedContract();
        dtoIn.setId(relatedContract.getId());
        dtoIn.setRequestedAmount(mapInRequestedAmount(relatedContract.getRequestedAmount()));
        return dtoIn;
    }

    private DTOIntBank mapInBank(final Bank bank) {
        if (bank == null) {
            return null;
        }
        DTOIntBank dtoIn = new DTOIntBank();
        dtoIn.setId(bank.getId());
        dtoIn.setBranch(mapInBranch(bank.getBranch()));
        return dtoIn;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if (branch == null) {
            return null;
        }
        DTOIntBranch dtoIn = new DTOIntBranch();
        dtoIn.setId(branch.getId());
        return dtoIn;
    }

    private DTOIntRequestedAmount mapInRequestedAmount(final RequestedAmount requestedAmount) {
        if (requestedAmount == null) {
            return null;
        }
        DTOIntRequestedAmount dtoIn = new DTOIntRequestedAmount();
        dtoIn.setAmount(requestedAmount.getAmount());
        dtoIn.setCurrency(requestedAmount.getCurrency());
        return dtoIn;
    }

    private DTOIntSubproductRefund mapInSubProductRefund(final SubproductRefund subproductRefund) {
        if (subproductRefund == null) {
            return null;
        }
        DTOIntSubproductRefund dtoIn = new DTOIntSubproductRefund();
        String refundId = subproductRefund.getId();
        if (refundId != null) {
            dtoIn.setId(refundId);
            if (refundId.equalsIgnoreCase("MPTAR")) {
                if (subproductRefund.getRefundMode() != null) {
                    dtoIn.setRefundMode(translator.translateFrontendEnumValueStrictly("automaticRefundTransactions.refundMode", subproductRefund.getRefundMode()));
                }
            }
            //in common and default else
            dtoIn.setNumber(subproductRefund.getNumber());
        }

        return dtoIn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServiceResponse<AutomaticRefundTransaction> mapOut(
            final AutomaticRefundTransaction automaticRefundTransaction) {
        LOG.info("... called method ValidateAutomaticRefundTransactionMapper.mapOut ...");
        if (automaticRefundTransaction == null) {
            return null;
        }
        return ServiceResponse.data(automaticRefundTransaction).build();
    }
}
