package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created 02/02/2021
 *
 * @author Entelgy
 */
@XmlRootElement(name = "categorySearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "categorySearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CategorySearch implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Claim category identifier.
     */
    private String id;
    /**
     * Claim category name.
     */
    private String name;
    /**
     * Claim sub category.
     */
    private SubCategory subCategory;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }
}