package com.bbva.pzic.claims.facade.v0.dto;


/**
 * @author Entelgy
 */

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "values", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "values", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Values implements Serializable {
    private static final long serialVersionUID = 1L;

    private Factor factor;
    private String priceType;

    public Factor getFactor() {
        return factor;
    }

    public void setFactor(Factor factor) {
        this.factor = factor;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }
}