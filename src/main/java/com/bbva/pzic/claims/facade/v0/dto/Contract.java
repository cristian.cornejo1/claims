package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 22/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "contract", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "contract", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contract implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * private static final long serialVersionUID = 1L;
     */
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * Number of the contracted product.  DISCLAIMER: In case this field is not informed id is required.
     */
    @DatoAuditable(omitir = true)
    private String number;
    /**
     * Number type of the contracted product.  DISCLAIMER: In case number is informed this field is required.
     */
    private NumberType numberType;
    /**
     * Product associated with the contract.
     */
    private ContractProduct product;
    /**
     * Product type.
     */
    private String contractType;
    /**
     * Information of transactions related to reason of the claim.
     */
    private List<Transaction> transactions;
    /**
     * Card agreement.
     */
    private CardAgreement cardAgreement;
    /**
     * Unique identifier of the account.
     */
    private AccountType accountType;
    /**
     * Loan type.
     */
    private LoanType loanType;

    private SubProduct subProduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(NumberType numberType) {
        this.numberType = numberType;
    }

    public ContractProduct getProduct() {
        return product;
    }

    public void setProduct(ContractProduct product) {
        this.product = product;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public CardAgreement getCardAgreement() {
        return cardAgreement;
    }

    public void setCardAgreement(CardAgreement cardAgreement) {
        this.cardAgreement = cardAgreement;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

    public SubProduct getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(SubProduct subProduct) {
        this.subProduct = subProduct;
    }
}
