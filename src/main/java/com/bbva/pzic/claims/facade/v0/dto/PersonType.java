package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "personType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "personType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Person type identifier.
     */
    private String id;
    /**
     * Person type name.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
