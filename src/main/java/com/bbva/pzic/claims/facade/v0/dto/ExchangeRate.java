package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "exchangeRate", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "exchangeRate", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeRate implements Serializable {
    private static final long serialVersionUID = 1L;

    private Values values;

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }
}