package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.InputGetAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.facade.v0.mapper.IGetAutomaticRefundTransactionMapper;
import com.bbva.pzic.claims.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class GetAutomaticRefundTransactionMapper implements IGetAutomaticRefundTransactionMapper {

    private static final Log LOG = LogFactory.getLog(GetAutomaticRefundTransactionMapper.class);

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputGetAutomaticRefundTransaction mapIn(final String automaticRefundTransactionId) {
        LOG.info("... called method GetAutomaticRefundTransactionMapper.mapIn ...");
        InputGetAutomaticRefundTransaction input = new InputGetAutomaticRefundTransaction();
        input.setApp(serviceInvocationContext.getProperty(BackendContext.AAP));
        input.setAutomaticRefundTransactionId(automaticRefundTransactionId);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServiceResponse<AutomaticRefundTransaction> mapOut(
            final AutomaticRefundTransaction automaticRefundTransaction) {
        LOG.info("... called method GetAutomaticRefundTransactionMapper.mapOut ...");
        if (automaticRefundTransaction == null) {
            return null;
        }
        return ServiceResponse.data(automaticRefundTransaction).build();
    }
}