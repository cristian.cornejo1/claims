package com.bbva.pzic.claims.facade;

/**
 * @author Entelgy
 */
public final class RegistryIds {

    public static final String SMC_REGISTRY_ID_OF_LIST_AUTOMATIC_REFUND_TRANSACTIONS = "SMCPE1810332";

    public static final String SN_REGISTRY_GET_AUTOMATIC_REFUND_TRANSACTION = "SMCPE1810333";

    public static final String SN_REGISTRY_ID_CREATE_PRODUCT_CLAIM = "SMCPE1920052";

    public static final String SMC_REGISTRY_ID_OF_SEARCH_PRODUCT_CLAIMS = "SMCPE1920054";

    public static final String SN_REGISTRY_GET_PRODUCT_CLAIM = "SMCPE1920053";

    public static final String SN_REGISTRY_SEND_EMAIL_PRODUCT_CLAIM = "SMCPE1920055";

    public static final String SMC_REGISTRY_MODIFY_PARTIAL_AUTOMATIC_REFUND_TRANSACTION = "SMGG20200301";

    private RegistryIds() {
    }
}
