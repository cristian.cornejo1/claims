package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.claims.facade.v0.mapper.IModifyPartialAutomaticRefundTransactionMapper;
import com.bbva.pzic.claims.util.FunctionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.pzic.claims.util.Constants.*;

@Component
public class ModifyPartialAutomaticRefundTransactionMapper implements IModifyPartialAutomaticRefundTransactionMapper {

    private static final Log LOG = LogFactory.getLog(GetProductClaimMapper.class);

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Override
    public InputModifyAutomaticRefundTransaction mapIn(final String automaticRefundTransactionId,
                                                       final AutomaticRefundTransactionF automaticRefundTransaction) {
        LOG.info("... called method ModifyPartialAutomaticRefundTransactionMapper.mapIn ...");
        InputModifyAutomaticRefundTransaction input = new InputModifyAutomaticRefundTransaction();
        input.setUser(serviceInvocationContext.getUser());
        input.setIdCanal(serviceInvocationContext.getProperty(PROPERTIES_CANAL));
        input.setAutomaticRefundTransactionId(automaticRefundTransactionId);
        input.setPartialTransaction(mapInPartialTransaction(automaticRefundTransaction));
        return input;
    }

    private DTOIntPartialTransaction mapInPartialTransaction(final AutomaticRefundTransactionF automaticRefundTransaction) {
        if (automaticRefundTransaction == null) {
            return null;
        }
        DTOIntPartialTransaction input = new DTOIntPartialTransaction();
        input.setCustomerConfirmation(FunctionUtils.convertStringToBoolean(automaticRefundTransaction.getCustomerConfirmation()));
        input.setStatus(mapInStatus(automaticRefundTransaction.getStatus()));
        input.setSituation(mapInSituation(automaticRefundTransaction.getSituation()));
        return input;
    }

    private DTOIntSituation mapInSituation(final Situation situation){
        if(situation == null){
            return null;
        }
        DTOIntSituation input = new DTOIntSituation();
        input.setId(situation.getId());
        return input;
    }

    private DTOIntStatus mapInStatus(final Status status) {
        if( status == null){
            return null;
        }
        DTOIntStatus input = new DTOIntStatus();
        input.setSubStatus(mapInSubStatus(status.getSubStatus()));
        return input;
    }

    private DTOIntSubStatus mapInSubStatus(final SubStatus subStatus) {
        if(subStatus == null){
            return null;
        }
        DTOIntSubStatus input = new DTOIntSubStatus();
        input.setId(subStatus.getId());
        return input;
    }

    @Override
    public ServiceResponse<PartialTransaction> mapOut(final PartialTransaction dtoOut) {
        LOG.info("... called method ModifyPartialAutomaticRefundTransactionMapper.mapOut ...");
        if(dtoOut == null){
            return null;
        }
        return ServiceResponse.data(dtoOut).build();
    }
}