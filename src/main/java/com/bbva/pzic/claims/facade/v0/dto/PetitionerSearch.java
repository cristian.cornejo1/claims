package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.claims.canonic.Address;
import com.bbva.pzic.claims.canonic.Bank;
import com.bbva.pzic.claims.canonic.PetitionerType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;
/**
 * @author Entelgy
 */
@XmlRootElement(name = "petitionerSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "petitionerSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PetitionerSearch implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Petitioner identifier.
     */
    private String id;
    /**
     * Name of the person.
     */
    @DatoAuditable(omitir = true)
    private String firstName;
    /**
     * Person's name occurring between the firstName and family names, as a second or additonal given name.
     */
    private String middleName;
    /**
     * Family name. Name(s) written after the first name and the middle name. First surname.
     */
    @DatoAuditable(omitir = true)
    private String lastName;
    /**
     * Second surname. Mother's last name.
     */
    @DatoAuditable(omitir = true)
    private String secondLastName;
    /**
     * Petitioner type.
     */
    private PetitionerType petitionerType;
    /**
     * Indicator of the relation of the persons with the bank.
     */
    private String bankRelationType;
    /**
     * Petitioner address.
     */
    private Address address;

    /**
     * This object represents the specific information of the contact depending on its type,
     * this is optional for the AUTHORIZED type petitioner.
     */
    private List<ContactDetail> contactDetails;
    /**
     * It represents the segment that the petitioner belongs.
     */
    private Segment segment;
    /**
     * The bank that the petitioner is assigned.
     */
    private Bank bank;
    /**
     * Classification of the customer in the bank. According to the days of delay that a debtor has not been paying his credit (risk of default),
     * he receives a category, with which the bank identifies his behavior in the payments he has with his creditors.
     */
    private BankClassification bankClassification;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public PetitionerType getPetitionerType() {
        return petitionerType;
    }

    public void setPetitionerType(PetitionerType petitionerType) {
        this.petitionerType = petitionerType;
    }

    public String getBankRelationType() {
        return bankRelationType;
    }

    public void setBankRelationType(String bankRelationType) {
        this.bankRelationType = bankRelationType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public BankClassification getBankClassification() {
        return bankClassification;
    }

    public void setBankClassification(BankClassification bankClassification) {
        this.bankClassification = bankClassification;
    }
}