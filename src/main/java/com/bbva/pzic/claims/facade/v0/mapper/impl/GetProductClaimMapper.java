package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.claims.business.dto.InputGetProductClaim;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaimsData;
import com.bbva.pzic.claims.facade.v0.mapper.IGetProductClaimMapper;
import com.bbva.pzic.claims.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 28/11/2019.
 *
 * @author Entelgy
 */
@Mapper
public class GetProductClaimMapper implements IGetProductClaimMapper {

    private static final Log LOG = LogFactory.getLog(GetProductClaimMapper.class);

    @Override
    public InputGetProductClaim mapIn(final String productClaimId) {
        LOG.info("... called method GetProductClaimMapper.mapIn ...");
        InputGetProductClaim inputGetProductClaim = new InputGetProductClaim();
        inputGetProductClaim.setProductClaimId(productClaimId);
        return inputGetProductClaim;
    }

    @Override
    public ProductClaimsData mapOut(final ProductClaims productClaims) {
        LOG.info("... called method GetProductClaimMapper.mapOut ...");
        if (productClaims == null) {
            return null;
        }
        ProductClaimsData result = new ProductClaimsData();
        result.setData(productClaims);
        return result;
    }
}
