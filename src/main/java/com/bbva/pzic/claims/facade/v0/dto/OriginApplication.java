package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "originApplication", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "originApplication", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class OriginApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Application identifier.
     */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}