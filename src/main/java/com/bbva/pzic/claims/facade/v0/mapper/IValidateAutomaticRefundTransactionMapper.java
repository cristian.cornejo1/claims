package com.bbva.pzic.claims.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.claims.business.dto.DTOIntAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface IValidateAutomaticRefundTransactionMapper {

    DTOIntAutomaticRefundTransaction mapIn(
            AutomaticRefundTransaction automaticRefundTransaction);

    ServiceResponse<AutomaticRefundTransaction> mapOut(
            AutomaticRefundTransaction automaticRefundTransaction);
}