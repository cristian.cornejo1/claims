package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 10/03/2021.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "listProductClaimsData", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "listProductClaimsData", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListProductClaimsData implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<ProductClaims> data;

    public List<ProductClaims> getData() {
        return data;
    }

    public void setData(List<ProductClaims> data) {
        this.data = data;
    }
}
