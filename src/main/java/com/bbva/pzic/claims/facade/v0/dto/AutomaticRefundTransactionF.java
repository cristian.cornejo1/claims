package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.pzic.routine.commons.utils.BooleanAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;

@XmlRootElement(name = "automaticRefundTransactionF", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "automaticRefundTransactionF", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AutomaticRefundTransactionF implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private String customerConfirmation;

    private Status status;

    private Situation situation;

    public String getCustomerConfirmation() {
        return customerConfirmation;
    }

    public void setCustomerConfirmation(String customerConfirmation) {
        this.customerConfirmation = customerConfirmation;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Situation getSituation() {
        return situation;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }
}
