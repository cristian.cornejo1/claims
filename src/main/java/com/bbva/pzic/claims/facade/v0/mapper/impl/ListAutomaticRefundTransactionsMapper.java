package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.InputListAutomaticRefundTransactions;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.facade.v0.mapper.IListAutomaticRefundTransactionsMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class ListAutomaticRefundTransactionsMapper implements IListAutomaticRefundTransactionsMapper {

    private static final Log LOG = LogFactory.getLog(ListAutomaticRefundTransactionsMapper.class);

    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }

    @Override
    public InputListAutomaticRefundTransactions mapIn(final String customerId) {
        LOG.info("... called method ListAutomaticRefundTransactionsMapper.mapIn ...");
        InputListAutomaticRefundTransactions input = new InputListAutomaticRefundTransactions();
        input.setApp(serviceInvocationContext.getProperty(BackendContext.AAP));
        input.setCustomerId(mapCustomerIdIn(customerId));
        return input;
    }

    private String mapCustomerIdIn(final String customerId) {
        if (customerId == null) {
            return serviceInvocationContext.getProperty(BackendContext.ASTA_MX_USER_ID);
        }
        return customerId;
    }

    @Override
    public ServiceResponse<List<AutomaticRefundTransaction>> mapOut(final List<AutomaticRefundTransaction> automaticRefundTransaction) {
        LOG.info("... called method ListAutomaticRefundTransactionsMapper.mapOut ...");
        if (automaticRefundTransaction.isEmpty()) {
            return null;
        }
        return ServiceResponse.data(automaticRefundTransaction).build();
    }
}