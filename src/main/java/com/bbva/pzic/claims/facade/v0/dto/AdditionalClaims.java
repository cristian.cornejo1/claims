package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.pzic.claims.canonic.Bank;
import com.bbva.pzic.claims.canonic.ClaimType;
import com.bbva.pzic.claims.canonic.Product;
import com.bbva.pzic.routine.commons.utils.CustomCalendarAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "additionalClaims", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "additionalClaims", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdditionalClaims implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Type of claim made by a person or business.
     */
    private ClaimType claimType;
    /**
     * Product associated to the claim.
     */
    private Product product;
    /**
     * associated with the product, for which the petition or claim is made.
     */
    private ReasonAdditionalClaim reason;
    /**
     * Product contract.
     */
    private Contract contract;
    /**
     * Product contract.
     */
    private Amount claimAmount;
    /**
     * ATM where the issue, related to the claim, was held.
     */
    private Atm atm;
    /**
     * Bank where the issue, related to the claim, is originated.
     */
    private Bank bank;
    /**
     * String based on ISO-8601 for specifying the date when the incident occurred to the customer.
     */
    @XmlJavaTypeAdapter(CustomCalendarAdapter.class)
    @XmlSchemaType(name="dateTime")
    private Calendar issueDate;

    public ClaimType getClaimType() {
        return claimType;
    }

    public void setClaimType(ClaimType claimType) {
        this.claimType = claimType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ReasonAdditionalClaim getReason() {
        return reason;
    }

    public void setReason(ReasonAdditionalClaim reason) {
        this.reason = reason;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Amount getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(Amount claimAmount) {
        this.claimAmount = claimAmount;
    }

    public Atm getAtm() {
        return atm;
    }

    public void setAtm(Atm atm) {
        this.atm = atm;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Calendar getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Calendar issueDate) {
        this.issueDate = issueDate;
    }
}