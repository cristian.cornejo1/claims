package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.pzic.claims.canonic.Petitioner;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "search", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "search", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Search implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Petitioner who generated the claim or request.
     */
    private Petitioner petitioner;

    public Petitioner getPetitioner() {
        return petitioner;
    }

    public void setPetitioner(Petitioner petitioner) {
        this.petitioner = petitioner;
    }
}
