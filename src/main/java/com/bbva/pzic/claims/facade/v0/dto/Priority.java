package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "priority", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "priority", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Priority implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Indicates whether the claim is urgent or not.
     */
    private Boolean isUrgent;
    /**
     * Indicates the basis why the claim is urgent.
     */
    private String basis;

    public Boolean getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(Boolean urgent) {
        isUrgent = urgent;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis;
    }
}