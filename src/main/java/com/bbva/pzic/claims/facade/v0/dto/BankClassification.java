package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 04/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "bankClassification", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "bankClassification", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankClassification implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Bank classification type.
     */
    private BankClassificationType bankClassificationType;

    public BankClassificationType getBankClassificationType() {
        return bankClassificationType;
    }

    public void setBankClassificationType(BankClassificationType bankClassificationType) {
        this.bankClassificationType = bankClassificationType;
    }
}