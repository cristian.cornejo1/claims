package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.DTOIntDocumentType;
import com.bbva.pzic.claims.business.dto.DTOIntIdentityDocument;
import com.bbva.pzic.claims.business.dto.DTOIntUnknownPetitioner;
import com.bbva.pzic.claims.business.dto.InputSearchProductClaims;
import com.bbva.pzic.claims.canonic.DocumentType;
import com.bbva.pzic.claims.canonic.IdentityDocument;
import com.bbva.pzic.claims.facade.v0.dto.ListProductClaimsData;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.Search;
import com.bbva.pzic.claims.facade.v0.mapper.ISearchProductClaimsMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext.AAP;
import static com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext.ASTA_MX_CLIENT_ID;
import static com.bbva.pzic.claims.util.Enums.ENUM_CLAIMS_IDENTITYDOCUMENT_DOCUMENTYPE_ID;

/**
 * Created on 26/11/2019.
 *
 * @author Entelgy
 */
@Component
public class SearchProductClaimsMapper implements ISearchProductClaimsMapper {

    private static final Log LOG = LogFactory.getLog(SearchProductClaimsMapper.class);

    private ServiceInvocationContext serviceInvocationContext;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }

    @Override
    public InputSearchProductClaims mapIn(final Search search) {
        InputSearchProductClaims input = new InputSearchProductClaims();

        String aap = serviceInvocationContext.getProperty(AAP);
        LOG.debug(String.format("Se ha capturado el AAP '%s'", aap));
        input.setAap(aap);

        String clientId = serviceInvocationContext.getProperty(ASTA_MX_CLIENT_ID);
        LOG.debug(String.format("Se ha capturado el ASTA_MX_CLIENT_ID '%s'", clientId));

        if (StringUtils.isEmpty(clientId)) {
            input.setPetitioner(mapInPetitioner(search));
        } else {
            input.setClientId(clientId);
        }
        return input;
    }

    private DTOIntUnknownPetitioner mapInPetitioner(final Search search) {
        if (search == null) {
            return null;
        }
        DTOIntUnknownPetitioner dtoIntPetitioner = new DTOIntUnknownPetitioner();
        if (search.getPetitioner() != null) {
            dtoIntPetitioner.setIdentityDocument(mapInIdentityDocument(search.getPetitioner().getIdentityDocument()));
        }
        return dtoIntPetitioner;
    }

    private DTOIntIdentityDocument mapInIdentityDocument(final IdentityDocument identityDocument) {
        if (identityDocument == null) {
            return null;
        }
        DTOIntIdentityDocument dtoIntIdentityDocument = new DTOIntIdentityDocument();
        dtoIntIdentityDocument.setDocumentNumber(identityDocument.getDocumentNumber());
        dtoIntIdentityDocument.setDocumentType(mapInDocumentType(identityDocument.getDocumentType()));
        return dtoIntIdentityDocument;
    }

    private DTOIntDocumentType mapInDocumentType(final DocumentType documentType) {
        if (documentType == null) {
            return null;
        }
        DTOIntDocumentType dtoIntDocumentType = new DTOIntDocumentType();
        dtoIntDocumentType.setId(translator.translateFrontendEnumValue(ENUM_CLAIMS_IDENTITYDOCUMENT_DOCUMENTYPE_ID,
                documentType.getId()));
        return dtoIntDocumentType;
    }

    @Override
    public ListProductClaimsData mapOut(final List<ProductClaims> productClaims) {
        LOG.info("... called method SearchProductClaimsMapper.mapOut ...");
        if (CollectionUtils.isEmpty(productClaims)) {
            return null;
        }
        ListProductClaimsData data = new ListProductClaimsData();
        data.setData(productClaims);
        return data;
    }
}
