package com.bbva.pzic.claims.facade.v0.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseNoContent;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.pzic.claims.business.ISrvIntClaims;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.ProductClaim;
import com.bbva.pzic.claims.facade.v0.ISrvClaimsV0;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.claims.facade.v0.mapper.*;
import com.bbva.pzic.claims.util.BusinessServiceUtil;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.claims.facade.RegistryIds.*;
import static com.bbva.pzic.claims.util.Constants.CUSTOMER_ID;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Path("/v0")
@Produces(MediaType.APPLICATION_JSON)
@SN(registryID = "SNPE1800095", logicalID = "claims")
@VN(vnn = "v0")
@Service
public class SrvClaimsV0 implements ISrvClaimsV0, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvClaimsV0.class);
    private static final String ENTITIES_EXPANDS = "tracking";

    private final String PRODUCT_CLAIM_ID = "product-claim-id";
    private final String EXPAND = "expand";
    private final String AUTOMATIC_REFUND_TRANSACTION_ID = "automatic-refund-transaction-id";

    public HttpHeaders httpHeaders;
    public UriInfo uriInfo;

    @Autowired
    private IListAutomaticRefundTransactionsMapper listAutomaticRefundTransactionsMapper;
    @Autowired
    private IGetAutomaticRefundTransactionMapper getAutomaticRefundTransactionMapper;
    @Autowired
    private ISrvIntClaims srvIntClaims;
    @Autowired
    private ICreateProductClaimMapper createProductClaimMapper;
    @Autowired
    private ISearchProductClaimsMapper searchProductClaimsMapper;
    @Autowired
    private IGetProductClaimMapper getProductClaimMapper;
    @Autowired
    private ISendEmailProductClaimMapper sendEmailProductClaimMapper;
    @Autowired
    private IValidateAutomaticRefundTransactionMapper validateAutomaticRefundTransactionMapper;
    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;
    @Autowired
    private IModifyPartialAutomaticRefundTransactionMapper modifyPartialAutomaticRefundTransactionMapper;

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Override
    public void setUriInfo(UriInfo uriInfo) {
        this.uriInfo = uriInfo;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/product-claims")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @SMC(registryID = SN_REGISTRY_ID_CREATE_PRODUCT_CLAIM, logicalID = "createProductClaim")
    public ProductClaimTransactionData createProductClaim(
            @Multipart(value = "data", type = MediaType.TEXT_PLAIN) final String productClaimString,
            @Multipart(value = "files", required = false) final List<DataSource> attachments) {
        LOG.info("----- Invoking service createProductClaim -----");

        ProductClaim productClaim = createProductClaimMapper.mapStringToProductClaim(productClaimString);

        inputDataProcessingExecutor.perform(SN_REGISTRY_ID_CREATE_PRODUCT_CLAIM, productClaim, null, null);

        ProductClaimTransactionData serviceResponse = createProductClaimMapper.mapOut(
                srvIntClaims.createProductClaim(createProductClaimMapper.mapIn(productClaim, attachments)));

        outputDataProcessingExecutor.perform(SN_REGISTRY_ID_CREATE_PRODUCT_CLAIM, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/product-claims/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_SEARCH_PRODUCT_CLAIMS, logicalID = "searchProductClaims")
    public ListProductClaimsData searchProductClaims(final Search search) {
        LOG.info("----- Invoking service searchProductClaims -----");

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEARCH_PRODUCT_CLAIMS, search, null, null);

        ListProductClaimsData serviceResponse = searchProductClaimsMapper.mapOut(
                srvIntClaims.searchProductClaims(
                        searchProductClaimsMapper.mapIn(search)
                )
        );

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEARCH_PRODUCT_CLAIMS, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/product-claims/{product-claim-id}")
    @SMC(registryID = SN_REGISTRY_GET_PRODUCT_CLAIM, logicalID = "getProductClaim")
    public ProductClaimsData getProductClaim(@PathParam(PRODUCT_CLAIM_ID) final String productClaimId,
                                             @QueryParam(EXPAND) final String expand) {
        LOG.info("----- Invoking service getProductClaim -----");
        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(PRODUCT_CLAIM_ID, productClaimId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(EXPAND, expand);

        inputDataProcessingExecutor.perform(SN_REGISTRY_GET_PRODUCT_CLAIM, null, pathParams, queryParams);

        ProductClaimsData serviceResponse = getProductClaimMapper.mapOut(
                srvIntClaims.getProductClaim(getProductClaimMapper.mapIn(
                        pathParams.get(PRODUCT_CLAIM_ID).toString())));

        if (serviceResponse != null) {
            BusinessServiceUtil.expand(serviceResponse.getData(), ENTITIES_EXPANDS, expand == null ? null : queryParams.get(EXPAND).toString());
        }

        outputDataProcessingExecutor.perform(SN_REGISTRY_GET_PRODUCT_CLAIM, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/product-claims/{product-claim-id}/send-email")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SN_REGISTRY_SEND_EMAIL_PRODUCT_CLAIM, logicalID = "sendEmailProductClaim")
    public ServiceResponseNoContent sendEmailProductClaim(@PathParam(PRODUCT_CLAIM_ID) final String productClaimId,
                                                          final SendEmail sendEmail) {
        LOG.info("----- Invoking service sendEmailProductClaim -----");
        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(PRODUCT_CLAIM_ID, productClaimId);

        inputDataProcessingExecutor.perform(SN_REGISTRY_SEND_EMAIL_PRODUCT_CLAIM, sendEmail, pathParams, null);

        srvIntClaims.sendEmailProductClaim(
                sendEmailProductClaimMapper.mapIn(pathParams.get(PRODUCT_CLAIM_ID).toString(), sendEmail));

        return ServiceResponseNoContent.ServiceResponseNoContentBuilder.build();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/automatic-refund-transactions/{automatic-refund-transaction-id}")
    @SMC(registryID = SN_REGISTRY_GET_AUTOMATIC_REFUND_TRANSACTION, logicalID = "getAutomaticRefundTransaction")
    public ServiceResponse<AutomaticRefundTransaction> getAutomaticRefundTransaction(
            @PathParam("automatic-refund-transaction-id") final String automaticRefundTransactionId) {
        LOG.info("----- Invoking service getAutomaticRefundTransaction -----");
        return getAutomaticRefundTransactionMapper.mapOut(
                srvIntClaims.getAutomaticRefundTransaction(
                        getAutomaticRefundTransactionMapper.mapIn(automaticRefundTransactionId)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/automatic-refund-transactions")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_AUTOMATIC_REFUND_TRANSACTIONS, logicalID = "listAutomaticRefundTransactions")
    public ServiceResponse<List<AutomaticRefundTransaction>> listAutomaticRefundTransactions(
            @QueryParam(CUSTOMER_ID) final String customerId) {
        LOG.info("----- Invoking service listAutomaticRefundTransactions -----");
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put(CUSTOMER_ID, customerId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_AUTOMATIC_REFUND_TRANSACTIONS, null, null, queryParams);

        ServiceResponse<List<AutomaticRefundTransaction>> serviceResponse = listAutomaticRefundTransactionsMapper.mapOut(
                srvIntClaims.listAutomaticRefundTransactions(
                        listAutomaticRefundTransactionsMapper.mapIn(
                                (String) queryParams.get(CUSTOMER_ID)
                        )
                )
        );

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_AUTOMATIC_REFUND_TRANSACTIONS, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/automatic-refund-transactions/validate-automatic-refund-transaction")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = "SMCPE1810334", logicalID = "validateAutomaticRefundTransaction")
    public ServiceResponse<AutomaticRefundTransaction> validateAutomaticRefundTransaction(
            final AutomaticRefundTransaction automaticRefundTransaction) {
        LOG.info("----- Invoking service validateAutomaticRefundTransaction -----");

        inputDataProcessingExecutor.perform("SMCPE1810334", automaticRefundTransaction, null, null);

        ServiceResponse<AutomaticRefundTransaction> serviceResponse = validateAutomaticRefundTransactionMapper.mapOut(
                srvIntClaims.validateAutomaticRefundTransaction(
                        validateAutomaticRefundTransactionMapper.mapIn(automaticRefundTransaction)));

        outputDataProcessingExecutor.perform("SMCPE1810334", serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/automatic-refund-transactions/{automatic-refund-transaction-id}")
    @SMC(registryID = SMC_REGISTRY_MODIFY_PARTIAL_AUTOMATIC_REFUND_TRANSACTION, logicalID = "modifyPartialAutomaticRefundTransaction", forcedCatalog = "gabiCatalog")
    public ServiceResponse<PartialTransaction> modifyPartialAutomaticRefundTransaction(
            @PathParam(AUTOMATIC_REFUND_TRANSACTION_ID) final String automaticRefundTransactionId,
            final AutomaticRefundTransactionF automaticRefundTransaction) {
        LOG.info("----- Invoking service modifyPartialAutomaticRefundTransaction -----");
        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(AUTOMATIC_REFUND_TRANSACTION_ID, automaticRefundTransactionId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_MODIFY_PARTIAL_AUTOMATIC_REFUND_TRANSACTION,
                automaticRefundTransaction, pathParams,null);
        ServiceResponse<PartialTransaction> serviceResponse = modifyPartialAutomaticRefundTransactionMapper.mapOut(
                srvIntClaims.modifyPartialAutomaticRefundTransaction(
                        modifyPartialAutomaticRefundTransactionMapper.mapIn(
                                (String) pathParams.get(AUTOMATIC_REFUND_TRANSACTION_ID),
                                automaticRefundTransaction
                        )
                )
        );
        outputDataProcessingExecutor.perform(SMC_REGISTRY_MODIFY_PARTIAL_AUTOMATIC_REFUND_TRANSACTION,serviceResponse,null,null);
        return serviceResponse;
    }
}
