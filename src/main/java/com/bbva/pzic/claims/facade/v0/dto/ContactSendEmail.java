package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "contactSendEmail", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "contactSendEmail", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactSendEmail implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact email identifier.
     */
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * Customer email address. DISCLAIMER:This value will be required only for non-session channels.
     */
    @DatoAuditable(omitir = true)
    private String address;
    /**
     * Contact information type.
     */
    private String contactDetailType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }
}
