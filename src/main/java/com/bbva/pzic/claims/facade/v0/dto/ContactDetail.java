package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created 02/02/2021
 *
 * @author Entelgy
 */
@XmlRootElement(name = "contactDetail", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "contactDetail", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact information.
     */
    private BaseContactSearch contact;

    public BaseContactSearch getContact() { return contact; }

    public void setContact(BaseContactSearch contact) { this.contact = contact; }
}