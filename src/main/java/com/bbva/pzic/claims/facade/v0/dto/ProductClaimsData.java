package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "productClaimsData", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "productClaimsData", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductClaimsData implements Serializable {

    private static final long serialVersionUID = 1L;

    private ProductClaims data;

    public ProductClaims getData() {
        return data;
    }

    public void setData(ProductClaims data) {
        this.data = data;
    }
}
