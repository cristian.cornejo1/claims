package com.bbva.pzic.claims.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.claims.business.dto.InputModifyAutomaticRefundTransaction;
import com.bbva.pzic.claims.facade.v0.dto.AutomaticRefundTransactionF;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;

public interface IModifyPartialAutomaticRefundTransactionMapper {
    InputModifyAutomaticRefundTransaction mapIn(String automaticRefundTransactionId,
                                                AutomaticRefundTransactionF automaticRefundTransaction );

    ServiceResponse<PartialTransaction> mapOut(PartialTransaction partialTransaction);
}
