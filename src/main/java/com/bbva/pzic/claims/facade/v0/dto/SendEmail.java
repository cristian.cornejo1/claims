package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "sendEmail", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "sendEmail", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class SendEmail implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * The contact information that will receive the information. In case no information is reported,
     * it will be taken from the information reported in the claim discharge.
     */
    private ContactSendEmail contact;
    /**
     * Documents requested to be attached to the email, which were generated during the registration of the claim.
     */
    private List<Document> documents;

    public ContactSendEmail getContact() {
        return contact;
    }

    public void setContact(ContactSendEmail contact) {
        this.contact = contact;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
}
