package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 29/10/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "deliveryType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "deliveryType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeliveryType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Delivery type identifier.
     */
    private String id;
    /**
     * Delivery type description.
     */
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
