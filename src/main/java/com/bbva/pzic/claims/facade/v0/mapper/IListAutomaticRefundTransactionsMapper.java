package com.bbva.pzic.claims.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.claims.business.dto.InputListAutomaticRefundTransactions;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;

import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface IListAutomaticRefundTransactionsMapper {

    InputListAutomaticRefundTransactions mapIn(String customerId);

    ServiceResponse<List<AutomaticRefundTransaction>> mapOut(
            List<AutomaticRefundTransaction> automaticRefundTransaction);
}