package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "reasonSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "reasonSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReasonSearch implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Reason identifier.
     */
    private String id;
    /**
     * Reason name.
     */
    private String name;
    /**
     * Additional comments to detail the claim.
     */
    private String comments;
    /**
     * Subreason associated with the reason, it permits to get more detail about the claim.
     */
    private SubReason subReason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public SubReason getSubReason() {
        return subReason;
    }

    public void setSubReason(SubReason subReason) {
        this.subReason = subReason;
    }
}
