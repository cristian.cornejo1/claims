package com.bbva.pzic.claims.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.claims.business.dto.DTOIntProductClaim;
import com.bbva.pzic.claims.canonic.ProductClaim;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaimTransactionData;

import javax.activation.DataSource;
import java.util.List;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
public interface ICreateProductClaimMapper {

    ProductClaim mapStringToProductClaim(String productClaimString);

    DTOIntProductClaim mapIn(ProductClaim productClaim, List<DataSource> attachments);

    ProductClaimTransactionData mapOut(ProductClaimTransaction productClaim);
}
