package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;

/**
 * Created on 29/10/2020.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "transaction", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "transaction", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier of the transaction.
     */
    private String id;
    /**
     * String based on ISO-8601 date format for providing the last date when the operation was performed by the client.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date operationDate;
    /**
     * Brief description of the operative that create the transaction. Backend must provide this information depending on the transaction’s type.
     */
    private String concept;
    /**
     * Transaction type. It talks about which kind of operative has created this transaction. DISCLAIMER: Nowadays some geographies have no possibility of identifying all the types of transaction that are detailed below. This should not become an impediment that in the near future this inconvenience be fixed by the corresponding backends and allow the customers to know and filter those transactions according to their criteria.
     */
    private TransactionType transactionType;
    /**
     *
     * Amount that is associated to the transaction on the local currency. This amount may not include any charges that the bank can carry with the operation.
     */
    private LocalAmount localAmount;
    /**
     * Store where the transaction was realized.
     */
    private Store store;
    private Status status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public LocalAmount getLocalAmount() {
        return localAmount;
    }

    public void setLocalAmount(LocalAmount localAmount) {
        this.localAmount = localAmount;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}