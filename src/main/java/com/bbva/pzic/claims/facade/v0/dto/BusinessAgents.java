package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 23/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "businessAgents", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "businessAgents", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessAgents implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Business agent identifier.
     */
    private String id;
    /**
     * Business agent full name.
     */
    private String fullName;
    /**
     * Business agent type.
     */
    private String agentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }
}
