package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "segment", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "segment", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Segment implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Petitioner segment identifier.
     */
    private String id;
    /**
     * Petitioner segment name.
     */
    private String description;
    /**
     * Petitioner segment name.
     */
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
