package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created 02/02/2021
 *
 * @author Entelgy
 */
@XmlRootElement(name = "baseContactSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "baseContactSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseContactSearch implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Contact information type.
     */
    private String contactDetailType;
    /**
     * Contact email identifier.
     *
     * Contact mobile number identifier.
     *
     * Contact landline number identifier.
     */
    private String id;
    /**
     * Customer email address. This value will be required only for non-session channels.
     */
    @DatoAuditable(omitir = true)
    private String address;
    /**
     * Indicates if this contact information has been verified and can be used to send notifications to the customer.
     */
    private Boolean receivesNotifications;
    /**
     * Contact mobile number. This value will be masked when basic authentication state. If the authentication
     * state is advanced the value will be clear.
     *
     * Contact landline number. This value will be masked when basic authentication state. If the authentication
     * state is advanced the value will be clear.
     */
    @DatoAuditable(omitir = true)
    private String number;
    /**
     * International prefix for the phone number. For example +34 or 0034.
     */
    private String countryCode;
    /**
     * Contact vendor information.
     */
    private PhoneCompanySearch phoneCompany;
    /**
     * The type of landine.
     */
    private String phoneType;
    /**
     * Phone number's country.
     */
    private CountrySearch country;
    /**
     * Phone city code.
     */
    private String regionalCode;
    /**
     * Phone extension.
     */
    private String extension;

    public String getContactDetailType() { return contactDetailType; }

    public void setContactDetailType(String contactDetailType) { this.contactDetailType = contactDetailType; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public Boolean getReceivesNotifications() { return receivesNotifications; }

    public void setReceivesNotifications(Boolean receivesNotifications) { this.receivesNotifications = receivesNotifications; }

    public String getNumber() { return number; }

    public void setNumber(String number) { this.number = number; }

    public String getCountryCode() { return countryCode; }

    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

    public PhoneCompanySearch getPhoneCompany() { return phoneCompany; }

    public void setPhoneCompany(PhoneCompanySearch phoneCompany) { this.phoneCompany = phoneCompany; }

    public String getPhoneType() { return phoneType; }

    public void setPhoneType(String phoneType) { this.phoneType = phoneType; }

    public CountrySearch getCountry() { return country; }

    public void setCountry(CountrySearch country) { this.country = country; }

    public String getRegionalCode() { return regionalCode; }

    public void setRegionalCode(String regionalCode) { this.regionalCode = regionalCode; }

    public String getExtension() { return extension; }

    public void setExtension(String extension) { this.extension = extension; }
}