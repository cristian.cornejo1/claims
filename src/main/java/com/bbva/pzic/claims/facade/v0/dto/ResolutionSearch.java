package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.pzic.routine.commons.utils.BooleanAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "resolutionSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "resolutionSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResolutionSearch implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Resolution identifier.
     */
    private String id;
    /**
     * Resolution description.
     */
    private String name;
    /**
     * Delivery of the resolution to the petitioner.
     */
    private Delivery delivery;
    /**
     * Returned amount, provided by the bank, to the customer. It can be represented in two currencies.
     * Also, the returned amount can be divided in two parts which could be represented by different currencies.
     */
    private List<ReturnedAmounts> returnedAmounts;
    /**
     * Operational amount, It’s the amount that the claim analyst  specifies for the claim after he or she reviews the case.
     * It can be represented in two currencies.
     */
    private List<OperationalAmounts> operationalAmounts;

    /**
     * Resolution fundament.
     */
    private Fundament fundament;
    /**
     * Resolution type.
     */
    private ResolutionType resolutionType;
    /**
     * Additional information about the resolution.
     */
    private String additionalInformation;
    /**
     * It indicates if the resolution flow was manual.
     */
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private String isManualFlow;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public List<ReturnedAmounts> getReturnedAmounts() {
        return returnedAmounts;
    }

    public void setReturnedAmounts(List<ReturnedAmounts> returnedAmounts) {
        this.returnedAmounts = returnedAmounts;
    }

    public List<OperationalAmounts> getOperationalAmounts() {
        return operationalAmounts;
    }

    public void setOperationalAmounts(List<OperationalAmounts> operationalAmounts) {
        this.operationalAmounts = operationalAmounts;
    }

    public Fundament getFundament() {
        return fundament;
    }

    public void setFundament(Fundament fundament) {
        this.fundament = fundament;
    }

    public ResolutionType getResolutionType() {
        return resolutionType;
    }

    public void setResolutionType(ResolutionType resolutionType) {
        this.resolutionType = resolutionType;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getIsManualFlow() {
        return isManualFlow;
    }

    public void setIsManualFlow(String isManualFlow) {
        this.isManualFlow = isManualFlow;
    }
}
