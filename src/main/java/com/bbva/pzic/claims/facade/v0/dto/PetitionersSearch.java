package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlRootElement(name = "petitionersSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "petitionersSearch", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class PetitionersSearch implements Serializable {

    private static final long serialVersionUID = 1L;

    private PetitionerSearch petitioner;

    public PetitionerSearch getPetitioner() {
        return petitioner;
    }

    public void setPetitioner(PetitionerSearch petitioner) {
        this.petitioner = petitioner;
    }
}
