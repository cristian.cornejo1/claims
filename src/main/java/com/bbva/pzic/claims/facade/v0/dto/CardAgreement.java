package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 22/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "cardAgreement", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "cardAgreement", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class CardAgreement implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Card agreement identifier.
     */
    private String id;
    /**
     * Subproduct of the contract.
     */
    private SubProduct subProduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SubProduct getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(SubProduct subProduct) {
        this.subProduct = subProduct;
    }
}
