package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "reason", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "reason", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reason implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Reason identifier.
     */
    private String id;
    /**
     * Reason name.
     */
    private String name;
    /**
     * Subreason associated with the reason, it permits to get more detail about the claim.
     */
    private SubReason subReason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubReason getSubReason() {
        return subReason;
    }

    public void setSubReason(SubReason subReason) {
        this.subReason = subReason;
    }
}