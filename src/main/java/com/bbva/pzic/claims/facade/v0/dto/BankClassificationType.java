package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 04/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "bankClassificationType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "bankClassificationType", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankClassificationType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Bank classification type identifier.
     */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}