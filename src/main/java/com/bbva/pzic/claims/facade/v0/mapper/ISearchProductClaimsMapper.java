package com.bbva.pzic.claims.facade.v0.mapper;

import com.bbva.pzic.claims.business.dto.InputSearchProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.ListProductClaimsData;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.Search;

import java.util.List;

/**
 * Created on 26/11/2019.
 *
 * @author Entelgy
 */
public interface ISearchProductClaimsMapper {

    InputSearchProductClaims mapIn(Search search);

    ListProductClaimsData mapOut(List<ProductClaims> productClaims);
}
