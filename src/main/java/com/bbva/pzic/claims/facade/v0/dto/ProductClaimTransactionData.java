package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.pzic.claims.canonic.ProductClaimTransaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "productClaimTransactionData", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "productClaimTransactionData", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductClaimTransactionData implements Serializable {

    private static final long serialVersionUID = 1L;

    private ProductClaimTransaction data;

    public ProductClaimTransaction getData() {
        return data;
    }

    public void setData(ProductClaimTransaction data) {
        this.data = data;
    }
}
