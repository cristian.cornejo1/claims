package com.bbva.pzic.claims.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 03/02/2021.
 *
 * @author Entelgy.
 */
@XmlRootElement(name = "fundament", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlType(name = "fundament", namespace = "urn:com:bbva:pzic:claims:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Fundament implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Resolution fundament identifier.
     */
    private String id;
    /**
     * Resolution fundament description.
     */
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}