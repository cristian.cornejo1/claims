package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.Delivery;
import com.bbva.pzic.claims.canonic.DeliveryType;
import com.bbva.pzic.claims.canonic.*;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.claims.facade.v0.mapper.ICreateProductClaimMapper;
import com.bbva.pzic.claims.util.mappers.ObjectMapperHelper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.activation.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext.AAP;
import static com.bbva.pzic.claims.util.Errors.TECHNICAL_ERROR;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
@Component
public class CreateProductClaimMapper implements ICreateProductClaimMapper {

    private static final Log LOG = LogFactory.getLog(CreateProductClaimMapper.class);

    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    public void setServiceInvocationContext(ServiceInvocationContext serviceInvocationContext) {
        this.serviceInvocationContext = serviceInvocationContext;
    }


    @Override
    public ProductClaim mapStringToProductClaim(final String productClaimString) {
        try {
            return ObjectMapperHelper.getInstance().readValue(productClaimString, ProductClaim.class);
        } catch (IOException e) {
            throw new BusinessServiceException(TECHNICAL_ERROR, e);
        }
    }

    @Override
    public DTOIntProductClaim mapIn(final ProductClaim productClaim, final List<DataSource> attachments) {
        LOG.info("... called method CreateProductClaimTransactionMapper.mapIn ...");
        DTOIntProductClaim dtoInt = new DTOIntProductClaim();
        dtoInt.setPersonType(mapInPersonType(productClaim.getPersonType()));
        dtoInt.setCompany(mapInCompany(productClaim.getCompany()));
        dtoInt.setIsUnderage(mapInIsUnderage(productClaim.getIsUnderage()));
        dtoInt.setClaimType(mapInClaimType(productClaim.getClaimType()));
        dtoInt.setProduct(mapInProduct(productClaim.getProduct()));
        dtoInt.setReason(mapInReason(productClaim.getReason()));
        dtoInt.setResolution(mapInResolution(productClaim.getResolution()));
        dtoInt.setFiles(mapInAttachments(attachments));
        dtoInt.setPetitioners(mapInPetitioners(productClaim.getPetitioners()));
        dtoInt.setAap(serviceInvocationContext.getProperty(AAP));
        dtoInt.setContract(mapInContract(productClaim.getContract()));
        dtoInt.setClaimAmount(mapInClaimAmount(productClaim.getClaimAmount()));
        dtoInt.setAtm(mapInAtm(productClaim.getAtm()));
        dtoInt.setBank(mapInBank(productClaim.getBank()));
        dtoInt.setChannel(mapInChannel(productClaim.getChannel()));
        dtoInt.setGeolocation(mapInGeolocation(productClaim.getGeolocation()));
        dtoInt.setDelivery(mapInDelivery(productClaim.getDelivery()));
        dtoInt.setPetition(mapInPetition(productClaim.getPetition()));
        dtoInt.setPriority(mapInPriority(productClaim.getPriority()));
        dtoInt.setEnrollmentDate(productClaim.getEnrollmentDate() == null ? null : DateUtils.rebuildDateTime(productClaim.getEnrollmentDate()));
        dtoInt.setPredecessorClaim(mapInPredecessorClaim(productClaim.getPredecessorClaim()));
        dtoInt.setIsOnlineRefundFlow(productClaim.getOnlineRefundFlow());
        dtoInt.setPreviousAttentionCode(productClaim.getPreviousAttentionCode());
        dtoInt.setAdditionalClaims(mapInAdditionalClaims(productClaim.getAdditionalClaims()));
        dtoInt.setIssueDate(productClaim.getIssueDate() == null ? null : DateUtils.rebuildDateTime(productClaim.getIssueDate()));
        return dtoInt;
    }

    private DTOIntChannel mapInChannel(final Channel channel) {
        if(channel == null) {
            return null;
        }

        DTOIntChannel result = new DTOIntChannel();
        result.setId(channel.getId());
        return result;
    }

    private DTOIntGeolocation mapInGeolocation(final Geolocation geolocation) {
        if(geolocation == null) {
            return null;
        }

        DTOIntGeolocation result = new DTOIntGeolocation();
        result.setCode(geolocation.getCode());
        return result;
    }

    private DTOIntPetition mapInPetition(final Petition petition) {
        if(petition == null) {
            return null;
        }

        DTOIntPetition result = new DTOIntPetition();
        result.setDescription(petition.getDescription());
        return result;
    }

    private DTOIntPriority mapInPriority(final Priority priority) {
        if(priority == null) {
            return null;
        }

        DTOIntPriority result = new DTOIntPriority();
        result.setIsUrgent(priority.getIsUrgent());
        result.setBasis(priority.getBasis());
        return result;
    }

    private DTOIntPredecessorClaim mapInPredecessorClaim(final PredecessorClaim predecessorClaim) {
        if (predecessorClaim == null) {
            return null;
        }

        DTOIntPredecessorClaim result = new DTOIntPredecessorClaim();
        result.setId(predecessorClaim.getId());
        return result;
    }

    private List<DTOIntAdditionalClaims> mapInAdditionalClaims(final List<AdditionalClaims> additionalClaims) {
        if(CollectionUtils.isEmpty(additionalClaims)) {
            return null;
        }
        return additionalClaims.stream().filter(Objects::nonNull).map(this::mapInAdditionalClaim).collect(Collectors.toList());
    }

    private DTOIntAdditionalClaims mapInAdditionalClaim(final AdditionalClaims additionalClaims) {
        if(additionalClaims == null) {
            return null;
        }
        DTOIntAdditionalClaims result = new DTOIntAdditionalClaims();
        result.setClaimType(mapInClaimType(additionalClaims.getClaimType()));
        result.setProduct(mapInProduct(additionalClaims.getProduct()));
        result.setReason(mapInReasonClaim(additionalClaims.getReason()));
        result.setContract(mapInContractClaim(additionalClaims.getContract()));
        result.setClaimAmount(mapInClaimAmount(additionalClaims.getClaimAmount()));
        result.setAtm(mapInAtm(additionalClaims.getAtm()));
        result.setBank(mapInBank(additionalClaims.getBank()));
        result.setIssueDate(additionalClaims.getIssueDate() == null ? null : DateUtils.rebuildDateTime(additionalClaims.getIssueDate()));

        return result;
    }

    private DTOIntReason mapInReasonClaim(final ReasonAdditionalClaim reasonAdditionalClaim) {
        if(reasonAdditionalClaim == null) {
            return null;
        }

        DTOIntReason result = new DTOIntReason();
        result.setId(reasonAdditionalClaim.getId());
        result.setSubReason(mapInSubReason(reasonAdditionalClaim.getSubReason()));
        return result;
    }

    private DTOIntContract mapInContractClaim(final Contract contract) {
        if(contract == null) {
            return null;
        }

        DTOIntContract result = new DTOIntContract();
        result.setId(contract.getId());
        result.setNumber(contract.getNumber());
        result.setNumberType(mapInNumberType(contract.getNumberType()));
        result.setProduct(mapInProductClaim(contract.getProduct()));
        return result;

    }

    private DTOIntProduct mapInProductClaim(final ContractProduct product) {
        if(product == null) {
            return null;
        }

        DTOIntProduct result = new DTOIntProduct();
        result.setId(product.getId());
        return result;
    }

    private DTOIntAtm mapInAtm(final Atm atm) {
        if(atm == null) {
            return null;
        }

        DTOIntAtm result = new DTOIntAtm();
        result.setId(atm.getId());
        return result;
    }

    private DTOIntBank mapInBank(final Bank bank) {
        if(bank == null) {
            return null;
        }

        DTOIntBank result = new DTOIntBank();
        result.setId(bank.getId());
        result.setBranch(mapInBranch(bank.getBranch()));
        return result;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if(branch == null) {
            return null;
        }

        DTOIntBranch result = new DTOIntBranch();
        result.setId(branch.getId());
        return result;
    }

    private DTOIntAmount mapInClaimAmount(final Amount claimAmount) {
        if (claimAmount == null) {
            return null;
        }

        DTOIntAmount result = new DTOIntAmount();
        result.setAmount(claimAmount.getAmount());
        result.setCurrency(claimAmount.getCurrency());
        return result;
    }

    private DTOIntContract mapInContract(final Contract contract) {
        if (contract == null) {
            return null;
        }

        DTOIntContract result = new DTOIntContract();
        result.setId(contract.getId());
        result.setNumber(contract.getNumber());
        result.setNumberType(mapInNumberType(contract.getNumberType()));
        result.setProduct(mapInContractProduct(contract.getProduct()));
        return result;
    }

    private DTOIntProduct mapInContractProduct(final ContractProduct product) {
        if(product == null) {
            return null;
        }
        DTOIntProduct result = new DTOIntProduct();
        result.setId(product.getId());
        return result;
    }

    private DTOIntNumberType mapInNumberType(final NumberType numberType) {
        if (numberType == null) {
            return null;
        }

        DTOIntNumberType result = new DTOIntNumberType();
        result.setId(numberType.getId());
        return result;
    }

    private DTOIntPersonType mapInPersonType(final ProductType personType) {
        if (personType == null) {
            return null;
        }
        DTOIntPersonType dtoIntPersonType = new DTOIntPersonType();
        dtoIntPersonType.setId(personType.getId());
        return dtoIntPersonType;
    }

    private DTOIntCompany mapInCompany(final Company company) {
        if (company == null) return null;
        DTOIntCompany dtoIntCompany = new DTOIntCompany();
        dtoIntCompany.setId(company.getId());
        return dtoIntCompany;
    }

    private String mapInIsUnderage(Boolean isUnderage) {
        if (isUnderage == null) {
            return null;
        }
        return isUnderage ? "1" : "0";
    }

    private DTOIntClaimType mapInClaimType(final ClaimType claimType) {
        if (claimType == null) {
            return null;
        }
        DTOIntClaimType dtoIntClaimType = new DTOIntClaimType();
        dtoIntClaimType.setId(claimType.getId());
        return dtoIntClaimType;
    }

    private DTOIntProduct mapInProduct(final Product product) {
        if (product == null) {
            return null;
        }
        DTOIntProduct dtoIntProduct = new DTOIntProduct();
        dtoIntProduct.setId(product.getId());
        return dtoIntProduct;
    }

    private DTOIntReason mapInReason(final ReasonClaim reason) {
        if (reason == null) {
            return null;
        }
        DTOIntReason dtoIntReason = new DTOIntReason();
        dtoIntReason.setId(reason.getId());
        dtoIntReason.setComments(reason.getComments());
        dtoIntReason.setSubReason(mapInSubReason(reason.getSubReason()));
        return dtoIntReason;
    }

    private DTOIntSubReason mapInSubReason(final SubReason subReason) {
        if (subReason == null) {
            return null;
        }

        DTOIntSubReason result = new DTOIntSubReason();
        result.setId(subReason.getId());
        return result;
    }

    private DTOIntResolution mapInResolution(final Resolution resolution) {
        if (resolution == null) {
            return null;
        }

        DTOIntResolution dtoIntResolution = new DTOIntResolution();
        dtoIntResolution.setDelivery(mapInDelivery(resolution.getDelivery()));
        return dtoIntResolution;
    }

    private DTOIntDelivery mapInDelivery(final Delivery delivery) {
        if (delivery == null) {
            return null;
        }

        DTOIntDelivery dtoIntDelivery = new DTOIntDelivery();
        dtoIntDelivery.setDeliveryType(mapInDeliveryType(delivery.getDeliveryType()));
        return dtoIntDelivery;
    }

    private DTOIntDeliveryType mapInDeliveryType(final DeliveryType deliveryType) {
        if (deliveryType == null) {
            return null;
        }

        DTOIntDeliveryType dtoIntDeliveryType = new DTOIntDeliveryType();
        dtoIntDeliveryType.setId(deliveryType.getId());
        return dtoIntDeliveryType;
    }

    private List<DTOIntFile> mapInAttachments(final List<DataSource> attachments) {
        if (CollectionUtils.isEmpty(attachments)) {
            return null;
        }

        List<DTOIntFile> dtoIntFiles = new ArrayList<>();
        attachments.forEach(attc -> {
            try (InputStream inputStream = attc.getInputStream()) {
                DTOIntFile dtoIntFile = new DTOIntFile();
                dtoIntFile.setContent(java.util.Base64.getEncoder().encodeToString(IOUtils.toByteArray(inputStream)));
                dtoIntFile.setName(attc.getName());
                dtoIntFiles.add(dtoIntFile);
            } catch (IOException e) {
                LOG.error("Se detectó un error al consumir el archivo adjunto, nombre: " + attc.getName());
                throw new BusinessServiceException(TECHNICAL_ERROR, e);
            }
        });

        return dtoIntFiles;
    }

    private List<DTOIntPetitioners> mapInPetitioners(final List<Petitioners> petitioners) {
        if (CollectionUtils.isEmpty(petitioners)) {
            return null;
        }

        return
                petitioners
                        .stream()
                        .map(ptr -> {
                            if (ptr.getPetitioner() != null) {
                                String bankRelationType = ptr.getPetitioner().getBankRelationType();
                                if ("KNOWN".equals(bankRelationType)) {
                                    return mapInKnownPetitioner(ptr);
                                } else if ("UNKNOWN".equals(bankRelationType)) {
                                    return mapInUnknownPetitioner(ptr);
                                }
                            }
                            return new DTOIntPetitioners();
                        }).collect(Collectors.toList());
    }

    private DTOIntPetitioners mapInUnknownPetitioner(final Petitioners petitioners) {
        if (petitioners.getPetitioner() == null) {
            return null;
        }
        Petitioner petitioner = petitioners.getPetitioner();
        DTOIntUnknownPetitioner dtoIntUnknown = new DTOIntUnknownPetitioner();
        dtoIntUnknown.setBankRelationType(petitioner.getBankRelationType());
        dtoIntUnknown.setPetitionerType(mapInPetitionerType(petitioner.getPetitionerType()));
        dtoIntUnknown.setId(petitioner.getId());
        dtoIntUnknown.setAddress(mapInAddress(petitioner.getAddress()));

        if (dtoIntUnknown.getPetitionerType() != null
                && ("HOLDER".equalsIgnoreCase(dtoIntUnknown.getPetitionerType().getId()) || "AUTHORIZED".equalsIgnoreCase(dtoIntUnknown.getPetitionerType().getId()))) {
            dtoIntUnknown.setIdentityDocument(mapInIdentityDocument(petitioner.getIdentityDocument()));
            dtoIntUnknown.setFirstName(petitioner.getFirstName());
            dtoIntUnknown.setLastName(petitioner.getLastName());
            dtoIntUnknown.setSecondLastName(petitioner.getSecondLastName());
            dtoIntUnknown.setSegment(mapInUnknownSegment(petitioner.getSegment()));

            if ("HOLDER".equals(dtoIntUnknown.getPetitionerType().getId())) {
                dtoIntUnknown.setBank(mapInUnknownBank(petitioner.getBank()));
            }
        }


        if (isValidAddressComponents(petitioner)) {
            mapInUnknownAddressComponents(dtoIntUnknown, petitioner);
        }
        if (isValidContactDetail(petitioner)) {
            mapInUnknownContactDetails(dtoIntUnknown, petitioner);
        }

        DTOIntPetitioners dtoIntPetitioner = new DTOIntPetitioners();
        dtoIntPetitioner.setPetitioner(dtoIntUnknown);
        return dtoIntPetitioner;
    }

    private DTOIntBank mapInUnknownBank(final Bank bank) {
        if(bank == null) {
            return null;
        }
        DTOIntBank result = new DTOIntBank();
        result.setId(bank.getId());
        result.setBranch(mapInUnknownBranch(bank.getBranch()));
        return result;
    }

    private DTOIntBranch mapInUnknownBranch(final Branch branch) {
        if(branch == null) {
            return null;
        }
        DTOIntBranch result = new DTOIntBranch();
        result.setId(branch.getId());
        return result;
    }

    private DTOIntSegment mapInUnknownSegment(final Segment segment) {
        if(segment == null) {
            return null;
        }
        DTOIntSegment result = new DTOIntSegment();
        result.setId(segment.getId());
        return result;
    }

    private DTOIntPetitionerType mapInPetitionerType(final PetitionerType petitionerType) {
        if (petitionerType == null) {
            return null;
        }
        DTOIntPetitionerType dtoIntPetitionerType = new DTOIntPetitionerType();
        dtoIntPetitionerType.setId(petitionerType.getId());
        return dtoIntPetitionerType;
    }

    private DTOIntAddress mapInAddress(final Address address) {
        if (address == null) return null;
        DTOIntAddress dtoIntAddress = new DTOIntAddress();
        dtoIntAddress.setId(address.getId());
        dtoIntAddress.setLocation(mapInLocation(address.getLocation()));
        return dtoIntAddress;
    }

    private DTOIntLocation mapInLocation(final Location location) {
        if (location == null) return null;
        DTOIntLocation dtoIntLocation = new DTOIntLocation();
        dtoIntLocation.setFormattedAddress(location.getFormattedAddress());
        dtoIntLocation.setAdditionalInformation(location.getAdditionalInformation());
        return dtoIntLocation;
    }


    private DTOIntIdentityDocument mapInIdentityDocument(final IdentityDocument identityDocument) {
        if (identityDocument == null) {
            return null;
        }

        DTOIntIdentityDocument dtoIntIdentityDocument = new DTOIntIdentityDocument();
        dtoIntIdentityDocument.setDocumentNumber(identityDocument.getDocumentNumber());
        if (identityDocument.getDocumentType() == null) {
            return dtoIntIdentityDocument;
        }

        DTOIntDocumentType dtoIntDocumentType = new DTOIntDocumentType();
        dtoIntDocumentType.setId(identityDocument.getDocumentType().getId());
        dtoIntIdentityDocument.setDocumentType(dtoIntDocumentType);
        return dtoIntIdentityDocument;
    }

    private void mapInUnknownContactDetails(final DTOIntUnknownPetitioner dtoIntUnknown, final Petitioner petitioner) {
        if (dtoIntUnknown.getAddress() == null) {
            dtoIntUnknown.setAddress(new DTOIntAddress());
        }
        dtoIntUnknown.setContactDetails(mapInContactDetail(petitioner));
    }

    private boolean isValidContactDetail(final Petitioner petitioner) {
        return !CollectionUtils.isEmpty(petitioner.getContactDetails());
    }

    private boolean isValidAddressComponents(final Petitioner petitioner) {
        return !(petitioner.getAddress() == null || petitioner.getAddress().getLocation() == null ||
                CollectionUtils.isEmpty(petitioner.getAddress().getLocation().getAddressComponents()));
    }

    private void mapInUnknownAddressComponents(final DTOIntUnknownPetitioner dtoIntUnknown, final Petitioner petitioner) {
        if (dtoIntUnknown.getAddress() == null) {
            dtoIntUnknown.setAddress(new DTOIntAddress());
        }
        if (dtoIntUnknown.getAddress().getLocation() == null) {
            dtoIntUnknown.getAddress().setLocation(new DTOIntLocation());
        }

        dtoIntUnknown.getAddress().getLocation().setAddressComponents(mapInAddressComponents(petitioner));
    }

    private DTOIntPetitioners mapInKnownPetitioner(final Petitioners petitioners) {
        if (petitioners.getPetitioner() == null) {
            return null;
        }

        Petitioner petitioner = petitioners.getPetitioner();
        DTOIntKnownPetitioner dtoIntKnown = new DTOIntKnownPetitioner();
        dtoIntKnown.setBankRelationType(petitioner.getBankRelationType());
        dtoIntKnown.setPetitionerType(mapInPetitionerType(petitioner.getPetitionerType()));
        dtoIntKnown.setAddress(mapInAddress(petitioner.getAddress()));

        if (dtoIntKnown.getPetitionerType() != null) {
            if ("HOLDER".equals(dtoIntKnown.getPetitionerType().getId())) {
                dtoIntKnown.setId(mapInKnownPetitionerHolder(petitioner.getId()));
                dtoIntKnown.setSegment(mapInKnownSegment(petitioner.getSegment()));
                dtoIntKnown.setBank(mapInKnownBankHolder(petitioner.getBank()));
            } else if ("AUTHORIZED".equals(dtoIntKnown.getPetitionerType().getId())) {
                dtoIntKnown.setId(mapInKnowPetitionerAuthorized(petitioner.getId()));
                dtoIntKnown.setSegment(mapInKnownSegment(petitioner.getSegment()));
            }
        }

        if (isValidAddressComponents(petitioner)) {
            mapInKnownAddressComponents(dtoIntKnown, petitioner);
        }

        if (isValidContactDetail(petitioner)) {
            mapInKnownContactDetails(dtoIntKnown, petitioner);
        }

        DTOIntPetitioners dtoIntPetitioners = new DTOIntPetitioners();
        dtoIntPetitioners.setPetitioner(dtoIntKnown);
        return dtoIntPetitioners;
    }

    private DTOIntSegment mapInKnownSegment(final Segment segment) {
        if(segment == null) {
            return null;
        }
        DTOIntSegment result = new DTOIntSegment();
        result.setId(segment.getId());
        return result;
    }

    private DTOIntBank mapInKnownBankHolder(final Bank bank) {
        if(bank == null) {
            return null;
        }
        DTOIntBank result = new DTOIntBank();
        result.setId(bank.getId());
        result.setBranch(mapInKnownBranchHolder(bank.getBranch()));
        return result;
    }

    private DTOIntBranch mapInKnownBranchHolder(final Branch branch) {
        if(branch == null) {
            return null;
        }
        DTOIntBranch result = new DTOIntBranch();
        result.setId(branch.getId());
        return result;
    }

    private void mapInKnownContactDetails(final DTOIntKnownPetitioner dtoIntKnown, final Petitioner petitioner) {
        if (dtoIntKnown.getAddress() == null) {
            dtoIntKnown.setAddress(new DTOIntAddress());
        }

        dtoIntKnown.setContactDetails(mapInContactDetail(petitioner));
    }

    private List<DTOIntContact> mapInContactDetail(final Petitioner petitioner) {
        return petitioner.getContactDetails()
                .stream()
                .map(cd -> {
                    DTOIntContact dtoIntContact = new DTOIntContact();
                    if (cd.getContact() != null) {
                        if (StringUtils.isNotEmpty(cd.getContact().getContactDetailType())) {
                            if ("EMAIL".equalsIgnoreCase(cd.getContact().getContactDetailType())) {
                                DTOIntEmail dtoIntEmail = new DTOIntEmail();
                                dtoIntEmail.setId(cd.getContact().getId());
                                dtoIntEmail.setAddress(cd.getContact().getAddress());
                                dtoIntEmail.setContactDetailType(mapInContactDetailType(cd.getContact().getContactDetailType()));
                                dtoIntContact.setContact(dtoIntEmail);
                                return dtoIntContact;
                            } else if ("MOBILE".equalsIgnoreCase(cd.getContact().getContactDetailType()) ||
                                    "LANDLINE".equalsIgnoreCase(cd.getContact().getContactDetailType())) {
                                DTOIntMobile dtoIntMobile = new DTOIntMobile();
                                dtoIntMobile.setPhoneCompany(mapInPhoneCompany(cd.getContact().getPhoneCompany()));
                                dtoIntMobile.setId(cd.getContact().getId());
                                dtoIntMobile.setNumber(cd.getContact().getNumber());
                                dtoIntMobile.setContactDetailType(mapInContactDetailType(cd.getContact().getContactDetailType()));
                                dtoIntContact.setContact(dtoIntMobile);
                                return dtoIntContact;
                            }
                        } else {
                            DTOIntBaseContract dtoIntBaseContract = new DTOIntBaseContract();
                            dtoIntBaseContract.setContactDetailType(null);
                            dtoIntContact.setContact(dtoIntBaseContract);
                            return dtoIntContact;
                        }
                    }
                    return dtoIntContact;
                }).collect(Collectors.toList());
    }


    private DTOIntPhoneCompany mapInPhoneCompany(final PhoneCompany phoneCompany) {
        if (phoneCompany == null) {
            return null;
        }

        DTOIntPhoneCompany dtoIntPhoneCompany = new DTOIntPhoneCompany();
        dtoIntPhoneCompany.setId(phoneCompany.getId());
        return dtoIntPhoneCompany;
    }

    private DTOContactDetailType mapInContactDetailType(final String contactDetailType) {
        DTOContactDetailType dtoContactDetailType = new DTOContactDetailType();
        dtoContactDetailType.setId(contactDetailType);
        return dtoContactDetailType;
    }

    private void mapInKnownAddressComponents(final DTOIntKnownPetitioner dtoIntKnown, final Petitioner petitioner) {
        if (dtoIntKnown.getAddress() == null) {
            dtoIntKnown.setAddress(new DTOIntAddress());
        }
        if (dtoIntKnown.getAddress().getLocation() == null) {
            dtoIntKnown.getAddress().setLocation(new DTOIntLocation());
        }

        dtoIntKnown.getAddress().getLocation().setAddressComponents(mapInAddressComponents(petitioner));
    }

    private List<DTOIntAddressComponent> mapInAddressComponents(final Petitioner petitioner) {
        List<AddressComponent> acNotRepeat = new ArrayList<>();
        List<DTOIntAddressComponent> dtoIntAddressComponents = petitioner.getAddress().getLocation().getAddressComponents()
                .stream()
                .map(ac -> {
                    if (CollectionUtils.isNotEmpty(ac.getComponentTypes())) {
                        String componentTypeCode = ac.getComponentTypes()
                                .stream()
                                .filter(ct -> StringUtils.isNotEmpty(ct) &&
                                        ("DEPARTMENT".equalsIgnoreCase(ct) ||
                                                "PROVINCE".equalsIgnoreCase(ct) ||
                                                "DISTRICT".equalsIgnoreCase(ct) ||
                                                "UBIGEO".equalsIgnoreCase(ct)) &&
                                        !found(ct, acNotRepeat))
                                .findAny()
                                .orElse(null);
                        acNotRepeat.add(ac);
                        return mapInAddressComponent(componentTypeCode, ac.getCode(), ac.getComponentTypes());
                    }
                    return null;
                })
                .collect(Collectors.toList());

        dtoIntAddressComponents.removeAll(Collections.singleton(null));
        if (CollectionUtils.isEmpty(dtoIntAddressComponents)) {
            return null;
        }
        return dtoIntAddressComponents;
    }

    private DTOIntAddressComponent mapInAddressComponent(final String componentTypeCode, final String componentCode,
                                                         final List<String> componentTypes) {
        if (StringUtils.isEmpty(componentTypeCode)) {
            return null;
        }

        DTOIntAddressComponent dtoIntAddressComponent = new DTOIntAddressComponent();
        dtoIntAddressComponent.setCode(componentCode);
        dtoIntAddressComponent.setComponentTypes(componentTypes);
        return dtoIntAddressComponent;
    }

    private boolean found(String categoryUbigeo, List<AddressComponent> acNotRepeat) {
        for (AddressComponent ac : acNotRepeat) {
            if (!CollectionUtils.isEmpty(ac.getComponentTypes()) && categoryUbigeo.equals(ac.getComponentTypes().get(0)))
                return true;
        }
        return false;
    }

    private String mapInKnowPetitionerAuthorized(final String petitionerId) {
        if (StringUtils.isEmpty(petitionerId)) {
            return null;
        }

        return petitionerId;
    }

    private String mapInKnownPetitionerHolder(final String petitionerId) {
        if (StringUtils.isEmpty(petitionerId)) {
            return null;
        }

        String clientId = serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID);
        if (StringUtils.isEmpty(clientId)) {
            return petitionerId;
        }

        return clientId;
    }

    @Override
    public ProductClaimTransactionData mapOut(final ProductClaimTransaction dtoOut) {
        if (dtoOut == null) {
            return null;
        }

        ProductClaimTransactionData result = new ProductClaimTransactionData();
        result.setData(dtoOut);
        return result;
    }
}
