package com.bbva.pzic.claims.dao.rest.model.harec;

import java.util.Calendar;

public class TaxonomiasAdicionales {

    private Tipo tipo;
    private Producto producto;
    private Motivo motivo;
    private Submotivo submotivo;
    private Double importeReclamado;
    private String divisa;
    private String cajero;
    private BancoOriginante bancoOriginante;
    private String codigoOficinaOriginante;
    private Calendar fechaOperacion;
    private OficinaOriginante oficinaOriginante;

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Motivo getMotivo() {
        return motivo;
    }

    public void setMotivo(Motivo motivo) {
        this.motivo = motivo;
    }

    public Submotivo getSubmotivo() {
        return submotivo;
    }

    public void setSubmotivo(Submotivo submotivo) {
        this.submotivo = submotivo;
    }

    public Double getImporteReclamado() {
        return importeReclamado;
    }

    public void setImporteReclamado(Double importeReclamado) {
        this.importeReclamado = importeReclamado;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getCajero() {
        return cajero;
    }

    public void setCajero(String cajero) {
        this.cajero = cajero;
    }

    public BancoOriginante getBancoOriginante() {
        return bancoOriginante;
    }

    public void setBancoOriginante(BancoOriginante bancoOriginante) {
        this.bancoOriginante = bancoOriginante;
    }

    public String getCodigoOficinaOriginante() {
        return codigoOficinaOriginante;
    }

    public void setCodigoOficinaOriginante(String codigoOficinaOriginante) {
        this.codigoOficinaOriginante = codigoOficinaOriginante;
    }

    public Calendar getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Calendar fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public OficinaOriginante getOficinaOriginante() {
        return oficinaOriginante;
    }

    public void setOficinaOriginante(OficinaOriginante oficinaOriginante) {
        this.oficinaOriginante = oficinaOriginante;
    }
}