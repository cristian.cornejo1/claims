package com.bbva.pzic.claims.dao.rest.model.harec;

/**
 * Created on 03/02/2021.
 *
 * @author Entelgy.
 */
public class DictamenType {

    protected String codigo;
    protected TipoType fundamento;
    protected TipoType clase;
    protected String nombre;
    protected String sustento;
    protected String operativaManual;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public TipoType getFundamento() {
        return fundamento;
    }

    public void setFundamento(TipoType fundamento) {
        this.fundamento = fundamento;
    }

    public TipoType getClase() {
        return clase;
    }

    public void setClase(TipoType clase) {
        this.clase = clase;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSustento() {
        return sustento;
    }

    public void setSustento(String sustento) {
        this.sustento = sustento;
    }

    public String getOperativaManual() {
        return operativaManual;
    }

    public void setOperativaManual(String operativaManual) {
        this.operativaManual = operativaManual;
    }
}
