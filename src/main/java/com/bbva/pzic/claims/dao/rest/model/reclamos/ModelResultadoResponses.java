package com.bbva.pzic.claims.dao.rest.model.reclamos;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelResultadoResponses {
    private List<ModelResultado> resultado;
    private List<Message> messages;

    public List<ModelResultado> getResultado() {
        return resultado;
    }

    public void setResultado(List<ModelResultado> resultado) {
        this.resultado = resultado;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
