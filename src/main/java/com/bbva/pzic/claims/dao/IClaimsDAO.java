package com.bbva.pzic.claims.dao;

import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;


import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface IClaimsDAO {

    ProductClaimTransaction createProductClaim(DTOIntProductClaim dtoInt);

    AutomaticRefundTransaction validateAutomaticRefundTransaction(
            DTOIntAutomaticRefundTransaction dtoInt);
    List<ProductClaims> searchProductClaims(InputSearchProductClaims dtoInt);

    AutomaticRefundTransaction getAutomaticRefundTransaction(
            InputGetAutomaticRefundTransaction input);
    ProductClaims getProductClaim(InputGetProductClaim input);

    void sendEmailProductClaim(InputSendEmailProductClaim input);

    List<AutomaticRefundTransaction> listAutomaticRefundTransactions(
            InputListAutomaticRefundTransactions input);
    PartialTransaction modifyPartialAutomaticRefundTransaction(InputModifyAutomaticRefundTransaction input);

}