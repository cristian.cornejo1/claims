package com.bbva.pzic.claims.dao.rest.model.harec;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para requestType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="requestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="data" type="{/harec-rest-aso/registro/requerimiento}requestDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestType", propOrder = {
        "data",
        "canal",
        "tipoDoi",
        "numeroDoi",
        "codigoCentral",
        "numeroRequerimiento",
        "titular",
        "numero",
        "correos",
        "tipoContacto",
        "adjuntos",
        "titular"
})
public class RequestType {

    @XmlElement(required = true)
    protected RequestDataType data;
    @XmlElement(required = true)
    protected String tipoDoi;
    @XmlElement(required = true)
    @DatoAuditable(omitir = true)
    protected String numeroDoi;
    @XmlElement(required = true)
    protected String codigoCentral;
    @XmlElement(required = true)
    protected String numeroRequerimiento;
    @XmlElement(required = true)
    protected TitularType titular;
    @XmlElement(required = true)
    protected String numero;
    protected List<CorreosType> correos;
    @XmlElement(required = true)
    protected String tipoContacto;
    protected List<AdjuntosType> adjuntos;

    protected String canal;

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    /**
     * Obtiene el valor de la propiedad data.
     *
     * @return possible object is
     * {@link RequestDataType }
     */
    public RequestDataType getData() {
        return data;
    }

    /**
     * Define el valor de la propiedad data.
     *
     * @param value allowed object is
     *              {@link RequestDataType }
     */
    public void setData(RequestDataType value) {
        this.data = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDoi.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipoDoi() {
        return tipoDoi;
    }

    /**
     * Define el valor de la propiedad tipoDoi.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipoDoi(String value) {
        this.tipoDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroDoi.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNumeroDoi() {
        return numeroDoi;
    }

    /**
     * Define el valor de la propiedad numeroDoi.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNumeroDoi(String value) {
        this.numeroDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCodigoCentral() {
        return codigoCentral;
    }

    /**
     * Define el valor de la propiedad codigoCentral.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCodigoCentral(String value) {
        this.codigoCentral = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroRequerimiento.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNumeroRequerimiento() {
        return numeroRequerimiento;
    }

    /**
     * Define el valor de la propiedad numeroRequerimiento.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNumeroRequerimiento(String value) {
        this.numeroRequerimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     *
     * @return possible object is
     * {@link TitularType }
     */
    public TitularType getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     *
     * @param value allowed object is
     *              {@link TitularType }
     */
    public void setTitular(TitularType value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad numero.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Define el valor de la propiedad numero.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the correos property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the correos property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorreos().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorreosType }
     */
    public List<CorreosType> getCorreos() {
        if (correos == null) {
            correos = new ArrayList<>();
        }
        return this.correos;
    }

    /**
     * Obtiene el valor de la propiedad tipoContacto.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipoContacto() {
        return tipoContacto;
    }

    /**
     * Define el valor de la propiedad tipoContacto.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipoContacto(String value) {
        this.tipoContacto = value;
    }

    /**
     * Gets the value of the adjuntos property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjuntos property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjuntos().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdjuntosType }
     */
    public List<AdjuntosType> getAdjuntos() {
        if (adjuntos == null) {
            adjuntos = new ArrayList<>();
        }
        return this.adjuntos;
    }
}
