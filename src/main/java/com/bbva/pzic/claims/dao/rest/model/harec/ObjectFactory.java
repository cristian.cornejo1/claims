package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the harec_rest_aso.registro.requerimiento package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Request_QNAME = new QName("/harec-rest-aso/registro/requerimiento", "request");
    private final static QName _Response_QNAME = new QName("/harec-rest-aso/registro/requerimiento", "response");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: harec_rest_aso.registro.requerimiento
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RequestType }
     */
    public RequestType createRequestType() {
        return new RequestType();
    }

    /**
     * Create an instance of {@link ResponseType }
     */
    public ResponseType createResponseType() {
        return new ResponseType();
    }

    /**
     * Create an instance of {@link RequestDataType }
     */
    public RequestDataType createRequestDataType() {
        return new RequestDataType();
    }

    /**
     * Create an instance of {@link TitularType }
     */
    public TitularType createTitularType() {
        return new TitularType();
    }

    /**
     * Create an instance of {@link RepresentanteType }
     */
    public RepresentanteType createRepresentanteType() {
        return new RepresentanteType();
    }

    /**
     * Create an instance of {@link CorreoType }
     */
    public CorreoType createCorreoType() {
        return new CorreoType();
    }

    /**
     * Create an instance of {@link CorreosType }
     */
    public CorreosType createCorreosType() {
        return new CorreosType();
    }

    /**
     * Create an instance of {@link TelefonoType }
     */
    public TelefonoType createTelefonoType() {
        return new TelefonoType();
    }

    /**
     * Create an instance of {@link TelefonosType }
     */
    public TelefonosType createTelefonosType() {
        return new TelefonosType();
    }

    /**
     * Create an instance of {@link DireccionType }
     */
    public DireccionType createDireccionType() {
        return new DireccionType();
    }

    /**
     * Create an instance of {@link UbigeoType }
     */
    public UbigeoType createUbigeoType() {
        return new UbigeoType();
    }

    /**
     * Create an instance of {@link TaxonomiaType }
     */
    public TaxonomiaType createTaxonomiaType() {
        return new TaxonomiaType();
    }

    /**
     * Create an instance of {@link ObjectType }
     */
    public ObjectType createObjectType() {
        return new ObjectType();
    }

    /**
     * Create an instance of {@link AdjuntoType }
     */
    public AdjuntoType createAdjuntoType() {
        return new AdjuntoType();
    }

    /**
     * Create an instance of {@link AdjuntosType }
     */
    public AdjuntosType createAdjuntosType() {
        return new AdjuntosType();
    }

    /**
     * Create an instance of {@link RequerimientoType }
     */
    public RequerimientoType createRequerimientoType() {
        return new RequerimientoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestType }{@code >}}
     */
    @XmlElementDecl(namespace = "/harec-rest-aso/registro/requerimiento", name = "request")
    public JAXBElement<RequestType> createRequest(RequestType value) {
        return new JAXBElement<RequestType>(_Request_QNAME, RequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseType }{@code >}}
     */
    @XmlElementDecl(namespace = "/harec-rest-aso/registro/requerimiento", name = "response")
    public JAXBElement<ResponseType> createResponse(ResponseType value) {
        return new JAXBElement<ResponseType>(_Response_QNAME, ResponseType.class, null, value);
    }

}
