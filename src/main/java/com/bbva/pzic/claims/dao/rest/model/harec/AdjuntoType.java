package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para adjuntoType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="adjuntoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="adjunto" type="{/harec-rest-aso/registro/requerimiento}adjuntosType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adjuntoType", propOrder = {
        "est"
})
public class AdjuntoType {

    protected List<AdjuntosType> est;
    protected List<AdjuntosType> adjunto;

    /**
     * Gets the value of the est property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the est property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEst().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdjuntosType }
     */
    public List<AdjuntosType> getEst() {
        if (est == null) {
            est = new ArrayList<>();
        }
        return this.est;
    }

    /**
     * Gets the value of the est property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the est property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEst().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdjuntosType }
     */
    public List<AdjuntosType> getAdjunto() {
        if (adjunto == null) {
            adjunto = new ArrayList<>();
        }
        return this.adjunto;
    }
}
