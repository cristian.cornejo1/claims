package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.StatusRefund;
import com.bbva.pzic.claims.dao.rest.model.reclamos.*;
import com.bbva.pzic.claims.dao.rest.mapper.IRestValidateAutomaticRefundTransactionMapper;
import com.bbva.pzic.claims.facade.v0.dto.Situation;
import com.bbva.pzic.claims.facade.v0.dto.SubStatus;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext.CALLING_CHANNEL;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestValidateAutomaticRefundTransactionMapper
        implements IRestValidateAutomaticRefundTransactionMapper {

    private static final Log LOG = LogFactory.getLog(RestValidateAutomaticRefundTransactionMapper.class);

    @Autowired
    private Translator translator;

    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Override
    public Map<String, String> mapHeaderIn(final DTOIntAutomaticRefundTransaction input) {
        LOG.info("... called method RestValidateAutomaticRefundTransactionMapper.mapHeaderIn ...");
        final HashMap<String, String> headerParams = new HashMap<>();
        headerParams.put("idCanal", input.getApp());
        headerParams.put("callingChannel", serviceInvocationContext.getProperty(CALLING_CHANNEL));
        return headerParams;
    }

    @Override
    public ModelResultado mapIn(final DTOIntAutomaticRefundTransaction input) {
        LOG.info("... called method RestValidateAutomaticRefundTransactionMapper.mapIn ...");
        if (input == null) {
            return null;
        }
        ModelResultado result = new ModelResultado();
        result.setTitular(mapinTitular(input.getPetitioner()));
        result.setRequerimientos(mapInRequerimientos(input.getPetitioner()));
        result.setNumeroCaso(mapInNumeroCaso(input));
        result.setMotivo(mapInMotivo(input));
        result.setOperacion(mapInOperacion(input.getOperations()));
        return result;
    }

    private ModelMotivo mapInMotivo(final DTOIntAutomaticRefundTransaction input) {
        if (input == null) {
            return null;
        }
        ModelMotivo result = new ModelMotivo();
        result.setCodigo(mapInMotivoCodigo(input.getReason()));
        return result;
    }

    private String mapInMotivoCodigo(final DTOIntReasonRefund reason) {
        if (reason == null) {
            return null;
        }
        return reason.getId();
    }

    private String mapInNumeroCaso(final DTOIntAutomaticRefundTransaction input) {
        if (input == null || input.getRelatedClaim() == null) {
            return null;
        }
        return input.getRelatedClaim().getId();
    }

    private ModelRequerimientos mapInRequerimientos(final DTOIntPetitioners petitioner) {
        if (petitioner == null || petitioner.getPetitioner() == null || CollectionUtils.isEmpty(petitioner.getPetitioner().getContactDetails())) {
            return null;
        }
        ModelRequerimientos result = new ModelRequerimientos();
        result.setContactos(mapInContactos(petitioner.getPetitioner().getContactDetails()));
        return result;
    }

    private List<ModelContacto> mapInContactos(final List<DTOIntContact> contactDetails) {
        if (CollectionUtils.isEmpty(contactDetails)) {
            return null;
        }
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContacto).collect(Collectors.toList());
    }

    private ModelContacto mapInContacto(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null) {
            return null;
        }
        ModelContacto result = new ModelContacto();
        result.setTipoContacto(mapInTipoContacto(dtoIntContact));

        if (result.getTipoContacto().equalsIgnoreCase("PHONE")) {
            result.setNumero(mapInNumero(dtoIntContact));
            result.setTipoFijo(mapInTipoFijo(dtoIntContact));
            result.setPaisFijo(mapInPaisFijo(dtoIntContact));
            result.setCodigoPaisFijo(mapInCodigoPaisFijo(dtoIntContact));
            result.setCodigoRegionalFijo(mapInRegionalFijo(dtoIntContact));
            result.setAnexoFijo(mapInAnexoFijo(dtoIntContact));
        }
        if (result.getTipoContacto().equalsIgnoreCase("MOBILE")) {
            result.setNumero(mapInNumero(dtoIntContact));
            result.setOperadorCelular(mapInOperadorCelular(dtoIntContact));
            result.setCodigoPaisCelular(mapInCodigoPaisCelular(dtoIntContact));
        }
        if (result.getTipoContacto().equalsIgnoreCase("EMAIL")) {
            result.setCorreo(mapInCorreo(dtoIntContact));
            result.setNotificacionesCorreo(mapInNotificionesCorreo(dtoIntContact));
        }

        return result;
    }

    private String mapInNotificionesCorreo(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return FunctionUtils.convertBooleanToString(dtoIntContact.getContacto().getReceivesNotifications());
    }

    private String mapInCorreo(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getAddress();
    }

    private String mapInCodigoPaisCelular(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getCountryCode();
    }

    private ModelOperadorCelular mapInOperadorCelular(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        ModelOperadorCelular result = new ModelOperadorCelular();
        result.setCodigo(mapInCodigo(dtoIntContact.getContacto()));
        return result;
    }

    private String mapInCodigo(final DTOIntBaseContact contacto) {
        if (contacto == null || contacto.getPhoneCompany() == null) {
            return null;
        }
        return contacto.getPhoneCompany().getId();
    }

    private String mapInAnexoFijo(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getExtension();
    }

    private String mapInRegionalFijo(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getRegionalCode();
    }

    private String mapInCodigoPaisFijo(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getCountryCode();
    }

    private String mapInPaisFijo(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null || dtoIntContact.getContacto().getCountry() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getCountry().getId();
    }

    private String mapInTipoFijo(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getPhoneType();
    }

    private String mapInNumero(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getNumber();
    }

    private String mapInTipoContacto(final DTOIntContact dtoIntContact) {
        if (dtoIntContact == null || dtoIntContact.getContacto() == null) {
            return null;
        }
        return dtoIntContact.getContacto().getContactDetailType();
    }

    private ModelTitular mapinTitular(final DTOIntPetitioners petitioner) {
        if (petitioner == null || petitioner.getPetitioner() == null) {
            return null;
        }
        ModelTitular result = new ModelTitular();
        result.setCodigoCentral(petitioner.getId());
        result.setTipoDoi(mapInTipodoi(petitioner.getPetitioner().getIdentityDocument()));
        result.setDoi(mapInDoi(petitioner.getPetitioner().getIdentityDocument()));
        result.setSegmento(mapInSegmento(petitioner.getPetitioner().getSegment()));
        return result;
    }

    private ModelSegmento mapInSegmento(final DTOIntSegment segment) {
        if (segment == null) {
            return null;
        }
        ModelSegmento result = new ModelSegmento();
        result.setCodigo(segment.getId());
        return result;
    }

    private String mapInDoi(final DTOIntIdentityDocument identityDocument) {
        if (identityDocument == null) {
            return null;
        }
        return identityDocument.getDocumentNumber();
    }

    private String mapInTipodoi(final DTOIntIdentityDocument identityDocument) {
        if (identityDocument == null || identityDocument.getDocumentType() == null) {
            return null;
        }
        return identityDocument.getDocumentType().getId();
    }

    private List<ModelOperacion> mapInOperacion(final List<DTOIntOperation> dtoIntOperations) {
        if (CollectionUtils.isEmpty(dtoIntOperations)) {
            return null;
        }
        return dtoIntOperations.stream().filter(Objects::nonNull).map(this::mapInModelOperation).collect(Collectors.toList());
    }

    private ModelOperacion mapInModelOperation(final DTOIntOperation dtoIntOperation) {
        if (dtoIntOperation == null) {
            return null;
        }
        ModelOperacion result = new ModelOperacion();
        result.setContrato(mapInContrato(dtoIntOperation.getSubproductRefund()));
        result.setModoAbono(mapInModoAbono(dtoIntOperation.getSubproductRefund()));
        result.setObservacion(dtoIntOperation.getDetail());
        result.setTipoOperacion(mapInTipoOperacion(dtoIntOperation.getOperationType()));
        result.setMoneda(mapInMoneda(dtoIntOperation.getRequestedAmount()));
        result.setImporte(mapInImporte(dtoIntOperation.getRequestedAmount()));
        result.setBanco(mapInBanco(dtoIntOperation.getBank()));
        result.setCentroContable(mapInCentroContrable(dtoIntOperation.getBank()));
        result.setProductoSeguro(mapInProductoSeguro(dtoIntOperation.getRelatedContract()));
        result.setContratoOrigen(mapInContratoOrigen(dtoIntOperation.getClaimReason()));
        result.setMovimiento(mapInMovimientos(dtoIntOperation.getClaimReason()));
        return result;
    }

    private List<ModelMovimiento> mapInMovimientos(final DTOIntClaimReason claimReason) {
        if (claimReason == null || claimReason.getContract() == null || CollectionUtils.isEmpty(claimReason.getContract().getTransactions())) {
            return null;
        }
        return claimReason.getContract().getTransactions().stream().filter(Objects::nonNull).map(this::mapInMovimiento).collect(Collectors.toList());
    }

    private ModelMovimiento mapInMovimiento(final DTOIntTransaction dtoIntTransaction) {
        if (dtoIntTransaction == null) {
            return null;
        }
        ModelMovimiento result = new ModelMovimiento();
        result.setNumero(dtoIntTransaction.getId());
        result.setFecha(FunctionUtils.dateToString(dtoIntTransaction.getOperationDate()));
        result.setDescripcion(dtoIntTransaction.getConcept());
        result.setTipo(mapInMovimientoTipo(dtoIntTransaction.getTransactionType()));
        result.setImporte(mapInMovimientoImporte(dtoIntTransaction.getLocalAmount()));
        result.setMoneda(mapInMovimientoMoneda(dtoIntTransaction.getLocalAmount()));
        result.setEstablecimiento(mapInEstablecimiento(dtoIntTransaction.getStore()));
        return result;
    }

    private ModelEstablecimiento mapInEstablecimiento(final DTOIntStore store) {
        if (store == null) {
            return null;
        }
        ModelEstablecimiento result = new ModelEstablecimiento();
        result.setCodigo(store.getId());
        result.setNombre(store.getName());
        return result;
    }

    private ModelMoneda mapInMovimientoMoneda(final DTOIntLocalAmount localAmount) {
        if (localAmount == null) {
            return null;
        }
        ModelMoneda result = new ModelMoneda();
        result.setCodigo(localAmount.getCurrency());
        return result;
    }

    private BigDecimal mapInMovimientoImporte(final DTOIntLocalAmount localAmount) {
        if (localAmount == null) {
            return null;
        }
        return localAmount.getAmount();
    }

    private String mapInMovimientoTipo(final DTOIntTransactionType transactionType) {
        if (transactionType == null) {
            return null;
        }
        return transactionType.getId();
    }

    private ModelContratoOrigen mapInContratoOrigen(final DTOIntClaimReason claimReason) {
        if (claimReason == null) {
            return null;
        }
        ModelContratoOrigen result = new ModelContratoOrigen();
        result.setTipo(mapInContratoOrigenTipo(claimReason.getContract()));
        if (result.getTipo().equalsIgnoreCase("TARJE")) {
            result.setTarjeta(mapInTarjeta(claimReason.getContract()));
            result.setNumeroTipo(mapInNumeroTipo(claimReason.getContract()));
            result.setNumero(mapInContratoOrigenNumero(claimReason.getContract()));
            result.setSubTipo(mapInSubTipo(claimReason.getContract()));
        }
        if (result.getTipo().equalsIgnoreCase("CUENT")) {
            result.setNumero(mapInAccountNumero(claimReason.getContract()));
            result.setNumeroTipo(mapInNumeroTipo(claimReason.getContract()));
            result.setSubTipo(mapInAccountSubTipo(claimReason.getContract()));
        }
        if (result.getTipo().equalsIgnoreCase("PREST")) {
            result.setNumero(mapInLoanNumero(claimReason.getContract()));
            result.setNumeroTipo(mapInNumeroTipo(claimReason.getContract()));
            result.setSubTipo(mapInLoanSubTipo(claimReason.getContract()));
        }

        return result;
    }

    private String mapInLoanSubTipo(final DTOIntBaseContract contract) {
        if (contract == null || contract.getLoanType() == null) {
            return null;
        }
        return contract.getLoanType().getId();
    }

    private String mapInLoanNumero(final DTOIntBaseContract contract) {
        if (contract == null) {
            return null;
        }
        return contract.getNumber();
    }

    private String mapInAccountSubTipo(final DTOIntBaseContract contract) {
        if (contract == null || contract.getAccountType() == null) {
            return null;
        }
        return contract.getAccountType().getId();
    }

    private String mapInAccountNumero(final DTOIntBaseContract contract) {
        if (contract == null) {
            return null;
        }
        return contract.getNumber();
    }

    private String mapInSubTipo(final DTOIntBaseContract contract) {
        if (contract == null || contract.getCardAgreement() == null || contract.getCardAgreement().getSubProduct() == null) {
            return null;
        }
        return contract.getCardAgreement().getSubProduct().getId();
    }

    private String mapInContratoOrigenNumero(final DTOIntBaseContract contract) {
        if (contract == null || contract.getCardAgreement() == null) {
            return null;
        }
        return contract.getCardAgreement().getId();
    }

    private String mapInNumeroTipo(final DTOIntBaseContract contract) {
        if (contract == null || contract.getNumberType() == null) {
            return null;
        }
        return contract.getNumberType().getId();
    }

    private ModelTarjeta mapInTarjeta(final DTOIntBaseContract contract) {
        if (contract == null) {
            return null;
        }
        ModelTarjeta result = new ModelTarjeta();
        result.setNumero(contract.getNumber());
        return result;
    }

    private String mapInContratoOrigenTipo(final DTOIntBaseContract contract) {
        if (contract == null) {
            return null;
        }
        return contract.getContractType();
    }

    private ModelProductoSeguro mapInProductoSeguro(final DTOIntRelatedContract relatedContract) {
        if (relatedContract == null) {
            return null;
        }
        ModelProductoSeguro result = new ModelProductoSeguro();
        result.setCodigo(relatedContract.getId());
        result.setMoneda(mapInMoneda(relatedContract.getRequestedAmount()));
        result.setImporte(mapInImporte(relatedContract.getRequestedAmount()));
        return result;
    }

    private ModelCentroContable mapInCentroContrable(final DTOIntBank bank) {
        if (bank == null || bank.getBranch() == null) {
            return null;
        }
        ModelCentroContable result = new ModelCentroContable();
        result.setCodigo(bank.getBranch().getId());
        return result;
    }

    private ModelBanco mapInBanco(final DTOIntBank bank) {
        if (bank == null) {
            return null;
        }
        ModelBanco result = new ModelBanco();
        result.setId(bank.getId());
        return result;
    }

    private BigDecimal mapInImporte(final DTOIntRequestedAmount requestedAmount) {
        if (requestedAmount == null) {
            return null;
        }
        return requestedAmount.getAmount();
    }

    private ModelMoneda mapInMoneda(final DTOIntRequestedAmount requestedAmount) {
        if (requestedAmount == null) {
            return null;
        }
        ModelMoneda result = new ModelMoneda();
        result.setCodigo(requestedAmount.getCurrency());
        return result;
    }

    private ModelTipoOperacion mapInTipoOperacion(final String operationType) {
        if (operationType == null) {
            return null;
        }
        ModelTipoOperacion result = new ModelTipoOperacion();
        result.setCodigo(operationType);
        return result;
    }

    private ModelModoAbono mapInModoAbono(final DTOIntSubproductRefund subproductRefund) {
        if (subproductRefund == null) {
            return null;
        }
        ModelModoAbono result = new ModelModoAbono();
        if (subproductRefund.getId().equalsIgnoreCase("MPTAR")) {
            result.setCodigo(subproductRefund.getRefundMode());
            return result;
        } else {
            return null;
        }
    }

    private ModelContrato mapInContrato(final DTOIntSubproductRefund subproductRefund) {
        if (subproductRefund == null) {
            return null;
        }
        ModelContrato result = new ModelContrato();
        result.setAliasProducto(subproductRefund.getId());
        if (result.getAliasProducto().equalsIgnoreCase("MPTAR")) {
            result.setTarjeta(subproductRefund.getNumber());
        } else {
            result.setNumero(subproductRefund.getNumber());
        }
        return result;
    }

    @Override
    public AutomaticRefundTransaction mapOut(final ModelResultadoResponse model) {
        LOG.info("... called method RestValidateAutomaticRefundTransactionMapper.mapOut ...");
        if (model.getResultado() == null) {
            return null;
        }
        AutomaticRefundTransaction result = new AutomaticRefundTransaction();
        result.setId(model.getResultado().getNumero());
        result.setStatus(mapOutStatus(model.getResultado()));
        return result;
    }

    private StatusRefund mapOutStatus(final ModelResultado resultado) {
        if (resultado.getEstado() == null || resultado.getEstadoAtencion() == null || resultado.getSituacion() == null) {
            return null;
        }
        StatusRefund result = new StatusRefund();
        result.setId(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.status.id", resultado.getEstado().getCodigo()));
        result.setName(resultado.getEstado().getNombre());
        result.setSubStatus(mapOutSubStatus(resultado.getEstadoAtencion()));
        result.setSituation(mapOutSituation(resultado.getSituacion()));
        return result;
    }

    private Situation mapOutSituation(final ModelSituacion situacion) {
        if (situacion == null) {
            return null;
        }
        Situation result = new Situation();
        result.setId(situacion.getCodigo());
        result.setName(situacion.getDescripcion());
        return result;
    }

    private SubStatus mapOutSubStatus(final ModelEstadoAtencion estadoAtencion) {
        if (estadoAtencion == null) {
            return null;
        }
        SubStatus result = new SubStatus();
        result.setId(estadoAtencion.getCodigo());
        result.setName(estadoAtencion.getDescripcion());
        return result;
    }
}

