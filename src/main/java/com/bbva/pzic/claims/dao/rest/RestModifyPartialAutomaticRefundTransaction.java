package com.bbva.pzic.claims.dao.rest;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.jee.arq.spring.core.rest.RestConnectorType;
import com.bbva.jee.arq.spring.core.rest.requests.RestRequest;
import com.bbva.pzic.claims.business.dto.InputModifyAutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.mapper.IRestModifyPartialAutomaticRefundTransactionMapper;
import com.bbva.pzic.claims.dao.rest.model.conformidad.Error;
import com.bbva.pzic.claims.dao.rest.model.conformidad.ErrorMessage;
import com.bbva.pzic.claims.dao.rest.model.conformidad.ResultadoConformidad;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;
import com.bbva.pzic.routine.commons.utils.rest.templates.templates.JSONDefaultRestConnectionProcessor;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.pzic.claims.facade.RegistryIds.SMC_REGISTRY_MODIFY_PARTIAL_AUTOMATIC_REFUND_TRANSACTION;

/**
 * Created on 21/05/2021.
 *
 * @author V.Grimaldos
 */
@Component
public class RestModifyPartialAutomaticRefundTransaction extends
        JSONDefaultRestConnectionProcessor<InputModifyAutomaticRefundTransaction, PartialTransaction, ResultadoConformidad, Error> {

    @Autowired
    private IRestModifyPartialAutomaticRefundTransactionMapper mapper;

    public RestModifyPartialAutomaticRefundTransaction() {
        super(RestConnectorType.BASIC, new TypeReference<ResultadoConformidad>() {
                }, new TypeReference<Error>() {
                },
                "servicing.smc.configuration.SMGG20200301.backend.url",
                "servicing.smc.configuration.SMGG20200301.backend.proxy",
                "servicing.smc.configuration.SMGG20200301.bbva_resuelve.backendId");
    }

    @Override
    public RestRequest mapInput(InputModifyAutomaticRefundTransaction input, String url, boolean useProxy) {
        return new RestRequest.Builder("PUT", url)
                .headers(mapper.mapHeaderIn(input))
                .useProxy(useProxy)
                .payload(convertObjectToJSON(mapper.mapBodyIn(input)))
                .build();
    }

    @Override
    public PartialTransaction mapOutput(RestConnectorResponse restConnectorResponse, ResultadoConformidad response, InputModifyAutomaticRefundTransaction input) {
        if (response == null) {
            return null;
        }
        return mapper.mapOut(response);
    }

    @Override
    protected void throwErrorIfExists(RestConnectorResponse restConnectorResponse, Error error) {
        ErrorMessage firstErrorMessage;

        if (error.getMessages() == null || error.getMessages().isEmpty()) {
            firstErrorMessage = new ErrorMessage();
        } else {
            firstErrorMessage = error.getMessages().get(0);
        }
        super.throwErrorIfExists(firstErrorMessage.getCode(), firstErrorMessage.getMessage(), SMC_REGISTRY_MODIFY_PARTIAL_AUTOMATIC_REFUND_TRANSACTION, restConnectorResponse.getStatusCode());
    }
}