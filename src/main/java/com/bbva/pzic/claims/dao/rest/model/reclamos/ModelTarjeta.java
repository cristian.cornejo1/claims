package com.bbva.pzic.claims.dao.rest.model.reclamos;

public class ModelTarjeta {

    private String numero;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
