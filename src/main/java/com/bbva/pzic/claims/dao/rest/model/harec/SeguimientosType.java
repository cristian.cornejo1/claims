package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para seguimientosType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="seguimientosType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="estado" type="{/harec-rest-aso/consulta/requerimiento}estadoType"/&gt;
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prende" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "seguimientosType", propOrder = {
        "estado",
        "fecha",
        "prende"
})
public class SeguimientosType {

    @XmlElement(required = true)
    protected EstadoType estado;
    @XmlElement(required = true)
    protected String fecha;
    @XmlElement(required = true)
    protected String prende;

    /**
     * Obtiene el valor de la propiedad estado.
     *
     * @return possible object is
     * {@link EstadoType }
     */
    public EstadoType getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     *
     * @param value allowed object is
     *              {@link EstadoType }
     */
    public void setEstado(EstadoType value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad prende.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPrende() {
        return prende;
    }

    /**
     * Define el valor de la propiedad prende.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPrende(String value) {
        this.prende = value;
    }

}
