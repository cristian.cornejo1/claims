package com.bbva.pzic.claims.dao.rest.model.reclamos;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
public class ModelPersona {

    private String id;
    private String number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
