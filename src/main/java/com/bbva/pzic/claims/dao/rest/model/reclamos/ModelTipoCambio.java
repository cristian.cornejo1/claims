package com.bbva.pzic.claims.dao.rest.model.reclamos;

import java.math.BigDecimal;

public class ModelTipoCambio {

    private BigDecimal valor;
    private String tipo;

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
