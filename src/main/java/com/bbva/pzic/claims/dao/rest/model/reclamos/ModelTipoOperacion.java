package com.bbva.pzic.claims.dao.rest.model.reclamos;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelTipoOperacion {
    private String codigo;
    private String descripcion;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
