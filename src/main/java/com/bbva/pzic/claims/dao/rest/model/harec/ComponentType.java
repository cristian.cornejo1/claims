package com.bbva.pzic.claims.dao.rest.model.harec;

import java.util.List;

/**
 * Created on 03/02/2021
 *
 * @author Entelgy
 */
public class ComponentType {

    protected String codigo;

    protected String nombre;

    protected List<String> tipoComponentes;

    public String getCodigo() { return codigo; }

    public void setCodigo(String codigo) { this.codigo = codigo; }

    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public List<String> getTipoComponentes() { return tipoComponentes; }

    public void setTipoComponentes(List<String> tipoComponentes) { this.tipoComponentes = tipoComponentes; }
}
