package com.bbva.pzic.claims.dao.rest.model.harec;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para representanteType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="representanteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codigoCentral" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tipoDoi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="doi" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nombres" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="apellidoPaterno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="apellidoMaterno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "representanteType", propOrder = {
        "codigoCentral",
        "tipoDoi",
        "doi",
        "nombres",
        "apellidoPaterno",
        "apellidoMaterno"
})
public class RepresentanteType {

    @XmlElement(required = true)
    protected String codigoCentral;
    @XmlElement(required = true)
    protected String tipoDoi;
    @XmlElement(required = true)
    @DatoAuditable(omitir = true)
    protected String doi;
    @XmlElement(required = true)
    @DatoAuditable(omitir = true)
    protected String nombres;
    @XmlElement(required = true)
    @DatoAuditable(omitir = true)
    protected String apellidoPaterno;
    @XmlElement(required = true)
    @DatoAuditable(omitir = true)
    protected String apellidoMaterno;

    private Segmento segmento;

    /**
     * Obtiene el valor de la propiedad codigoCentral.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCodigoCentral() {
        return codigoCentral;
    }

    /**
     * Define el valor de la propiedad codigoCentral.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCodigoCentral(String value) {
        this.codigoCentral = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDoi.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipoDoi() {
        return tipoDoi;
    }

    /**
     * Define el valor de la propiedad tipoDoi.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipoDoi(String value) {
        this.tipoDoi = value;
    }

    /**
     * Obtiene el valor de la propiedad doi.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDoi() {
        return doi;
    }

    /**
     * Define el valor de la propiedad doi.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDoi(String value) {
        this.doi = value;
    }

    /**
     * Obtiene el valor de la propiedad nombres.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * Define el valor de la propiedad nombres.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNombres(String value) {
        this.nombres = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoPaterno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * Define el valor de la propiedad apellidoPaterno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setApellidoPaterno(String value) {
        this.apellidoPaterno = value;
    }

    /**
     * Obtiene el valor de la propiedad apellidoMaterno.
     *
     * @return possible object is
     * {@link String }
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * Define el valor de la propiedad apellidoMaterno.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setApellidoMaterno(String value) {
        this.apellidoMaterno = value;
    }

    public Segmento getSegmento() {
        return segmento;
    }

    public void setSegmento(Segmento segmento) {
        this.segmento = segmento;
    }
}
