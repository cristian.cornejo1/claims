package com.bbva.pzic.claims.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.RestGetProductClaim;
import com.bbva.pzic.claims.dao.rest.mock.stubs.ResultadoModelsStubs;
import com.bbva.pzic.claims.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * Created on 27/11/2019.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestGetProductClaimMock extends RestGetProductClaim {

    public static final String EMPTY_TEST = "999";

    @Override
    public ResponseType connect(final String urlPropertyValue, final Map<String, String> headers,
                                final RequestType entityPayload, final String registryId) {

        String requerimiento = entityPayload.getNumeroRequerimiento();

        if (EMPTY_TEST.equals(requerimiento)) {
            return new ResponseType();
        }

        try {
            return ResultadoModelsStubs.INSTANCE.getResponseType();
        } catch (IOException ex) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, ex);
        }
    }
}
