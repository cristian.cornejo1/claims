package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.InputGetProductClaim;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;

/**
 * Created on 28/11/2019.
 *
 * @author Entelgy
 */
public interface IRestGetProductClaimMapper {

    RequestType mapInBody(InputGetProductClaim input);

    ProductClaims mapOut(ResponseType model);
}
