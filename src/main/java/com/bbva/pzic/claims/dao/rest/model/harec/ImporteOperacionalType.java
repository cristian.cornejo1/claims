package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Clase Java para objectType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="objectType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "importeOperacionalType", propOrder = {
        "divisa",
        "valor"
})
public class ImporteOperacionalType {

    protected String divisa;
    protected BigDecimal valor;

    /**
     * Obtiene el valor de la propiedad divisa.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDivisa() {
        return divisa;
    }

    /**
     * Define el valor de la propiedad divisa.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDivisa(String value) {
        this.divisa = value;
    }

    /**
     * Obtiene el valor de la propiedad valor.
     *
     * @return possible object is
     * {@link String }
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Define el valor de la propiedad valor.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }
}
