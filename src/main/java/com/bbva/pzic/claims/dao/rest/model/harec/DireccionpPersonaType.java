package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para direccionpPersonaType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="direccionpPersonaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="direccion" type="{/harec-rest-aso/consulta/requerimiento}direccionType"/&gt;
 *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "direccionpPersonaType", propOrder = {
        "direccion",
        "referencia"
})
public class DireccionpPersonaType {

    @XmlElement(required = true)
    protected DireccionType direccion;
    @XmlElement(required = true)
    protected String referencia;
    protected TipoType segmento;

    /**
     * Obtiene el valor de la propiedad direccion.
     *
     * @return possible object is
     * {@link DireccionType }
     */
    public DireccionType getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     *
     * @param value allowed object is
     *              {@link DireccionType }
     */
    public void setDireccion(DireccionType value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Define el valor de la propiedad referencia.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReferencia(String value) {
        this.referencia = value;
    }

    public TipoType getSegmento() {
        return segmento;
    }

    public void setSegmento(TipoType segmento) {
        this.segmento = segmento;
    }
}
