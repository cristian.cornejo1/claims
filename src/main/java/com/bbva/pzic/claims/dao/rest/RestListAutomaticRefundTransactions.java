package com.bbva.pzic.claims.dao.rest;

import com.bbva.pzic.claims.business.dto.InputListAutomaticRefundTransactions;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponses;
import com.bbva.pzic.claims.dao.rest.mapper.IRestListAutomaticRefundTransactionsMapper;
import com.bbva.pzic.claims.util.connection.rest.RestGetConnection;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

import static com.bbva.pzic.claims.facade.RegistryIds.SMC_REGISTRY_ID_OF_LIST_AUTOMATIC_REFUND_TRANSACTIONS;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestListAutomaticRefundTransactions extends RestGetConnection<ModelResultadoResponses> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1810332.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1810332.backend.proxy";

    @Resource(name = "restListAutomaticRefundTransactionsMapper")
    private IRestListAutomaticRefundTransactionsMapper mapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
    }

    public List<AutomaticRefundTransaction> invoke(InputListAutomaticRefundTransactions input) {
        final ModelResultadoResponses result = connect(
                URL_PROPERTY,
                mapper.mapInQuery(input),
                mapper.mapInHeader(input));
        return mapper.mapOut(result);
    }

    @Override
    protected void evaluateResponse(ModelResultadoResponses response, int statusCode) {
        super.evaluateMessagesResponse(response.getMessages(), SMC_REGISTRY_ID_OF_LIST_AUTOMATIC_REFUND_TRANSACTIONS, statusCode);
    }
}