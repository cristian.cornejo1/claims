package com.bbva.pzic.claims.dao.rest.model.reclamos;

import java.util.Date;
import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelResultado {

    private String numero;

    private Date fecha;

    private ModelMotivo motivo;

    private ModelEstado estado;

    private ModelEstadoAtencion estadoAtencion;

    private ModelEquipoResponsable equipoResponsable;

    private List<ModelUsuarios> usuarios;

    private ModelTitular titular;

    private List<ModelOperacion> operacion;

    private String codigoCentral;

    private ModelRequerimientos requerimientos;

    private String numeroCaso;

    private ModelSituacion situacion;

    private List<ModelAdjunto> adjunto;

    private List<ModelContacto> contactos;

    private String conformidadAtencion;
    private ModelCanal canal;

    private ModelSegmento segmento;

    public ModelEquipoResponsable getEquipoResponsable() {
        return equipoResponsable;
    }

    public void setEquipoResponsable(ModelEquipoResponsable equipoResponsable) {
        this.equipoResponsable = equipoResponsable;
    }

    public List<ModelUsuarios> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<ModelUsuarios> usuarios) {
        this.usuarios = usuarios;
    }

    public ModelSegmento getSegmento() {
        return segmento;
    }

    public void setSegmento(ModelSegmento segmento) {
        this.segmento = segmento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public ModelMotivo getMotivo() {
        return motivo;
    }

    public void setMotivo(ModelMotivo motivo) {
        this.motivo = motivo;
    }

    public ModelEstado getEstado() {
        return estado;
    }

    public void setEstado(ModelEstado estado) {
        this.estado = estado;
    }

    public ModelTitular getTitular() {
        return titular;
    }

    public void setTitular(ModelTitular titular) {
        this.titular = titular;
    }

    public List<ModelOperacion> getOperacion() {
        return operacion;
    }

    public void setOperacion(List<ModelOperacion> operacion) {
        this.operacion = operacion;
    }

    public String getCodigoCentral() {
        return codigoCentral;
    }

    public void setCodigoCentral(String codigoCentral) {
        this.codigoCentral = codigoCentral;
    }

    public ModelRequerimientos getRequerimientos() {
        return requerimientos;
    }

    public void setRequerimientos(ModelRequerimientos requerimientos) {
        this.requerimientos = requerimientos;
    }

    public String getNumeroCaso() {
        return numeroCaso;
    }

    public void setNumeroCaso(String numeroCaso) {
        this.numeroCaso = numeroCaso;
    }

    public ModelEstadoAtencion getEstadoAtencion() {
        return estadoAtencion;
    }

    public void setEstadoAtencion(ModelEstadoAtencion estadoAtencion) {
        this.estadoAtencion = estadoAtencion;
    }

    public ModelSituacion getSituacion() {
        return situacion;
    }

    public void setSituacion(ModelSituacion situacion) {
        this.situacion = situacion;
    }

    public List<ModelAdjunto> getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(List<ModelAdjunto> adjunto) {
        this.adjunto = adjunto;
    }


    public List<ModelContacto> getContactos() {
        return contactos;
    }

    public void setContactos(List<ModelContacto> contactos) {
        this.contactos = contactos;
    }

    public String getConformidadAtencion() {
        return conformidadAtencion;
    }

    public void setConformidadAtencion(String conformidadAtencion) {
        this.conformidadAtencion = conformidadAtencion;
    }

    public ModelCanal getCanal() {
        return canal;
    }

    public void setCanal(ModelCanal canal) {
        this.canal = canal;
    }

}
