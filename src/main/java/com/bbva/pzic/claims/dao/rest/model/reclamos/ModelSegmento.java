package com.bbva.pzic.claims.dao.rest.model.reclamos;

/**
 * Created on 24/02/2021.
 *
 * @author Entelgy
 */
public class ModelSegmento {

    private String codigo;
    private String descripcion;
    private String nombre;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
