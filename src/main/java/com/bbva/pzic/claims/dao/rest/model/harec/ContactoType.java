package com.bbva.pzic.claims.dao.rest.model.harec;

/**
 * Created on 03/02/2021.
 *
 * @author Entelgy.
 */
public class ContactoType {

    protected String id;
    protected String correo;
    protected String tipoContacto;
    protected String notificacionesCorreo;
    protected String numero;
    protected String tipoFijo;
    protected String paisFijo;
    protected String codigoPaisFijo;
    protected String codigoRegionalFijo;
    protected String anexoFijo;
    protected String codigoPaisCelular;
    protected TipoType operadorCelular;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public String getNotificacionesCorreo() {
        return notificacionesCorreo;
    }

    public void setNotificacionesCorreo(String notificacionesCorreo) {
        this.notificacionesCorreo = notificacionesCorreo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipoFijo() {
        return tipoFijo;
    }

    public void setTipoFijo(String tipoFijo) {
        this.tipoFijo = tipoFijo;
    }

    public String getPaisFijo() {
        return paisFijo;
    }

    public void setPaisFijo(String paisFijo) {
        this.paisFijo = paisFijo;
    }

    public String getCodigoPaisFijo() {
        return codigoPaisFijo;
    }

    public void setCodigoPaisFijo(String codigoPaisFijo) {
        this.codigoPaisFijo = codigoPaisFijo;
    }

    public String getCodigoRegionalFijo() {
        return codigoRegionalFijo;
    }

    public void setCodigoRegionalFijo(String codigoRegionalFijo) {
        this.codigoRegionalFijo = codigoRegionalFijo;
    }

    public String getAnexoFijo() {
        return anexoFijo;
    }

    public void setAnexoFijo(String anexoFijo) {
        this.anexoFijo = anexoFijo;
    }

    public String getCodigoPaisCelular() {
        return codigoPaisCelular;
    }

    public void setCodigoPaisCelular(String codigoPaisCelular) {
        this.codigoPaisCelular = codigoPaisCelular;
    }

    public TipoType getOperadorCelular() {
        return operadorCelular;
    }

    public void setOperadorCelular(TipoType operadorCelular) {
        this.operadorCelular = operadorCelular;
    }
}
