package com.bbva.pzic.claims.dao.rest.model.harec;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * <p>Clase Java para requestDataType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="requestDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tipoPersona" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="esMenor" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="titular" type="{/harec-rest-aso/registro/requerimiento}titularType"/&gt;
 *         &lt;element name="representante" type="{/harec-rest-aso/registro/requerimiento}representanteType"/&gt;
 *         &lt;element name="correos" type="{/harec-rest-aso/registro/requerimiento}correosType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *  *         &lt;element name="telefonos" type="{/harec-rest-aso/registro/requerimiento}telefonosType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="direccion" type="{/harec-rest-aso/registro/requerimiento}direccionType"/&gt;
 *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ubigeo" type="{/harec-rest-aso/registro/requerimiento}ubigeoType"/&gt;
 *         &lt;element name="cartaRespuesta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="taxonomia" type="{/harec-rest-aso/registro/requerimiento}taxonomiaType"/&gt;
 *         &lt;element name="detalle" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="adjuntos" type="{/harec-rest-aso/registro/requerimiento}adjuntosType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestDataType", propOrder = {
        "empresa",
        "tipoPersona",
        "esMenor",
        "titular",
        "representante",
        "correos",
        "telefonos",
        "direccion",
        "referencia",
        "ubigeo",
        "cartaRespuesta",
        "taxonomia",
        "detalle",
        "adjuntos"
})
public class RequestDataType {

    @XmlElement(required = true)
    protected String empresa;
    @XmlElement(required = true)
    protected String tipoPersona;
    @XmlElement(required = true)
    protected String esMenor;
    @XmlElement(required = true)
    protected TitularType titular;
    @XmlElement(required = true)
    protected RepresentanteType representante;
    protected List<CorreosType> correos;
    protected List<TelefonosType> telefonos;
    @XmlElement(required = true)
    protected DireccionType direccion;
    @XmlElement(required = true)
    @DatoAuditable(omitir = true)
    protected String referencia;
    @XmlElement(required = true)
    protected UbigeoType ubigeo;
    @XmlElement(required = true)
    protected String cartaRespuesta;
    @XmlElement(required = true)
    protected TaxonomiaType taxonomia;
    @XmlElement(required = true)
    protected String detalle;
    protected List<AdjuntosType> adjuntos;
    @DatoAuditable(omitir = true)
    protected String contrato;
    protected String divisa;
    protected BigDecimal importeReclamado;

    private Interposicion interposicion;
    private Ubicacion ubicacion;
    private String entregaHRCS;
    private String pedido;
    private String esUrgente;
    private String sustento;
    private Calendar fechaPresentacion;
    private String numeroCasoReiterativo;
    private String indicadorFCR;
    private String numeroAtencionLinea;
    private List<TaxonomiasAdicionales> taxonomiasAdicionales;


    public Interposicion getInterposicion() {
        return interposicion;
    }

    public void setInterposicion(Interposicion interposicion) {
        this.interposicion = interposicion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPersona.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Define el valor de la propiedad tipoPersona.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipoPersona(String value) {
        this.tipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad esMenor.
     *
     * @return possible object is
     * {@link String }
     */
    public String getEsMenor() {
        return esMenor;
    }

    /**
     * Define el valor de la propiedad esMenor.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setEsMenor(String value) {
        this.esMenor = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     *
     * @return possible object is
     * {@link TitularType }
     */
    public TitularType getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     *
     * @param value allowed object is
     *              {@link TitularType }
     */
    public void setTitular(TitularType value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad representante.
     *
     * @return possible object is
     * {@link RepresentanteType }
     */
    public RepresentanteType getRepresentante() {
        return representante;
    }

    /**
     * Define el valor de la propiedad representante.
     *
     * @param value allowed object is
     *              {@link RepresentanteType }
     */
    public void setRepresentante(RepresentanteType value) {
        this.representante = value;
    }

    /**
     * Gets the value of the correos property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the correos property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorreos().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorreosType }
     */
    public List<CorreosType> getCorreos() {
        if (correos == null) {
            correos = new ArrayList<>();
        }
        return this.correos;
    }

    /**
     * Gets the value of the telefonos property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the telefonos property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTelefonos().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TelefonosType }
     */
    public List<TelefonosType> getTelefonos() {
        if (telefonos == null) {
            telefonos = new ArrayList<>();
        }
        return this.telefonos;
    }

    /**
     * Obtiene el valor de la propiedad direccion.
     *
     * @return possible object is
     * {@link DireccionType }
     */
    public DireccionType getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     *
     * @param value allowed object is
     *              {@link DireccionType }
     */
    public void setDireccion(DireccionType value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia.
     *
     * @return possible object is
     * {@link String }
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Define el valor de la propiedad referencia.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setReferencia(String value) {
        this.referencia = value;
    }

    /**
     * Obtiene el valor de la propiedad ubigeo.
     *
     * @return possible object is
     * {@link UbigeoType }
     */
    public UbigeoType getUbigeo() {
        return ubigeo;
    }

    /**
     * Define el valor de la propiedad ubigeo.
     *
     * @param value allowed object is
     *              {@link UbigeoType }
     */
    public void setUbigeo(UbigeoType value) {
        this.ubigeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cartaRespuesta.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCartaRespuesta() {
        return cartaRespuesta;
    }

    /**
     * Define el valor de la propiedad cartaRespuesta.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCartaRespuesta(String value) {
        this.cartaRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad taxonomia.
     *
     * @return possible object is
     * {@link TaxonomiaType }
     */
    public TaxonomiaType getTaxonomia() {
        return taxonomia;
    }

    /**
     * Define el valor de la propiedad taxonomia.
     *
     * @param value allowed object is
     *              {@link TaxonomiaType }
     */
    public void setTaxonomia(TaxonomiaType value) {
        this.taxonomia = value;
    }

    /**
     * Obtiene el valor de la propiedad detalle.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDetalle() {
        return detalle;
    }

    /**
     * Define el valor de la propiedad detalle.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDetalle(String value) {
        this.detalle = value;
    }

    /**
     * Gets the value of the adjuntos property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjuntos property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjuntos().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdjuntosType }
     */
    public List<AdjuntosType> getAdjuntos() {
        if (adjuntos == null) {
            adjuntos = new ArrayList<AdjuntosType>();
        }
        return this.adjuntos;
    }

    /**
     * Obtiene el valor de la propiedad contrato.
     *
     * @return possible object is
     * {@link String }
     */
    public String getContrato() {
        return contrato;
    }

    /**
     * Define el valor de la propiedad contrato.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setContrato(String value) {
        this.contrato = value;
    }

    /**
     * Obtiene el valor de la propiedad divisa.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDivisa() {
        return divisa;
    }

    /**
     * Define el valor de la propiedad divisa.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDivisa(String value) {
        this.divisa = value;
    }

    /**
     * Obtiene el valor de la propiedad importeReclamado.
     *
     * @return possible object is
     * {@link String }
     */
    public BigDecimal getImporteReclamado() {
        return importeReclamado;
    }

    /**
     * Define el valor de la propiedad importeReclamado.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setImporteReclamado(BigDecimal value) {
        this.importeReclamado = value;
    }

    public String getEntregaHRCS() {
        return entregaHRCS;
    }

    public void setEntregaHRCS(String entregaHRCS) {
        this.entregaHRCS = entregaHRCS;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public String getEsUrgente() {
        return esUrgente;
    }

    public void setEsUrgente(String esUrgente) {
        this.esUrgente = esUrgente;
    }

    public String getSustento() {
        return sustento;
    }

    public void setSustento(String sustento) {
        this.sustento = sustento;
    }

    public Calendar getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(Calendar fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public String getNumeroCasoReiterativo() {
        return numeroCasoReiterativo;
    }

    public void setNumeroCasoReiterativo(String numeroCasoReiterativo) {
        this.numeroCasoReiterativo = numeroCasoReiterativo;
    }

    public String getIndicadorFCR() {
        return indicadorFCR;
    }

    public void setIndicadorFCR(String indicadorFCR) {
        this.indicadorFCR = indicadorFCR;
    }

    public String getNumeroAtencionLinea() {
        return numeroAtencionLinea;
    }

    public void setNumeroAtencionLinea(String numeroAtencionLinea) {
        this.numeroAtencionLinea = numeroAtencionLinea;
    }

    public List<TaxonomiasAdicionales> getTaxonomiasAdicionales() {
        return taxonomiasAdicionales;
    }

    public void setTaxonomiasAdicionales(List<TaxonomiasAdicionales> taxonomiasAdicionales) {
        this.taxonomiasAdicionales = taxonomiasAdicionales;
    }
}