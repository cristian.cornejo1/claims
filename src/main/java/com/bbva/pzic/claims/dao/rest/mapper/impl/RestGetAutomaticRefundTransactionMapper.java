package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.business.dto.InputGetAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.ContactDetail;
import com.bbva.pzic.claims.canonic.*;
import com.bbva.pzic.claims.dao.rest.model.reclamos.*;
import com.bbva.pzic.claims.dao.rest.mapper.IRestGetAutomaticRefundTransactionMapper;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.claims.util.mappers.Mapper;
import com.bbva.pzic.claims.util.orika.impl.ConfigurableMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestGetAutomaticRefundTransactionMapper extends ConfigurableMapper
        implements IRestGetAutomaticRefundTransactionMapper {

    private static final Log LOG = LogFactory.getLog(RestGetAutomaticRefundTransactionMapper.class);

    @Autowired
    private Translator translator;

    @Override
    public HashMap<String, String> mapPathIn(final InputGetAutomaticRefundTransaction input) {
        LOG.info("... called method RestGetAutomaticRefundTransactionMapper.mapPathIn ...");
        final HashMap<String, String> paths = new HashMap<>();
        paths.put("numero", input.getAutomaticRefundTransactionId());
        return paths;
    }

    @Override
    public Map<String, String> mapHeaderIn(final InputGetAutomaticRefundTransaction input) {
        LOG.info("... called method RestGetAutomaticRefundTransactionMapper.mapHeaderIn ...");
        final Map<String, String> paths = new HashMap<>();
        paths.put("idCanal", input.getApp());
        return paths;
    }

    @Override
    public AutomaticRefundTransaction mapOut(final ModelResultadoResponse model) {
        LOG.info("... called method RestGetAutomaticRefundTransactionMapper.mapOut ...");
        if (model == null || model.getResultado() == null) {
            return null;
        }
        AutomaticRefundTransaction automaticRefundTransaction = new AutomaticRefundTransaction();
        automaticRefundTransaction.setId(model.getResultado().getNumero());
        automaticRefundTransaction.setCreationDate(model.getResultado().getFecha());
        automaticRefundTransaction.setReason(mapOutReason(model.getResultado().getMotivo()));
        automaticRefundTransaction.setCustomerConfirmation(FunctionUtils.convertTo(model.getResultado().getConformidadAtencion()));
        automaticRefundTransaction.setPetitioner(mapOutPetitioner(model.getResultado().getTitular(), model.getResultado().getContactos()));
        automaticRefundTransaction.setRelatedClaim(mapOutRelatedClaim(model.getNumeroCaso()));
        automaticRefundTransaction.setOperations(mapOutOperation(model.getResultado().getOperacion()));
        automaticRefundTransaction.setStatus(mapOutStatus(model.getResultado().getEstado(), model.getResultado().getEstadoAtencion()));
        automaticRefundTransaction.setSituation(mapOutSituation(model.getResultado().getSituacion()));
        automaticRefundTransaction.setChannel(mapOutChannel(model.getResultado().getCanal()));
        automaticRefundTransaction.setFiles(mapOutFiles(model.getResultado().getAdjunto()));
        automaticRefundTransaction.setBusinessTeam(mapOutBusinessTeam(model.getResultado().getEquipoResponsable()));
        automaticRefundTransaction.setBusinessAgents(mapOutBusinessAgents(model.getResultado().getUsuarios()));
        return automaticRefundTransaction;
    }

    private ReasonRefund mapOutReason(final ModelMotivo motivo) {
        if (motivo == null) {
            return null;
        }
        ReasonRefund reasonRefund = new ReasonRefund();
        reasonRefund.setId(motivo.getCodigo());
        reasonRefund.setName(motivo.getNombre());
        return reasonRefund;
    }

    private List<BusinessAgents> mapOutBusinessAgents(final List<ModelUsuarios> usuarios) {
        if (usuarios == null) {
            return null;
        }
        return usuarios.stream().filter(Objects::nonNull).map(this::mapOutBusinessAgents).collect(Collectors.toList());
    }

    private BusinessAgents mapOutBusinessAgents(final ModelUsuarios usuario) {
        BusinessAgents businessAgents = new BusinessAgents();
        businessAgents.setId(usuario.getCodigo());
        businessAgents.setFullName(usuario.getNombre());
        if (usuario.getTipo() != null) {
            businessAgents.setAgentType(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.businessAgents.id", usuario.getTipo()));
        }
        return businessAgents;
    }

    private BusinessTeam mapOutBusinessTeam(final ModelEquipoResponsable equipoResponsable) {
        if (equipoResponsable == null) {
            return null;
        }
        BusinessTeam businessTeam = new BusinessTeam();
        businessTeam.setId(equipoResponsable.getCodigo());
        businessTeam.setName(equipoResponsable.getNombre());
        return businessTeam;
    }

    private List<Files> mapOutFiles(final List<ModelAdjunto> adjunto) {
        if (adjunto == null) {
            return null;
        }
        return adjunto.stream().filter(Objects::nonNull).map(this::mapOutFiles).collect(Collectors.toList());
    }

    private Files mapOutFiles(final ModelAdjunto modelAdjunto) {
        Files files = new Files();
        files.setId(modelAdjunto.getId());
        files.setName(modelAdjunto.getNombre());
        files.setGroup(modelAdjunto.getGrupo());
        return files;
    }

    private Channel mapOutChannel(final ModelCanal canal) {
        if (canal == null) {
            return null;
        }
        Channel channel = new Channel();
        channel.setId(canal.getCodigo());
        channel.setName(canal.getNombre());
        return channel;
    }

    private Situation mapOutSituation(final ModelSituacion modelSituacion) {
        if (modelSituacion == null) {
            return null;
        }
        Situation situation = new Situation();
        situation.setId(modelSituacion.getCodigo());
        situation.setName(modelSituacion.getNombre());
        return situation;
    }

    private StatusRefund mapOutStatus(final ModelEstado estado, final ModelEstadoAtencion estadoAtencion) {
        if (estado == null && estadoAtencion == null) {
            return null;
        }
        StatusRefund statusRefund = new StatusRefund();
        if (estado != null) {
            if (estado.getCodigo() != null) {
                statusRefund.setId(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.status.id", estado.getCodigo()));
            }
            statusRefund.setName(estado.getNombre());
        }
        statusRefund.setSubStatus(mapOutSubStatus(estadoAtencion));
        return statusRefund;
    }

    private SubStatus mapOutSubStatus(final ModelEstadoAtencion estadoAtencion) {
        if (estadoAtencion == null) {
            return null;
        }
        SubStatus status = new SubStatus();
        status.setId(estadoAtencion.getCodigo());
        status.setName(estadoAtencion.getNombre());
        return status;
    }

    private RelatedClaim mapOutRelatedClaim(final String numeroCaso) {
        if (numeroCaso == null) {
            return null;
        }
        RelatedClaim relatedClaim = new RelatedClaim();
        relatedClaim.setId(numeroCaso);
        return relatedClaim;
    }

    List<Operation> mapOutOperation(final List<ModelOperacion> inputOperaciones) {
        if (inputOperaciones == null) {
            return null;
        }
        final List<Operation> operations = new ArrayList<>();
        for (ModelOperacion operacion : inputOperaciones) {
            Operation operation = new Operation();
            if (operacion.getTipo() != null) {
                operation.setOperationType(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.operations.operationType", operacion.getTipo().getCodigo()));
                operation.setOperationTypeName(operacion.getTipo().getDescripcion());
            }
            operation.setBank(mapOutBank(operacion.getBanco(), operacion.getCentroContable()));
            operation.setExchangeRate(mapOutExchangeRate(operacion.getTipoCambio()));
            operation.setRelatedContract(mapOutRelatedContract(operacion.getProductoSeguro()));
            operation.setClaimReason(mapOutClaimReason(operacion));
            operation.setSubproductRefund(mapOutSubproductRefund(operacion.getContrato(), operacion.getModoAbono()));
            operation.setRequestedAmount(mapOutRequestAmount(operacion.getMoneda(), operacion.getImporte()));
            operations.add(operation);
        }
        return operations;
    }

    private ClaimReason mapOutClaimReason(final ModelOperacion operacion) {
        if (operacion == null) {
            return null;
        }
        ClaimReason claimReason = new ClaimReason();
        claimReason.setContract(mapOutContract(operacion));
        return claimReason;
    }

    private Contract mapOutContract(final ModelOperacion operacion) {
        if (operacion.getContratoOrigen() == null) {
            return null;
        }
        Contract contract = new Contract();
        String contractType = translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.contractType", operacion.getContratoOrigen().getTipo());
        if ("CARD".equals(contractType)) {
            contract = mapOutContractTypeCard(operacion);
        }
        if ("ACCOUNT".equals(contractType)) {
            contract = mapOutContractTypeAccount(operacion);

        }
        if ("LOAN".equals(contractType)) {
            contract = mapOutContractTypeLoan(operacion);
        }
        contract.setContractType(contractType);
        return contract;
    }

    private Contract mapOutContractTypeLoan(final ModelOperacion operacion) {
        Contract contract = new Contract();
        if (operacion.getContratoOrigen() != null) {
            contract.setId(operacion.getContratoOrigen().getNumero());
            contract.setNumber(operacion.getContratoOrigen().getNumero());
            contract.setNumberType(mapOutNumberType(operacion.getContratoOrigen().getNumeroTipo()));
            contract.setLoanType(mapOutLoanType(operacion.getContratoOrigen().getSubTipo()));
        }
        contract.setTransactions(mapOutTransactions(operacion.getMovimiento()));
        return contract;
    }

    private LoanType mapOutLoanType(final String subTipo) {
        if (subTipo == null) {
            return null;
        }
        LoanType loanType = new LoanType();
        loanType.setId(subTipo);
        return loanType;
    }

    private Contract mapOutContractTypeAccount(final ModelOperacion operacion) {
        Contract contract = new Contract();
        if (operacion.getContratoOrigen() != null) {
            contract.setId(operacion.getContratoOrigen().getNumero());
            contract.setNumber(operacion.getContratoOrigen().getNumero());
            contract.setNumberType(mapOutNumberType(operacion.getContratoOrigen().getNumeroTipo()));
            contract.setAccountType(mapOutAccountType(operacion.getContratoOrigen()));
        }
        contract.setTransactions(mapOutTransactions(operacion.getMovimiento()));
        return contract;
    }

    private AccountType mapOutAccountType(final ModelContratoOrigen contratoOrigen) {
        if (contratoOrigen == null) {
            return null;
        }
        AccountType accountType = new AccountType();
        accountType.setId(contratoOrigen.getSubTipo());
        return accountType;
    }

    private Contract mapOutContractTypeCard(final ModelOperacion operacion) {
        Contract contract = new Contract();
        if (operacion.getContratoOrigen().getTarjeta() != null) {
            contract.setId(operacion.getContratoOrigen().getTarjeta().getNumero());
            contract.setNumber(operacion.getContratoOrigen().getTarjeta().getNumero());
        }
        contract.setNumberType(mapOutNumberType(operacion.getContratoOrigen().getNumeroTipo()));
        contract.setTransactions(mapOutTransactions(operacion.getMovimiento()));
        contract.setCardAgreement(mapOutCardAgreement(operacion.getContratoOrigen()));
        contract.setSubProduct(mapOutSubproduct(operacion.getContratoOrigen()));
        return contract;
    }

    private SubProduct mapOutSubproduct(final ModelContratoOrigen contratoOrigen) {
        if (contratoOrigen == null) {
            return null;
        }
        SubProduct subProduct = new SubProduct();
        subProduct.setId(contratoOrigen.getSubTipo());
        return subProduct;
    }


    private CardAgreement mapOutCardAgreement(final ModelContratoOrigen contratoOrigen) {
        if (contratoOrigen == null) {
            return null;
        }
        CardAgreement cardAgreement = new CardAgreement();
        cardAgreement.setId(contratoOrigen.getNumero());
        return cardAgreement;
    }

    private List<Transaction> mapOutTransactions(final List<ModelMovimiento> movimiento) {
        if(CollectionUtils.isEmpty(movimiento)){
            return null;
        }
        return movimiento.stream().filter(Objects::nonNull).map(this::mapOutTransaction).collect(Collectors.toList());
    }

    private Transaction mapOutTransaction(final ModelMovimiento modelMovimiento) {
        Transaction transaction = new Transaction();
        transaction.setId(modelMovimiento.getNumero());
        transaction.setOperationDate(FunctionUtils.buildDate(modelMovimiento.getFecha()));
        transaction.setConcept(modelMovimiento.getNumero());
        transaction.setTransactionType(mapOutTransactionType(modelMovimiento.getTipo(), modelMovimiento.getTipoOperacion()));
        if (modelMovimiento.getMoneda() != null) {
            transaction.setLocalAmount(mapOutLocalAmount(modelMovimiento.getImporte(), modelMovimiento.getMoneda().getCodigo()));
        }
        transaction.setStore(mapOutStore(modelMovimiento.getEstablecimiento()));
        transaction.setStatus(mapOutStatus(modelMovimiento.getEstado()));
        return transaction;
    }

    private Status mapOutStatus(final ModelEstado estado) {
        if (estado == null) {
            return null;
        }
        Status status = new Status();
        status.setId(estado.getCodigo());
        status.setName(estado.getDescripcion());
        return status;
    }

    private Store mapOutStore(final ModelEstablecimiento establecimiento) {
        if (establecimiento == null) {
            return null;
        }
        Store store = new Store();
        store.setId(establecimiento.getCodigo());
        store.setName(establecimiento.getNombre());
        return store;
    }

    private LocalAmount mapOutLocalAmount(final BigDecimal importe, final String moneda) {
        if (importe == null && moneda == null) {
            return null;
        }
        LocalAmount localAmount = new LocalAmount();
        if (importe != null) {
            localAmount.setAmount(importe);
        }
        localAmount.setCurrency(moneda);
        return localAmount;

    }

    private TransactionType mapOutTransactionType(final String tipo, final ModelTipoOperacion tipoOperacion) {
        if (tipo == null && tipoOperacion == null) {
            return null;
        }
        TransactionType transactionType = new TransactionType();
        if (tipo != null) {
            transactionType.setId(translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.transactionType", tipo));
        }
        if (tipoOperacion != null) {
            transactionType.setName(tipoOperacion.getDescripcion());
        }
        return transactionType;
    }

    private NumberType mapOutNumberType(final String numeroTipo) {
        if (numeroTipo == null) {
            return null;
        }
        NumberType numberType = new NumberType();
        numberType.setId(translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.numberType", numeroTipo));
        return numberType;
    }

    private RelatedContract mapOutRelatedContract(final ModelProductoSeguro productoSeguro) {
        if (productoSeguro == null) {
            return null;
        }
        RelatedContract relatedContract = new RelatedContract();
        relatedContract.setId(productoSeguro.getCodigo());
        relatedContract.setRequestedAmount(mapOutRequestAmount(productoSeguro));
        relatedContract.setProduct(mapOutProduct(productoSeguro));
        return relatedContract;
    }

    private Product mapOutProduct(final ModelProductoSeguro productoSeguro) {
        if (productoSeguro == null) {
            return null;
        }
        Product product = new Product();
        product.setId(productoSeguro.getId());
        product.setName(productoSeguro.getProducto());
        return product;
    }

    private RequestedAmount mapOutRequestAmount(final ModelProductoSeguro productoSeguro) {
        RequestedAmount requestedAmount = new RequestedAmount();
        requestedAmount.setAmount(productoSeguro.getImporte());
        if (productoSeguro.getMoneda() != null) {
            requestedAmount.setCurrency(productoSeguro.getMoneda().getCodigo());
        }
        return requestedAmount;
    }

    private ExchangeRate mapOutExchangeRate(final ModelTipoCambio tipoCambio) {
        if (tipoCambio == null) {
            return null;
        }
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setValues(mapOutValues(tipoCambio));
        return exchangeRate;
    }

    private Values mapOutValues(final ModelTipoCambio tipoCambio) {
        Values values = new Values();
        values.setFactor(mapOutFactor(tipoCambio));
        if (tipoCambio.getTipo() != null) {
            values.setPriceType(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.operations.exchangeRate", tipoCambio.getTipo()));
        }
        return values;
    }

    private Factor mapOutFactor(final ModelTipoCambio tipoCambio) {
        Factor factor = new Factor();
        factor.setValue(tipoCambio.getValor());
        return factor;
    }

    private Bank mapOutBank(final ModelBanco banco, final ModelCentroContable centroContable) {
        if (banco == null) {
            return null;
        }
        Bank bank = new Bank();
        bank.setId(banco.getId());
        bank.setName(banco.getDescripcion());
        bank.setBranch(mapOutBranch(centroContable));
        return bank;
    }

    private Branch mapOutBranch(final ModelCentroContable centroContable) {
        if (centroContable == null) {
            return null;
        }
        Branch branch = new Branch();
        branch.setId(centroContable.getCodigo());
        branch.setName(centroContable.getDescripcion());
        return branch;
    }

    private RequestedAmount mapOutRequestAmount(final ModelMoneda moneda, final BigDecimal importe) {
        if (moneda == null && importe == null) {
            return null;
        }
        RequestedAmount requestedAmount = new RequestedAmount();
        if (moneda != null) {
            requestedAmount.setCurrency(moneda.getCodigo());
        }
        requestedAmount.setAmount(importe);
        return requestedAmount;
    }

    private SubproductRefund mapOutSubproductRefund(final ModelContrato contrato, final ModelModoAbono abono) {
        if (contrato == null) {
            return null;
        }
        SubproductRefund subproductRefund = new SubproductRefund();
        subproductRefund.setId(contrato.getAliasProducto());
        if (abono != null && abono.getCodigo() != null) {
            subproductRefund.setRefundMode(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.operations.subProductRefundMode", abono.getCodigo()));
        }
        if (abono != null && abono.getDescripcion() != null) {
            subproductRefund.setRefundModeDescription(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.operations.subProductRefundMode", abono.getDescripcion()));
        }
        if ("MPTAR".equals(subproductRefund.getId())) {
            subproductRefund.setNumber(contrato.getTarjeta());
        } else {
            subproductRefund.setNumber(contrato.getNumero());
        }
        return subproductRefund;
    }

    Petitioner mapOutPetitioner(final ModelTitular inputTitural, final List<ModelContacto> contactos) {
        if (inputTitural == null) {
            return null;
        }
        Petitioner petitioner = new Petitioner();
        petitioner.setIdentityDocument(mapOutIndentityDocument(inputTitural.getTipoDoi(), inputTitural.getDoi()));
        petitioner.setFirstName(inputTitural.getNombres());
        petitioner.setLastName(inputTitural.getApellidoPaterno());
        petitioner.setSecondLastName(inputTitural.getApellidoMaterno());
        petitioner.setSegment(mapOutSegment(inputTitural.getSegmento()));
        petitioner.setContactDetails(mapOutContractDetails(contactos));
        return petitioner;
    }

    private List<ContactDetail> mapOutContractDetails(final List<ModelContacto> contactos) {
        if (contactos == null) {
            return null;
        }
        return contactos.stream().filter(Objects::nonNull).map(this::mapOutContractDetail).collect(Collectors.toList());
    }

    private ContactDetail mapOutContractDetail(final ModelContacto modelContacto) {
        ContactDetail contactDetail = new ContactDetail();
        contactDetail.setId(modelContacto.getId());
        contactDetail.setContact(mapOutContact(modelContacto));
        return contactDetail;
    }

    private BaseContract mapOutContact(final ModelContacto modelContacto) {
        if (modelContacto == null) {
            return null;
        }
        BaseContract baseContract = new BaseContract();
        if (modelContacto.getTipoContacto() != null) {
            baseContract.setContactDetailType(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType", modelContacto.getTipoContacto()));
        }

        if ("LANDLINE".equals(baseContract.getContactDetailType())) {
            baseContract.setNumber(modelContacto.getNumero());
            if (modelContacto.getTipoFijo() != null) {
                baseContract.setPhoneType(translator.translateBackendEnumValueStrictly("claims.petitioner.contact.phoneType", modelContacto.getTipoFijo()));
            }
            baseContract.setCountry(mapOutCountry(modelContacto.getPaisFijo()));
            baseContract.setCountryCode(modelContacto.getCodigoPaisFijo());
            baseContract.setRegionalCode(modelContacto.getCodigoRegionalFijo());
            baseContract.setExtension(modelContacto.getAnexoFijo());
        }
        if ("MOBILE".equals(baseContract.getContactDetailType())) {
            baseContract.setNumber(modelContacto.getNumero());
            baseContract.setPhoneCompany(mapOutPhoneCompany(modelContacto.getOperadorCelular()));
            baseContract.setCountryCode(modelContacto.getCodigoPaisCelular());
        }
        if ("EMAIL".equals(baseContract.getContactDetailType())) {
            baseContract.setAddress(modelContacto.getCorreo());
            baseContract.setReceivesNotifications(FunctionUtils.convertToBoolean(modelContacto.getNotificacionesCorreo()));
        }
        return baseContract;
    }

    private PhoneCompany mapOutPhoneCompany(final ModelOperadorCelular operadorCelular) {
        if (operadorCelular == null) {
            return null;
        }
        PhoneCompany phoneCompany = new PhoneCompany();
        phoneCompany.setId(operadorCelular.getCodigo());
        phoneCompany.setName(operadorCelular.getDescripcion());
        return phoneCompany;
    }

    private Country mapOutCountry(final String paisFijo) {
        if (paisFijo == null) {
            return null;
        }
        Country country = new Country();
        country.setId(paisFijo);
        return country;
    }

    private Segment mapOutSegment(final ModelSegmento segmento) {
        if (segmento == null) {
            return null;
        }
        Segment segment = new Segment();
        segment.setId(segmento.getCodigo());
        segment.setName(segmento.getDescripcion());
        return segment;
    }

    private IdentityDocument mapOutIndentityDocument(final String tipoDoi, final String doi) {
        if (tipoDoi == null && doi == null) {
            return null;
        }
        IdentityDocument document = new IdentityDocument();
        document.setDocumentType(mapOutDocumentType(tipoDoi));
        document.setDocumentNumber(doi);
        return document;
    }

    private DocumentType mapOutDocumentType(final String tipoDoi) {
        if (tipoDoi == null) {
            return null;
        }
        DocumentType documentType = new DocumentType();
        documentType.setId(tipoDoi);
        return documentType;
    }
}