package com.bbva.pzic.claims.dao.rest.model.conformidad;

import java.util.List;

public class Error {

    private List<ErrorMessage> messages;

    public List<ErrorMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ErrorMessage> messages) {
        this.messages = messages;
    }
}
