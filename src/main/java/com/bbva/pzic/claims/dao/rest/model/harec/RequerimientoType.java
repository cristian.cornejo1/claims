package com.bbva.pzic.claims.dao.rest.model.harec;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * <p>Clase Java para requerimientoType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="requerimientoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requerimientoType", propOrder = {
        "numero"
})
public class RequerimientoType {

    protected List<RequerimientosType> req;
    @XmlElement(required = true)
    protected String numero;
    @XmlElement(required = true)
    protected String tipoPersona;
    @XmlElement(required = true)
    protected TipoType tipo;
    @XmlElement(required = true)
    protected TitularType titular;
    @XmlElement(required = true)
    protected RepresentanteType representante;
    @XmlElement(required = true)
    protected ProductoType producto;
    @XmlElement(required = true)
    protected ObjectType motivo;
    @XmlElement(required = true)
    protected DictamenType dictamen;
    @XmlElement(required = true)
    protected String resumen;
    @XmlElement(required = true)
    protected Calendar fechaRegistro;
    @XmlElement(required = true)
    protected EstadoType estado;
    protected SubmotivoType submotivo;
    protected List<SeguimientosType> seguimiento;
    protected List<AdjuntosType> adjunto;
    protected CartaRespuestaType cartaRespuesta;
    protected String divisaReclamado;
    protected BigDecimal importeReclamado;
    protected List<ImporteOperacionalType> importeOperacional;
    protected List<ImporteDevueltoType> importeDevuelto;
    protected InterposicionType interposicion;
    protected EntregaHRCSType entregaHRCS;
    @DatoAuditable(omitir = true)
    protected String contrato;
    protected List<TaxonomiaAdicionalType> taxonomiasAdicionales;
    protected List<ContactoType> contactos;
    protected TaxonomiaType taxonomia;
    protected String pedido;
    protected String esUrgente;
    protected TipoType ubigeo;
    protected Calendar fechaPresentacion;
    protected Calendar fechaCierre;
    protected TipoType categoria;
    protected String detalleComplementario;
    protected String appOrigenRegistro;
    protected String numeroCasoReiterativo;
    protected String indicadorFCR;
    protected String sustento;

    /**
     * Gets the value of the req property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the req property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReq().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequerimientosType }
     */
    public List<RequerimientosType> getReq() {
        if (req == null) {
            req = new ArrayList<>();
        }
        return this.req;
    }

    /**
     * Obtiene el valor de la propiedad numero.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Define el valor de la propiedad numero.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPersona.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Define el valor de la propiedad tipoPersona.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipoPersona(String value) {
        this.tipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad tipo.
     *
     * @return possible object is
     * {@link TipoType }
     */
    public TipoType getTipo() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     *
     * @param value allowed object is
     *              {@link TipoType }
     */
    public void setTipo(TipoType value) {
        this.tipo = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     *
     * @return possible object is
     * {@link TitularType }
     */
    public TitularType getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     *
     * @param value allowed object is
     *              {@link TitularType }
     */
    public void setTitular(TitularType value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad representante.
     *
     * @return possible object is
     * {@link RepresentanteType }
     */
    public RepresentanteType getRepresentante() {
        return representante;
    }

    /**
     * Define el valor de la propiedad representante.
     *
     * @param value allowed object is
     *              {@link RepresentanteType }
     */
    public void setRepresentante(RepresentanteType value) {
        this.representante = value;
    }

    /**
     * Obtiene el valor de la propiedad producto.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ProductoType getProducto() {
        return producto;
    }

    /**
     * Define el valor de la propiedad producto.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setProducto(ProductoType value) {
        this.producto = value;
    }

    /**
     * Obtiene el valor de la propiedad motivo.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ObjectType getMotivo() {
        return motivo;
    }

    /**
     * Define el valor de la propiedad motivo.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setMotivo(ObjectType value) {
        this.motivo = value;
    }

    /**
     * Obtiene el valor de la propiedad dictamen.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public DictamenType getDictamen() {
        return dictamen;
    }

    /**
     * Define el valor de la propiedad dictamen.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setDictamen(DictamenType value) {
        this.dictamen = value;
    }

    /**
     * Obtiene el valor de la propiedad resumen.
     *
     * @return possible object is
     * {@link String }
     */
    public String getResumen() {
        return resumen;
    }

    /**
     * Define el valor de la propiedad resumen.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setResumen(String value) {
        this.resumen = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRegistro.
     *
     * @return possible object is
     * {@link String }
     */
    public Calendar getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     * Define el valor de la propiedad fechaRegistro.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFechaRegistro(Calendar value) {
        this.fechaRegistro = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public EstadoType getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setEstado(EstadoType value) {
        this.estado = value;
    }

    /**
     * Gets the value of the seguimiento property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seguimiento property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeguimiento().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SeguimientosType }
     */
    public List<SeguimientosType> getSeguimiento() {
        if (seguimiento == null) {
            seguimiento = new ArrayList<>();
        }
        return this.seguimiento;
    }

    public void setSeguimiento(List<SeguimientosType> value) {
        this.seguimiento = value;
    }

    /**
     * Gets the value of the adjunto property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjunto property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjunto().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdjuntosType }
     */
    public List<AdjuntosType> getAdjunto() {
        if (adjunto == null) {
            adjunto = new ArrayList<>();
        }
        return this.adjunto;
    }

    public void setAdjunto(List<AdjuntosType> value) {
        this.adjunto = value;
    }

    /**
     * Obtiene el valor de la propiedad submotivo.
     *
     * @return possible object is
     * {@link SubmotivoType }
     */
    public SubmotivoType getSubmotivo() {
        return submotivo;
    }

    /**
     * Define el valor de la propiedad submotivo.
     *
     * @param value allowed object is
     *              {@link SubmotivoType }
     */
    public void setSubmotivo(SubmotivoType value) {
        this.submotivo = value;
    }

    /**
     * Obtiene el valor de la propiedad cartaRespuesta.
     *
     * @return possible object is
     * {@link CartaRespuestaType }
     */
    public CartaRespuestaType getCartaRespuesta() {
        return cartaRespuesta;
    }

    /**
     * Define el valor de la propiedad cartaRespuesta.
     *
     * @param value allowed object is
     *              {@link CartaRespuestaType }
     */
    public void setCartaRespuesta(CartaRespuestaType value) {
        this.cartaRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad divisaReclamado.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDivisaReclamado() {
        return divisaReclamado;
    }

    /**
     * Define el valor de la propiedad divisaReclamado.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDivisaReclamado(String value) {
        this.divisaReclamado = value;
    }

    /**
     * Obtiene el valor de la propiedad importeReclamado.
     *
     * @return possible object is
     * {@link String }
     */
    public BigDecimal getImporteReclamado() {
        return importeReclamado;
    }

    /**
     * Define el valor de la propiedad importeReclamado.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setImporteReclamado(BigDecimal value) {
        this.importeReclamado = value;
    }

    /**
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImporteOperacionalType }
     */
    public List<ImporteOperacionalType> getImporteOperacional() {
        if (importeOperacional == null) {
            importeOperacional = new ArrayList<>();
        }
        return this.importeOperacional;
    }

    /**
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImporteDevueltoType }
     */
    public List<ImporteDevueltoType> getImporteDevuelto() {
        if (importeDevuelto == null) {
            importeDevuelto = new ArrayList<>();
        }
        return this.importeDevuelto;
    }

    /**
     * Obtiene el valor de la propiedad cartaRespuesta.
     *
     * @return possible object is
     * {@link InterposicionType }
     */
    public InterposicionType getInterposicion() {
        return interposicion;
    }

    /**
     * Define el valor de la propiedad cartaRespuesta.
     *
     * @param value allowed object is
     *              {@link InterposicionType }
     */
    public void setInterposicion(InterposicionType value) {
        this.interposicion = value;
    }

    /**
     * Obtiene el valor de la propiedad entregaHRCS.
     *
     * @return possible object is
     * {@link EntregaHRCSType }
     */
    public EntregaHRCSType getEntregaHRCS() {
        return entregaHRCS;
    }

    /**
     * Define el valor de la propiedad entregaHRCS.
     *
     * @param value allowed object is
     *              {@link EntregaHRCSType }
     */
    public void setEntregaHRCS(EntregaHRCSType value) {
        this.entregaHRCS = value;
    }

    /**
     * Obtiene el valor de la propiedad contrato.
     *
     * @return possible object is
     * {@link String }
     */
    public String getContrato() {
        return contrato;
    }

    /**
     * Define el valor de la propiedad contrato.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setContrato(String value) {
        this.contrato = value;
    }

    public TaxonomiaType getTaxonomia() {
        return taxonomia;
    }

    public void setTaxonomia(TaxonomiaType taxonomia) {
        this.taxonomia = taxonomia;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public String getEsUrgente() {
        return esUrgente;
    }

    public void setEsUrgente(String esUrgente) {
        this.esUrgente = esUrgente;
    }

    public TipoType getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(TipoType ubigeo) {
        this.ubigeo = ubigeo;
    }

    public Calendar getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(Calendar fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public Calendar getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Calendar fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public TipoType getCategoria() {
        return categoria;
    }

    public void setCategoria(TipoType categoria) {
        this.categoria = categoria;
    }

    public String getDetalleComplementario() {
        return detalleComplementario;
    }

    public void setDetalleComplementario(String detalleComplementario) {
        this.detalleComplementario = detalleComplementario;
    }

    public String getAppOrigenRegistro() {
        return appOrigenRegistro;
    }

    public void setAppOrigenRegistro(String appOrigenRegistro) {
        this.appOrigenRegistro = appOrigenRegistro;
    }

    public String getNumeroCasoReiterativo() {
        return numeroCasoReiterativo;
    }

    public void setNumeroCasoReiterativo(String numeroCasoReiterativo) {
        this.numeroCasoReiterativo = numeroCasoReiterativo;
    }

    public String getIndicadorFCR() {
        return indicadorFCR;
    }

    public void setIndicadorFCR(String indicadorFCR) {
        this.indicadorFCR = indicadorFCR;
    }

    public List<TaxonomiaAdicionalType> getTaxonomiasAdicionales() {
        if(taxonomiasAdicionales == null){
            taxonomiasAdicionales = new ArrayList<>();
        }
        return taxonomiasAdicionales;
    }

    public List<ContactoType> getContactos() {
        if(contactos == null){
            contactos = new ArrayList<>();
        }
        return contactos;
    }

    public String getSustento() {
        return sustento;
    }

    public void setSustento(String sustento) {
        this.sustento = sustento;
    }
}
