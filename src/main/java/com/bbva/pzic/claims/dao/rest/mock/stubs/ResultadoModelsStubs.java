package com.bbva.pzic.claims.dao.rest.mock.stubs;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponse;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponses;
import com.bbva.pzic.claims.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public enum ResultadoModelsStubs {

    INSTANCE;

    private ObjectMapperHelper mapper;

    ResultadoModelsStubs() {
        this.mapper = ObjectMapperHelper.getInstance();
    }

    public ModelResultadoResponses getResultadoModelResponses() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/claims/dao/rest/mock/resultadoModels.json"), ModelResultadoResponses.class);
    }

    public ModelResultadoResponse getResultadoModelResponse() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/validateAutomaticRefundTransaction/modelResponse.json"), ModelResultadoResponse.class);
    }

    public ResponseType getResponseType() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/claims/dao/rest/mock/responseType.json"), ResponseType.class);
    }

    public Message getFatalMessage() throws IOException {
        final List<Message> messages = getMessages();
        return messages.get(0);
    }

    public Message getErrorMessage() throws IOException {
        final List<Message> messages = getMessages();
        return messages.get(1);
    }

    private List<Message> getMessages() throws IOException {
        return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/claims/dao/rest/mock/messages.json"), new TypeReference<List<Message>>() {
        });
    }
}
