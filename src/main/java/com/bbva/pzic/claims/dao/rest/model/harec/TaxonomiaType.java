package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Calendar;


/**
 * <p>Clase Java para taxonomiaType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="taxonomiaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tipo" type="{/harec-rest-aso/registro/requerimiento}objectType"/&gt;
 *         &lt;element name="producto" type="{/harec-rest-aso/registro/requerimiento}objectType"/&gt;
 *         &lt;element name="motivo" type="{/harec-rest-aso/registro/requerimiento}objectType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taxonomiaType", propOrder = {
        "tipo",
        "producto",
        "motivo"
})
public class TaxonomiaType {

    @XmlElement(required = true)
    protected ObjectType tipo;
    @XmlElement(required = true)
    protected ProductoType producto;
    @XmlElement(required = true)
    protected ObjectType motivo;
    @XmlElement(required = true)
    protected ObjectType submotivo;

    private String cajero;
    private BancoOriginante bancoOriginante;
    private OficinaOriginante oficinaOriginante;
    protected TipoType agrupacion;
    private Calendar fechaOperacion;

    public String getCajero() {
        return cajero;
    }

    public void setCajero(String cajero) {
        this.cajero = cajero;
    }

    public BancoOriginante getBancoOriginante() {
        return bancoOriginante;
    }

    public void setBancoOriginante(BancoOriginante bancoOriginante) {
        this.bancoOriginante = bancoOriginante;
    }

    public OficinaOriginante getOficinaOriginante() {
        return oficinaOriginante;
    }

    public void setOficinaOriginante(OficinaOriginante oficinaOriginante) {
        this.oficinaOriginante = oficinaOriginante;
    }

    /**
     * Obtiene el valor de la propiedad tipo.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ObjectType getTipo() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setTipo(ObjectType value) {
        this.tipo = value;
    }

    /**
     * Obtiene el valor de la propiedad producto.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ProductoType getProducto() {
        return producto;
    }

    /**
     * Define el valor de la propiedad producto.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setProducto(ProductoType value) {
        this.producto = value;
    }

    /**
     * Obtiene el valor de la propiedad motivo.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ObjectType getMotivo() {
        return motivo;
    }

    /**
     * Define el valor de la propiedad motivo.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setMotivo(ObjectType value) {
        this.motivo = value;
    }

    /**
     * Obtiene el valor de la propiedad motivo.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ObjectType getSubmotivo() {
        return submotivo;
    }

    /**
     * Define el valor de la propiedad motivo.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setSubmotivo(ObjectType value) {
        this.submotivo = value;
    }


    public TipoType getAgrupacion() {
        return agrupacion;
    }

    public void setAgrupacion(TipoType agrupacion) {
        this.agrupacion = agrupacion;
    }

    public Calendar getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Calendar fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }
}