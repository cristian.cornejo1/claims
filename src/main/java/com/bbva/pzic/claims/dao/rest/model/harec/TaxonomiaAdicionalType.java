package com.bbva.pzic.claims.dao.rest.model.harec;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Created on 03/02/2021.
 *
 * @author Entelgy.
 */
public class TaxonomiaAdicionalType {

    protected TipoType tipo;
    protected ProductoType producto;
    protected ObjectType motivo;
    protected ObjectType submotivo;
    protected BigDecimal importeReclamado;
    protected String divisa;
    protected String cajero;
    protected BancoOriginante bancoOriginante;
    protected OficinaOriginante oficinaOriginante;
    private Calendar fechaOperacion;

    public TipoType getTipo() {
        return tipo;
    }

    public void setTipo(TipoType tipo) {
        this.tipo = tipo;
    }

    public ProductoType getProducto() {
        return producto;
    }

    public void setProducto(ProductoType producto) {
        this.producto = producto;
    }

    public ObjectType getMotivo() {
        return motivo;
    }

    public void setMotivo(ObjectType motivo) {
        this.motivo = motivo;
    }

    public ObjectType getSubmotivo() {
        return submotivo;
    }

    public void setSubmotivo(ObjectType submotivo) {
        this.submotivo = submotivo;
    }

    public BigDecimal getImporteReclamado() {
        return importeReclamado;
    }

    public void setImporteReclamado(BigDecimal importeReclamado) {
        this.importeReclamado = importeReclamado;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getCajero() {
        return cajero;
    }

    public void setCajero(String cajero) {
        this.cajero = cajero;
    }

    public BancoOriginante getBancoOriginante() {
        return bancoOriginante;
    }

    public void setBancoOriginante(BancoOriginante bancoOriginante) {
        this.bancoOriginante = bancoOriginante;
    }

    public OficinaOriginante getOficinaOriginante() {
        return oficinaOriginante;
    }

    public void setOficinaOriginante(OficinaOriginante oficinaOriginante) {
        this.oficinaOriginante = oficinaOriginante;
    }

    public Calendar getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Calendar fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }
}
