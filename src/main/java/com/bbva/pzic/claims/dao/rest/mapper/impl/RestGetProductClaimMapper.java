package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.business.dto.InputGetProductClaim;
import com.bbva.pzic.claims.canonic.*;
import com.bbva.pzic.claims.dao.rest.model.harec.*;
import com.bbva.pzic.claims.dao.rest.mapper.IRestGetProductClaimMapper;
import com.bbva.pzic.claims.dao.rest.mapper.common.RestProductClaimMapper;
import com.bbva.pzic.claims.facade.v0.dto.ContactDetail;
import com.bbva.pzic.claims.facade.v0.dto.Delivery;
import com.bbva.pzic.claims.facade.v0.dto.DeliveryType;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.claims.util.mappers.Mapper;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.bbva.pzic.claims.util.Enums.CLAIMS_DELIVERY_DELIVERYTYPE;

/**
 * Created on 28/11/2019.
 *
 * @author Entelgy
 */
@Mapper
public class RestGetProductClaimMapper implements IRestGetProductClaimMapper {

    private static final Log LOG = LogFactory.getLog(RestGetProductClaimMapper.class);

    private RestProductClaimMapper restProductClaimMapper;
    private Translator translator;

    @Autowired
    public void setRestProductClaimMapper(RestProductClaimMapper restProductClaimMapper) {
        this.restProductClaimMapper = restProductClaimMapper;
    }

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public RequestType mapInBody(final InputGetProductClaim input) {
        LOG.info("... called method RestGetProductClaimMapper.mapInBody ...");
        RequestType requestType = new RequestType();
        requestType.setNumeroRequerimiento(input.getProductClaimId());
        return requestType;
    }

    @Override
    public ProductClaims mapOut(final ResponseType responseType) {
        LOG.info("... called method RestGetProductClaimMapper.mapOut ...");
        if (responseType == null || StringUtils.isEmpty(responseType.getNumeroAtencionLinea()) &&
                responseType.getRequerimiento() == null) {
            return null;
        }

        ProductClaims result = new ProductClaims();
        result.setPreviousAttentionCode(responseType.getNumeroAtencionLinea());
        if (responseType.getRequerimiento() == null) {
            return result;
        }

        result.setId(responseType.getRequerimiento().getNumero());
        result.setNumber(responseType.getRequerimiento().getNumero());
        result.setCreationDate(responseType.getRequerimiento().getFechaRegistro() == null ? null : DateUtils.rebuildDateTime(responseType.getRequerimiento().getFechaRegistro()));
        result.setPersonType(mapOutPersonType(responseType.getRequerimiento().getTipoPersona()));
        result.setClaimType(mapOutClaimType(responseType.getRequerimiento().getTipo()));
        result.setProduct(mapOutProduct(responseType.getRequerimiento().getProducto()));
        result.setReason(mapOutReason(responseType.getRequerimiento().getResumen(), responseType.getRequerimiento().getMotivo(), responseType.getRequerimiento().getSubmotivo()));
        result.setResolution(mapOutResolution(responseType.getRequerimiento().getDictamen(), responseType.getRequerimiento().getCartaRespuesta(), responseType.getRequerimiento().getImporteDevuelto(), responseType.getRequerimiento().getImporteOperacional()));
        result.setStatus(mapOutStatus(responseType.getRequerimiento().getEstado()));
        result.setDocuments(mapOutDocuments(responseType.getRequerimiento().getAdjunto()));
        result.setContract(mapOutContractProduct(responseType.getRequerimiento().getProducto()));
        result.setClaimAmount(mapOutClaimAmount(responseType.getRequerimiento().getImporteReclamado(), responseType.getRequerimiento().getDivisaReclamado()));
        result.setPersonType(restProductClaimMapper.mapOutPersonTypeEnum(result.getPersonType()));
        result.setDelivery(mapOutDelivery(responseType.getRequerimiento().getEntregaHRCS()));
        result.setChannel(mapOutChannel(responseType.getRequerimiento().getInterposicion()));
        result.setGeolocation(mapOutGeolocation(responseType.getRequerimiento().getUbigeo()));
        if (responseType.getRequerimiento().getTaxonomia() != null) {
            result.setAtm(mapOutAtm(responseType.getRequerimiento().getTaxonomia().getCajero()));
            result.setBank(mapOutBank(responseType.getRequerimiento().getTaxonomia().getBancoOriginante(),
                    responseType.getRequerimiento().getTaxonomia().getOficinaOriginante()));
        }

        result.setPetition(mapOutPetition(responseType.getRequerimiento().getPedido()));
        result.setPriority(mapOutPriority(responseType.getRequerimiento().getEsUrgente(), responseType.getRequerimiento().getSustento()));
        result.setEnrollmentDate(responseType.getRequerimiento().getFechaPresentacion() == null ? null : DateUtils.rebuildDateTime(responseType.getRequerimiento().getFechaPresentacion()));
        result.setEndDate(responseType.getRequerimiento().getFechaCierre() == null ? null : DateUtils.rebuildDateTime(responseType.getRequerimiento().getFechaCierre()));
        result.setCategory(mapOutCategory(responseType.getRequerimiento().getCategoria(), responseType.getRequerimiento().getTaxonomia()));
        result.setAdditionalInformation(responseType.getRequerimiento().getDetalleComplementario());
        result.setOriginApplication(mapOutOriginApplication(responseType.getRequerimiento().getAppOrigenRegistro()));
        result.setPredecessorClaim(mapOutPredecessorClaim(responseType.getRequerimiento().getNumeroCasoReiterativo()));
        result.setIsOnlineRefundFlow(responseType.getRequerimiento().getIndicadorFCR() == null ? null : FunctionUtils.convertToBoolean(responseType.getRequerimiento().getIndicadorFCR()).toString());
        result.setAdditionalClaims(mapOutAdditionalClaims(responseType.getRequerimiento().getTaxonomiasAdicionales()));
        result.setIssueDate(responseType.getRequerimiento().getTaxonomia().getFechaOperacion() == null ? null :
                DateUtils.rebuildDateTime(responseType.getRequerimiento().getTaxonomia().getFechaOperacion()));

        result.setPetitioners(mapOutPetitioners(responseType));
        result.setStatus(restProductClaimMapper.mapOutStatusEnum(result.getStatus()));
        result.setTracking(mapOutTrackings(responseType.getRequerimiento().getSeguimiento()));
        return result;
    }

    private List<AdditionalClaim> mapOutAdditionalClaims(final List<TaxonomiaAdicionalType> taxonomiasAdicionales) {
        if (CollectionUtils.isEmpty(taxonomiasAdicionales)) {
            return null;
        }

        return taxonomiasAdicionales.stream().filter(Objects::nonNull).map(this::mapOutAdditionalClaim).collect(Collectors.toList());
    }

    private AdditionalClaim mapOutAdditionalClaim(final TaxonomiaAdicionalType taxonomiaAdicionalType) {
        AdditionalClaim result = new AdditionalClaim();
        result.setClaimType(mapOutClaimType(taxonomiaAdicionalType.getTipo()));
        result.setProduct(mapOutClaimProduct(taxonomiaAdicionalType.getProducto()));
        result.setReason(mapOutClaimReason(taxonomiaAdicionalType.getMotivo(), taxonomiaAdicionalType.getSubmotivo()));
        result.setContract(mapOutContractProducts(taxonomiaAdicionalType.getProducto()));
        result.setClaimAmount(mapOutClaimAmount(taxonomiaAdicionalType.getImporteReclamado(), taxonomiaAdicionalType.getDivisa()));
        result.setAtm(mapOutAtm(taxonomiaAdicionalType.getCajero()));
        result.setBank(mapOutBank(taxonomiaAdicionalType.getBancoOriginante(), taxonomiaAdicionalType.getOficinaOriginante()));
        result.setIssueDate(taxonomiaAdicionalType.getFechaOperacion() == null ? null :
                DateUtils.rebuildDateTime(taxonomiaAdicionalType.getFechaOperacion()));
        return result;
    }

    private Reason mapOutClaimReason(final ObjectType motivo, final ObjectType submotivo) {
        if (motivo == null && submotivo == null) {
            return null;
        }

        Reason result = new Reason();
        result.setSubReason(mapOutSubReason(submotivo));
        if (motivo != null) {
            result.setId(motivo.getCodigo());
            result.setName(motivo.getNombre());
        }
        return result;
    }

    private SubReason mapOutSubReason(final ObjectType submotivo) {
        if (submotivo == null) {
            return null;
        }

        SubReason result = new SubReason();
        result.setId(submotivo.getCodigo());
        result.setDescription(submotivo.getNombre());
        return result;
    }

    private ContractProduct mapOutClaimProduct(final ProductoType producto) {
        if (producto == null) {
            return null;
        }

        ContractProduct result = new ContractProduct();
        result.setId(producto.getCodigo());
        result.setName(producto.getNombre());
        return result;
    }

    private PredecessorClaimSearch mapOutPredecessorClaim(final String numeroCasoReiterativo) {
        if (StringUtils.isEmpty(numeroCasoReiterativo)) {
            return null;
        }

        PredecessorClaimSearch result = new PredecessorClaimSearch();
        result.setId(numeroCasoReiterativo);
        return result;
    }

    private OriginApplication mapOutOriginApplication(final String appOrigenRegistro) {
        if (StringUtils.isEmpty(appOrigenRegistro)) {
            return null;
        }

        OriginApplication result = new OriginApplication();
        result.setId(appOrigenRegistro);
        return result;
    }

    private CategorySearch mapOutCategory(final TipoType categoria, final TaxonomiaType taxonomia) {
        if (categoria == null && taxonomia == null) {
            return null;
        }

        CategorySearch result = new CategorySearch();
        result.setSubCategory(mapOutSubCategory(taxonomia));
        if (categoria != null) {
            result.setId(translator.translateBackendEnumValueStrictly("claims.category.id", categoria.getCodigo()));
            result.setName(categoria.getDescripcion());
        }

        return result;
    }

    private SubCategory mapOutSubCategory(TaxonomiaType taxonomia) {
        if (taxonomia == null || taxonomia.getAgrupacion() == null) {
            return null;
        }

        SubCategory result = new SubCategory();
        result.setId(taxonomia.getAgrupacion().getCodigo());
        result.setName(taxonomia.getAgrupacion().getDescripcion());
        return result;
    }

    private Priority mapOutPriority(final String esUrgente, final String sustento) {
        if (StringUtils.isEmpty(esUrgente) && StringUtils.isEmpty(sustento)) {
            return null;
        }

        Priority result = new Priority();
        result.setIsUrgent(FunctionUtils.convertToBoolean(esUrgente));
        result.setBasis(sustento);
        return result;
    }

    private Petition mapOutPetition(final String pedido) {
        if (StringUtils.isEmpty(pedido)) {
            return null;
        }

        Petition result = new Petition();
        result.setDescription(pedido);
        return result;
    }

    private Bank mapOutBank(final BancoOriginante bancoOriginante, final OficinaOriginante oficinaOriginante) {
        if (bancoOriginante == null && oficinaOriginante == null) {
            return null;
        }

        Bank result = new Bank();
        if (bancoOriginante != null) {
            result.setId(bancoOriginante.getCodigo());
            result.setName(bancoOriginante.getDescripcion());
        }

        if (oficinaOriginante != null) {
            result.setBranch(new Branch());
            result.getBranch().setId(oficinaOriginante.getCodigo());
            result.getBranch().setName(oficinaOriginante.getDescripcion());
        }

        return result;
    }

    private Atm mapOutAtm(final String cajero) {
        if (StringUtils.isEmpty(cajero)) {
            return null;
        }

        Atm result = new Atm();
        result.setId(cajero);
        return result;
    }

    private Geolocation mapOutGeolocation(final TipoType ubigeo) {
        if (ubigeo == null) {
            return null;
        }

        Geolocation result = new Geolocation();
        result.setCode(ubigeo.getCodigo());
        result.setDescription(ubigeo.getDescripcion());
        return result;
    }

    private Channel mapOutChannel(final InterposicionType interposicion) {
        if (interposicion == null) {
            return null;
        }

        Channel result = new Channel();
        result.setId(interposicion.getCodigo());
        result.setDescription(interposicion.getNombre());
        return result;
    }

    private Delivery mapOutDelivery(final EntregaHRCSType entregaHRCS) {
        if (entregaHRCS == null) {
            return null;
        }

        Delivery result = new Delivery();
        result.setDeliveryType(new DeliveryType());
        result.getDeliveryType().setId(translator.translateBackendEnumValueStrictly(CLAIMS_DELIVERY_DELIVERYTYPE, entregaHRCS.getCodigo()));
        result.getDeliveryType().setDescription(entregaHRCS.getValue());
        return result;
    }

    private Amount mapOutClaimAmount(final BigDecimal importeReclamado, final String divisaReclamado) {
        if (importeReclamado == null && StringUtils.isEmpty(divisaReclamado)) {
            return null;
        }

        Amount result = new Amount();
        result.setAmount(importeReclamado);
        result.setCurrency(divisaReclamado);
        return result;
    }

    private Contract mapOutContractProducts(final ProductoType producto) {
        if (producto == null) {
            return null;
        }

        Contract result = this.mapOutContract(producto.getContrato());
        if (result == null) {
            return null;
        }
        result.setProduct(mapOutContractProduct(producto.getContrato().getAliasProducto(), producto.getContrato().getDescripcion()));
        return result;
    }

    private Contract mapOutContractProduct(final ProductoType producto) {
        if (producto == null) {
            return null;
        }
        Contract result = this.mapOutContract(producto.getContrato());
        if (result == null) {
            return null;
        }
        result.setProduct(mapOutContractProduct(producto.getContrato().getAliasProducto(), producto.getDescripcion()));
        return result;
    }

    private Contract mapOutContract(final Contrato contrato) {
        if (contrato == null) {
            return null;
        }
        Contract result = new Contract();
        result.setId(contrato.getId());
        result.setNumber(contrato.getNumero());
        result.setNumberType(mapOutNumberType(contrato.getTipoNumero()));

        return result;
    }

    private ContractProduct mapOutContractProduct(final String aliasProducto, final String descripcionProducto) {
        if (StringUtils.isEmpty(aliasProducto) && StringUtils.isEmpty(descripcionProducto)) {
            return null;
        }

        ContractProduct result = new ContractProduct();
        result.setId(aliasProducto);
        result.setName(descripcionProducto);
        return result;
    }

    private NumberType mapOutNumberType(final String tipoNumero) {
        if (StringUtils.isEmpty(tipoNumero)) {
            return null;
        }

        NumberType result = new NumberType();
        result.setId(translator.translateBackendEnumValueStrictly("claims.contract.numberType", tipoNumero));
        return result;
    }

    private StatusSearch mapOutStatus(final EstadoType estado) {
        if (estado == null) {
            return null;
        }

        StatusSearch result = new StatusSearch();
        result.setId(estado.getCodigo());
        result.setDescription(estado.getNombre());
        return result;
    }

    private ResolutionSearch mapOutResolution(final DictamenType dictamen, final CartaRespuestaType cartaRespuesta, final List<ImporteDevueltoType> importeDevuelto, final List<ImporteOperacionalType> importeOperacional) {
        if (dictamen == null && cartaRespuesta == null) {
            return null;
        }

        ResolutionSearch result = new ResolutionSearch();
        if (dictamen != null) {
            result.setId(translator.translateBackendEnumValueStrictly("claims.resolution.id", dictamen.getCodigo()));
            result.setName(dictamen.getNombre());
            result.setFundament(mapOutFundament(dictamen.getFundamento()));
            result.setResolutionType(mapOutResolutionType(dictamen.getClase()));
            result.setAdditionalInformation(dictamen.getSustento());
            Boolean isManualFlow = FunctionUtils.convertToBoolean(dictamen.getOperativaManual());
            result.setIsManualFlow(isManualFlow == null ? null : isManualFlow.toString());
        }

        if (cartaRespuesta != null) {
            result.setDelivery(new Delivery());
            result.getDelivery().setDeliveryType(new DeliveryType());
            result.getDelivery().getDeliveryType().setId(translator.translateBackendEnumValueStrictly("claims.resolution.deliveryType", cartaRespuesta.getCodigo()));
            result.getDelivery().getDeliveryType().setDescription(cartaRespuesta.getValue());
        }

        result.setOperationalAmounts(mapOutOperationalAmounts(importeOperacional));
        result.setReturnedAmounts(mapOutReturnedAmounts(importeDevuelto));
        return result;
    }

    private ResolutionType mapOutResolutionType(final TipoType clase) {
        if (clase == null) {
            return null;
        }

        ResolutionType result = new ResolutionType();
        result.setId(translator.translateBackendEnumValueStrictly("claims.resolution.resolutionType", clase.getCodigo()));
        result.setDescription(clase.getDescripcion());
        return result;
    }

    private Fundament mapOutFundament(final TipoType fundamento) {
        if (fundamento == null) {
            return null;
        }

        Fundament result = new Fundament();
        result.setId(fundamento.getCodigo());
        result.setDescription(fundamento.getDescripcion());
        return result;
    }

    private List<ReturnedAmounts> mapOutReturnedAmounts(final List<ImporteDevueltoType> importeDevuelto) {
        if (CollectionUtils.isEmpty(importeDevuelto)) {
            return null;
        }

        return importeDevuelto.stream().filter(Objects::nonNull).map(this::mapOutReturnedAmount).collect(Collectors.toList());

    }

    private ReturnedAmounts mapOutReturnedAmount(ImporteDevueltoType importeDevueltoType) {
        ReturnedAmounts result = new ReturnedAmounts();
        result.setAmount(importeDevueltoType.getValor());
        result.setCurrency(importeDevueltoType.getDivisa());
        return result;
    }

    private List<OperationalAmounts> mapOutOperationalAmounts(final List<ImporteOperacionalType> importeOperacional) {
        if (CollectionUtils.isEmpty(importeOperacional)) {
            return null;
        }

        return importeOperacional.stream().filter(Objects::nonNull).map(this::mapOutOperationalAmount).collect(Collectors.toList());
    }

    private OperationalAmounts mapOutOperationalAmount(final ImporteOperacionalType importeOperacionalType) {
        OperationalAmounts result = new OperationalAmounts();
        result.setAmount(importeOperacionalType.getValor());
        result.setCurrency(importeOperacionalType.getDivisa());
        return result;
    }

    private ReasonSearch mapOutReason(final String resumen, final ObjectType motivo, final SubmotivoType submotivo) {
        if (StringUtils.isEmpty(resumen) && motivo == null && submotivo == null) {
            return null;
        }

        ReasonSearch result = new ReasonSearch();
        if (motivo != null) {
            result.setId(motivo.getCodigo());
            result.setName(motivo.getNombre());
        }

        if (submotivo != null) {
            result.setSubReason(new SubReason());
            result.getSubReason().setId(submotivo.getCodigo());
            result.getSubReason().setDescription(submotivo.getNombre());
        }

        result.setComments(resumen);
        return result;
    }

    private ProductSearch mapOutProduct(final ProductoType producto) {
        if (producto == null) {
            return null;
        }

        ProductSearch result = new ProductSearch();
        result.setId(producto.getCodigo());
        result.setName(producto.getNombre());
        return result;
    }

    private ClaimType mapOutClaimType(final TipoType tipo) {
        if (tipo == null) {
            return null;
        }

        ClaimType result = new ClaimType();
        result.setId(translator.translateBackendEnumValueStrictly("claims.claimType.id", tipo.getCodigo()));
        result.setDescription(tipo.getDescripcion());
        return result;
    }

    private PersonType mapOutPersonType(final String tipoPersona) {
        if (StringUtils.isEmpty(tipoPersona)) {
            return null;
        }

        PersonType result = new PersonType();
        result.setId(tipoPersona);
        return result;
    }

    private List<Tracking> mapOutTrackings(final List<SeguimientosType> seguimientosType) {
        if (seguimientosType == null || CollectionUtils.isEmpty(seguimientosType)) {
            return null;
        }

        return seguimientosType.stream().filter(Objects::nonNull).map(this::mapOutTracking).collect(Collectors.toList());
    }

    private Tracking mapOutTracking(final SeguimientosType seguimientosType) {
        if (seguimientosType == null) {
            return null;
        }

        Tracking result = new Tracking();
        result.setStatus(mapOutStatusTracking(seguimientosType.getEstado(), seguimientosType.getPrende()));
        result.setLastUpdateDate(seguimientosType.getFecha());
        return result;
    }

    private StatusTracking mapOutStatusTracking(final EstadoType estado, final String prende) {
        if (estado == null && StringUtils.isEmpty(prende)) {
            return null;
        }

        StatusTracking result = new StatusTracking();
        if (estado != null) {
            result.setId(translator.translateBackendEnumValueStrictly("claims.status.id", estado.getCodigo()));
            result.setDescription(estado.getNombre());
        }
        result.setIsActive(FunctionUtils.convertTo(prende));
        return result;
    }

    private List<PetitionersSearch> mapOutPetitioners(final ResponseType responseType) {
        if (responseType.getRequerimiento().getTitular() == null && responseType.getRequerimiento().getRepresentante() == null) {
            return null;
        }

        PetitionersSearch petitioners = mapOutTitularPetitionersSearch(responseType);
        if (petitioners == null) {
            return null;
        }
        List<PetitionersSearch> petitionersSearchList = new ArrayList<>();
        petitionersSearchList.add(petitioners);
        PetitionersSearch petitionersSearch = mapOutRepresentantePetitionersSearch(responseType);
        if (petitionersSearch != null) {
            petitionersSearchList.add(petitionersSearch);
        }
        return petitionersSearchList;
    }

    private PetitionersSearch mapOutTitularPetitionersSearch(final ResponseType responseType) {
        PetitionerSearch petitioner = restProductClaimMapper.mapOutPetitioner(
                responseType.getRequerimiento().getTitular());
        if (petitioner == null) {
            return null;
        }

        if ("HOLDER".equalsIgnoreCase(petitioner.getPetitionerType().getId())) {
            petitioner.setAddress(mapOutAddressComponents(responseType.getTitular(), responseType.getRequerimiento().getTitular() == null ? null : responseType.getRequerimiento().getTitular().getDireccion()));
            petitioner.setContactDetails(mapOutContactDetails(responseType.getRequerimiento().getContactos()));
            petitioner.setSegment(mapOutSegment(responseType.getRequerimiento().getTitular().getSegmento()));
            petitioner.setBankClassification(mapOutBankClassification(responseType.getRequerimiento().getTitular().getClasificacion()));
            petitioner.setBank(mapOutPetitionerBank(responseType.getRequerimiento().getTitular().getBanco(),
                    responseType.getRequerimiento().getTitular().getOficina()));
        }

        PetitionersSearch petitioners = new PetitionersSearch();
        petitioners.setPetitioner(petitioner);
        return petitioners;
    }

    private PetitionersSearch mapOutRepresentantePetitionersSearch(final ResponseType responseType) {
        PetitionerSearch representativePetitioner = restProductClaimMapper.mapOutRepresentativePetitioner(responseType.getRequerimiento().getRepresentante());
        if (representativePetitioner == null) {
            return null;
        }

        if ("KNOWN".equalsIgnoreCase(representativePetitioner.getBankRelationType())) {
            representativePetitioner.setAddress(mapOutAddress(responseType.getRepresentante().getDireccion().getNombre(),
                    responseType.getRepresentante().getReferencia()));
        } else if ("UNKNOWN".equalsIgnoreCase(representativePetitioner.getBankRelationType())) {
            representativePetitioner.setId(null);
            representativePetitioner.setAddress(mapOutAddress(responseType.getTitular().getDireccion().getNombre(),
                    responseType.getTitular().getReferencia()));
        }

        representativePetitioner.setSegment(mapOutSegment(responseType.getRequerimiento().getRepresentante().getSegmento()));
        PetitionersSearch representativePetitioners = new PetitionersSearch();
        representativePetitioners.setPetitioner(representativePetitioner);
        return representativePetitioners;
    }

    private Address mapOutAddress(String nombre, String referencia) {
        if (StringUtils.isEmpty(nombre) && StringUtils.isEmpty(referencia)) {
            return null;
        }
        Address address = new Address();
        address.setLocation(mapOutLocation(nombre, referencia));
        return address;
    }

    private Location mapOutLocation(String nombre, String referencia) {
        Location location = new Location();
        location.setFormattedAddress(nombre);
        location.setAdditionalInformation(referencia);
        return location;
    }


    private BankClassification mapOutBankClassification(final String clasificacion) {
        if (StringUtils.isEmpty(clasificacion)) {
            return null;
        }

        BankClassification result = new BankClassification();
        result.setBankClassificationType(new BankClassificationType());
        result.getBankClassificationType().setId(translator.translateBackendEnumValueStrictly("claims.petitioners.bankClassificationType", clasificacion));
        return result;
    }

    private Bank mapOutPetitionerBank(final Banco banco, final Oficina oficina) {
        if (banco == null && oficina == null) {
            return null;
        }

        Bank result = new Bank();
        if (banco != null) {
            result.setId(banco.getCodigo());
            result.setName(banco.getDescripcion());
        }

        if (oficina != null) {
            result.setBranch(new Branch());
            result.getBranch().setId(oficina.getCodigo());
            result.getBranch().setName(oficina.getDescripcion());
        }
        return result;
    }

    private List<ContactDetail> mapOutContactDetails(final List<ContactoType> contactos) {
        if (CollectionUtils.isEmpty(contactos)) {
            return null;
        }

        return contactos.stream().filter(Objects::nonNull).map(this::mapOutContactDetail).collect(Collectors.toList());
    }

    private ContactDetail mapOutContactDetail(final ContactoType contactoType) {
        ContactDetail result = new ContactDetail();
        result.setContact(new BaseContactSearch());
        result.getContact().setContactDetailType(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType", contactoType.getTipoContacto()));
        result.getContact().setId(contactoType.getId());
        if ("EMAIL".equalsIgnoreCase(result.getContact().getContactDetailType())) {
            mapOutEmailContact(result.getContact(), contactoType);
        }
        if ("LANDLINE".equalsIgnoreCase(result.getContact().getContactDetailType())) {
            mapOutLandLineContact(result.getContact(), contactoType);
        }
        if ("MOBILE".equalsIgnoreCase(result.getContact().getContactDetailType())) {
            mapOutMobileContact(result.getContact(), contactoType);
        }
        return result;
    }

    private void mapOutMobileContact(final BaseContactSearch contact, final ContactoType contactoType) {
        contact.setNumber(contactoType.getNumero());
        contact.setCountryCode(contactoType.getCodigoPaisCelular());
        contact.setPhoneCompany(mapOutPhoneCompany(contactoType.getOperadorCelular()));
    }

    private PhoneCompanySearch mapOutPhoneCompany(final TipoType operadorCelular) {
        if (operadorCelular == null) {
            return null;
        }

        PhoneCompanySearch result = new PhoneCompanySearch();
        result.setId(operadorCelular.getCodigo());
        result.setName(operadorCelular.getDescripcion());
        return result;
    }

    private void mapOutLandLineContact(final BaseContactSearch contact, final ContactoType contactoType) {
        contact.setNumber(contactoType.getNumero());
        contact.setPhoneType(translator.translateBackendEnumValue("claims.petitioner.contact.phoneType", contactoType.getTipoFijo()));
        contact.setCountry(mapOutCountryContact(contactoType.getPaisFijo()));
        contact.setCountryCode(contactoType.getCodigoPaisFijo());
        contact.setRegionalCode(contactoType.getCodigoRegionalFijo());
        contact.setExtension(contactoType.getAnexoFijo());
    }

    private CountrySearch mapOutCountryContact(final String paisFijo) {
        if (StringUtils.isEmpty(paisFijo)) {
            return null;
        }

        CountrySearch result = new CountrySearch();
        result.setId(paisFijo);
        return result;
    }

    private void mapOutEmailContact(final BaseContactSearch contact, final ContactoType contactoType) {
        contact.setAddress(contactoType.getCorreo());
        contact.setReceivesNotifications(FunctionUtils.convertToBoolean(contactoType.getNotificacionesCorreo()));
    }

    private Address mapOutAddressComponents(final DireccionpPersonaType persona, final DireccionType direccion) {
        if (persona == null && direccion == null) {
            return null;
        }

        Address result = mapOutGeneralAddress(persona);
        if (direccion == null) {
            return result;
        }

        if(result == null) {
            result = new Address();
        }

        if (result.getLocation() == null) {
            result.setLocation(new Location());
        }

        result.getLocation().setAddressComponents(mapOutAddressComponent(direccion.getUbigeo()));
        return result;
    }

    private List<AddressComponent> mapOutAddressComponent(final TipoType ubigeo) {
        if (ubigeo == null) {
            return null;
        }

        AddressComponent result = new AddressComponent();
        result.setComponentTypes(Collections.singletonList("UBIGEO"));
        result.setCode(ubigeo.getCodigo());
        result.setName(ubigeo.getDescripcion());
        return Collections.singletonList(result);
    }

    private Segment mapOutSegment(final Segmento segmento) {
        if (segmento == null) {
            return null;
        }
        Segment result = new Segment();
        result.setId(segmento.getCodigo());
        result.setDescription(segmento.getDescripcion());
        return result;
    }

    private Address mapOutGeneralAddress(final DireccionpPersonaType titular) {
        if (titular == null) {
            return null;
        }

        Address result = new Address();
        result.setLocation(new Location());
        result.getLocation().setFormattedAddress(titular.getDireccion() == null ? null : titular.getDireccion().getNombre());
        result.getLocation().setAdditionalInformation(titular.getReferencia());
        return result;
    }

    private List<Document> mapOutDocuments(final List<AdjuntosType> adjuntosType) {
        if (adjuntosType == null || CollectionUtils.isEmpty(adjuntosType)) {
            return null;
        }

        return adjuntosType.stream().filter(Objects::nonNull).map(this::mapOutDocument).collect(Collectors.toList());
    }

    private Document mapOutDocument(final AdjuntosType adjuntosType) {
        Document result = new Document();
        result.setId(adjuntosType.getId());
        result.setGroup(adjuntosType.getGrupo());
        result.setName(adjuntosType.getNombre());
        return result;
    }
}
