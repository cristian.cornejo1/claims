package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.dao.rest.model.harec.*;
import com.bbva.pzic.claims.dao.rest.RestCreateProductClaim;
import com.bbva.pzic.claims.dao.rest.mapper.IRestCreateProductClaimMapper;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 22/11/2019.
 *
 * @author Entelgy
 */
@Component
public class RestCreateProductClaimMapper implements IRestCreateProductClaimMapper {

    private static final Log LOG = LogFactory.getLog(RestCreateProductClaim.class);

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public Map<String, String> mapHeaderIn(final DTOIntProductClaim input) {
        LOG.info("... called method RestCreateProductClaimMapper.mapHeaderIn ...");
        Map<String, String> headerParams = new HashMap<>();
        headerParams.put("canal", input.getAap());
        return headerParams;
    }

    @Override
    public RequestDataType mapInBody(final DTOIntProductClaim input) {
        LOG.info("... called method RestCreateProductClaimMapper.mapInBody ...");
        if (input == null)
            return null;

        RequestDataType result = new RequestDataType();
        result.setTipoPersona(mapInTipoPersona(input.getPersonType()));
        result.setEmpresa(input.getCompany() == null ? null : input.getCompany().getId());
        result.setEsMenor(input.getIsUnderage());
        result.setTaxonomia(mapInTaxonomia(input));
        result.setContrato(input.getContract() == null ? null : input.getContract().getId());
        result.setDetalle(input.getReason() == null ? null : input.getReason().getComments());
        result.setCartaRespuesta(mapInCartaRespuesta(input.getResolution()));
        result.setInterposicion(mapInInterposicion(input.getChannel()));
        result.setUbicacion(mapInUbicacion(input.getGeolocation()));
        result.setEntregaHRCS(mapInDelivery(input.getDelivery()));
        result.setPedido(input.getPetition() == null ? null : input.getPetition().getDescription());
        result.setEsUrgente(input.getPriority() == null ? null : FunctionUtils.convertBooleanToString(input.getPriority().getIsUrgent()));
        result.setSustento(input.getPriority() == null ? null : input.getPriority().getBasis());
        result.setFechaPresentacion(input.getEnrollmentDate());
        result.setNumeroCasoReiterativo(input.getPredecessorClaim() == null ? null : input.getPredecessorClaim().getId());
        result.setIndicadorFCR(FunctionUtils.convertBooleanToString(input.getIsOnlineRefundFlow()));
        result.setNumeroAtencionLinea(input.getPreviousAttentionCode());
        result.setTaxonomiasAdicionales(mapInTaxonomiasAdicionales(input.getAdditionalClaims()));
        if (input.getClaimAmount() != null) {
            result.setImporteReclamado(input.getClaimAmount().getAmount());
            result.setDivisa(input.getClaimAmount().getCurrency());
        }

        if (input.getFiles() != null) {
            result.getAdjuntos().addAll(mapInAdjuntoType(input.getFiles()));
        }

        if (CollectionUtils.isNotEmpty(input.getPetitioners())) {
            input.getPetitioners()
                    .stream()
                    .filter(ptr -> ptr != null && ptr.getPetitioner() != null && StringUtils.isNotEmpty(ptr.getPetitioner().getBankRelationType()))
                    .forEach(ptr -> {
                        String bankRelationType = ptr.getPetitioner().getBankRelationType();
                        if ("KNOWN".equalsIgnoreCase(bankRelationType)) {
                            mapInKnown(result, ptr);
                        } else if ("UNKNOWN".equalsIgnoreCase(bankRelationType)) {
                            mapInUnknown(result, ptr);
                        }
                        mapInAddressComponents(result, ptr);
                        mapInContactDetails(result, ptr);
                    });
        }
        return result;
    }

    private String mapInDelivery(final DTOIntDelivery delivery) {
        if (delivery == null){
            return null;
        }
        return mapInDeliveryType(delivery.getDeliveryType());
    }

    private String mapInDeliveryType(final DTOIntDeliveryType deliveryType) {
        if (deliveryType == null){
            return null;
        }
        return translator.translateFrontendEnumValueStrictly("claims.delivery.deliveryTypeId", deliveryType.getId());
    }

    private List<TaxonomiasAdicionales> mapInTaxonomiasAdicionales(final List<DTOIntAdditionalClaims> additionalClaims) {
        if(CollectionUtils.isEmpty(additionalClaims)) {
            return null;
        }
        return additionalClaims.stream().filter(Objects::nonNull).map(this::mapInTaxonomiaAdicional).collect(Collectors.toList());
    }

    private TaxonomiasAdicionales mapInTaxonomiaAdicional(final DTOIntAdditionalClaims dtoIntAdditionalClaims) {
        if(dtoIntAdditionalClaims == null) {
            return null;
        }
        TaxonomiasAdicionales result = new TaxonomiasAdicionales();
        result.setTipo(mapInTipo(dtoIntAdditionalClaims.getClaimType()));
        result.setProducto(mapInProducto(dtoIntAdditionalClaims.getProduct(), dtoIntAdditionalClaims.getContract()));
        result.setMotivo(mapInMotivo(dtoIntAdditionalClaims.getReason()));
        result.setSubmotivo(mapInSubMotivo(dtoIntAdditionalClaims.getReason() == null ? null : dtoIntAdditionalClaims.getReason().getSubReason()));
        result.setImporteReclamado(mapInImporteReclamadoo(dtoIntAdditionalClaims.getClaimAmount()));
        result.setDivisa(dtoIntAdditionalClaims.getClaimAmount() == null ? null : dtoIntAdditionalClaims.getClaimAmount().getCurrency());
        result.setCajero(dtoIntAdditionalClaims.getAtm() == null ? null : dtoIntAdditionalClaims.getAtm().getId());
        result.setBancoOriginante(mapInBancoOriginante(dtoIntAdditionalClaims.getBank()));
        result.setOficinaOriginante(mapInCodigo(dtoIntAdditionalClaims.getBank() == null ? null : dtoIntAdditionalClaims.getBank().getBranch()));
        result.setFechaOperacion(dtoIntAdditionalClaims.getIssueDate());

        return result;
    }

    private Double mapInImporteReclamadoo(final DTOIntAmount claimAmount) {
        if (claimAmount == null){
            return null;
        }
        return claimAmount.getAmount() == null ? null : claimAmount.getAmount().doubleValue();
    }

    private Tipo mapInTipo(final DTOIntClaimType claimType) {
        if(claimType == null) {
            return null;
        }
        Tipo result = new Tipo();
        result.setCodigo(translator.translateFrontendEnumValueStrictly("claims.claimType.id", claimType.getId()));
        return result;
    }

    private Producto mapInProducto(final DTOIntProduct product, DTOIntContract contract) {
        if(product == null && contract == null) {
            return null;
        }
        Producto result = new Producto();

        if(product != null) {
            result.setCodigo(product.getId());
        }
        if(contract != null) {
            if(result.getContrato() == null) {
                result.setContrato(new Contrato());
            }
            result.getContrato().setId(contract.getId());
            result.getContrato().setNumero(contract.getNumber());
            if (contract.getNumberType() != null){
                result.getContrato().setTipoNumero(translator.translateFrontendEnumValueStrictly("claims.contract.numberTypeId",
                        contract.getNumberType().getId()));
            }
            result.getContrato().setAliasProducto(contract.getProduct() == null ? null : contract.getProduct().getId());

        }

        return result;
    }

    private Motivo mapInMotivo(final DTOIntReason reason) {
        if(reason == null) {
            return null;
        }
        Motivo result = new Motivo();
        result.setCodigo(reason.getId());
        return result;
    }

    private Submotivo mapInSubMotivo(final DTOIntSubReason subReason) {
        if(subReason == null) {
            return null;
        }
        Submotivo result = new Submotivo();
        result.setCodigo(subReason.getId());
        return result;
    }

    private BancoOriginante mapInBancoOriginante(final DTOIntBank bank) {
        if(bank == null) {
            return null;
        }
        BancoOriginante result = new BancoOriginante();
        result.setCodigo(bank.getId());
        return result;
    }

    private OficinaOriginante mapInCodigo(final DTOIntBranch dtoIntBranch) {
        if(dtoIntBranch == null) {
            return null;
        }
        OficinaOriginante result = new OficinaOriginante();
        result.setCodigo(dtoIntBranch.getId());
        return result;
    }

    private Interposicion mapInInterposicion(final DTOIntChannel channel) {
        if(channel == null) {
            return null;
        }
        Interposicion interposicion = new Interposicion();
        interposicion.setCodigo(channel.getId());
        return interposicion;
    }

    private Ubicacion mapInUbicacion(final DTOIntGeolocation geolocation) {
        if(geolocation == null) {
            return null;
        }
        Ubicacion ubicacion = new Ubicacion();
        ubicacion.setCodigo(geolocation.getCode());
        return ubicacion;
    }

    private String mapInTipoPersona(final DTOIntPersonType personType) {
        if (personType == null) {
            return null;
        }
        return translator.translateFrontendEnumValueStrictly("claims.personType.id", personType.getId());
    }

    private TaxonomiaType mapInTaxonomia(final DTOIntProductClaim productClaim) {
        if (productClaim == null) {
            return null;
        }
        TaxonomiaType taxonomiaType = new TaxonomiaType();
        if (productClaim.getClaimType() != null) {
            ObjectType tipo = new ObjectType();
            tipo.setCodigo(translator.translateFrontendEnumValueStrictly("claims.claimType.id", productClaim.getClaimType().getId()));
            taxonomiaType.setTipo(tipo);
        }
        if (productClaim.getProduct() != null) {
            ProductoType producto = new ProductoType();
            producto.setCodigo(productClaim.getProduct().getId());
            taxonomiaType.setProducto(producto);
        }
        if (productClaim.getReason() != null) {
            ObjectType motivo = new ObjectType();
            motivo.setCodigo(productClaim.getReason().getId());
            taxonomiaType.setMotivo(motivo);
            if (productClaim.getReason().getSubReason() != null) {
                ObjectType submotivo = new ObjectType();
                submotivo.setCodigo(productClaim.getReason().getSubReason().getId());
                taxonomiaType.setSubmotivo(submotivo);
            }
        }
        if (productClaim.getContract() != null) {
            if (taxonomiaType.getProducto() == null) {
                taxonomiaType.setProducto(new ProductoType());
            }
            ProductoType productoType = taxonomiaType.getProducto();
            if (productoType.getContrato() == null) {
                productoType.setContrato(new Contrato());
            }
            productoType.getContrato().setId(productClaim.getContract().getId());
            productoType.getContrato().setNumero(productClaim.getContract().getNumber());
            if (productClaim.getContract().getNumberType() != null) {
                productoType.getContrato().setTipoNumero(translator.translateFrontendEnumValueStrictly("claims.contract.numberTypeId", productClaim.getContract().getNumberType().getId()));
            }
            if (productClaim.getContract().getProduct() != null) {
                productoType.getContrato().setAliasProducto(productClaim.getContract().getProduct().getId());
            }

            if (productClaim.getAtm() != null) {
                taxonomiaType.setCajero(productClaim.getAtm().getId());
            }
            if (productClaim.getBank() != null) {
                BancoOriginante bancoOriginante = new BancoOriginante();
                bancoOriginante.setCodigo(productClaim.getBank().getId());
                taxonomiaType.setBancoOriginante(bancoOriginante);

                if(productClaim.getBank().getBranch() != null) {
                    OficinaOriginante oficinaOriginante = new OficinaOriginante();
                    oficinaOriginante.setCodigo(productClaim.getBank().getBranch().getId());
                    taxonomiaType.setOficinaOriginante(oficinaOriginante);
                }
            }
        }

        taxonomiaType.setFechaOperacion(productClaim.getIssueDate());
        return taxonomiaType;
    }

    private String mapInCartaRespuesta(final DTOIntResolution resolution) {
        if (resolution == null || resolution.getDelivery() == null || resolution.getDelivery().getDeliveryType() == null)
            return null;
        return translator.translateFrontendEnumValueStrictly("claims.verdict.deliveryType.id",
                resolution.getDelivery().getDeliveryType().getId());
    }


    private List<AdjuntosType> mapInAdjuntoType(final List<DTOIntFile> dtoIntFile) {
        if (dtoIntFile == null)
            return null;

        return dtoIntFile.stream().map(file -> {
            AdjuntosType adjuntosType = new AdjuntosType();
            adjuntosType.setBinario(file.getContent());
            adjuntosType.setNombre(file.getName());
            return adjuntosType;
        }).collect(Collectors.toList());
    }

    private void mapInContactDetails(final RequestDataType requestDataType, final DTOIntPetitioners dtoIntPetitioners) {
        if (dtoIntPetitioners.getPetitioner() == null ||
                CollectionUtils.isEmpty(dtoIntPetitioners.getPetitioner().getContactDetails())) {
            return;
        }

        dtoIntPetitioners.getPetitioner().getContactDetails().forEach(cd -> {
            if (cd.getContact().getContactDetailType() != null) {
                if ("EMAIL".equalsIgnoreCase(cd.getContact().getContactDetailType().getId())) {
                    DTOIntEmail dtoIntEmail = (DTOIntEmail) cd.getContact();
                    mapInCorreosTypes(requestDataType, dtoIntEmail);
                } else if ("LANDLINE".equalsIgnoreCase(cd.getContact().getContactDetailType().getId()) ||
                        "MOBILE".equalsIgnoreCase(cd.getContact().getContactDetailType().getId())) {
                    DTOIntMobile dtoIntMobile = (DTOIntMobile) cd.getContact();
                    mapInTelefonosTypes(requestDataType, dtoIntMobile);
                }
            }
        });
    }

    private void mapInTelefonosTypes(final RequestDataType requestDataType, final DTOIntMobile dtoIntMobile) {
        if (requestDataType.getTelefonos() == null) {
            return;
        }

        TelefonosType telefonosType = new TelefonosType();
        telefonosType.setId(dtoIntMobile.getId());
        telefonosType.setNumero(dtoIntMobile.getNumber());
        telefonosType.setTipo(dtoIntMobile.getContactDetailType() == null ? null : translator.translateFrontendEnumValueStrictly("claims.contactDetails.contact.contactDetailType", dtoIntMobile.getContactDetailType().getId()));
        telefonosType.setOperador(dtoIntMobile.getPhoneCompany() == null ? null : dtoIntMobile.getPhoneCompany().getId());
        requestDataType.getTelefonos().add(telefonosType);
    }

    private void mapInCorreosTypes(final RequestDataType requestDataType, final DTOIntEmail dtoIntEmail) {
        if (requestDataType.getCorreos() == null) {
            return;
        }

        CorreosType correosType = new CorreosType();
        correosType.setId(dtoIntEmail.getId());
        correosType.setCorreo(dtoIntEmail.getAddress());
        requestDataType.getCorreos().add(correosType);
    }

    private void mapInAddressComponents(final RequestDataType result, final DTOIntPetitioners dtoIntPetitioners) {
        if (dtoIntPetitioners.getPetitioner() == null || dtoIntPetitioners.getPetitioner().getAddress() == null ||
                dtoIntPetitioners.getPetitioner().getAddress().getLocation() == null ||
                CollectionUtils.isEmpty(dtoIntPetitioners.getPetitioner().getAddress().getLocation().getAddressComponents()) ||
                CollectionUtils.isEmpty(dtoIntPetitioners.getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes()) ||
                !"UBIGEO".equalsIgnoreCase(dtoIntPetitioners.getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0))
        ) {
            return;
        }

        UbigeoType ubigeoType = new UbigeoType();
        ubigeoType.setCodUbigeo(dtoIntPetitioners.getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        result.setUbigeo(ubigeoType);
    }

    private void mapInUnknown(final RequestDataType requestDataType, final DTOIntPetitioners dtoIntPetitioners) {
        if (dtoIntPetitioners == null || dtoIntPetitioners.getPetitioner() == null) {
            return;
        }

        DTOIntUnknownPetitioner dtoIntUnknownPetitioner = (DTOIntUnknownPetitioner) dtoIntPetitioners.getPetitioner();
        String petitionerTypeId = dtoIntUnknownPetitioner.getPetitionerType().getId();
        if ("HOLDER".equals(petitionerTypeId)) {
            requestDataType.setTitular(mapInUnknownTitular(dtoIntUnknownPetitioner));
        } else if ("AUTHORIZED".equals(petitionerTypeId)) {
            requestDataType.setRepresentante(mapInUnknownRepresentante(dtoIntUnknownPetitioner));
        }
        requestDataType.setDireccion(mapInDireccion(dtoIntUnknownPetitioner.getAddress()));
        requestDataType.setReferencia(mapInReferencia(dtoIntUnknownPetitioner.getAddress()));

    }

    private RepresentanteType mapInUnknownRepresentante(final DTOIntUnknownPetitioner dtoIntUnknownPetitioner) {
        RepresentanteType representanteType = new RepresentanteType();
        representanteType.setTipoDoi(mapInTipoDoi(dtoIntUnknownPetitioner.getIdentityDocument()));
        representanteType.setDoi(dtoIntUnknownPetitioner.getIdentityDocument() == null ? null : dtoIntUnknownPetitioner.getIdentityDocument().getDocumentNumber());
        representanteType.setNombres(dtoIntUnknownPetitioner.getFirstName());
        representanteType.setApellidoPaterno(dtoIntUnknownPetitioner.getLastName());
        representanteType.setApellidoMaterno(dtoIntUnknownPetitioner.getSecondLastName());
        representanteType.setSegmento(mapInSegmento(dtoIntUnknownPetitioner.getSegment()));
        return representanteType;
    }

    private TitularType mapInUnknownTitular(final DTOIntUnknownPetitioner dtoIntUnknownPetitioner) {
        TitularType titularType = new TitularType();
        titularType.setTipoDoi(mapInTipoDoi(dtoIntUnknownPetitioner.getIdentityDocument()));
        titularType.setDoi(dtoIntUnknownPetitioner.getIdentityDocument() == null ? null : dtoIntUnknownPetitioner.getIdentityDocument().getDocumentNumber());
        titularType.setNombres(dtoIntUnknownPetitioner.getFirstName());
        titularType.setApellidoPaterno(dtoIntUnknownPetitioner.getLastName());
        titularType.setApellidoMaterno(dtoIntUnknownPetitioner.getSecondLastName());
        titularType.setSegmento(mapInSegmento(dtoIntUnknownPetitioner.getSegment()));
        titularType.setBanco(mapInBanco(dtoIntUnknownPetitioner.getBank()));
        titularType.setOficina(mapInOficina(dtoIntUnknownPetitioner.getBank() == null ? null : dtoIntUnknownPetitioner.getBank().getBranch()));
        return titularType;
    }

    private String mapInTipoDoi(final DTOIntIdentityDocument dtoIntIdentityDocument) {
        if (dtoIntIdentityDocument == null || dtoIntIdentityDocument.getDocumentType() == null) {
            return null;
        }

        return translator.translateFrontendEnumValueStrictly("claims.identityDocument.documentType.id", dtoIntIdentityDocument.getDocumentType().getId());
    }

    private void mapInKnown(final RequestDataType requestDataType, final DTOIntPetitioners dtoIntPetitioners) {
        if (dtoIntPetitioners == null || dtoIntPetitioners.getPetitioner() == null) {
            return;
        }

        String petitionerTypeId = null;
        if (dtoIntPetitioners.getPetitioner().getPetitionerType() != null) {
            petitionerTypeId = dtoIntPetitioners.getPetitioner().getPetitionerType().getId();
        }

        if (StringUtils.isNotEmpty(petitionerTypeId) && "HOLDER".equalsIgnoreCase(petitionerTypeId)) {
            requestDataType.setTitular(mapInKnownTitular(dtoIntPetitioners.getPetitioner()));
        } else if (StringUtils.isNotEmpty(petitionerTypeId) && "AUTHORIZED".equalsIgnoreCase(petitionerTypeId)) {
            requestDataType.setRepresentante(mapInKnownRepresentante(dtoIntPetitioners.getPetitioner()));
        }
        requestDataType.setDireccion(mapInDireccion(dtoIntPetitioners.getPetitioner().getAddress()));
        requestDataType.setReferencia(mapInReferencia(dtoIntPetitioners.getPetitioner().getAddress()));
    }

    private RepresentanteType mapInKnownRepresentante(final DTOIntPetitioner petitioner) {
        if(petitioner == null) {
            return null;
        }
        if (StringUtils.isEmpty(petitioner.getId())) {
            return null;
        }

        RepresentanteType representanteType = new RepresentanteType();
        representanteType.setCodigoCentral(petitioner.getId());
        representanteType.setSegmento(mapInSegmento(petitioner.getSegment()));
        return representanteType;
    }

    private TitularType mapInKnownTitular(final DTOIntPetitioner petitioner) {
        if(petitioner == null) {
            return null;
        }
        if (StringUtils.isEmpty(petitioner.getId())) {
            return null;
        }

        TitularType titularType = new TitularType();
        titularType.setCodigoCentral(petitioner.getId());
        titularType.setSegmento(mapInSegmento(petitioner.getSegment()));
        titularType.setBanco(mapInBanco(petitioner.getBank()));
        titularType.setOficina(mapInOficina(petitioner.getBank() == null ? null : petitioner.getBank().getBranch()));
        return titularType;
    }

    private Segmento mapInSegmento(final DTOIntSegment segment) {
        if(segment == null) {
            return null;
        }
        Segmento result = new Segmento();
        result.setCodigo(segment.getId());
        return result;
    }

    private Banco mapInBanco(final DTOIntBank bank) {
        if(bank == null) {
            return null;
        }
        Banco result = new Banco();
        result.setCodigo(bank.getId());
        return result;
    }

    private Oficina mapInOficina(final DTOIntBranch dtoIntBranch) {
        if(dtoIntBranch == null) {
            return null;
        }
        Oficina result = new Oficina();
        result.setCodigo(dtoIntBranch.getId());
        return result;
    }

    private String mapInReferencia(final DTOIntAddress dtoIntAddress) {
        if (dtoIntAddress == null || dtoIntAddress.getLocation() == null) {
            return null;
        }

        return dtoIntAddress.getLocation().getAdditionalInformation();
    }

    private DireccionType mapInDireccion(final DTOIntAddress dtoIntAddress) {
        if (dtoIntAddress == null) {
            return null;
        }

        DireccionType directionType = new DireccionType();
        directionType.setId(dtoIntAddress.getId());
        directionType.setNombre(dtoIntAddress.getLocation() == null ? null : dtoIntAddress.getLocation().getFormattedAddress());
        return directionType;
    }

    @Override
    public ProductClaimTransaction mapOut(final ResponseType model) {
        LOG.info("... called method RestCreateProductClaimMapper.mapOut ...");
        if (model == null || model.getRequerimiento() == null)
            return null;

        ProductClaimTransaction response = new ProductClaimTransaction();
        response.setId(model.getRequerimiento().getNumero());
        response.setNumber(model.getRequerimiento().getNumero());
        return response;
    }
}
