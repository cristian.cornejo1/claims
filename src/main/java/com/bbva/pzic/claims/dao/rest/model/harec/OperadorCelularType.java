package com.bbva.pzic.claims.dao.rest.model.harec;

/**
 * Created on 03/02/2021
 *
 * @author Entelgy
 */
public class OperadorCelularType {

    protected String codigo;
    protected String descripcion;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
