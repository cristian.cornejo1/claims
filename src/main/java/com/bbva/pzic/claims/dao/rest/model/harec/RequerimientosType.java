package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;


/**
 * <p>Clase Java para requerimientosType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="requerimientosType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tipoPersona" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tipo" type="{/harec-rest-aso/consulta/historico}tipoType"/&gt;
 *         &lt;element name="producto" type="{/harec-rest-aso/consulta/historico}productoType"/&gt;
 *         &lt;element name="titular" type="{/harec-rest-aso/consulta/historico}titularType"/&gt;
 *         &lt;element name="representante" type="{/harec-rest-aso/consulta/historico}representanteType"/&gt;
 *         &lt;element name="motivo" type="{/harec-rest-aso/consulta/historico}objectType"/&gt;
 *         &lt;element name="dictamen" type="{/harec-rest-aso/consulta/historico}objectType"/&gt;
 *         &lt;element name="estado" type="{/harec-rest-aso/consulta/historico}objectType"/&gt;
 *         &lt;element name="fechaRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requerimientosType", propOrder = {
        "numero",
        "tipoPersona",
        "tipo",
        "producto",
        "titular",
        "representante",
        "motivo",
        "dictamen",
        "estado",
        "fechaRegistro",
        "submotivo",
        "categoria",
        "numeroCasoReiterativo",
        "direccion",
        "contactos"
})
public class RequerimientosType {

    @XmlElement(required = true)
    protected String numero;
    @XmlElement(required = true)
    protected String tipoPersona;
    @XmlElement(required = true)
    protected TipoType tipo;
    @XmlElement(required = true)
    protected ProductoType producto;
    @XmlElement(required = true)
    protected TitularType titular;
    @XmlElement(required = true)
    protected RepresentanteType representante;
    @XmlElement(required = true)
    protected ObjectType motivo;
    @XmlElement(required = true)
    protected ObjectType dictamen;
    @XmlElement(required = true)
    protected ObjectType estado;
    @XmlElement(required = true)
    protected String fechaRegistro;

    protected ObjectType submotivo;

    protected CategoryType categoria;

    protected String numeroCasoReiterativo;

    protected DireccionType direccion;

    protected List<ContactType> contactos;

    public List<ContactType> getContactos() { return contactos; }

    public void setContactos(List<ContactType> contactos) { this.contactos = contactos; }

    public DireccionType getDireccion() { return direccion; }

    public void setDireccion(DireccionType direccion) { this.direccion = direccion; }

    public String getNumeroCasoReiterativo() { return numeroCasoReiterativo; }

    public void setNumeroCasoReiterativo(String numeroCasoReiterativo) { this.numeroCasoReiterativo = numeroCasoReiterativo; }

    public CategoryType getCategoria() { return categoria; }

    public void setCategoria(CategoryType categoria) { this.categoria = categoria; }

    public ObjectType getSubmotivo() { return submotivo; }

    public void setSubmotivo(ObjectType submotivo) { this.submotivo = submotivo; }

    /**
     * Obtiene el valor de la propiedad numero.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Define el valor de la propiedad numero.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPersona.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * Define el valor de la propiedad tipoPersona.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTipoPersona(String value) {
        this.tipoPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad tipo.
     *
     * @return possible object is
     * {@link TipoType }
     */
    public TipoType getTipo() {
        return tipo;
    }

    /**
     * Define el valor de la propiedad tipo.
     *
     * @param value allowed object is
     *              {@link TipoType }
     */
    public void setTipo(TipoType value) {
        this.tipo = value;
    }

    /**
     * Obtiene el valor de la propiedad producto.
     *
     * @return possible object is
     * {@link ProductoType }
     */
    public ProductoType getProducto() {
        return producto;
    }

    /**
     * Define el valor de la propiedad producto.
     *
     * @param value allowed object is
     *              {@link ProductoType }
     */
    public void setProducto(ProductoType value) {
        this.producto = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     *
     * @return possible object is
     * {@link TitularType }
     */
    public TitularType getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     *
     * @param value allowed object is
     *              {@link TitularType }
     */
    public void setTitular(TitularType value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad representante.
     *
     * @return possible object is
     * {@link RepresentanteType }
     */
    public RepresentanteType getRepresentante() {
        return representante;
    }

    /**
     * Define el valor de la propiedad representante.
     *
     * @param value allowed object is
     *              {@link RepresentanteType }
     */
    public void setRepresentante(RepresentanteType value) {
        this.representante = value;
    }

    /**
     * Obtiene el valor de la propiedad motivo.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ObjectType getMotivo() {
        return motivo;
    }

    /**
     * Define el valor de la propiedad motivo.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setMotivo(ObjectType value) {
        this.motivo = value;
    }

    /**
     * Obtiene el valor de la propiedad dictamen.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ObjectType getDictamen() {
        return dictamen;
    }

    /**
     * Define el valor de la propiedad dictamen.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setDictamen(ObjectType value) {
        this.dictamen = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     *
     * @return possible object is
     * {@link ObjectType }
     */
    public ObjectType getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     *
     * @param value allowed object is
     *              {@link ObjectType }
     */
    public void setEstado(ObjectType value) {
        this.estado = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRegistro.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     * Define el valor de la propiedad fechaRegistro.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFechaRegistro(String value) {
        this.fechaRegistro = value;
    }

}
