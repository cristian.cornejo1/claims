package com.bbva.pzic.claims.dao.rest;

import com.bbva.pzic.claims.business.dto.InputGetProductClaim;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.mapper.IRestGetProductClaimMapper;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.util.connection.rest.RestPostConnectionHeader;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

import static com.bbva.pzic.claims.facade.RegistryIds.SN_REGISTRY_GET_PRODUCT_CLAIM;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
@Component
public class RestGetProductClaim extends RestPostConnectionHeader<RequestType> {

    private static final String GET_PRODUCT_CLAIMS_URL = "servicing.smc.configuration.SMCPE1920053.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1920053.backend.proxy";

    @Autowired
    private IRestGetProductClaimMapper mapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
    }

    public ProductClaims invoke(final InputGetProductClaim dtoInt) {
        List<String> obfuscationFields = Arrays.asList("contrato", "numero", "nombres", "apellidoPaterno", "apellidoMaterno", "correo", "nombre", "codigo");
        return mapper.mapOut(connect(GET_PRODUCT_CLAIMS_URL, null, mapper.mapInBody(dtoInt), SN_REGISTRY_GET_PRODUCT_CLAIM, obfuscationFields));
    }
}
