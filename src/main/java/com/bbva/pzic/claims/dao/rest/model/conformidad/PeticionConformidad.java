package com.bbva.pzic.claims.dao.rest.model.conformidad;

public class PeticionConformidad {

    private String numero;

    private Atencion estadoAtencion;

    private Situacion situacion;

    private Integer conformidadAtencion;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Atencion getEstadoAtencion() {
        return estadoAtencion;
    }

    public void setEstadoAtencion(Atencion estadoAtencion) {
        this.estadoAtencion = estadoAtencion;
    }

    public Situacion getSituacion() {
        return situacion;
    }

    public void setSituacion(Situacion situacion) {
        this.situacion = situacion;
    }

    public Integer getConformidadAtencion() {
        return conformidadAtencion;
    }

    public void setConformidadAtencion(Integer conformidadAtencion) {
        this.conformidadAtencion = conformidadAtencion;
    }
}

