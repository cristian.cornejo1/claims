package com.bbva.pzic.claims.dao.rest;

import com.bbva.pzic.claims.business.dto.InputGetAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponse;
import com.bbva.pzic.claims.dao.rest.mapper.IRestGetAutomaticRefundTransactionMapper;
import com.bbva.pzic.claims.util.connection.rest.RestGetConnection;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import static com.bbva.pzic.claims.facade.RegistryIds.SN_REGISTRY_GET_AUTOMATIC_REFUND_TRANSACTION;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestGetAutomaticRefundTransaction extends RestGetConnection<ModelResultadoResponse> {
    private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1810333.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1810333.backend.proxy";

    @Resource(name = "restGetAutomaticRefundTransactionMapper")
    private IRestGetAutomaticRefundTransactionMapper mapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
    }

    public AutomaticRefundTransaction invoke(final InputGetAutomaticRefundTransaction input) {
        final ModelResultadoResponse result = connect(URL_PROPERTY,
                mapper.mapPathIn(input),
                null,
                mapper.mapHeaderIn(input));
        return mapper.mapOut(result);
    }

    @Override
    protected void evaluateResponse(final ModelResultadoResponse response, final int statusCode) {
        super.evaluateMessagesResponse(response.getMessages(), SN_REGISTRY_GET_AUTOMATIC_REFUND_TRANSACTION, statusCode);
    }
}