package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.business.dto.DTOIntUnknownPetitioner;
import com.bbva.pzic.claims.business.dto.InputSearchProductClaims;
import com.bbva.pzic.claims.canonic.Address;
import com.bbva.pzic.claims.canonic.AddressComponent;
import com.bbva.pzic.claims.canonic.ClaimType;
import com.bbva.pzic.claims.canonic.Location;
import com.bbva.pzic.claims.dao.rest.model.harec.*;
import com.bbva.pzic.claims.dao.rest.mapper.IRestSearchProductClaimsMapper;
import com.bbva.pzic.claims.dao.rest.mapper.common.RestProductClaimMapper;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.claims.util.Constants.DATE_LONG_FORMAT;
import static com.bbva.pzic.claims.util.Enums.*;
import static com.bbva.pzic.claims.util.FunctionUtils.buildDatetime;
import static com.bbva.pzic.claims.util.FunctionUtils.convertToBoolean;
/**
 * Created on 26/11/2019.
 *
 * @author Entelgy
 */
@Component
public class RestSearchProductClaimsMapper implements IRestSearchProductClaimsMapper {

    private static final Log LOG = LogFactory.getLog(RestSearchProductClaimsMapper.class);

    @Autowired
    private RestProductClaimMapper restProductClaimMapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public RequestType mapIn(final InputSearchProductClaims input) {
        LOG.info("... called method RestSearchProductClaimsMapper.mapInBody ...");
        RequestType requestType = new RequestType();
        requestType.setCanal(input.getAap());
        if (StringUtils.isEmpty(input.getClientId())) {
            if (input.getPetitioner() != null && ((DTOIntUnknownPetitioner) input.getPetitioner()).getIdentityDocument() != null) {
                DTOIntUnknownPetitioner dtoIntUnknownPetitioner = (DTOIntUnknownPetitioner) input.getPetitioner();
                requestType.setNumeroDoi(dtoIntUnknownPetitioner.getIdentityDocument().getDocumentNumber());
                if (dtoIntUnknownPetitioner.getIdentityDocument().getDocumentType() != null) {
                    requestType.setTipoDoi(dtoIntUnknownPetitioner.getIdentityDocument().getDocumentType().getId());
                }
            }
        } else {
            requestType.setCodigoCentral(input.getClientId());
        }
        return requestType;
    }

    @Override
    public List<ProductClaims> mapOut(final ResponseType model) {
        LOG.info("... called method RestSearchProductClaimsMapper.mapOut ...");
        if (model == null || CollectionUtils.isEmpty(model.getRequerimientos())) {
            return null;
        }

        return model.getRequerimientos().stream().filter(Objects::nonNull).map(this::mapOutProductClaim).collect(Collectors.toList());
    }

    private ProductClaims mapOutProductClaim(final RequerimientosType requerimientosType) {
        ProductClaims productClaims = new ProductClaims();
        productClaims.setId(requerimientosType.getNumero());
        productClaims.setNumber(requerimientosType.getNumero());
        productClaims.setCreationDate(buildDatetime(requerimientosType.getFechaRegistro(), DATE_LONG_FORMAT));
        productClaims.setPersonType(mapOutPersonType(requerimientosType.getTipoPersona()));
        productClaims.setClaimType(mapOutClaimType(requerimientosType.getTipo()));
        productClaims.setProduct(mapOutProduct(requerimientosType.getProducto()));
        productClaims.setReason(mapOutReason(requerimientosType.getMotivo(), requerimientosType.getSubmotivo()));
        productClaims.setCategory(mapOutCategory(requerimientosType.getCategoria()));
        productClaims.setPredecessorClaim(mapOutPredecessorClaim(requerimientosType.getNumeroCasoReiterativo()));
        productClaims.setResolution(mapOutResolution(requerimientosType.getDictamen()));
        productClaims.setStatus(mapOutStatus(requerimientosType.getEstado()));
        productClaims.setPersonType(restProductClaimMapper.mapOutPersonTypeEnum(productClaims.getPersonType()));
        productClaims.setClaimType(restProductClaimMapper.mapOutClaimTypeEnum(productClaims.getClaimType()));
        productClaims.setStatus(restProductClaimMapper.mapOutStatusEnum(productClaims.getStatus()));
        productClaims.setPetitioners(mapOutPetitioners(requerimientosType));
        return productClaims;
    }

    private PredecessorClaimSearch mapOutPredecessorClaim(final String numeroCasoReiterativo) {
        if (StringUtils.isEmpty(numeroCasoReiterativo)) {
            return null;
        }
        PredecessorClaimSearch dtoOut = new PredecessorClaimSearch();
        dtoOut.setId(numeroCasoReiterativo);
        return dtoOut;
    }

    private CategorySearch mapOutCategory(final CategoryType categoria) {
        if (categoria == null) {
            return null;
        }
        CategorySearch dtoOut = new CategorySearch();
        dtoOut.setId(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_CATEGORY_ID, categoria.getCodigo()));
        dtoOut.setName(categoria.getDescripcion());
        return dtoOut;
    }

    private PersonType mapOutPersonType(final String tipoPersona) {
        if (StringUtils.isEmpty(tipoPersona)) {
            return null;
        }
        PersonType personType = new PersonType();
        personType.setId(tipoPersona);
        return personType;
    }

    private ClaimType mapOutClaimType(final TipoType tipo) {
        if (tipo == null) {
            return null;
        }
        ClaimType claimType = new ClaimType();
        claimType.setId(tipo.getCodigo());
        claimType.setDescription(tipo.getDescripcion());
        return claimType;
    }

    private ProductSearch mapOutProduct(final ProductoType producto) {
        if (producto == null) {
            return null;
        }
        ProductSearch productSearch = new ProductSearch();
        productSearch.setId(producto.getCodigo());
        productSearch.setName(producto.getNombre());
        return productSearch;
    }

    private ReasonSearch mapOutReason(final ObjectType motivo, final ObjectType submotivo) {
        if (motivo == null) {
            return null;
        }
        ReasonSearch reasonSearch = new ReasonSearch();
        reasonSearch.setId(motivo.getCodigo());
        reasonSearch.setName(motivo.getNombre());
        reasonSearch.setSubReason(mapOutSubReason(submotivo));
        return reasonSearch;
    }

    private SubReason mapOutSubReason(final ObjectType submotivo) {
        if (submotivo == null) {
            return null;
        }
        SubReason dtoOut = new SubReason();
        dtoOut.setId(submotivo.getCodigo());
        dtoOut.setDescription(submotivo.getNombre());
        return dtoOut;
    }

    private ResolutionSearch mapOutResolution(final ObjectType dictamen) {
        if (dictamen == null) {
            return null;
        }
        ResolutionSearch resolutionSearch = new ResolutionSearch();
        resolutionSearch.setId(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_RESOLUTION_ID, dictamen.getCodigo()));
        resolutionSearch.setName(dictamen.getNombre());
        return resolutionSearch;
    }

    private StatusSearch mapOutStatus(final ObjectType estado) {
        if (estado == null) {
            return null;
        }
        StatusSearch statusSearch = new StatusSearch();
        statusSearch.setId(estado.getCodigo());
        statusSearch.setDescription(estado.getNombre());
        return statusSearch;
    }

    private List<PetitionersSearch> mapOutPetitioners(final RequerimientosType requerimientosType) {
        PetitionerSearch petitionerSearch = restProductClaimMapper.mapOutPetitioner(requerimientosType.getTitular());
        if (petitionerSearch == null) {
            return null;
        }

        if ("HOLDER".equalsIgnoreCase(petitionerSearch.getPetitionerType().getId()) &&
                "KNOWN".equalsIgnoreCase(petitionerSearch.getBankRelationType())) {
            petitionerSearch.setAddress(mapOutAddress(requerimientosType.getDireccion()));
            petitionerSearch.setContactDetails(mapOutContactDetails(requerimientosType.getContactos()));
        } else if ("HOLDER".equalsIgnoreCase(petitionerSearch.getPetitionerType().getId()) &&
                "UNKNOWN".equalsIgnoreCase(petitionerSearch.getBankRelationType())) {
            petitionerSearch.setId(null);
            petitionerSearch.setAddress(mapOutAddress(requerimientosType.getDireccion()));
            petitionerSearch.setContactDetails(mapOutContactDetails(requerimientosType.getContactos()));
        }
        PetitionersSearch petitionersSearch = new PetitionersSearch();
        petitionersSearch.setPetitioner(petitionerSearch);

        List<PetitionersSearch> petitionersSearchList = new ArrayList<>();
        petitionersSearchList.add(petitionersSearch);

        PetitionersSearch petitionersSearchRepresentante = mapOutRepresentantePetitionersSearch(requerimientosType);

        if (petitionersSearchRepresentante != null) {
            petitionersSearchList.add(petitionersSearchRepresentante);
        }

        return petitionersSearchList;
    }

    private PetitionersSearch mapOutRepresentantePetitionersSearch(final RequerimientosType requerimientosType) {
        PetitionerSearch representativePetitioner = restProductClaimMapper.mapOutRepresentativePetitioner(requerimientosType.getRepresentante());
        if (representativePetitioner == null) {
            return null;
        }

        if ("UNKNOWN".equalsIgnoreCase(representativePetitioner.getBankRelationType())) {
            representativePetitioner.setId(null);
        }

        PetitionersSearch representativePetitioners = new PetitionersSearch();
        representativePetitioners.setPetitioner(representativePetitioner);
        return representativePetitioners;
    }

    private List<ContactDetail> mapOutContactDetails(final List<ContactType> contactos) {
        if (CollectionUtils.isEmpty(contactos)) {
            return null;
        }
        return contactos.stream().filter(Objects::nonNull).map(this::mapOutContactDetail).collect(Collectors.toList());
    }

    private ContactDetail mapOutContactDetail(final ContactType contacto) {
        if (contacto == null) {
            return null;
        }
        ContactDetail dtoOut = new ContactDetail();
        dtoOut.setContact(mapOutContact(contacto));
        return dtoOut;
    }

    private BaseContactSearch mapOutContact(final ContactType contacto) {
        if (contacto == null) {
            return null;
        }
        BaseContactSearch dtoOut = new BaseContactSearch();
        dtoOut.setContactDetailType(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PETITIONER_CONTACTDETAILTYPE,
                contacto.getTipoContacto()));

        if (dtoOut.getContactDetailType().equalsIgnoreCase("EMAIL")) {
            dtoOut.setId(contacto.getId());
            dtoOut.setAddress(contacto.getCorreo());
            dtoOut.setReceivesNotifications(convertToBoolean(contacto.getNotificacionesCorreo()));
        }

        if (dtoOut.getContactDetailType().equalsIgnoreCase("LANDLINE")) {
            dtoOut.setId(contacto.getId());
            dtoOut.setNumber(contacto.getNumero());
            dtoOut.setPhoneType(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PETITIONER_CONTACT_PHONETYPE,
                    contacto.getTipoFijo()));
            dtoOut.setCountry(mapOutCountry(contacto.getPaisFijo()));
            dtoOut.setCountryCode(contacto.getCodigoPaisFijo());
            dtoOut.setRegionalCode(contacto.getCodigoRegionalFijo());
            dtoOut.setExtension(contacto.getAnexoFijo());
        }

        if (dtoOut.getContactDetailType().equalsIgnoreCase("MOBILE")) {
            dtoOut.setId(contacto.getId());
            dtoOut.setNumber(contacto.getNumero());
            dtoOut.setCountryCode(contacto.getCodigoPaisCelular());
            dtoOut.setPhoneCompany(mapOutPhoneCompany(contacto.getOperadorCelular()));
        }
        return dtoOut;
    }

    private PhoneCompanySearch mapOutPhoneCompany(final OperadorCelularType operadorCelular) {
        if (operadorCelular == null) {
            return null;
        }
        PhoneCompanySearch dtoOut = new PhoneCompanySearch();
        dtoOut.setId(operadorCelular.getCodigo());
        dtoOut.setName(operadorCelular.getDescripcion());
        return dtoOut;
    }

    private CountrySearch mapOutCountry(final String paisFijo) {
        if (StringUtils.isEmpty(paisFijo)) {
            return null;
        }
        CountrySearch dtoOut = new CountrySearch();
        dtoOut.setId(paisFijo);
        return dtoOut;
    }

    private Address mapOutAddress(final DireccionType direccion) {
        if (direccion == null) {
            return null;
        }
        Address dtoOut = new Address();
        dtoOut.setId(direccion.getId());
        dtoOut.setLocation(mapOutLocation(direccion.getNombre(), direccion.getReferencia(), direccion.getComponentes()));
        return dtoOut;
    }

    private Location mapOutLocation(final String nombre, final String referencia, final List<ComponentType> componentes) {
        if (StringUtils.isEmpty(nombre) && StringUtils.isEmpty(referencia) && CollectionUtils.isEmpty(componentes)) {
            return null;
        }
        Location dtoOut = new Location();
        dtoOut.setFormattedAddress(nombre);
        dtoOut.setAdditionalInformation(referencia);
        dtoOut.setAddressComponents(mapOutComponents(componentes));
        return dtoOut;
    }

    private List<AddressComponent> mapOutComponents(final List<ComponentType> componentes) {
        if (CollectionUtils.isEmpty(componentes)) {
            return null;
        }
        return componentes.stream().filter(Objects::nonNull).map(this::mapOutComponent).collect(Collectors.toList());
    }

    private AddressComponent mapOutComponent(final ComponentType componentType) {
        if (componentType == null) {
            return null;
        }
        AddressComponent dtoOut = new AddressComponent();
        dtoOut.setComponentTypes(componentType.getTipoComponentes());
        dtoOut.setCode(componentType.getCodigo());
        dtoOut.setName(componentType.getNombre());
        return dtoOut;
    }
}