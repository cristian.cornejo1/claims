package com.bbva.pzic.claims.dao.rest.mapper.common;

import com.bbva.pzic.claims.canonic.ClaimType;
import com.bbva.pzic.claims.canonic.PetitionerType;
import com.bbva.pzic.claims.dao.rest.model.harec.RepresentanteType;
import com.bbva.pzic.claims.dao.rest.model.harec.TitularType;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.bbva.pzic.claims.util.Enums.*;

/**
 * Created on 11/29/2019.
 *
 * @author Entelgy
 */
@Component
public class RestProductClaimMapper {

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    public PetitionerSearch mapOutPetitioner(final TitularType titularType) {
        if (titularType == null) {
            return null;
        }

        String UNKNOWN_BANK_RELATION_TYPE = "UNKNOWN";
        String KNOWN_BANK_RELATION_TYPE = "KNOWN";
        String codigoTitular = titularType.getCodigoCentral();
        String nombreTitular = titularType.getNombres();

        PetitionerSearch petitioner = null;

        if (StringUtils.isNotEmpty(codigoTitular) && StringUtils.isNotEmpty(nombreTitular)) {
            petitioner = mapOutTitularPetitioner(titularType, KNOWN_BANK_RELATION_TYPE);

        } else if (StringUtils.isEmpty(codigoTitular) && StringUtils.isNotEmpty(nombreTitular)) {
            petitioner = mapOutTitularPetitioner(titularType, UNKNOWN_BANK_RELATION_TYPE);
        }
        return petitioner;
    }

    public PetitionerSearch mapOutRepresentativePetitioner(final RepresentanteType representanteType) {
        if (representanteType == null) {
            return null;
        }

        String UNKNOWN_BANK_RELATION_TYPE = "UNKNOWN";
        String KNOWN_BANK_RELATION_TYPE = "KNOWN";
        String codigoRepresentante = representanteType.getCodigoCentral();
        String nombreRepresentante = representanteType.getNombres();

        PetitionerSearch petitioner = null;

        if (StringUtils.isNotEmpty(codigoRepresentante) && StringUtils.isNotEmpty(nombreRepresentante)) {
            petitioner = mapOutRepresentantePetitioner(representanteType, KNOWN_BANK_RELATION_TYPE);

        } else if (StringUtils.isEmpty(codigoRepresentante) && StringUtils.isNotEmpty(nombreRepresentante)) {
            petitioner = mapOutRepresentantePetitioner(representanteType, UNKNOWN_BANK_RELATION_TYPE);
        }
        return petitioner;
    }


    private PetitionerSearch mapOutRepresentantePetitioner(final RepresentanteType representanteType, final String relationType) {
        PetitionerSearch petitionerSearch = new PetitionerSearch();
        petitionerSearch.setBankRelationType(relationType);
        petitionerSearch.setId(representanteType.getCodigoCentral());
        petitionerSearch.setFirstName(representanteType.getNombres());
        petitionerSearch.setLastName(representanteType.getApellidoPaterno());
        petitionerSearch.setSecondLastName(representanteType.getApellidoMaterno());
        petitionerSearch.setPetitionerType(new PetitionerType());
        petitionerSearch.getPetitionerType().setId("AUTHORIZED");
        return petitionerSearch;
    }

    private PetitionerSearch mapOutTitularPetitioner(final TitularType titularType, final String relationType) {
        PetitionerSearch petitionerSearch = new PetitionerSearch();
        petitionerSearch.setId(titularType.getCodigoCentral());
        petitionerSearch.setFirstName(titularType.getNombres());
        petitionerSearch.setLastName(titularType.getApellidoPaterno());
        petitionerSearch.setSecondLastName(titularType.getApellidoMaterno());
        petitionerSearch.setBankRelationType(relationType);
        petitionerSearch.setPetitionerType(new PetitionerType());
        petitionerSearch.getPetitionerType().setId("HOLDER");
        return petitionerSearch;
    }

    public ClaimType mapOutClaimTypeEnum(final ClaimType type) {
        if (type != null) {
            type.setId(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_CLAIMTYPE_ID, type.getId()));
        }
        return type;
    }

    public ResolutionSearch mapOutResolutionEnum(final ResolutionSearch resolution) {
        if (resolution != null) {
            resolution.setId(translator.translateBackendEnumValueStrictly("claims.resolution.id", resolution.getId()));
        }
        return resolution;
    }

    public StatusSearch mapOutStatusEnum(final StatusSearch status) {
        if (status != null) {
            status.setId(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_STATUS_ID, status.getId()));
        }
        return status;
    }

    public PersonType mapOutPersonTypeEnum(final PersonType personType) {
        if (personType != null) {
            personType.setId(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PERSONTYPE_ID, personType.getId()));
        }
        return personType;
    }
}
