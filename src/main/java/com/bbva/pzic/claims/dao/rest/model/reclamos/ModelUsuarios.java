package com.bbva.pzic.claims.dao.rest.model.reclamos;

/**
 * Created on 24/02/2021.
 *
 * @author Entelgy
 */
public class ModelUsuarios {

    private String codigo;

    private String nombre;

    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
