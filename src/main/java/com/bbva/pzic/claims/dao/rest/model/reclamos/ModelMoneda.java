package com.bbva.pzic.claims.dao.rest.model.reclamos;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelMoneda {
    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
