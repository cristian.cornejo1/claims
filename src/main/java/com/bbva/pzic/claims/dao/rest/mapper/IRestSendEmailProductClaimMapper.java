package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.InputSendEmailProductClaim;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;

import java.util.Map;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy
 */
public interface IRestSendEmailProductClaimMapper {

    Map<String, String> mapInHeader(InputSendEmailProductClaim input);

    RequestType mapInBody(InputSendEmailProductClaim input);
}
