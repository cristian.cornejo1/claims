package com.bbva.pzic.claims.dao.rest;

import com.bbva.pzic.claims.business.dto.DTOIntProductClaim;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestDataType;
import com.bbva.pzic.claims.dao.rest.mapper.IRestCreateProductClaimMapper;
import com.bbva.pzic.claims.util.connection.rest.RestPostConnectionHeader;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import java.util.Arrays;
import java.util.List;

import static com.bbva.pzic.claims.facade.RegistryIds.SN_REGISTRY_ID_CREATE_PRODUCT_CLAIM;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
@Component
public class RestCreateProductClaim extends RestPostConnectionHeader<RequestDataType> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1920052.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1920052.backend.proxy";

    @Autowired
    private IRestCreateProductClaimMapper mapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
    }

    public ProductClaimTransaction perform(final DTOIntProductClaim dtoInt) {
        List<String> obfuscationMask = Arrays.asList("numero", "nombre", "referencia", "id", "correo", "doi", "nombres", "apellidoPaterno", "apellidoMaterno");
        return mapper.mapOut(connect(URL_PROPERTY, mapper.mapHeaderIn(dtoInt), mapper.mapInBody(dtoInt), SN_REGISTRY_ID_CREATE_PRODUCT_CLAIM, obfuscationMask));
    }
}
