package com.bbva.pzic.claims.dao.rest.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestDataType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.RestCreateProductClaim;
import com.bbva.pzic.claims.dao.rest.mock.stubs.ResultadoModelsStubs;
import com.bbva.pzic.claims.util.Errors;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * Created on 22/11/2019.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestCreateProductClaimMock extends RestCreateProductClaim {

    public static final String EMPTY_TEST = "999";

    @Override
    public ResponseType connect(final String urlPropertyValue, final Map<String, String> headerParam, final RequestDataType entityPayload, final String registryId) {
        String productId = entityPayload.getTaxonomia().getProducto().getCodigo();

        if (EMPTY_TEST.equals(productId)) {
            return new ResponseType();
        }

        try {
            return ResultadoModelsStubs.INSTANCE.getResponseType();
        } catch (IOException ex) {
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR, ex);
        }
    }
}
