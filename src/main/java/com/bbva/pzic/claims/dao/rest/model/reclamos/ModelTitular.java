package com.bbva.pzic.claims.dao.rest.model.reclamos;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelTitular {
    private String tipoDoi;
    private String doi;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String codigoCentral;
    private ModelSegmento segmento;

    public String getTipoDoi() {
        return tipoDoi;
    }

    public void setTipoDoi(String tipoDoi) {
        this.tipoDoi = tipoDoi;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCodigoCentral() {
        return codigoCentral;
    }

    public void setCodigoCentral(String codigoCentral) {
        this.codigoCentral = codigoCentral;
    }

    public ModelSegmento getSegmento() {
        return segmento;
    }

    public void setSegmento(ModelSegmento segmento) {
        this.segmento = segmento;
    }
}
