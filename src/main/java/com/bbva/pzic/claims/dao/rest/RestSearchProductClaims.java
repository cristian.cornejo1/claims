package com.bbva.pzic.claims.dao.rest;

import com.bbva.pzic.claims.business.dto.InputSearchProductClaims;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.mapper.IRestSearchProductClaimsMapper;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.util.connection.rest.RestPostConnectionHeader;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Arrays;

import static com.bbva.pzic.claims.facade.RegistryIds.SMC_REGISTRY_ID_OF_SEARCH_PRODUCT_CLAIMS;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
@Component
public class RestSearchProductClaims extends RestPostConnectionHeader<RequestType> {

    private static final String URL_PROPERTY = "servicing.smc.configuration.SMCPE1920054.backend.url";
    private static final String URL_PROPERTY_PROXY = "servicing.smc.configuration.SMCPE1920054.backend.proxy";

    @Autowired
    private IRestSearchProductClaimsMapper mapper;

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @PostConstruct
    public void init() {
        useProxy = Boolean.parseBoolean(translator.translate(URL_PROPERTY_PROXY, "false"));
    }

    public List<ProductClaims> invoke(final InputSearchProductClaims dtoInt) {
        List<String> obfuscationFields = Arrays.asList("numeroDoi", "nombres","apellidoMaterno","apellidoPaterno", "nombre", "referencia",
                "correo","numero");
        final ResponseType result = connect(URL_PROPERTY, mapper.mapIn(dtoInt), SMC_REGISTRY_ID_OF_SEARCH_PRODUCT_CLAIMS, obfuscationFields);
        return mapper.mapOut(result);
    }
}
