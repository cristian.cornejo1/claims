package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.DTOIntProductClaim;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestDataType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;

import java.util.Map;

/**
 * Created on 22/11/2019.
 *
 * @author Entelgy
 */
public interface IRestCreateProductClaimMapper {

    Map<String, String> mapHeaderIn(DTOIntProductClaim input);

    RequestDataType mapInBody(DTOIntProductClaim input);

    ProductClaimTransaction mapOut(ResponseType model);
}
