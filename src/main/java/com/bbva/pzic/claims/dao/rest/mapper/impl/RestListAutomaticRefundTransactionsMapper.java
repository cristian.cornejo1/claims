package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.business.dto.InputListAutomaticRefundTransactions;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.Petitioner;
import com.bbva.pzic.claims.canonic.ReasonRefund;
import com.bbva.pzic.claims.canonic.StatusRefund;
import com.bbva.pzic.claims.dao.rest.model.reclamos.*;
import com.bbva.pzic.claims.dao.rest.mapper.IRestListAutomaticRefundTransactionsMapper;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.bbva.pzic.claims.util.Constants.CODIGO_CENTRAL;
import static com.bbva.pzic.claims.util.Constants.ID_CANAL;
import static com.bbva.pzic.claims.util.Enums.AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID;
import static com.bbva.pzic.claims.util.Enums.AUTOMATICREFUNDTRANSACTIONS_STATUS_ID;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestListAutomaticRefundTransactionsMapper implements IRestListAutomaticRefundTransactionsMapper {

    private static final Log LOG = LogFactory.getLog(RestListAutomaticRefundTransactionsMapper.class);

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public HashMap<String, String> mapInQuery(final InputListAutomaticRefundTransactions input) {
        LOG.info("... called method RestListAutomaticRefundTransactionsMapper.mapInQuery ...");
        final HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put(CODIGO_CENTRAL, input.getCustomerId());
        return queryParams;
    }

    @Override
    public Map<String, String> mapInHeader(final InputListAutomaticRefundTransactions input) {
        LOG.info("... called method RestListAutomaticRefundTransactionsMapper.mapInHeader ...");
        final Map<String, String> headerParams = new HashMap<>();
        headerParams.put(ID_CANAL, input.getApp());
        return headerParams;
    }

    @Override
    public List<AutomaticRefundTransaction> mapOut(final ModelResultadoResponses output) {
        LOG.info("... called method RestListAutomaticRefundTransactionsMapper.mapOut ...");
        if (output == null || CollectionUtils.isEmpty(output.getResultado())) {
            return null;
        }
        return output.getResultado().stream().filter(Objects::nonNull).map(this::mapOutAutomaticRefundTransaction).
                collect(Collectors.toList());
    }

    private AutomaticRefundTransaction mapOutAutomaticRefundTransaction(final ModelResultado modelResultado) {
        if (modelResultado == null) {
            return null;
        }
        AutomaticRefundTransaction dtoOut = new AutomaticRefundTransaction();
        dtoOut.setId(modelResultado.getNumero());
        dtoOut.setCreationDate(modelResultado.getFecha());
        dtoOut.setReason(mapOutReason(modelResultado.getMotivo()));
        dtoOut.setStatus(mapOutStatus(modelResultado.getEstado(), modelResultado.getEstadoAtencion()));
        dtoOut.setBusinessTeam(mapOutBusinessTeam(modelResultado.getEquipoResponsable()));
        dtoOut.setBusinessAgents(mapOutBusinessAgents(modelResultado.getUsuarios()));
        dtoOut.setPetitioner(mapOutPetitioner(modelResultado.getTitular(), modelResultado.getSegmento()));
        return dtoOut;
    }

    private Petitioner mapOutPetitioner(final ModelTitular titular, final ModelSegmento segmento) {
        if (titular == null) {
            return null;
        }
        Petitioner dtoOut = new Petitioner();
        dtoOut.setFirstName(titular.getNombres());
        dtoOut.setLastName(titular.getApellidoPaterno());
        dtoOut.setSegment(mapOutSegment(segmento));
        return dtoOut;
    }

    private Segment mapOutSegment(final ModelSegmento segmento) {
        if (segmento == null) {
            return null;
        }
        Segment dtoOut = new Segment();
        dtoOut.setId(segmento.getCodigo());
        dtoOut.setName(segmento.getNombre());
        return dtoOut;
    }

    private List<BusinessAgents> mapOutBusinessAgents(final List<ModelUsuarios> usuarios) {
        if (CollectionUtils.isEmpty(usuarios)) {
            return null;
        }
        return usuarios.stream().filter(Objects::nonNull).map(this::mapOutBusinessAgent).collect(Collectors.toList());
    }

    private BusinessAgents mapOutBusinessAgent(final ModelUsuarios modelUsuarios) {
        if (modelUsuarios == null) {
            return null;
        }
        BusinessAgents dtoOut = new BusinessAgents();
        dtoOut.setId(modelUsuarios.getCodigo());
        dtoOut.setFullName(modelUsuarios.getNombre());
        dtoOut.setAgentType(translator.translateBackendEnumValueStrictly(AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID,
                modelUsuarios.getTipo()));
        return dtoOut;
    }

    private BusinessTeam mapOutBusinessTeam(final ModelEquipoResponsable equipoResponsable) {
        if (equipoResponsable == null) {
            return null;
        }
        BusinessTeam dtoOut = new BusinessTeam();
        dtoOut.setId(equipoResponsable.getCodigo());
        dtoOut.setName(equipoResponsable.getNombre());
        return dtoOut;
    }

    private StatusRefund mapOutStatus(final ModelEstado estado, final ModelEstadoAtencion estadoAtencion) {
        if (estado == null) {
            return null;
        }
        StatusRefund dtoOut = new StatusRefund();
        dtoOut.setId(translator.translateBackendEnumValueStrictly(AUTOMATICREFUNDTRANSACTIONS_STATUS_ID,
                estado.getCodigo()));
        dtoOut.setName(estado.getNombre());
        dtoOut.setSubStatus(mapOutSubStatus(estadoAtencion));
        return dtoOut;
    }

    private SubStatus mapOutSubStatus(final ModelEstadoAtencion estadoAtencion) {
        if (estadoAtencion == null) {
            return null;
        }
        SubStatus dtoOut = new SubStatus();
        dtoOut.setId(estadoAtencion.getCodigo());
        dtoOut.setName(estadoAtencion.getNombre());
        return dtoOut;
    }

    private ReasonRefund mapOutReason(final ModelMotivo motivo) {
        if (motivo == null) {
            return null;
        }
        ReasonRefund dtoOut = new ReasonRefund();
        dtoOut.setId(motivo.getCodigo());
        dtoOut.setName(motivo.getNombre());
        return dtoOut;
    }
}