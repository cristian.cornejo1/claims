package com.bbva.pzic.claims.dao.rest.model.reclamos;

import java.math.BigDecimal;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelProductoSeguro {
    private String codigo;
    private ModelMoneda moneda;
    private BigDecimal importe;
    private String producto;
    private String id;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public ModelMoneda getMoneda() {
        return moneda;
    }

    public void setMoneda(ModelMoneda moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}