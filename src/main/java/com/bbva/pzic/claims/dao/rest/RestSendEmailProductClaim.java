package com.bbva.pzic.claims.dao.rest;

import com.bbva.pzic.claims.business.dto.InputSendEmailProductClaim;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.mapper.IRestSendEmailProductClaimMapper;
import com.bbva.pzic.claims.util.connection.rest.RestPostConnectionHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import static com.bbva.pzic.claims.facade.RegistryIds.SN_REGISTRY_SEND_EMAIL_PRODUCT_CLAIM;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
@Component
public class RestSendEmailProductClaim extends RestPostConnectionHeader<RequestType> {

    private static final String SEND_EMAIL_PRODUCT_CLAIMS_URL = "servicing.smc.configuration.SMCPE1920055.backend.url";

    @Autowired
    private IRestSendEmailProductClaimMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = false;
    }

    public void invoke(final InputSendEmailProductClaim input) {
        connect(SEND_EMAIL_PRODUCT_CLAIMS_URL, mapper.mapInHeader(input),
                mapper.mapInBody(input), SN_REGISTRY_SEND_EMAIL_PRODUCT_CLAIM);
    }
}
