package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.business.dto.DTOIntSituation;
import com.bbva.pzic.claims.business.dto.DTOIntStatus;
import com.bbva.pzic.claims.business.dto.InputModifyAutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.conformidad.*;
import com.bbva.pzic.claims.dao.rest.mapper.IRestModifyPartialAutomaticRefundTransactionMapper;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;
import com.bbva.pzic.claims.facade.v0.dto.Situation;
import com.bbva.pzic.claims.facade.v0.dto.Status;
import com.bbva.pzic.claims.facade.v0.dto.SubStatus;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.bbva.pzic.claims.util.Enums.AUTOMATICREFUNDTRANSACTIONS_STATUS_ID;

@Component
public class RestModifyPartialAutomaticRefundTransactionMapper implements IRestModifyPartialAutomaticRefundTransactionMapper {

    private static final Log LOG = LogFactory.getLog(RestModifyPartialAutomaticRefundTransactionMapper.class);

    @Autowired
    Translator translator;

    @Override
    public Map<String, String> mapHeaderIn(final InputModifyAutomaticRefundTransaction input) {
        LOG.info("... called method RestModifyPartialAutomaticRefundTransactionMapper.mapHeaderIn ...");
        if(input == null){
            return null;
        }
        final Map<String, String> map = new HashMap<>();
        map.put("user", input.getUser());
        map.put("idCanal", input.getIdCanal());
        return map;
    }

    @Override
    public PeticionConformidad mapBodyIn(final InputModifyAutomaticRefundTransaction input) {
        LOG.info("... called method RestModifyPartialAutomaticRefundTransactionMapper.mapBodyIn ...");
        if (input == null){
            return null;
        }
        PeticionConformidad inputBody = new PeticionConformidad();
        inputBody.setNumero(input.getAutomaticRefundTransactionId());
        inputBody.setConformidadAtencion(FunctionUtils.convertBooleanToInteger(input.getPartialTransaction().getCustomerConfirmation()));
        inputBody.setEstadoAtencion(mapBodyInEstadoAtencion(input.getPartialTransaction().getStatus()));
        inputBody.setSituacion(mapBodyInSituacion(input.getPartialTransaction().getSituation()));
        return inputBody;
    }

    private Situacion mapBodyInSituacion(final DTOIntSituation situation) {
        if (situation == null){
            return null;
        }
        Situacion input = new Situacion();
        input.setCodigo(situation.getId());
        return  input;
    }

    private Atencion mapBodyInEstadoAtencion(final DTOIntStatus status) {
        if(status == null || status.getSubStatus() == null){
            return null;
        }
        Atencion input = new Atencion();
        input.setCodigo(status.getSubStatus().getId());
        return input;
    }

    @Override
    public PartialTransaction mapOut(final ResultadoConformidad response) {
        LOG.info("... called method RestModifyPartialAutomaticRefundTransactionMapper.mapOut ...");
        if (response.getResultado() == null){
            return null;
        }
        PartialTransaction output = new PartialTransaction();
        output.setStatus(mapOutStatus(response.getResultado().getEstado(),response.getResultado().getEstadoAtencion()));
        output.setSituation(mapOutSituation(response.getResultado().getSituacion()));
        return output;
    }

    private Situation mapOutSituation(final Situacion situacion) {
        if (situacion == null){
            return null;
        }
        Situation output = new Situation();
        output.setId(situacion.getCodigo());
        output.setName(situacion.getNombre());
        return output;
    }

    private Status mapOutStatus(final Estado estado, final Atencion estadoAtencion) {
        if (estado == null && estadoAtencion == null){
            return null;
        }
        Status output = new Status();
        if(estado == null){
            output.setSubStatus(mapOutSubStatus(estadoAtencion));
            return output;
        }
        output.setId(translator.translateBackendEnumValueStrictly(AUTOMATICREFUNDTRANSACTIONS_STATUS_ID,estado.getCodigo()));
        output.setName(estado.getNombre());
        output.setSubStatus(mapOutSubStatus(estadoAtencion));
        return output;
    }

    private SubStatus mapOutSubStatus(final Atencion estadoAtencion) {
        if (estadoAtencion == null){
            return null;
        }
        SubStatus output = new SubStatus();
        output.setId(estadoAtencion.getCodigo());
        output.setName(estadoAtencion.getNombre());
        return output;
    }
}