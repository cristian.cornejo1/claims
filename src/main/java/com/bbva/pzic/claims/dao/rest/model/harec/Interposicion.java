package com.bbva.pzic.claims.dao.rest.model.harec;

public class Interposicion {

    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
