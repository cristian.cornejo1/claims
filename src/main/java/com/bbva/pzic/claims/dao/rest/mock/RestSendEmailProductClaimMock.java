package com.bbva.pzic.claims.dao.rest.mock;

import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.RestSendEmailProductClaim;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy
 */
@Primary
@Component
public class RestSendEmailProductClaimMock extends RestSendEmailProductClaim {

    @Override
    public ResponseType connect(final String urlPropertyValue, final Map<String, String> headerParam, final RequestType entityPayload, final String registryId) {
        return null;
    }
}
