package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.business.dto.DTOIntContactSendEmail;
import com.bbva.pzic.claims.business.dto.DTOIntDocument;
import com.bbva.pzic.claims.business.dto.InputSendEmailProductClaim;
import com.bbva.pzic.claims.dao.rest.model.harec.AdjuntosType;
import com.bbva.pzic.claims.dao.rest.model.harec.CorreosType;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.TitularType;
import com.bbva.pzic.claims.dao.rest.mapper.IRestSendEmailProductClaimMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy
 */
@Component
public class RestSendEmailProductClaimMapper implements IRestSendEmailProductClaimMapper {

    private static final Log LOG = LogFactory.getLog(RestSendEmailProductClaimMapper.class);

    private Translator translator;

    @Autowired
    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    @Override
    public Map<String, String> mapInHeader(final InputSendEmailProductClaim input) {
        LOG.info("... called method RestSendEmailProductClaimMapper.mapInHeader ...");
        Map<String, String> headerParams = new HashMap<>();
        headerParams.put("canal", input.getAap());
        return headerParams;
    }

    @Override
    public RequestType mapInBody(final InputSendEmailProductClaim input) {
        LOG.info("... called method RestSendEmailProductClaimMapper.mapInBody ...");
        RequestType requestType = new RequestType();
        requestType.setNumero(input.getProductClaimId());
        requestType.setTitular(mapInTitular(input));

        if (input.getContact() != null) {
            requestType.getCorreos().add(mapInCorreos(input.getContact()));
        }
        requestType.setTipoContacto(mapInTipoContacto(input.getContact()));
        if (input.getDocuments() != null) {
            requestType.getAdjuntos().addAll(mapInAdjuntos(input.getDocuments()));
        }
        return requestType;
    }

    private TitularType mapInTitular(final InputSendEmailProductClaim input) {
        if (StringUtils.isEmpty(input.getClientId())) {
            return null;
        }
        TitularType titularType = new TitularType();
        titularType.setCodigoCentral(input.getClientId());
        return titularType;
    }

    private String mapInTipoContacto(final DTOIntContactSendEmail contact) {
        if (contact == null) {
            return null;
        }
        return translator.translateFrontendEnumValueStrictly("claim.contactDetails.contactType.id", contact.getContactDetailType());
    }

    private CorreosType mapInCorreos(final DTOIntContactSendEmail contact) {
        if (contact == null) {
            return null;
        }
        CorreosType correosType = new CorreosType();
        correosType.setId(contact.getId());
        correosType.setCorreo(contact.getAddress());
        return correosType;
    }

    private List<AdjuntosType> mapInAdjuntos(final List<DTOIntDocument> dtoIntDocuments) {
        if (CollectionUtils.isEmpty(dtoIntDocuments)) {
            return null;
        }

        List<AdjuntosType> adjuntosType = new ArrayList<>();
        adjuntosType.addAll(dtoIntDocuments.stream().filter(Objects::nonNull).map(this::mapInAdjunto).collect(Collectors.toList()));
        return adjuntosType;
    }

    private AdjuntosType mapInAdjunto(final DTOIntDocument dtoIntDocument) {
        AdjuntosType adjuntosType = new AdjuntosType();
        adjuntosType.setId(dtoIntDocument.getId());
        return adjuntosType;
    }
}
