package com.bbva.pzic.claims.dao.rest.model.reclamos;

import java.util.List;

/**
 * Created on 22/02/2021.
 *
 * @author Entelgy
 */
public class ModelRequerimientos {
    private List<ModelContacto> contactos;

    public List<ModelContacto> getContactos() {
        return contactos;
    }

    public void setContactos(List<ModelContacto> contactos) {
        this.contactos = contactos;
    }
}
