package com.bbva.pzic.claims.dao.rest.model.reclamos;

public class ModelContacto {
    private String tipoContacto;
    private String numero;
    private String tipoFijo;
    private String paisFijo;
    private String codigoPaisFijo;
    private String codigoRegionalFijo;
    private String anexoFijo;
    private ModelOperadorCelular operadorCelular;
    private String codigoPaisCelular;
    private String correo;
    private String notificacionesCorreo;
    private String id;

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipoFijo() {
        return tipoFijo;
    }

    public void setTipoFijo(String tipoFijo) {
        this.tipoFijo = tipoFijo;
    }

    public String getPaisFijo() {
        return paisFijo;
    }

    public void setPaisFijo(String paisFijo) {
        this.paisFijo = paisFijo;
    }

    public String getCodigoPaisFijo() {
        return codigoPaisFijo;
    }

    public void setCodigoPaisFijo(String codigoPaisFijo) {
        this.codigoPaisFijo = codigoPaisFijo;
    }

    public String getCodigoRegionalFijo() {
        return codigoRegionalFijo;
    }

    public void setCodigoRegionalFijo(String codigoRegionalFijo) {
        this.codigoRegionalFijo = codigoRegionalFijo;
    }

    public String getAnexoFijo() {
        return anexoFijo;
    }

    public void setAnexoFijo(String anexoFijo) {
        this.anexoFijo = anexoFijo;
    }

    public ModelOperadorCelular getOperadorCelular() {
        return operadorCelular;
    }

    public void setOperadorCelular(ModelOperadorCelular operadorCelular) {
        this.operadorCelular = operadorCelular;
    }

    public String getCodigoPaisCelular() {
        return codigoPaisCelular;
    }

    public void setCodigoPaisCelular(String codigoPaisCelular) {
        this.codigoPaisCelular = codigoPaisCelular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNotificacionesCorreo() {
        return notificacionesCorreo;
    }

    public void setNotificacionesCorreo(String notificacionesCorreo) {
        this.notificacionesCorreo = notificacionesCorreo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
