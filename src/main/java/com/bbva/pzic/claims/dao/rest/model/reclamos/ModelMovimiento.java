package com.bbva.pzic.claims.dao.rest.model.reclamos;

import java.math.BigDecimal;

public class ModelMovimiento {
    private String numero;
    private String fecha;
    private String descripcion;
    private String tipo;
    private ModelMoneda moneda;
    private BigDecimal importe;
    private ModelEstablecimiento establecimiento;
    private ModelEstado estado;
    private ModelTipoOperacion tipoOperacion;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ModelMoneda getMoneda() {
        return moneda;
    }

    public void setMoneda(ModelMoneda moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public ModelEstablecimiento getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(ModelEstablecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }

    public ModelEstado getEstado() {
        return estado;
    }

    public void setEstado(ModelEstado estado) {
        this.estado = estado;
    }

    public ModelTipoOperacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(ModelTipoOperacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
}
