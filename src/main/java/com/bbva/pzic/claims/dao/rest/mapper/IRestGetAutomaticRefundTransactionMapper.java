package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.InputGetAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface IRestGetAutomaticRefundTransactionMapper {
    HashMap<String, String> mapPathIn(InputGetAutomaticRefundTransaction input);

    Map<String, String> mapHeaderIn(InputGetAutomaticRefundTransaction input);

    AutomaticRefundTransaction mapOut(ModelResultadoResponse model);
}