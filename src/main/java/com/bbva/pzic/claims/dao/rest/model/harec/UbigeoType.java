package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ubigeoType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="ubigeoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codUbigeo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ubigeoType", propOrder = {
        "codUbigeo"
})
public class UbigeoType {

    @XmlElement(required = true)
    protected String codUbigeo;

    /**
     * Obtiene el valor de la propiedad codUbigeo.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCodUbigeo() {
        return codUbigeo;
    }

    /**
     * Define el valor de la propiedad codUbigeo.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCodUbigeo(String value) {
        this.codUbigeo = value;
    }

}
