package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.InputListAutomaticRefundTransactions;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponses;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface IRestListAutomaticRefundTransactionsMapper {

    HashMap<String, String> mapInQuery(InputListAutomaticRefundTransactions input);

    Map<String, String> mapInHeader(InputListAutomaticRefundTransactions input);

    List<AutomaticRefundTransaction> mapOut(ModelResultadoResponses output);
}