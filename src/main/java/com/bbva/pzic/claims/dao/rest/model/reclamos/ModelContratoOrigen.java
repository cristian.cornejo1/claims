package com.bbva.pzic.claims.dao.rest.model.reclamos;

public class ModelContratoOrigen {
    private String tipo;
    private ModelTarjeta tarjeta;
    private String numeroTipo;
    private String numero;
    private String subTipo;

    public ModelTarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(ModelTarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getNumeroTipo() {
        return numeroTipo;
    }

    public void setNumeroTipo(String numeroTipo) {
        this.numeroTipo = numeroTipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSubTipo() {
        return subTipo;
    }

    public void setSubTipo(String subTipo) {
        this.subTipo = subTipo;
    }
}
