package com.bbva.pzic.claims.dao.rest;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.jee.arq.spring.core.rest.RestConnectorType;
import com.bbva.jee.arq.spring.core.rest.requests.RestRequest;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.claims.business.dto.DTOIntAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponse;
import com.bbva.pzic.claims.dao.rest.mapper.IRestValidateAutomaticRefundTransactionMapper;
import com.bbva.pzic.routine.commons.utils.rest.templates.templates.JSONDefaultRestConnectionProcessor;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestValidateAutomaticRefundTransaction extends JSONDefaultRestConnectionProcessor<DTOIntAutomaticRefundTransaction, AutomaticRefundTransaction, ModelResultadoResponse, ModelResultadoResponse> {

    @Resource(name = "restValidateAutomaticRefundTransactionMapper")
    private IRestValidateAutomaticRefundTransactionMapper mapper;

    public RestValidateAutomaticRefundTransaction() {
        super(RestConnectorType.BASIC, new TypeReference<ModelResultadoResponse>() {},
                new TypeReference<ModelResultadoResponse>() {},
                "servicing.smc.configuration.SMCPE1810334.backend.url",
                "servicing.smc.configuration.SMCPE1810334.backend.proxy",
                "servicing.smc.configuration.SMCPE1810334.harec_bbva_resuelve.backendId");
    }

    @Override
    protected RestRequest mapInput(final DTOIntAutomaticRefundTransaction dtoIntAutomaticRefundTransaction, final String url, final boolean useProxy) {
        return new RestRequest.Builder("POST", url)
                .useProxy(useProxy)
                .headers(mapper.mapHeaderIn(dtoIntAutomaticRefundTransaction))
                .payload(convertObjectToJSON(mapper.mapIn(dtoIntAutomaticRefundTransaction)))
                .build();
    }

    @Override
    protected AutomaticRefundTransaction mapOutput(final RestConnectorResponse restConnectorResponse, final ModelResultadoResponse modelResultadoResponse, final DTOIntAutomaticRefundTransaction dtoIntAutomaticRefundTransaction) {
        if (modelResultadoResponse == null) {
            return null;
        }

        return mapper.mapOut(modelResultadoResponse);
    }

    @Override
    protected void throwErrorIfExists(final RestConnectorResponse restConnectorResponse, final ModelResultadoResponse modelResponse) {
        if (modelResponse == null || CollectionUtils.isEmpty(modelResponse.getMessages())) {
            return;
        }

        Message message = modelResponse.getMessages().get(0);
        super.throwErrorIfExists(message.getCode(), message.getMessage(), "SMCPE1810334", restConnectorResponse.getStatusCode());
    }
}