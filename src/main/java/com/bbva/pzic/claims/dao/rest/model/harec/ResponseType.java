package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para responseType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="responseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         *         &lt;element name="requerimiento" type="{/harec-rest-aso/consulta/historico}requerimientosType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responseType", propOrder = {
        "requerimientos",
        "requerimiento",
        "titular",
        "representante"
})
public class ResponseType {

    protected List<RequerimientosType> requerimientos;
    @XmlElement(required = true)
    protected RequerimientoType requerimiento;
    @XmlElement(required = true)
    protected DireccionpPersonaType titular;
    @XmlElement(required = true)
    protected DireccionpPersonaType representante;
    protected String numeroAtencionLinea;

    /**
     * Gets the value of the requerimientos property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requerimientos property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequerimientos().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequerimientosType }
     *
     *
     */
    public List<RequerimientosType> getRequerimientos() {
        if (requerimientos == null) {
            requerimientos = new ArrayList<>();
        }
        return this.requerimientos;
    }

    /**
     * Obtiene el valor de la propiedad requermiento.
     *
     * @return possible object is
     * {@link RequerimientoType }
     */
    public RequerimientoType getRequerimiento() {
        return requerimiento;
    }

    /**
     * Define el valor de la propiedad requermiento.
     *
     * @param value allowed object is
     *              {@link RequerimientoType }
     */
    public void setRequerimiento(RequerimientoType value) {
        this.requerimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad titular.
     *
     * @return possible object is
     * {@link DireccionpPersonaType }
     */
    public DireccionpPersonaType getTitular() {
        return titular;
    }

    /**
     * Define el valor de la propiedad titular.
     *
     * @param value allowed object is
     *              {@link DireccionpPersonaType }
     */
    public void setTitular(DireccionpPersonaType value) {
        this.titular = value;
    }

    /**
     * Obtiene el valor de la propiedad representante.
     *
     * @return possible object is
     * {@link DireccionpPersonaType }
     */
    public DireccionpPersonaType getRepresentante() {
        return representante;
    }

    /**
     * Define el valor de la propiedad representante.
     *
     * @param value allowed object is
     *              {@link DireccionpPersonaType }
     */
    public void setRepresentante(DireccionpPersonaType value) {
        this.representante = value;
    }

    public String getNumeroAtencionLinea() {
        return numeroAtencionLinea;
    }

    public void setNumeroAtencionLinea(String numeroAtencionLinea) {
        this.numeroAtencionLinea = numeroAtencionLinea;
    }
}
