package com.bbva.pzic.claims.dao.rest.model.conformidad;

public class Resultado {

    private Estado estado;

    private Atencion estadoAtencion;

    private Situacion situacion;

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Atencion getEstadoAtencion() {
        return estadoAtencion;
    }

    public void setEstadoAtencion(Atencion estadoAtencion) {
        this.estadoAtencion = estadoAtencion;
    }

    public Situacion getSituacion() {
        return situacion;
    }

    public void setSituacion(Situacion situacion) {
        this.situacion = situacion;
    }
}
