package com.bbva.pzic.claims.dao.impl;

import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.dao.IClaimsDAO;
import com.bbva.pzic.claims.dao.rest.*;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class ClaimsDAO implements IClaimsDAO {

    private static final Log LOG = LogFactory.getLog(ClaimsDAO.class);

    @Autowired
    private RestListAutomaticRefundTransactions restListAutomaticRefundTransactions;
    @Autowired
    private RestGetAutomaticRefundTransaction restGetAutomaticRefundTransaction;
    @Autowired
    private RestCreateProductClaim restCreateProductClaim;
    @Autowired
    private RestValidateAutomaticRefundTransaction restValidateAutomaticRefundTransaction;
    @Autowired
    private RestSearchProductClaims restSearchProductClaims;
    @Autowired
    private RestGetProductClaim restGetProductClaim;
    @Autowired
    private RestSendEmailProductClaim restSendEmailProductClaim;
    @Autowired
    private RestModifyPartialAutomaticRefundTransaction restModifyPartialAutomaticRefundTransaction;

    @Override
    public List<AutomaticRefundTransaction> listAutomaticRefundTransactions(
            final InputListAutomaticRefundTransactions input) {
        LOG.info("... Invoking method ClaimsDAO.listAutomaticRefundTransactions ...");
        return restListAutomaticRefundTransactions.invoke(input);
    }

    @Override
    public AutomaticRefundTransaction getAutomaticRefundTransaction(
            final InputGetAutomaticRefundTransaction input) {
        LOG.info("... Invoking method ClaimsDAO.getAutomaticRefundTransaction ...");
        return restGetAutomaticRefundTransaction.invoke(input);
    }


    @Override
    public ProductClaimTransaction createProductClaim(final DTOIntProductClaim dtoInt) {
        LOG.info("... Invoking method ClaimsDAO.createProductClaim ...");
        return restCreateProductClaim.perform(dtoInt);
    }

    @Override
    public List<ProductClaims> searchProductClaims(final InputSearchProductClaims dtoInt) {
        LOG.info("... Invoking method ClaimsDAO.searchProductClaims ...");
        return restSearchProductClaims.invoke(dtoInt);
    }

    @Override
    public ProductClaims getProductClaim(final InputGetProductClaim input) {
        LOG.info("... Invoking method ClaimsDAO.getProductClaim ...");
        return restGetProductClaim.invoke(input);
    }

    @Override
    public void sendEmailProductClaim(final InputSendEmailProductClaim input) {
        LOG.info("... Invoking method ClaimsDAO.sendEmailProductClaim ...");
        restSendEmailProductClaim.invoke(input);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public AutomaticRefundTransaction validateAutomaticRefundTransaction(
            final DTOIntAutomaticRefundTransaction dtoInt) {
        LOG.info("... Invoking method ClaimsDAO.validateAutomaticRefundTransaction ...");
        return restValidateAutomaticRefundTransaction.perform(dtoInt);
    }
    @Override
    public PartialTransaction modifyPartialAutomaticRefundTransaction(final InputModifyAutomaticRefundTransaction input) {
        LOG.info("... Invoking method ClaimsDAO.modifyPartialAutomaticRefundTransaction ...");
        return restModifyPartialAutomaticRefundTransaction.perform(input);
    }
}
