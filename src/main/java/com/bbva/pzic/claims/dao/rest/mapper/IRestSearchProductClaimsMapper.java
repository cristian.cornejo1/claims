package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.InputSearchProductClaims;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;

import java.util.List;

/**
 * Created on 26/11/2019.
 *
 * @author Entelgy
 */
public interface IRestSearchProductClaimsMapper {

    RequestType mapIn(InputSearchProductClaims input);

    List<ProductClaims> mapOut(ResponseType model);
}
