package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.DTOIntAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultado;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponse;

import java.util.Map;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface IRestValidateAutomaticRefundTransactionMapper {

    Map<String, String> mapHeaderIn(DTOIntAutomaticRefundTransaction input);

    ModelResultado mapIn(DTOIntAutomaticRefundTransaction input);

    AutomaticRefundTransaction mapOut(ModelResultadoResponse model);
}