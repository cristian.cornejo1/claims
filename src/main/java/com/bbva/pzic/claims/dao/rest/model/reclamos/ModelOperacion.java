package com.bbva.pzic.claims.dao.rest.model.reclamos;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelOperacion {
    private ModelContrato contrato;
    private ModelMoneda moneda;
    private BigDecimal importe;
    private ModelModoAbono modoAbono;
    private String observacion;
    private ModelTipoOperacion tipoOperacion;
    private ModelCentroContable centroContable;
    private ModelProductoSeguro productoSeguro;
    private ModelBanco banco;
    private ModelContratoOrigen contratoOrigen;
    private List<ModelMovimiento> movimiento;
    private ModelTipoCambio tipoCambio;
    private ModelTipo tipo;

    public ModelContrato getContrato() {
        return contrato;
    }

    public void setContrato(ModelContrato contrato) {
        this.contrato = contrato;
    }

    public ModelMoneda getMoneda() {
        return moneda;
    }

    public void setMoneda(ModelMoneda moneda) {
        this.moneda = moneda;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public ModelModoAbono getModoAbono() {
        return modoAbono;
    }

    public void setModoAbono(ModelModoAbono modoAbono) {
        this.modoAbono = modoAbono;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public ModelTipoOperacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(ModelTipoOperacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public ModelCentroContable getCentroContable() {
        return centroContable;
    }

    public void setCentroContable(ModelCentroContable centroContable) {
        this.centroContable = centroContable;
    }

    public ModelProductoSeguro getProductoSeguro() {
        return productoSeguro;
    }

    public void setProductoSeguro(ModelProductoSeguro productoSeguro) {
        this.productoSeguro = productoSeguro;
    }

    public ModelBanco getBanco() {
        return banco;
    }

    public void setBanco(ModelBanco banco) {
        this.banco = banco;
    }

    public ModelContratoOrigen getContratoOrigen() {
        return contratoOrigen;
    }

    public void setContratoOrigen(ModelContratoOrigen contratoOrigen) {
        this.contratoOrigen = contratoOrigen;
    }

    public List<ModelMovimiento> getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(List<ModelMovimiento> movimiento) {
        this.movimiento = movimiento;
    }

    public ModelTipoCambio getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(ModelTipoCambio tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public ModelTipo getTipo() {
        return tipo;
    }

    public void setTipo(ModelTipo tipo) {
        this.tipo = tipo;
    }
}
