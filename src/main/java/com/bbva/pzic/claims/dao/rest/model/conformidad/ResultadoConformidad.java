package com.bbva.pzic.claims.dao.rest.model.conformidad;

public class ResultadoConformidad {

    private Resultado resultado;

    public Resultado getResultado() {
        return resultado;
    }

    public void setResultado(Resultado resultado) {
        this.resultado = resultado;
    }
}
