package com.bbva.pzic.claims.dao.rest.mapper;

import com.bbva.pzic.claims.business.dto.InputModifyAutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.conformidad.PeticionConformidad;
import com.bbva.pzic.claims.dao.rest.model.conformidad.ResultadoConformidad;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;

import java.util.Map;

public interface IRestModifyPartialAutomaticRefundTransactionMapper {

    Map<String, String> mapHeaderIn(InputModifyAutomaticRefundTransaction input);

    PeticionConformidad mapBodyIn(InputModifyAutomaticRefundTransaction input);

    PartialTransaction mapOut(ResultadoConformidad response);
}
