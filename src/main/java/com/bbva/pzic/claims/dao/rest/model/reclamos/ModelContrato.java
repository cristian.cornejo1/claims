package com.bbva.pzic.claims.dao.rest.model.reclamos;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class ModelContrato {
    private String aliasProducto;
    private String tarjeta;
    private String numero;

    public String getAliasProducto() {
        return aliasProducto;
    }

    public void setAliasProducto(String aliasProducto) {
        this.aliasProducto = aliasProducto;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
