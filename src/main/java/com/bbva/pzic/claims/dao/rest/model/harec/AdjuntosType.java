package com.bbva.pzic.claims.dao.rest.model.harec;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para adjuntosType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="adjuntosType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="binario" type="{http://www.w3.org/2001/XMLSchema}string"/&gt; *
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="grupo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adjuntosType", propOrder = {
        "id",
        "binario",
        "nombre",
        "grupo"
})
public class AdjuntosType {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String binario;
    @XmlElement(required = true)
        protected String nombre;
    @XmlElement(required = true)
    protected String grupo;

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param id allowed object is
     *           {@link String }
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Obtiene el valor de la propiedad binario.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBinario() {
        return binario;
    }

    /**
     * Define el valor de la propiedad binario.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBinario(String value) {
        this.binario = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     *
     * @return possible object is
     * {@link String }
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad grupo.
     *
     * @return possible object is
     * {@link String }
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Define el valor de la propiedad grupo.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setGrupo(String value) {
        this.grupo = value;
    }
}
