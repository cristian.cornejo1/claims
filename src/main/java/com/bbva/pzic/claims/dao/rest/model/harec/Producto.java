package com.bbva.pzic.claims.dao.rest.model.harec;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

public class Producto {

    private String codigo;
    private Contrato contrato;
    @DatoAuditable(omitir = true)
    private String numero;
    private String tipoNumero;
    private String aliasProducto;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipoNumero() {
        return tipoNumero;
    }

    public void setTipoNumero(String tipoNumero) {
        this.tipoNumero = tipoNumero;
    }

    public String getAliasProducto() {
        return aliasProducto;
    }

    public void setAliasProducto(String aliasProducto) {
        this.aliasProducto = aliasProducto;
    }
}