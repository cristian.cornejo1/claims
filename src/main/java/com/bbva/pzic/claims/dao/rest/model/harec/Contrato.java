package com.bbva.pzic.claims.dao.rest.model.harec;

public class Contrato {

    private String id;
    private String aliasProducto;
    private String tarjeta;
    private String numero;
    private String tipoNumero;
    private String descripcion;

    public String getAliasProducto() {
        return aliasProducto;
    }

    public void setAliasProducto(String aliasProducto) {
        this.aliasProducto = aliasProducto;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipoNumero() {
        return tipoNumero;
    }

    public void setTipoNumero(String tipoNumero) {
        this.tipoNumero = tipoNumero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
