package com.bbva.pzic.claims.canonic;
import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "addressComponent", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "addressComponent", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressComponent implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * This array indicates the type of the returned result. It contains a set of zero or more tags identifying
     * the type of feature returned in the result, For example, an address according to the Territorial
     * Organization of Peru, presents the following divisions:
     * DISTRICT : Type indicator district.
     * DEPARTMENT: Type indicator Department.
     * PROVINCE: Type indicator Province.
     */
    private List<String> componentTypes;
    /**
     * An abbreviated textual name for the address component. For example, an address component for Spain,
     * may have a longName of "Spain", and a shortName of ES, using the ISO-3166-1 2-digit code.
     */
    @DatoAuditable(omitir = true)
    private String code;
    /**
     * The full text description or name of the address component.
     */
    private String name;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
    public List<String> getComponentTypes() {
        return componentTypes;
    }

    public void setComponentTypes(List<String> componentTypes) {
        this.componentTypes = componentTypes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
