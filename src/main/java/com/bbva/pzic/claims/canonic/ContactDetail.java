package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "contactDetail", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "contactDetail", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private BaseContract contact;
    private String id;

    public BaseContract getContact() {
        return contact;
    }

    public void setContact(BaseContract contact) {
        this.contact = contact;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
