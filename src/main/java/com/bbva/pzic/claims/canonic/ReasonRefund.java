package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "reasonRefund", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "reasonRefund", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ReasonRefund implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Reason identifier.
     */
    private String id;
    /**
     * Reason description.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}