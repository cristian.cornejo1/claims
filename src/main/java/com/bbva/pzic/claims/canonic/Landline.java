package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "landline", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "landline", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Landline extends BaseContract implements Serializable {

    private static final long serialVersionUID = 1L;

    private String number;
    private PhoneCompany phoneCompany;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public PhoneCompany getPhoneCompany() {
        return phoneCompany;
    }

    @Override
    public void setPhoneCompany(PhoneCompany phoneCompany) {
        this.phoneCompany = phoneCompany;
    }
}
