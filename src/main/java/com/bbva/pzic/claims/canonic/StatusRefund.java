package com.bbva.pzic.claims.canonic;

import com.bbva.pzic.claims.facade.v0.dto.Situation;
import com.bbva.pzic.claims.facade.v0.dto.SubStatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "statusRefund", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "statusRefund", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusRefund implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Status identifier.
     */
    private String id;
    /**
     * Status name.
     */
    private String name;
    /**
     * Substatus of the automatic refund transaction. It indicates in what phase of the attention process is the refund request.
     */
    private SubStatus subStatus;
    /**
     * Situation of the automatic refund transaction. It gives more information about the status in order to know if the customer is satisfied and other details to understand why the request is in the current status.
     */
    private Situation situation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubStatus getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(SubStatus subStatus) {
        this.subStatus = subStatus;
    }

    public Situation getSituation() {
        return situation;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }
}