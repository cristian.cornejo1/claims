package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "relatedContract", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "relatedContract", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class RelatedContract implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier associated to the relationship between the contract and the
     * reason for the request for reimbursement.
     */
    private String id;
    /**
     * Amount to the money refund request.
     */
    private RequestedAmount requestedAmount;


    private Product product;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RequestedAmount getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(RequestedAmount requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }
}