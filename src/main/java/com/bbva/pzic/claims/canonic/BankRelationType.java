package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "bankRelationType", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "bankRelationType", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankRelationType implements Serializable {

    private static final long serialVersionUID = 1L;

    private String bankRelationType;
    private PetitionerType petitionerType;
    private Address address;
    private IdentityDocument identityDocument;
    private String firstName;
    private String lastName;
    private String secondLastName;

    private List<ContactDetail> contactDetails;

    public String getBankRelationType() {
        return bankRelationType;
    }

    public void setBankRelationType(String bankRelationType) {
        this.bankRelationType = bankRelationType;
    }

    public PetitionerType getPetitionerType() {
        return petitionerType;
    }

    public void setPetitionerType(PetitionerType petitionerType) {
        this.petitionerType = petitionerType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }
}
