package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "mobile", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "mobile", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Mobile extends BaseContract implements Serializable {

    private static final long serialVersionUID = 1L;

    private String number;
    private PhoneCompany phoneCompany;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneCompany getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(PhoneCompany phoneCompany) {
        this.phoneCompany = phoneCompany;
    }
}
