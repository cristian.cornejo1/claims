package com.bbva.pzic.claims.canonic;

import com.bbva.jee.arq.spring.core.servicing.utils.ShortDateAdapter;
import com.bbva.pzic.claims.facade.v0.dto.*;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "automaticRefundTransaction", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "automaticRefundTransaction", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AutomaticRefundTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Automatic refund transaction identifier.
     */
    private String id;
    /**
     * String based on ISO-8601 for specifying when the evaluation of the money
     * refund request was made through the automated attention of claims.
     */
    @XmlSchemaType(name = "date")
    @XmlJavaTypeAdapter(ShortDateAdapter.class)
    private Date creationDate;
    /**
     * Reason for request for reimbursement of money by the customer to the
     * bank.
     */
    private ReasonRefund reason;
    /**
     * Information of the customer who makes the money refund request.
     */
    private Petitioner petitioner;
    /**
     * Operations related to the reason selected by the client. A customer can
     * request the return of multiple amounts related to a reason.
     */
    private List<Operation> operations;
    /**
     * Status of the automatic refund transaction.
     */
    private StatusRefund status;
    /**
     * Claim associated with the automatic refund.
     */
    private RelatedClaim relatedClaim;
    private Boolean customerConfirmation;
    private Situation situation;
    private Channel channel;
    private List<Files> files;
    /**
     * Responsible business team for the refund attention.
     */
    private BusinessTeam businessTeam;
    /**
     * Business agents that attend the refund request.
     */
    private List<BusinessAgents> businessAgents;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public ReasonRefund getReason() {
        return reason;
    }

    public void setReason(ReasonRefund reason) {
        this.reason = reason;
    }

    public Petitioner getPetitioner() {
        return petitioner;
    }

    public void setPetitioner(Petitioner petitioner) {
        this.petitioner = petitioner;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    public StatusRefund getStatus() {
        return status;
    }

    public void setStatus(StatusRefund status) {
        this.status = status;
    }

    public RelatedClaim getRelatedClaim() {
        return relatedClaim;
    }

    public void setRelatedClaim(RelatedClaim relatedClaim) {
        this.relatedClaim = relatedClaim;
    }

    public Situation getSituation() {
        return situation;
    }

    public void setSituation(Situation situation) {
        this.situation = situation;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<Files> getFiles() {
        return files;
    }

    public void setFiles(List<Files> files) {
        this.files = files;
    }

    public BusinessTeam getBusinessTeam() {
        return businessTeam;
    }

    public void setBusinessTeam(BusinessTeam businessTeam) {
        this.businessTeam = businessTeam;
    }

    public List<BusinessAgents> getBusinessAgents() {
        return businessAgents;
    }

    public void setBusinessAgents(List<BusinessAgents> businessAgents) {
        this.businessAgents = businessAgents;
    }

    public Boolean getCustomerConfirmation() {
        return customerConfirmation;
    }

    public void setCustomerConfirmation(Boolean customerConfirmation) {
        this.customerConfirmation = customerConfirmation;
    }
}