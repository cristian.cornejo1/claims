package com.bbva.pzic.claims.canonic;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.claims.facade.v0.dto.CountrySearch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "baseContract", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "baseContract", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseContract implements Serializable {

    private static final long serialVersionUID = 1L;

    private String contactDetailType;
    @DatoAuditable(omitir = true)
    private String id;
    @DatoAuditable(omitir = true)
    private String address;
    @DatoAuditable(omitir = true)
    private String number;
    private PhoneCompany phoneCompany;
    /**
     * The type of landine.
     */
    private String phoneType;
    /**
     * Phone number's country.International prefix for the phone number. For example +34 or 0034.
     */
    private Country country;
    /**
     * International prefix for the phone number. For example +34 or 0034.
     */
    private String countryCode;
    /**
     * Phone city code.
     */
    private String regionalCode;
    /**
     * Phone extension.
     */
    private String extension;
    /**
     * Indicates if this contact information has been verified and can be used to
     *           send notifications to the customer.
     */
    private Boolean receivesNotifications;

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneCompany getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(PhoneCompany phoneCompany) {
        this.phoneCompany = phoneCompany;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRegionalCode() {
        return regionalCode;
    }

    public void setRegionalCode(String regionalCode) {
        this.regionalCode = regionalCode;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Boolean getReceivesNotifications() {
        return receivesNotifications;
    }

    public void setReceivesNotifications(Boolean receivesNotifications) {
        this.receivesNotifications = receivesNotifications;
    }
}
