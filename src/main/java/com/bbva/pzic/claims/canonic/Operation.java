package com.bbva.pzic.claims.canonic;

import com.bbva.pzic.claims.facade.v0.dto.ClaimReason;
import com.bbva.pzic.claims.facade.v0.dto.ExchangeRate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "operation", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "operation", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Operation implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Subproduct indicated by the customer where the requested amount will be
     * reimbursed.
     */
    private SubproductRefund subproductRefund;
    /**
     * Amount related to the request.
     */
    private RequestedAmount requestedAmount;
    /**
     * Detail of the operation where the customer requests the refund of the
     * total or a fraction an amount that considers that it should not have been
     * made.
     */
    private String detail;
    /**
     * Indicates the type of operation to be reimbursed, for example a
     * commission, interest or insurance premiums.
     */
    private String operationType;
    /**
     * Entity associated to the product.
     */
    private Bank bank;
    /**
     * Represents the relationship between the contract and the reason for the
     * request for reimbursement.
     */
    private RelatedContract relatedContract;
    /**
     * Data indicated by the client related to reason of the claim.
     */
    private ClaimReason claimReason;
    private String operationTypeName;
    private ExchangeRate exchangeRate;

    public SubproductRefund getSubproductRefund() {
        return subproductRefund;
    }

    public void setSubproductRefund(SubproductRefund subproductRefund) {
        this.subproductRefund = subproductRefund;
    }

    public RequestedAmount getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(RequestedAmount requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public RelatedContract getRelatedContract() {
        return relatedContract;
    }

    public void setRelatedContract(RelatedContract relatedContract) {
        this.relatedContract = relatedContract;
    }

    public ClaimReason getClaimReason() {
        return claimReason;
    }

    public void setClaimReason(ClaimReason claimReason) {
        this.claimReason = claimReason;
    }

    public String getOperationTypeName() {
        return operationTypeName;
    }

    public void setOperationTypeName(String operationTypeName) {
        this.operationTypeName = operationTypeName;
    }

    public ExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}