package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "subproductRefund", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "subproductRefund", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubproductRefund implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Subproduct identifier.
     */
    private String id;
    /**
     * Subproduct number.
     */
    private String number;
    /**
     * Refund mode chosen by the customer. It only applies when the subproduct
     * is a credit card. The customer can choose to be reimbursed from a payment
     * or in a current debt.
     */
    private String refundMode;

    private String refundModeDescription;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRefundMode() {
        return refundMode;
    }

    public void setRefundMode(String refundMode) {
        this.refundMode = refundMode;
    }

    public String getRefundModeDescription() { return refundModeDescription; }

    public void setRefundModeDescription(String refundModeDescription) { this.refundModeDescription = refundModeDescription; }
}