package com.bbva.pzic.claims.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import com.bbva.pzic.claims.facade.v0.dto.Segment;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "petitioner", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlType(name = "petitioner", namespace = "urn:com:bbva:pzic:claims:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class Petitioner implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identity document information related to the customer.
     */
    private IdentityDocument identityDocument;
    /**
     * Name of the person.
     */
    private String firstName;
    /**
     * Name(s) written after the "given names" and could be the main name.
     */
    private String lastName;
    /**
     * Complement the "last name" and some other cases this could preceed the
     * "given name(s)".
     */
    private String secondLastName;
    /**
     * Petitioner identifier.
     */
    private String id;
    /**
     * Petitioner type.
     */
    private PetitionerType petitionerType;
    /**
     * Petitioner address.
     */
    private Address address;
    /**
     * Petitioner bank relation type.
     */
    private String bankRelationType;
    /**
     * Petitioner contact details.
     */
    private List<ContactDetail> contactDetails;
    /**
     * It represents the segment that the petitioner belongs.
     */
    private Segment segment;

    /**
     * The bank that the petitioner is assigned.
     */
    private Bank bank;

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PetitionerType getPetitionerType() {
        return petitionerType;
    }

    public void setPetitionerType(PetitionerType petitionerType) {
        this.petitionerType = petitionerType;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getBankRelationType() {
        return bankRelationType;
    }

    public void setBankRelationType(String bankRelationType) {
        this.bankRelationType = bankRelationType;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }
}