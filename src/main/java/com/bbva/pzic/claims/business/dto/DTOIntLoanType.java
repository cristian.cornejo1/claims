package com.bbva.pzic.claims.business.dto;

public class DTOIntLoanType {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
