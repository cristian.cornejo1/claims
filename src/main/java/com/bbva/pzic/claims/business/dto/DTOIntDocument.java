package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy.
 */
public class DTOIntDocument {

    @NotNull(groups = ValidationGroup.SendEmailProductClaim.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
