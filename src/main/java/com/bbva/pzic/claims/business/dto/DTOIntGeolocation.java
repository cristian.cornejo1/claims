package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntGeolocation {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}