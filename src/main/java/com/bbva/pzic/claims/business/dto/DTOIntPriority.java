package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntPriority {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private Boolean isUrgent;

    private String basis;

    public Boolean getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(Boolean urgent) {
        isUrgent = urgent;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis;
    }
}