package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntPetition {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}