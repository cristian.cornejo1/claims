package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntBaseContract {

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOContactDetailType contactDetailType;

    private String id;
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String contractType;
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String number;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntNumberType numberType;
    @Valid
    private List<DTOIntTransaction> transactions;
    private DTOIntCardAgreement cardAgreement;
    private DTOIntAccountType accountType;
    private DTOIntLoanType loanType;

    public DTOContactDetailType getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(DTOContactDetailType contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntNumberType getNumberType() {
        return numberType;
    }

    public void setNumberType(DTOIntNumberType numberType) {
        this.numberType = numberType;
    }

    public List<DTOIntTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<DTOIntTransaction> transactions) {
        this.transactions = transactions;
    }

    public DTOIntCardAgreement getCardAgreement() {
        return cardAgreement;
    }

    public void setCardAgreement(DTOIntCardAgreement cardAgreement) {
        this.cardAgreement = cardAgreement;
    }

    public DTOIntAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(DTOIntAccountType accountType) {
        this.accountType = accountType;
    }

    public DTOIntLoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(DTOIntLoanType loanType) {
        this.loanType = loanType;
    }
}
