package com.bbva.pzic.claims.business.impl;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.claims.business.ISrvIntClaims;
import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.dao.IClaimsDAO;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;
import com.bbva.pzic.claims.util.ValidationGroup;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import static com.bbva.pzic.claims.util.Errors.MANDATORY_PARAMETERS_MISSING;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@Component
public class SrvIntClaims implements ISrvIntClaims {

    private static final Log LOG = LogFactory.getLog(SrvIntClaims.class);

    @Autowired
    private IClaimsDAO claimsDAO;
    @Autowired
    private Validator validator;


    /**
     * {@inheritDoc}
     */
    @Override
    public List<AutomaticRefundTransaction> listAutomaticRefundTransactions(final InputListAutomaticRefundTransactions input) {
        LOG.info("... Invoking method SrvIntClaims.listAutomaticRefundTransactions ...");
        validator.validate(input, ValidationGroup.ValidateAutomaticRefundTransaction.class);
        return claimsDAO.listAutomaticRefundTransactions(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductClaimTransaction createProductClaim(final DTOIntProductClaim dtoInt) {
        LOG.info("... Invoking method SrvIntClaims.createProductClaim ...");
        LOG.info("... Validating createProductClaim dtoInt parameter ...");
        validator.validate(dtoInt, ValidationGroup.CreateProductClaim.class);
        if(dtoInt.getReason().getComments() == null) {
            throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING, "reason.comments field is mandatory.");
        }
        if(dtoInt.getAdditionalClaims() != null){
            for (DTOIntAdditionalClaims dtoIntAdditionalClaims : dtoInt.getAdditionalClaims()) {
                final DTOIntContract contract = dtoIntAdditionalClaims.getContract();
                if (contract != null && contract.getId() == null) {
                    throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING, "additionalClaims[].contract.id field is mandatory.");
                }
            }
        }

        return claimsDAO.createProductClaim(dtoInt);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductClaims> searchProductClaims(final InputSearchProductClaims dtoInt) {
        LOG.info("... Invoking method SrvIntClaims.searchProductClaims ...");
        validator.validate(dtoInt, ValidationGroup.SearchProductClaims.class);
        return claimsDAO.searchProductClaims(dtoInt);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductClaims getProductClaim(final InputGetProductClaim input) {
        LOG.info("... Invoking method SrvIntClaims.getProductClaim ...");
        validator.validate(input, ValidationGroup.GetProductClaim.class);
        return claimsDAO.getProductClaim(input);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendEmailProductClaim(final InputSendEmailProductClaim input) {
        LOG.info("... Invoking method SrvIntClaims.sendEmailProductClaim ...");
        validator.validate(input, ValidationGroup.SendEmailProductClaim.class);
        claimsDAO.sendEmailProductClaim(input);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public AutomaticRefundTransaction getAutomaticRefundTransaction(
            final InputGetAutomaticRefundTransaction input) {
        LOG.info("... Invoking method SrvIntClaims.getAutomaticRefundTransaction ...");
        return claimsDAO.getAutomaticRefundTransaction(input);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AutomaticRefundTransaction validateAutomaticRefundTransaction(
            final DTOIntAutomaticRefundTransaction dtoInt) {
        LOG.info("... Invoking method SrvIntClaims.validateAutomaticRefundTransaction ...");
        LOG.info("... Validating validateAutomaticRefundTransaction input parameter ...");
        for (DTOIntOperation dtoIntOperation : dtoInt.getOperations()) {
            final DTOIntRelatedContract relatedContract = dtoIntOperation.getRelatedContract();
            if (relatedContract == null || relatedContract.getRequestedAmount() == null
                    || relatedContract.getRequestedAmount().getAmount() == null
                    || relatedContract.getRequestedAmount().getCurrency() == null) {
                continue;
            }
        }
        validator.validate(dtoInt, ValidationGroup.ValidateAutomaticRefundTransaction.class);

        //in common general
        if (dtoInt.getPetitioner().getPetitioner().getIdentityDocument() != null) {
            if (dtoInt.getPetitioner().getPetitioner().getId() == null ||
                    dtoInt.getPetitioner().getPetitioner().getIdentityDocument().getDocumentType() == null ||
                    dtoInt.getPetitioner().getPetitioner().getIdentityDocument().getDocumentType().getId() == null ||
                    dtoInt.getPetitioner().getPetitioner().getIdentityDocument().getDocumentNumber() == null ||
                    dtoInt.getReason().getId() == null
            ) {
                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
            }
        }

        if (dtoInt.getPetitioner().getPetitioner().getSegment() != null) {
            if (dtoInt.getPetitioner().getPetitioner().getSegment().getId() == null) {
                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
            }
        }

        if (dtoInt.getRelatedClaim() != null) {
            if (dtoInt.getRelatedClaim().getId() == null) {
                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
            }
        }

        if (dtoInt.getPetitioner().getPetitioner().getContactDetails() != null) {
            for (DTOIntContact contact : dtoInt.getPetitioner().getPetitioner().getContactDetails()) {

                if (contact.getContacto() == null || contact.getContacto().getContactDetailType() == null) {
                    throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                }

                String contacDetailType = contact.getContacto().getContactDetailType();
                // FRONT: EMAIL -> BACK: EMAIL || FRONT: LANDLINE -> BACK: PHONE || FRONT:MOBILE -> BACK: MOBILE
                if (contacDetailType.equalsIgnoreCase("EMAIL")) {
                    if (contact.getContacto().getAddress() == null) {
                        throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                    }
                }
                if (contacDetailType.equalsIgnoreCase("PHONE") || contacDetailType.equalsIgnoreCase("MOBILE")) {
//                    in common
                    if (contact.getContacto().getNumber() == null) {
                        throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                    }

                    if (contacDetailType.equalsIgnoreCase("PHONE")) {
                        if (contact.getContacto().getCountry() != null) {
                            if (contact.getContacto().getCountry().getId() == null) {
                                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                            }
                        }
                    }
                    if (contacDetailType.equalsIgnoreCase("MOBILE")) {
                        if (contact.getContacto().getPhoneCompany() != null) {
                            if (contact.getContacto().getPhoneCompany().getId() == null) {
                                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                            }
                        }
                    }
                }
            }
        }
        if (dtoInt.getOperations() != null) {
            for (DTOIntOperation operation : dtoInt.getOperations()) {
                if (operation.getSubproductRefund().getId() == null ||
                        operation.getDetail() == null ||
                        operation.getRequestedAmount().getCurrency() == null ||
                        operation.getRequestedAmount().getAmount() == null
                ) {
                    throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                }
                if (operation.getBank() != null) {
                    if (operation.getBank().getId() == null || operation.getBank().getBranch() == null || operation.getBank().getBranch().getId() == null) {
                        throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                    }
                }
                if (operation.getClaimReason() != null) {
                    if (operation.getClaimReason().getContract() == null || operation.getClaimReason().getContract().getContractType() == null) {
                        throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                    }

                    String contractType = operation.getClaimReason().getContract().getContractType();
                    // FRONT: CARD -> BACK: TARJE || FRONT: ACCOUNT -> BACK: CUENT || FRONT:LOAN -> BACK: PREST
                    if (contractType.equalsIgnoreCase("TARJE") || contractType.equalsIgnoreCase("CUENT") || contractType.equalsIgnoreCase("PREST")) {
                        //in common
                        if (operation.getClaimReason().getContract().getNumber() == null ||
                                operation.getClaimReason().getContract().getNumberType() == null ||
                                operation.getClaimReason().getContract().getNumberType().getId() == null ||
                                operation.getClaimReason().getContract().getTransactions() == null
                        ) {
                            throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                        }
                        operation.getClaimReason().getContract().getTransactions().forEach(
                                transaction -> {
                                    if (transaction.getOperationDate() == null ||
                                            transaction.getConcept() == null ||
                                            transaction.getTransactionType() == null ||
                                            transaction.getTransactionType().getId() == null ||
                                            transaction.getLocalAmount() == null ||
                                            transaction.getLocalAmount().getAmount() == null ||
                                            transaction.getLocalAmount().getCurrency() == null
                                    ) {
                                        throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                                    }
                                    if (transaction.getStore() != null) {
                                        if (transaction.getStore().getId() == null) {
                                            throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                                        }
                                    }
                                }
                        );
                        if (contractType.equalsIgnoreCase("TARJE")) {
                            if (operation.getClaimReason().getContract().getCardAgreement() == null ||
                                    operation.getClaimReason().getContract().getCardAgreement().getId() == null
                            ) {
                                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                            }
                            if (operation.getClaimReason().getContract().getCardAgreement().getSubProduct() != null) {
                                if(operation.getClaimReason().getContract().getCardAgreement().getSubProduct().getId() == null){
                                    throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                                }
                            }
                        }
                        if (contractType.equalsIgnoreCase("CUENT")) {
                            if (operation.getClaimReason().getContract().getAccountType() == null ||
                                    operation.getClaimReason().getContract().getAccountType().getId() == null
                            ) {
                                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                            }
                        }
                        if (contractType.equalsIgnoreCase("PREST")) {
                            if (operation.getClaimReason().getContract().getLoanType() == null ||
                                    operation.getClaimReason().getContract().getLoanType().getId() == null
                            ) {
                                throw new BusinessServiceException(MANDATORY_PARAMETERS_MISSING);
                            }
                        }
                    }
                }
            }

        }
        return claimsDAO.validateAutomaticRefundTransaction(dtoInt);
    }

    @Override
    public PartialTransaction modifyPartialAutomaticRefundTransaction(final InputModifyAutomaticRefundTransaction input) {
        LOG.info("... Invoking method SrvIntClaims.modifyPartialAutomaticRefundTransaction ...");
        validator.validate(input, ValidationGroup.ModifyPartialAutomaticRefundTransaction.class);
        return claimsDAO.modifyPartialAutomaticRefundTransaction(input);
    }

}
