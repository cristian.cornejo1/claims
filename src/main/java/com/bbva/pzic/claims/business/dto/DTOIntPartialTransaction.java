package com.bbva.pzic.claims.business.dto;

import javax.validation.Valid;

public class DTOIntPartialTransaction {

    private Boolean customerConfirmation;
    @Valid
    private DTOIntStatus status;
    @Valid
    private DTOIntSituation situation;

    public Boolean getCustomerConfirmation() {
        return customerConfirmation;
    }

    public void setCustomerConfirmation(Boolean customerConfirmation) {
        this.customerConfirmation = customerConfirmation;
    }

    public DTOIntStatus getStatus() {
        return status;
    }

    public void setStatus(DTOIntStatus status) {
        this.status = status;
    }

    public DTOIntSituation getSituation() {
        return situation;
    }

    public void setSituation(DTOIntSituation situation) {
        this.situation = situation;
    }
}