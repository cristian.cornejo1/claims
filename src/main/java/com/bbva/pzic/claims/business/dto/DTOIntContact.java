package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntContact {

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntBaseContract contact;

    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntBaseContact contacto;

    public DTOIntBaseContract getContact() {
        return contact;
    }

    public void setContact(DTOIntBaseContract contact) {
        this.contact = contact;
    }

    public DTOIntBaseContact getContacto() {
        return contacto;
    }

    public void setContacto(DTOIntBaseContact contacto) {
        this.contacto = contacto;
    }
}
