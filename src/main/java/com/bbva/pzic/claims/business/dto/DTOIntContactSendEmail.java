package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy.
 */
public class DTOIntContactSendEmail {

    private String id;
    private String address;
    @NotNull(groups = ValidationGroup.SendEmailProductClaim.class)
    private String contactDetailType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }
}
