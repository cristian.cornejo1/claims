package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntResolution {

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntDelivery delivery;

    public DTOIntDelivery getDelivery() {
        return delivery;
    }

    public void setDelivery(DTOIntDelivery delivery) {
        this.delivery = delivery;
    }
}
