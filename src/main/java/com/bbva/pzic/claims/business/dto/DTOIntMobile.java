package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntMobile extends DTOIntBaseContract {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String number;

    @Valid
    private DTOIntPhoneCompany phoneCompany;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntPhoneCompany getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(DTOIntPhoneCompany phoneCompany) {
        this.phoneCompany = phoneCompany;
    }
}
