package com.bbva.pzic.claims.business.dto;


/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class InputGetAutomaticRefundTransaction {

    private String app;
    private String automaticRefundTransactionId;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getAutomaticRefundTransactionId() {
        return automaticRefundTransactionId;
    }

    public void setAutomaticRefundTransactionId(
            String automaticRefundTransactionId) {
        this.automaticRefundTransactionId = automaticRefundTransactionId;
    }
}