package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntAddressComponent {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private List<String> componentTypes;

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String code;

    public List<String> getComponentTypes() {
        return componentTypes;
    }

    public void setComponentTypes(List<String> componentTypes) {
        this.componentTypes = componentTypes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
