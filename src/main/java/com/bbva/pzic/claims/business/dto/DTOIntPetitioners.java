package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntPetitioners {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String id;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntPetitioner petitioner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntPetitioner getPetitioner() {
        return petitioner;
    }

    public void setPetitioner(DTOIntPetitioner petitioner) {
        this.petitioner = petitioner;
    }
}