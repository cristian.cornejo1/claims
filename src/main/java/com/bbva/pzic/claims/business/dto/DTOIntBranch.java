package com.bbva.pzic.claims.business.dto;


import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntBranch {

    @NotNull(groups = {ValidationGroup.CreateProductClaim.class,
            ValidationGroup.ValidateAutomaticRefundTransaction.class})
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}