package com.bbva.pzic.claims.business;

import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;


import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface ISrvIntClaims {
    /**
     * It allows to consult the list of money reimbursement requirements made by
     * a customer.
     *
     * @param input dto with input fields to validate
     * @return {@link List}
     */
    List<AutomaticRefundTransaction> listAutomaticRefundTransactions(
            InputListAutomaticRefundTransactions input);
    /**
     * It allows to evaluate a requirement of money reimbursement by a customer,
     * this requirement must go through a series of validation filters by means
     * of a rules engine, to finally decide if the request is accepted or
     * rejected.
     *
     * @param dtoInt dto with input fields to validate
     * @return {@link AutomaticRefundTransaction}
     */
    AutomaticRefundTransaction validateAutomaticRefundTransaction(
            DTOIntAutomaticRefundTransaction dtoInt);

    /**
     * It allows to consult the detail of a money refund requirement made by a
     * customer, where you can also know the status of the request, if it was
     * accepted or rejected.
     *
     * @param input dto with input fields to validate
     * @return {@link AutomaticRefundTransaction}
     */
    AutomaticRefundTransaction getAutomaticRefundTransaction(
            InputGetAutomaticRefundTransaction input);
    /**
     * It allows to consult the detail of a money refund requirement made by a
     * customer, where you can also know the status of the request, if it was
     * accepted or rejected.
     *
     * @param dtoInt dto with input fields to validate
     * @return {@link ProductClaimTransaction}
     */
    ProductClaimTransaction createProductClaim(DTOIntProductClaim dtoInt);

    List<ProductClaims> searchProductClaims(InputSearchProductClaims mapIn);

    ProductClaims getProductClaim(InputGetProductClaim mapIn);

    void sendEmailProductClaim(InputSendEmailProductClaim mapIn);

    PartialTransaction modifyPartialAutomaticRefundTransaction(InputModifyAutomaticRefundTransaction input);

}