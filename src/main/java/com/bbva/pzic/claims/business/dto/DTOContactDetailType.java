package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOContactDetailType {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
