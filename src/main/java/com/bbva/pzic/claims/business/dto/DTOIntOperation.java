package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntOperation {

    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntSubproductRefund subproductRefund;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntRequestedAmount requestedAmount;
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String detail;
    private String operationType;
    private DTOIntBank bank;
    private DTOIntRelatedContract relatedContract;
    @Valid
    private DTOIntClaimReason claimReason;

    public DTOIntSubproductRefund getSubproductRefund() {
        return subproductRefund;
    }

    public void setSubproductRefund(DTOIntSubproductRefund subproductRefund) {
        this.subproductRefund = subproductRefund;
    }

    public DTOIntRequestedAmount getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(DTOIntRequestedAmount requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public DTOIntBank getBank() {
        return bank;
    }

    public void setBank(DTOIntBank bank) {
        this.bank = bank;
    }

    public DTOIntRelatedContract getRelatedContract() {
        return relatedContract;
    }

    public void setRelatedContract(DTOIntRelatedContract relatedContract) {
        this.relatedContract = relatedContract;
    }

    public DTOIntClaimReason getClaimReason() {
        return claimReason;
    }

    public void setClaimReason(DTOIntClaimReason claimReason) {
        this.claimReason = claimReason;
    }
}