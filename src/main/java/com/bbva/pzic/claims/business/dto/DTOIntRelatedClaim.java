package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 22/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntRelatedClaim {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
