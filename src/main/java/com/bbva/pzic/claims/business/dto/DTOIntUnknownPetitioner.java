package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntUnknownPetitioner extends DTOIntPetitioner {

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntIdentityDocument identityDocument;

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String firstName;

    private String lastName;
    private String secondLastName;

    public DTOIntIdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(DTOIntIdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }
}
