package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class DTOIntLocalAmount {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private BigDecimal amount;
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
