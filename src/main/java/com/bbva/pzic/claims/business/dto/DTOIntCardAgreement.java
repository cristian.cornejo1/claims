package com.bbva.pzic.claims.business.dto;

public class DTOIntCardAgreement {

    private String id;
    private DTOIntSubProduct subProduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntSubProduct getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(DTOIntSubProduct subProduct) {
        this.subProduct = subProduct;
    }
}
