package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntAutomaticRefundTransaction {
    private String app;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntReasonRefund reason;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntPetitioners petitioner;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private List<DTOIntOperation> operations;
    @Valid
    private DTOIntRelatedClaim relatedClaim;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public DTOIntReasonRefund getReason() {
        return reason;
    }

    public void setReason(DTOIntReasonRefund reason) {
        this.reason = reason;
    }

    public DTOIntPetitioners getPetitioner() {
        return petitioner;
    }

    public void setPetitioner(DTOIntPetitioners petitioner) {
        this.petitioner = petitioner;
    }

    public List<DTOIntOperation> getOperations() {
        return operations;
    }

    public void setOperations(List<DTOIntOperation> operations) {
        this.operations = operations;
    }

    public DTOIntRelatedClaim getRelatedClaim() {
        return relatedClaim;
    }

    public void setRelatedClaim(DTOIntRelatedClaim relatedClaim) {
        this.relatedClaim = relatedClaim;
    }
}