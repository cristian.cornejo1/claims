package com.bbva.pzic.claims.business.dto;


import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class InputListAutomaticRefundTransactions {

    @NotNull(groups = ValidationGroup.ListAutomaticRefundTransactions.class)
    private String app;

    private String customerId;

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}