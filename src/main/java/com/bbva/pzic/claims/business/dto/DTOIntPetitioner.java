package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntPetitioner {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String id;

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String bankRelationType;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntPetitionerType petitionerType;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntAddress address;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private List<DTOIntContact> contactDetails;

    @Valid
    private DTOIntSegment segment;

    @Valid
    private DTOIntBank bank;

    @Valid
    private DTOIntIdentityDocument identityDocument;

    @Valid
    private DTOIntRelatedClaim relatedClaim;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankRelationType() {
        return bankRelationType;
    }

    public void setBankRelationType(String bankRelationType) {
        this.bankRelationType = bankRelationType;
    }

    public DTOIntPetitionerType getPetitionerType() {
        return petitionerType;
    }

    public void setPetitionerType(DTOIntPetitionerType petitionerType) {
        this.petitionerType = petitionerType;
    }

    public DTOIntAddress getAddress() {
        return address;
    }

    public void setAddress(DTOIntAddress address) {
        this.address = address;
    }

    public List<DTOIntContact> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<DTOIntContact> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public DTOIntSegment getSegment() {
        return segment;
    }

    public void setSegment(DTOIntSegment segment) {
        this.segment = segment;
    }

    public DTOIntBank getBank() {
        return bank;
    }

    public void setBank(DTOIntBank bank) {
        this.bank = bank;
    }

    public DTOIntIdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(DTOIntIdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

    public DTOIntRelatedClaim getRelatedClaim() {
        return relatedClaim;
    }

    public void setRelatedClaim(DTOIntRelatedClaim relatedClaim) {
        this.relatedClaim = relatedClaim;
    }
}
