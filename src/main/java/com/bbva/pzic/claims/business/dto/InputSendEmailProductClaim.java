package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 5/12/2019.
 *
 * @author Entelgy
 */
public class InputSendEmailProductClaim {

    @NotNull(groups = ValidationGroup.SendEmailProductClaim.class)
    private String productClaimId;
    @NotNull(groups = ValidationGroup.SendEmailProductClaim.class)
    private String aap;
    private String clientId;
    @NotNull(groups = ValidationGroup.SendEmailProductClaim.class)
    @Valid
    private DTOIntContactSendEmail contact;
    @Valid
    private List<DTOIntDocument> documents;

    public String getProductClaimId() {
        return productClaimId;
    }

    public void setProductClaimId(String productClaimId) {
        this.productClaimId = productClaimId;
    }

    public String getAap() {
        return aap;
    }

    public void setAap(String aap) {
        this.aap = aap;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public DTOIntContactSendEmail getContact() {
        return contact;
    }

    public void setContact(DTOIntContactSendEmail contact) {
        this.contact = contact;
    }

    public List<DTOIntDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DTOIntDocument> documents) {
        this.documents = documents;
    }
}
