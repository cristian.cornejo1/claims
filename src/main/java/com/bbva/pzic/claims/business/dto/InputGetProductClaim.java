package com.bbva.pzic.claims.business.dto;


import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 28/11/2018.
 *
 * @author Entelgy
 */
public class InputGetProductClaim {

    @NotNull(groups = ValidationGroup.GetProductClaim.class)
    private String productClaimId;

    public String getProductClaimId() {
        return productClaimId;
    }

    public void setProductClaimId(String productClaimId) {
        this.productClaimId = productClaimId;
    }
}