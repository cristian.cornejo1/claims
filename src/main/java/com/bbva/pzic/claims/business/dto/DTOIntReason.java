package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntReason {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String id;

    private String comments;

    @Valid
    private DTOIntSubReason subReason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public DTOIntSubReason getSubReason() {
        return subReason;
    }

    public void setSubReason(DTOIntSubReason subReason) {
        this.subReason = subReason;
    }
}
