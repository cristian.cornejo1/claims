package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class InputModifyAutomaticRefundTransaction {

    @NotNull(groups = ValidationGroup.ModifyPartialAutomaticRefundTransaction.class)
    private String automaticRefundTransactionId;
    @Valid
    private DTOIntPartialTransaction partialTransaction;
    @NotNull(groups = ValidationGroup.ModifyPartialAutomaticRefundTransaction.class)
    private String user;
    @NotNull(groups = ValidationGroup.ModifyPartialAutomaticRefundTransaction.class)
    private String idCanal;

    public String getAutomaticRefundTransactionId() {
        return automaticRefundTransactionId;
    }

    public void setAutomaticRefundTransactionId(String automaticRefundTransactionId) {
        this.automaticRefundTransactionId = automaticRefundTransactionId;
    }

    public DTOIntPartialTransaction getPartialTransaction() {
        return partialTransaction;
    }

    public void setPartialTransaction(DTOIntPartialTransaction partialTransaction) {
        this.partialTransaction = partialTransaction;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(String idCanal) {
        this.idCanal = idCanal;
    }
}
