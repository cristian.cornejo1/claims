package com.bbva.pzic.claims.business.dto;

public class DTOIntAccountType {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
