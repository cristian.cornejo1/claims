package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntSubproductRefund {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String id;
    private String cardNumber;
    private String number;
    private String refundMode;
    private Class<?> validationGroup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRefundMode() {
        return refundMode;
    }

    public void setRefundMode(String refundMode) {
        this.refundMode = refundMode;
    }

    public Class<?> getValidationGroup() {
        return validationGroup;
    }

    public void setValidationGroup(Class<?> validationGroup) {
        this.validationGroup = validationGroup;
    }
}