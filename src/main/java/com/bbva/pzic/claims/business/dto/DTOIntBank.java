package com.bbva.pzic.claims.business.dto;


import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntBank {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String id;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntBranch branch;

    public DTOIntBranch getBranch() {
        return branch;
    }

    public void setBranch(DTOIntBranch branch) {
        this.branch = branch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}