package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 22/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntBaseContact {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String contactDetailType;
    private String number;
    @Valid
    private DTOIntPhoneCompany phoneCompany;
    @Valid
    private DTOIntCountry country;
    private String countryCode;
    private String regionalCode;
    private String extension;
    private String address;
    private Boolean receivesNotifications;
    private String phoneType;

    public String getContactDetailType() {
        return contactDetailType;
    }

    public void setContactDetailType(String contactDetailType) {
        this.contactDetailType = contactDetailType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DTOIntPhoneCompany getPhoneCompany() {
        return phoneCompany;
    }

    public void setPhoneCompany(DTOIntPhoneCompany phoneCompany) {
        this.phoneCompany = phoneCompany;
    }

    public DTOIntCountry getCountry() {
        return country;
    }

    public void setCountry(DTOIntCountry country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRegionalCode() {
        return regionalCode;
    }

    public void setRegionalCode(String regionalCode) {
        this.regionalCode = regionalCode;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getReceivesNotifications() {
        return receivesNotifications;
    }

    public void setReceivesNotifications(Boolean receivesNotifications) {
        this.receivesNotifications = receivesNotifications;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }
}
