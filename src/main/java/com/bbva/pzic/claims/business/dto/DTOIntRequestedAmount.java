package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntRequestedAmount {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String currency;
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private BigDecimal amount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}