package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created on 29/10/2020.
 *
 * @author Entelgy.
 */
public class DTOIntAmount  {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private BigDecimal amount;
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
