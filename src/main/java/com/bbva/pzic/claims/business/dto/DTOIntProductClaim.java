package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

import java.util.Calendar;
/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntProductClaim {

    private String aap;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntPersonType personType;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntCompany company;

    private String isUnderage;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntClaimType claimType;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntProduct product;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntReason reason;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntResolution resolution;

    @Valid
    private List<DTOIntPetitioners> petitioners;

    private List<DTOIntFile> files;

    @Valid
    private DTOIntContract contract;

    @Valid
    private DTOIntAmount claimAmount;

    @Valid
    private DTOIntBank bank;

    @Valid
    private DTOIntChannel channel;

    @Valid
    private DTOIntGeolocation geolocation;

    @Valid
    private DTOIntDelivery delivery;

    @Valid
    private DTOIntPetition petition;

    @Valid
    private DTOIntPriority priority;

    private Calendar enrollmentDate;

    @Valid
    private DTOIntPredecessorClaim predecessorClaim;

    private Boolean isOnlineRefundFlow;

    private String previousAttentionCode;
    @Valid
    private List<DTOIntAdditionalClaims> additionalClaims;

    private DTOIntAtm atm;

    private Calendar issueDate;

    public String getAap() {
        return aap;
    }

    public void setAap(String aap) {
        this.aap = aap;
    }

    public DTOIntPersonType getPersonType() {
        return personType;
    }

    public void setPersonType(DTOIntPersonType personType) {
        this.personType = personType;
    }

    public DTOIntCompany getCompany() {
        return company;
    }

    public void setCompany(DTOIntCompany company) {
        this.company = company;
    }

    public String getIsUnderage() {
        return isUnderage;
    }

    public void setIsUnderage(String isUnderage) {
        this.isUnderage = isUnderage;
    }

    public DTOIntClaimType getClaimType() {
        return claimType;
    }

    public void setClaimType(DTOIntClaimType claimType) {
        this.claimType = claimType;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

    public DTOIntReason getReason() {
        return reason;
    }

    public void setReason(DTOIntReason reason) {
        this.reason = reason;
    }

    public DTOIntResolution getResolution() {
        return resolution;
    }

    public void setResolution(DTOIntResolution resolution) {
        this.resolution = resolution;
    }

    public List<DTOIntPetitioners> getPetitioners() {
        return petitioners;
    }

    public void setPetitioners(List<DTOIntPetitioners> petitioners) {
        this.petitioners = petitioners;
    }

    public List<DTOIntFile> getFiles() {
        return files;
    }

    public void setFiles(List<DTOIntFile> files) {
        this.files = files;
    }

    public DTOIntContract getContract() {
        return contract;
    }

    public void setContract(DTOIntContract contract) {
        this.contract = contract;
    }

    public DTOIntAmount getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(DTOIntAmount claimAmount) {
        this.claimAmount = claimAmount;
    }

    public DTOIntBank getBank() {
        return bank;
    }

    public void setBank(DTOIntBank bank) {
        this.bank = bank;
    }

    public DTOIntChannel getChannel() {
        return channel;
    }

    public void setChannel(DTOIntChannel channel) {
        this.channel = channel;
    }

    public DTOIntGeolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(DTOIntGeolocation geolocation) {
        this.geolocation = geolocation;
    }

    public DTOIntDelivery getDelivery() {
        return delivery;
    }

    public void setDelivery(DTOIntDelivery delivery) {
        this.delivery = delivery;
    }

    public DTOIntPetition getPetition() {
        return petition;
    }

    public void setPetition(DTOIntPetition petition) {
        this.petition = petition;
    }

    public DTOIntPriority getPriority() {
        return priority;
    }

    public void setPriority(DTOIntPriority priority) {
        this.priority = priority;
    }

    public Calendar getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(Calendar enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public DTOIntPredecessorClaim getPredecessorClaim() {
        return predecessorClaim;
    }

    public void setPredecessorClaim(DTOIntPredecessorClaim predecessorClaim) {
        this.predecessorClaim = predecessorClaim;
    }

    public Boolean getIsOnlineRefundFlow() {
        return isOnlineRefundFlow;
    }

    public void setIsOnlineRefundFlow(Boolean onlineRefundFlow) {
        isOnlineRefundFlow = onlineRefundFlow;
    }

    public String getPreviousAttentionCode() {
        return previousAttentionCode;
    }

    public void setPreviousAttentionCode(String previousAttentionCode) {
        this.previousAttentionCode = previousAttentionCode;
    }

    public List<DTOIntAdditionalClaims> getAdditionalClaims() {
        return additionalClaims;
    }

    public void setAdditionalClaims(List<DTOIntAdditionalClaims> additionalClaims) {
        this.additionalClaims = additionalClaims;
    }

    public DTOIntAtm getAtm() {
        return atm;
    }

    public void setAtm(DTOIntAtm atm) {
        this.atm = atm;
    }

    public Calendar getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Calendar issueDate) {
        this.issueDate = issueDate;
    }
}
