package com.bbva.pzic.claims.business.dto;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public class DTOIntRelatedContract {

    private String id;
    private DTOIntRequestedAmount requestedAmount;
    private Class<?> validationGroup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntRequestedAmount getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(DTOIntRequestedAmount requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public Class<?> getValidationGroup() {
        return validationGroup;
    }

    public void setValidationGroup(Class<?> validationGroup) {
        this.validationGroup = validationGroup;
    }
}