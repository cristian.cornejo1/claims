package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntIdentityDocument {

    @Valid
    @NotNull(groups = {ValidationGroup.CreateProductClaim.class,
                ValidationGroup.ValidateAutomaticRefundTransaction.class})
    private DTOIntDocumentType documentType;

    @NotNull(groups = {ValidationGroup.CreateProductClaim.class,
            ValidationGroup.ValidateAutomaticRefundTransaction.class})
    private String documentNumber;

    public DTOIntDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntDocumentType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
