package com.bbva.pzic.claims.business.dto;


import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

/**
 * Created on 26/11/2018.
 *
 * @author Entelgy
 */
public class InputSearchProductClaims {
    @NotNull(groups = ValidationGroup.SearchProductClaims.class)
    private String aap;
    private String clientId;
    private DTOIntPetitioner petitioner;

    public String getAap() {
        return aap;
    }

    public void setAap(String aap) {
        this.aap = aap;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public DTOIntPetitioner getPetitioner() {
        return petitioner;
    }

    public void setPetitioner(DTOIntPetitioner petitioner) {
        this.petitioner = petitioner;
    }
}