package com.bbva.pzic.claims.business.dto;

import javax.validation.Valid;


public class DTOIntStatus {

    @Valid
    private DTOIntSubStatus subStatus;

    public DTOIntSubStatus getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(DTOIntSubStatus subStatus) {
        this.subStatus = subStatus;
    }
}
