package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntAdditionalClaims {

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntClaimType claimType;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntProduct product;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntReason reason;

    @Valid
    private DTOIntContract contract;

    @Valid
    private DTOIntAmount claimAmount;

    @Valid
    private DTOIntAtm atm;

    @Valid
    private DTOIntBank bank;

    private Calendar issueDate;

    public DTOIntClaimType getClaimType() {
        return claimType;
    }

    public void setClaimType(DTOIntClaimType claimType) {
        this.claimType = claimType;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

    public DTOIntReason getReason() {
        return reason;
    }

    public void setReason(DTOIntReason reason) {
        this.reason = reason;
    }

    public DTOIntContract getContract() {
        return contract;
    }

    public void setContract(DTOIntContract contract) {
        this.contract = contract;
    }

    public DTOIntAmount getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(DTOIntAmount claimAmount) {
        this.claimAmount = claimAmount;
    }

    public DTOIntAtm getAtm() {
        return atm;
    }

    public void setAtm(DTOIntAtm atm) {
        this.atm = atm;
    }

    public DTOIntBank getBank() {
        return bank;
    }

    public void setBank(DTOIntBank bank) {
        this.bank = bank;
    }

    public Calendar getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Calendar issueDate) {
        this.issueDate = issueDate;
    }
}