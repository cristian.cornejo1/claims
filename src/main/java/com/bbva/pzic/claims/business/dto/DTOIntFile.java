package com.bbva.pzic.claims.business.dto;

/**
 * Created on 11/26/2019.
 *
 * @author Entelgy
 */
public class DTOIntFile {

    private String content;
    private String name;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
