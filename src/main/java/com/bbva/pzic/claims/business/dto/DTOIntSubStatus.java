package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.constraints.NotNull;

public class DTOIntSubStatus {

    @NotNull(groups = ValidationGroup.ModifyPartialAutomaticRefundTransaction.class)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
