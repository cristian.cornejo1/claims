package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 22/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntClaimReason {

    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    @Valid
    private DTOIntBaseContract contract;

    public DTOIntBaseContract getContract() {
        return contract;
    }

    public void setContract(DTOIntBaseContract contract) {
        this.contract = contract;
    }
}
