package com.bbva.pzic.claims.business.dto;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntEmail extends DTOIntBaseContract {

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
