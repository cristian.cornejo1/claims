package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 18/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntLocation {

    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private String formattedAddress;

    private String additionalInformation;

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private List<DTOIntAddressComponent> addressComponents;

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public List<DTOIntAddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<DTOIntAddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }
}
