package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
public class DTOIntDelivery {

    @Valid
    @NotNull(groups = ValidationGroup.CreateProductClaim.class)
    private DTOIntDeliveryType deliveryType;

    public DTOIntDeliveryType getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(DTOIntDeliveryType deliveryType) {
        this.deliveryType = deliveryType;
    }
}
