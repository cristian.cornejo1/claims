package com.bbva.pzic.claims.business.dto;

import com.bbva.pzic.claims.util.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class DTOIntTransaction {

    private String id;
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private Date operationDate;
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private String concept;
    @Valid
    private DTOIntTransactionType transactionType;
    @Valid
    @NotNull(groups = ValidationGroup.ValidateAutomaticRefundTransaction.class)
    private DTOIntLocalAmount localAmount;
    @Valid
    private DTOIntStore store;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public DTOIntTransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(DTOIntTransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public DTOIntLocalAmount getLocalAmount() {
        return localAmount;
    }

    public void setLocalAmount(DTOIntLocalAmount localAmount) {
        this.localAmount = localAmount;
    }

    public DTOIntStore getStore() {
        return store;
    }

    public void setStore(DTOIntStore store) {
        this.store = store;
    }
}
