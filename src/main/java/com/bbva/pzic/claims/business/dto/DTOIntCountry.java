package com.bbva.pzic.claims.business.dto;

/**
 * Created on 22/02/2021.
 *
 * @author Entelgy
 */
public class DTOIntCountry {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
