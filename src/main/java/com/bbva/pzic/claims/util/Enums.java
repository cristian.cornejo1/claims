package com.bbva.pzic.claims.util;

/**
 * Created on 02/02/2021.
 *
 * @author Entelgy
 */
public final class Enums {

    public static final String ENUM_CLAIMS_IDENTITYDOCUMENT_DOCUMENTYPE_ID = "claims.identityDocument.documentType.id";
    public static final String ENUM_CLAIMS_PERSONTYPE_ID = "claims.personType.id";
    public static final String ENUM_CLAIMS_CLAIMTYPE_ID = "claims.claimType.id";
    public static final String ENUM_CLAIMS_RESOLUTION_ID = "claims.resolution.id";
    public static final String ENUM_CLAIMS_STATUS_ID = "claims.status.id";
    public static final String ENUM_CLAIMS_CATEGORY_ID = "claims.category.id";
    public static final String ENUM_CLAIMS_PETITIONER_CONTACTDETAILTYPE = "claims.petitioner.contactDetailType";
    public static final String ENUM_CLAIMS_PETITIONER_CONTACT_PHONETYPE = "claims.petitioner.contact.phoneType";
    public static final String CLAIMS_DELIVERY_DELIVERYTYPE = "claims.delivery.deliveryType";

    public static final String AUTOMATICREFUNDTRANSACTIONS_STATUS_ID = "automaticRefundTransactions.status.id";
    public static final String AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID = "automaticRefundTransactions.businessAgents.id";


    private Enums() {
    }
}