package com.bbva.pzic.claims.util.connection.rest;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.jee.arq.spring.core.rest.requests.RestRequest;
import com.bbva.pzic.claims.util.connection.RestConnectionProcessor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Created on 22/06/2016.
 *
 * @author Entelgy
 */
public abstract class RestGetConnection<S> extends RestConnectionProcessor {

    private static final Log LOG = LogFactory.getLog(RestGetConnection.class);

    public S connect(final String urlPropertyValue, final HashMap<String, String> queryParams, final Map<String, String> headerParams) {
        return connect(urlPropertyValue, null, queryParams, headerParams);
    }

    public S connect(final String urlPropertyValue, final Map<String, String> pathParams, final HashMap<String, String> queryParams, final Map<String, String> headerParams) {
        String url = getProperty(urlPropertyValue);
        if (pathParams != null) {
            url = replacePathParamToUrl(url, pathParams);
        }

        if (queryParams != null) {
            LOG.info("Request query queryParams: " + Arrays.toString(queryParams.entrySet().toArray()));
        }

        RestRequest request = new RestRequest.Builder("GET", url)
                .queryParams(queryParams)
                .headers(buildOptionalHeaders(headerParams))
                .useProxy(useProxy)
                .build();

        RestConnectorResponse rcr = proxyRestConnector.doRequest(request);

        final S response = buildResponse(rcr, 0);

        evaluateResponse(response, rcr.getStatusCode());

        return response;
    }

    protected abstract void evaluateResponse(S response, int statusCode);
}
