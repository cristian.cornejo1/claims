package com.bbva.pzic.claims.util;

/**
 * Created on 11/11/2020.
 *
 * @author Entelgy.
 */
public class Constants {

    public static final String DATE_LONG_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";
    public static final String CUSTOMER_ID = "customer.id";
    public static final String CODIGO_CENTRAL = "codigoCentral";
    public static final String ID_CANAL = "idCanal";
    public static final String PROPERTIES_CANAL = "aap";

    private Constants(){
    }
}
