package com.bbva.pzic.claims.util.connection.rest;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.pzic.claims.util.connection.RestConnectionProcessor;

import java.util.Map;

/**
 * Created on 22/06/2016.
 *
 * @author Entelgy
 */
public abstract class RestPostConnection<P, S> extends RestConnectionProcessor {

    protected S connect(final String urlPropertyValue, final P entityPayload) {
        return connect(urlPropertyValue, null, entityPayload);
    }

    protected S connect(final String urlPropertyValue, final Map<String, String> headerParams, final P entityPayload) {
        String url = getProperty(urlPropertyValue);
        String payload = buildPayload(entityPayload);

        RestConnectorResponse rcr = proxyRestConnector.doPost(url, null, buildOptionalHeaders(headerParams), payload, useProxy);

        final S response = buildResponse(rcr, 1);

        evaluateResponse(response, rcr.getStatusCode());

        return response;
    }

    protected abstract void evaluateResponse(S response, int statusCode);
}
