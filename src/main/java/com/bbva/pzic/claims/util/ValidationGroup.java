package com.bbva.pzic.claims.util;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
public interface ValidationGroup {

    interface ListAutomaticRefundTransactions {
    }

    interface ValidateAutomaticRefundTransaction {
    }

    interface CreateProductClaim {
    }

    interface SearchProductClaims {
    }

    interface GetProductClaim {
    }

    interface SendEmailProductClaim {
    }

    interface ModifyPartialAutomaticRefundTransaction{
    }
}