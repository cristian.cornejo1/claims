package com.bbva.pzic.claims.util.connection.rest;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.jee.arq.spring.core.rest.requests.RestRequest;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.util.connection.RestConnectionProcessorHeaders;
import java.util.List;
import java.util.Map;

public abstract class RestPostConnectionHeader<P> extends RestConnectionProcessorHeaders {

    protected ResponseType connect(final String urlPropertyValue, final P entityPayload, final String registryId, final List<String> obfuscationList) {
        return connect(urlPropertyValue, null, entityPayload, registryId, obfuscationList);
    }

    public ResponseType connect(final String urlPropertyValue, final Map<String, String> headers, final P entityPayload, final String registryId) {
        return connect(urlPropertyValue, headers, entityPayload, registryId, null);
    }

    public ResponseType connect(final String urlPropertyValue, final Map<String, String> headers, final P entityPayload, final String registryId, List<String> obfuscationList) {
        String url = getProperty(urlPropertyValue);
        String payload = buildPayload(entityPayload);

        RestRequest request = new RestRequest.Builder("POST", url)
                .headers(buildOptionalHeaders(headers))
                .obfuscationMask(obfuscationList)
                .payload(payload)
                .useProxy(useProxy)
                .build();

        RestConnectorResponse rcr = proxyRestConnector.doRequest(request);

        return buildResponse(rcr, registryId);
    }
}
