package com.bbva.pzic.claims.util.connection;

import com.bbva.jee.arq.spring.core.rest.RestConnectorResponse;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.util.Errors;
import com.bbva.pzic.claims.util.mappers.ObjectMapperHelper;
import com.bbva.pzic.routine.translator.facade.Translator;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Map;

/**
 * Created on 14/06/2016.
 *
 * @author Entelgy
 */
public class RestConnectionProcessorHeaders extends RestConnectionProcessor {

    private static final Log LOG = LogFactory.getLog(RestConnectionProcessor.class);

    protected ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();

    @Autowired
    private Translator translator;

    protected <S> ResponseType buildResponse(final RestConnectorResponse rcr, final String registryId) {
        if (rcr == null) {
            LOG.error("com.bbva.jee.arq.spring.core.rest.RestConnectorResponse is null for SocketTimeoutException");
            throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
        }

        if (rcr.getStatusCode() >= HttpStatus.SC_OK && rcr.getStatusCode() <= HttpStatus.SC_MULTI_STATUS) {
            try {
                String response = rcr.getResponseBody();
                if (response == null) {
                    return null;
                }
                return mapper.readValue(response, new TypeReference<ResponseType>() {
                });
            } catch (IOException e) {
                LOG.error(String.format("Error converting JSON: %s", e.getMessage()));
                throw new BusinessServiceException(Errors.TECHNICAL_ERROR, e);
            }
        } else {
            throw restConnectorResponseToError(rcr.getHeaders(), registryId);
        }
    }

    protected BusinessServiceException restConnectorResponseToError(final Map<String, String> responseHeaders, final String registryId) {
        String errorCode = responseHeaders.get("errorCode");
        String errorMessage = responseHeaders.get("errorMessage");
        LOG.debug(String.format("[Rest Connector] Loaded responseHeaders = %s", responseHeaders));
        LOG.debug(String.format("[Rest Connector] Loaded ERROR_CODE = %s", errorCode));
        LOG.debug(String.format("[Rest Connector] Loaded ERROR_MESSAGE = %s", errorMessage));
        String errorAlias = translator.translateErrorCode(registryId, errorCode);

        if (errorAlias == null) {
            BusinessServiceException businessServiceException = new BusinessServiceException(Errors.TECHNICAL_ERROR);

            if (errorCode != null)
                businessServiceException.setErrorCode(errorCode);

            if (errorMessage != null)
                businessServiceException.setErrorMessage(errorMessage);

            throw businessServiceException;

        } else {
            if (StringUtils.isEmpty(errorCode) || StringUtils.isEmpty(errorMessage)) {
                LOG.error("Can't create an exception with null errorCode or errorMessage");
                throw new BusinessServiceException(Errors.TECHNICAL_ERROR);
            }
            LOG.info(String.format("Creating exception with errorMessage: '%s'", errorMessage));
            BusinessServiceException businessServiceException = new BusinessServiceException(errorAlias);
            businessServiceException.setErrorCode(errorAlias);
            businessServiceException.setErrorMessage(errorMessage);
            throw businessServiceException;
        }
    }
}
