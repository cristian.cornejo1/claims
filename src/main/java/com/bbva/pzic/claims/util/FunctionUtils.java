package com.bbva.pzic.claims.util;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.routine.commons.utils.DateUtils;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FunctionUtils {

    private FunctionUtils() {
    }

    private static final String TRUE_1 = "1";
    private static final String TRUE_S = "S";
    private static final String DATE = "yyyy-MM-dd";
    private static final String TRUE = "true";

    public static Boolean convertTo(final String source) {
        return source == null ? null : (TRUE_1.equalsIgnoreCase(source) ? Boolean.TRUE : Boolean.FALSE);
    }

    public static Boolean convertToBoolean(final String source) {
        return source == null ? null : (TRUE_S.equalsIgnoreCase(source) ? Boolean.TRUE : Boolean.FALSE);
    }

    public static Calendar buildDatetime(String date, String pattern) {
        if (StringUtils.isEmpty(date) || StringUtils.isEmpty(pattern)) {
            return null;
        }

        try {
            return DateUtils.toDateTime(date, "", null, pattern);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    public static Date buildDate(String date) {
        if (date == null) {
            return null;
        }

        try {
            return DateUtils.toDate(date);
        } catch (ParseException e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    public static String convertBooleanToString(final Boolean value) {
        return value == null ? null : value ? "S" : "N";
    }

    public static String dateToString(Date date){
        if (date == null){
            return null;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE);
            return sdf.format(date);
        } catch (Exception e) {
            throw new BusinessServiceException(Errors.WRONG_DATE, e);
        }
    }

    public static Integer convertBooleanToInteger(final Boolean value) {
        return value == null ? null : value ? 1 : 0;
    }

    public static Boolean convertStringToBoolean(final String value) {
        return value == null ? null : (TRUE.equalsIgnoreCase(value) ? Boolean.TRUE : Boolean.FALSE);
    }
}