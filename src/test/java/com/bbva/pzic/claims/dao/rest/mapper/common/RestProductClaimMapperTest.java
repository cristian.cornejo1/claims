package com.bbva.pzic.claims.dao.rest.mapper.common;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.canonic.ClaimType;
import com.bbva.pzic.claims.dao.rest.model.harec.RepresentanteType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.model.harec.TitularType;
import com.bbva.pzic.claims.dao.rest.mock.stubs.ResultadoModelsStubs;
import com.bbva.pzic.claims.facade.v0.dto.PersonType;
import com.bbva.pzic.claims.facade.v0.dto.PetitionerSearch;
import com.bbva.pzic.claims.facade.v0.dto.StatusSearch;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.claims.EntityMock.*;
import static com.bbva.pzic.claims.util.Enums.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 12/1/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestProductClaimMapperTest {

    @InjectMocks
    private RestProductClaimMapper mapper;

    @Mock
    private Translator translator;

    @Test
    public void mapOutPersonTypeEnumTest() throws IOException {
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PERSONTYPE_ID, PERSON_TYPE_ID_LEGAL_FRONTEND_VALUE))
                .thenReturn(PERSON_TYPE_ID_LEGAL_ENUM_VALUE);

        PersonType input = EntityMock.getInstance().buildProductClaims().getPersonType();
        PersonType result = mapper.mapOutPersonTypeEnum(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getName());

        assertEquals(PERSON_TYPE_ID_LEGAL_ENUM_VALUE, result.getId());
        assertEquals(input.getName(), result.getName());
    }

    @Test
    public void mapOutPersonTypeEnumNullTest() {
        PersonType result = mapper.mapOutPersonTypeEnum(null);

        assertNull(result);
    }

    @Test
    public void mapOutClaimTypeEnumTest() throws IOException {
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_CLAIMTYPE_ID, CLAIM_TYPE_ID_PETITION_FRONTEND_VALUE))
                .thenReturn(CLAIM_TYPE_ID_PETITION_ENUM_VALUE);

        ClaimType input = EntityMock.getInstance().buildProductClaims().getClaimType();
        ClaimType result = mapper.mapOutClaimTypeEnum(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getDescription());

        assertEquals(CLAIM_TYPE_ID_PETITION_ENUM_VALUE, result.getId());
        assertEquals(input.getDescription(), result.getDescription());
    }

    @Test
    public void mapOutClaimTypeEnumNullTest() {
        ClaimType result = mapper.mapOutClaimTypeEnum(null);

        assertNull(result);
    }

    @Test
    public void mapOutStatusEnumTest() throws IOException {
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_STATUS_ID, STATUS_ID_PETITION_FRONTEND_VALUE))
                .thenReturn(STATUS_ID_PETITION_ENUM_VALUE);

        StatusSearch input = EntityMock.getInstance().buildProductClaims().getStatus();
        StatusSearch result = mapper.mapOutStatusEnum(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getDescription());

        assertEquals(STATUS_ID_PETITION_ENUM_VALUE, result.getId());
        assertEquals(input.getDescription(), result.getDescription());
    }

    @Test
    public void mapOutStatusEnumNullTest() {
        StatusSearch result = mapper.mapOutStatusEnum(null);

        assertNull(result);
    }

    @Test
    public void mapOutKnownHolderTitularTest() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        PetitionerSearch result = mapper.mapOutPetitioner(titularInput);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getSecondLastName());
        assertNotNull(result.getBankRelationType());
        assertNotNull(result.getPetitionerType());
        assertNotNull(result.getPetitionerType().getId());

        assertEquals(titularInput.getCodigoCentral(), result.getId());
        assertEquals(titularInput.getNombres(), result.getFirstName());
        assertEquals(titularInput.getApellidoPaterno(), result.getLastName());
        assertEquals(titularInput.getApellidoMaterno(), result.getSecondLastName());
        assertEquals("KNOWN", result.getBankRelationType());
        assertEquals("HOLDER", result.getPetitionerType().getId());
    }


    @Test
    public void mapOutUnknownHolderTitularTest() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        titularInput.setCodigoCentral(null);
        PetitionerSearch result = mapper.mapOutPetitioner(titularInput);

        assertNotNull(result);
        assertNull(result.getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getSecondLastName());
        assertNotNull(result.getBankRelationType());
        assertNotNull(result.getPetitionerType());
        assertNotNull(result.getPetitionerType().getId());

        assertEquals(titularInput.getNombres(), result.getFirstName());
        assertEquals(titularInput.getApellidoPaterno(), result.getLastName());
        assertEquals(titularInput.getApellidoMaterno(), result.getSecondLastName());
        assertEquals("UNKNOWN", result.getBankRelationType());
        assertEquals("HOLDER", result.getPetitionerType().getId());
    }

    @Test
    public void mapOutWithTitularNullTest() {
        PetitionerSearch result = mapper.mapOutPetitioner(null);

        assertNull(result);
    }

    @Test
    public void mapOutKnownRepresentativePetitionerTest() throws IOException {
    // IF(DTOdata.petitioners[0].petitioner.AUTHORIZED.id IS NOT NULL && DTORepresentante.nombres IS NOT NULL)
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        RepresentanteType representanteInput = input.getRequerimientos().get(1).getRepresentante();
        PetitionerSearch result = mapper.mapOutRepresentativePetitioner(representanteInput);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getSecondLastName());
        assertNotNull(result.getBankRelationType());
        assertNotNull(result.getPetitionerType());
        assertNotNull(result.getPetitionerType().getId());

        assertEquals(representanteInput.getCodigoCentral(), result.getId());
        assertEquals(representanteInput.getNombres(), result.getFirstName());
        assertEquals(representanteInput.getApellidoPaterno(), result.getLastName());
        assertEquals(representanteInput.getApellidoMaterno(), result.getSecondLastName());
        assertEquals("KNOWN", result.getBankRelationType());
        assertEquals("AUTHORIZED", result.getPetitionerType().getId());
    }

    @Test
    public void mapOutUknownRepresentativePetitionerTest() throws IOException {
        // IF(DTOdata.petitioners[0].petitioner.AUTHORIZED.id IS NULL && DTORepresentante.nombres IS NOT NULL)
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        RepresentanteType representanteInput = input.getRequerimientos().get(1).getRepresentante();
        representanteInput.setCodigoCentral(null);
        PetitionerSearch result = mapper.mapOutRepresentativePetitioner(representanteInput);

        assertNotNull(result);
        assertNull(result.getId());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getSecondLastName());
        assertNotNull(result.getBankRelationType());
        assertNotNull(result.getPetitionerType());
        assertNotNull(result.getPetitionerType().getId());

        assertEquals(representanteInput.getNombres(), result.getFirstName());
        assertEquals(representanteInput.getApellidoPaterno(), result.getLastName());
        assertEquals(representanteInput.getApellidoMaterno(), result.getSecondLastName());
        assertEquals("UNKNOWN", result.getBankRelationType());
        assertEquals("AUTHORIZED", result.getPetitionerType().getId());
    }

    @Test
    public void mapOutRepresentativePetitionerNullTest(){
        PetitionerSearch result = mapper.mapOutRepresentativePetitioner(null);
        assertNull(result);
    }
}
