package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.DTOIntAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultado;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponse;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import static com.bbva.pzic.claims.EntityMock.APPROVED_STATUS_ID_BACKEND_VALUE;
import static com.bbva.pzic.claims.EntityMock.APPROVED_STATUS_ID_ENUM_VALUE;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestValidateAutomaticRefundTransactionMapperTest {

    @InjectMocks
    private RestValidateAutomaticRefundTransactionMapper mapper;

    @Mock
    private Translator translator;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.status.id",
                APPROVED_STATUS_ID_BACKEND_VALUE)).thenReturn(APPROVED_STATUS_ID_ENUM_VALUE);
        when(serviceInvocationContext.getProperty(anyString())).thenReturn("1300001");
    }

    @Test
    public void mapInFull() throws IOException {
        DTOIntAutomaticRefundTransaction input = EntityMock.getInstance().getDTOIntAutomaticRefundTransaction();
        ModelResultado result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getTitular());
        assertNotNull(result.getTitular().getCodigoCentral());
        assertNotNull(result.getTitular().getTipoDoi());
        assertNotNull(result.getTitular().getDoi());
        assertNotNull(result.getTitular().getSegmento());
        assertNotNull(result.getTitular().getSegmento().getCodigo());
        assertNotNull(result.getRequerimientos());
        assertNotNull(result.getRequerimientos().getContactos());
        //PHONE
        assertNotNull(result.getRequerimientos().getContactos().get(0).getTipoContacto());
        assertNotNull(result.getRequerimientos().getContactos().get(0).getNumero());
        assertNotNull(result.getRequerimientos().getContactos().get(0).getTipoFijo());
        assertNotNull(result.getRequerimientos().getContactos().get(0).getPaisFijo());
        assertNotNull(result.getRequerimientos().getContactos().get(0).getCodigoPaisFijo());
        assertNotNull(result.getRequerimientos().getContactos().get(0).getCodigoRegionalFijo());
        assertNotNull(result.getRequerimientos().getContactos().get(0).getAnexoFijo());
        assertNull(result.getRequerimientos().getContactos().get(0).getOperadorCelular());
        assertNull(result.getRequerimientos().getContactos().get(0).getCodigoPaisCelular());
        assertNull(result.getRequerimientos().getContactos().get(0).getCorreo());
        assertNull(result.getRequerimientos().getContactos().get(0).getNotificacionesCorreo());

        //MOBILE
        assertNotNull(result.getRequerimientos().getContactos().get(1).getTipoContacto());
        assertNotNull(result.getRequerimientos().getContactos().get(1).getNumero());
        assertNotNull(result.getRequerimientos().getContactos().get(1).getOperadorCelular());
        assertNotNull(result.getRequerimientos().getContactos().get(1).getOperadorCelular().getCodigo());
        assertNotNull(result.getRequerimientos().getContactos().get(1).getCodigoPaisCelular());
        assertNull(result.getRequerimientos().getContactos().get(1).getCodigoPaisFijo());
        assertNull(result.getRequerimientos().getContactos().get(1).getCodigoRegionalFijo());
        assertNull(result.getRequerimientos().getContactos().get(1).getAnexoFijo());
        assertNull(result.getRequerimientos().getContactos().get(1).getTipoFijo());
        assertNull(result.getRequerimientos().getContactos().get(1).getPaisFijo());
        assertNull(result.getRequerimientos().getContactos().get(1).getCorreo());
        assertNull(result.getRequerimientos().getContactos().get(1).getNotificacionesCorreo());

        //EMAIL
        assertNotNull(result.getRequerimientos().getContactos().get(2).getCorreo());
        assertNotNull(result.getRequerimientos().getContactos().get(2).getNotificacionesCorreo());
        assertNull(result.getRequerimientos().getContactos().get(2).getCodigoPaisFijo());
        assertNull(result.getRequerimientos().getContactos().get(2).getCodigoRegionalFijo());
        assertNull(result.getRequerimientos().getContactos().get(2).getAnexoFijo());
        assertNull(result.getRequerimientos().getContactos().get(2).getTipoFijo());
        assertNull(result.getRequerimientos().getContactos().get(2).getPaisFijo());
        assertNull(result.getRequerimientos().getContactos().get(2).getOperadorCelular());
        assertNull(result.getRequerimientos().getContactos().get(2).getCodigoPaisCelular());

        assertNotNull(result.getNumeroCaso());
        assertNotNull(result.getMotivo());
        assertNotNull(result.getMotivo().getCodigo());
        assertNotNull(result.getOperacion());

        //MPTAR
        assertNotNull(result.getOperacion().get(0));
        assertNotNull(result.getOperacion().get(0).getContrato());
        assertNotNull(result.getOperacion().get(0).getContrato().getAliasProducto());
        assertNotNull(result.getOperacion().get(0).getContrato().getTarjeta());
        assertNotNull(result.getOperacion().get(0).getModoAbono());
        assertNotNull(result.getOperacion().get(0).getModoAbono().getCodigo());

        assertNotNull(result.getOperacion().get(0).getObservacion());
        assertNotNull(result.getOperacion().get(0).getTipoOperacion());
        assertNotNull(result.getOperacion().get(0).getTipoOperacion().getCodigo());
        assertNotNull(result.getOperacion().get(0).getMoneda().getCodigo());
        assertNotNull(result.getOperacion().get(0).getImporte());
        assertNotNull(result.getOperacion().get(0).getBanco());
        assertNotNull(result.getOperacion().get(0).getBanco().getId());
        assertNotNull(result.getOperacion().get(0).getCentroContable().getCodigo());
        assertNotNull(result.getOperacion().get(0).getProductoSeguro());
        assertNotNull(result.getOperacion().get(0).getProductoSeguro().getCodigo());
        assertNotNull(result.getOperacion().get(0).getProductoSeguro().getMoneda().getCodigo());
        assertNotNull(result.getOperacion().get(0).getProductoSeguro().getImporte());
        assertNotNull(result.getOperacion().get(0).getContratoOrigen());

        //TARJE
        assertNotNull(result.getOperacion().get(0).getContratoOrigen().getTipo());
        assertNotNull(result.getOperacion().get(0).getContratoOrigen().getTarjeta());
        assertNotNull(result.getOperacion().get(0).getContratoOrigen().getTarjeta().getNumero());
        assertNotNull(result.getOperacion().get(0).getContratoOrigen().getNumeroTipo());
        assertNotNull(result.getOperacion().get(0).getContratoOrigen().getNumero());
        assertNotNull(result.getOperacion().get(0).getContratoOrigen().getSubTipo());
        assertNotNull(result.getOperacion().get(0).getMovimiento());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0));
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getNumero());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getFecha());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getDescripcion());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getTipo());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getImporte());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getMoneda());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getEstablecimiento());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getEstablecimiento().getCodigo());
        assertNotNull(result.getOperacion().get(0).getMovimiento().get(0).getEstablecimiento().getNombre());

        //No MPTAR
        assertNotNull(result.getOperacion().get(1));
        assertNotNull(result.getOperacion().get(1).getContrato());
        assertNotNull(result.getOperacion().get(1).getContrato().getAliasProducto());
        assertNotNull(result.getOperacion().get(1).getContrato().getNumero());
        assertNull(result.getOperacion().get(1).getContrato().getTarjeta());
        assertNull(result.getOperacion().get(1).getModoAbono());
        assertNotNull(result.getOperacion().get(1).getObservacion());
        assertNotNull(result.getOperacion().get(1).getTipoOperacion());
        assertNotNull(result.getOperacion().get(1).getTipoOperacion().getCodigo());
        assertNotNull(result.getOperacion().get(1).getMoneda().getCodigo());
        assertNotNull(result.getOperacion().get(1).getImporte());
        assertNotNull(result.getOperacion().get(1).getBanco());
        assertNotNull(result.getOperacion().get(1).getBanco().getId());
        assertNotNull(result.getOperacion().get(1).getCentroContable().getCodigo());
        assertNotNull(result.getOperacion().get(1).getProductoSeguro());
        assertNotNull(result.getOperacion().get(1).getProductoSeguro().getCodigo());
        assertNotNull(result.getOperacion().get(1).getProductoSeguro().getMoneda().getCodigo());
        assertNotNull(result.getOperacion().get(1).getProductoSeguro().getImporte());
        assertNotNull(result.getOperacion().get(1).getContratoOrigen());

        //CUENT
        assertNotNull(result.getOperacion().get(1).getContratoOrigen().getTipo());
        assertNotNull(result.getOperacion().get(1).getContratoOrigen().getNumero());
        assertNotNull(result.getOperacion().get(1).getContratoOrigen().getNumeroTipo());
        assertNotNull(result.getOperacion().get(1).getContratoOrigen().getSubTipo());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getNumero());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getFecha());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getDescripcion());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getTipo());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getImporte());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getMoneda());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getEstablecimiento());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getEstablecimiento().getNombre());
        assertNotNull(result.getOperacion().get(1).getMovimiento().get(0).getEstablecimiento().getCodigo());

        //MPTAR
        assertNotNull(result.getOperacion().get(2));
        assertNotNull(result.getOperacion().get(2).getContrato());
        assertNotNull(result.getOperacion().get(2).getContrato().getAliasProducto());
        assertNotNull(result.getOperacion().get(2).getContrato().getTarjeta());
        assertNotNull(result.getOperacion().get(2).getModoAbono());
        assertNotNull(result.getOperacion().get(2).getModoAbono().getCodigo());

        assertNotNull(result.getOperacion().get(2).getObservacion());
        assertNotNull(result.getOperacion().get(2).getTipoOperacion());
        assertNotNull(result.getOperacion().get(2).getTipoOperacion().getCodigo());
        assertNotNull(result.getOperacion().get(2).getMoneda().getCodigo());
        assertNotNull(result.getOperacion().get(2).getImporte());
        assertNotNull(result.getOperacion().get(2).getBanco());
        assertNotNull(result.getOperacion().get(2).getBanco().getId());
        assertNotNull(result.getOperacion().get(2).getCentroContable().getCodigo());
        assertNotNull(result.getOperacion().get(2).getProductoSeguro());
        assertNotNull(result.getOperacion().get(2).getProductoSeguro().getMoneda().getCodigo());
        assertNotNull(result.getOperacion().get(2).getProductoSeguro().getImporte());
        assertNotNull(result.getOperacion().get(2).getContratoOrigen());

        //PREST
        assertNotNull(result.getOperacion().get(2).getContratoOrigen().getTipo());
        assertNotNull(result.getOperacion().get(2).getContratoOrigen().getNumero());
        assertNotNull(result.getOperacion().get(2).getContratoOrigen().getNumeroTipo());
        assertNotNull(result.getOperacion().get(2).getContratoOrigen().getSubTipo());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getNumero());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getFecha());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getDescripcion());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getTipo());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getImporte());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getMoneda());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getEstablecimiento());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getEstablecimiento().getNombre());
        assertNotNull(result.getOperacion().get(2).getMovimiento().get(0).getEstablecimiento().getCodigo());


        assertEquals(input.getPetitioner().getId(), result.getTitular().getCodigoCentral());
        assertEquals(input.getPetitioner().getPetitioner().getIdentityDocument().getDocumentType().getId(), result.getTitular().getTipoDoi());
        assertEquals(input.getPetitioner().getPetitioner().getIdentityDocument().getDocumentNumber(), result.getTitular().getDoi());
        assertEquals(input.getPetitioner().getPetitioner().getIdentityDocument().getDocumentType().getId(), result.getTitular().getTipoDoi());
        assertEquals(input.getPetitioner().getPetitioner().getSegment().getId(), result.getTitular().getSegmento().getCodigo());

        //PHONE
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getContactDetailType(), result.getRequerimientos().getContactos().get(0).getTipoContacto());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getNumber(), result.getRequerimientos().getContactos().get(0).getNumero());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getPhoneType(), result.getRequerimientos().getContactos().get(0).getTipoFijo());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getCountry().getId(), result.getRequerimientos().getContactos().get(0).getPaisFijo());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getCountryCode(), result.getRequerimientos().getContactos().get(0).getCodigoPaisFijo());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getRegionalCode(), result.getRequerimientos().getContactos().get(0).getCodigoRegionalFijo());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getExtension(), result.getRequerimientos().getContactos().get(0).getAnexoFijo());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getRegionalCode(), result.getRequerimientos().getContactos().get(0).getCodigoRegionalFijo());

        //MOBILE
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getContactDetailType(), result.getRequerimientos().getContactos().get(1).getTipoContacto());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getNumber(), result.getRequerimientos().getContactos().get(1).getNumero());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getPhoneCompany().getId(), result.getRequerimientos().getContactos().get(1).getOperadorCelular().getCodigo());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getCountryCode(), result.getRequerimientos().getContactos().get(1).getCodigoPaisCelular());

        //EMAIL
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getContactDetailType(), result.getRequerimientos().getContactos().get(2).getTipoContacto());
        assertEquals(input.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getAddress(), result.getRequerimientos().getContactos().get(2).getCorreo());
        assertEquals("S", result.getRequerimientos().getContactos().get(2).getNotificacionesCorreo());

        assertEquals(input.getRelatedClaim().getId(), result.getNumeroCaso());
        assertEquals(input.getReason().getId(), result.getMotivo().getCodigo());

        //MPTAR
        assertEquals(input.getOperations().get(0).getSubproductRefund().getId(), result.getOperacion().get(0).getContrato().getAliasProducto());
        assertEquals(input.getOperations().get(0).getSubproductRefund().getNumber(), result.getOperacion().get(0).getContrato().getTarjeta());
        assertEquals(input.getOperations().get(0).getSubproductRefund().getRefundMode(), result.getOperacion().get(0).getModoAbono().getCodigo());
        assertEquals(input.getOperations().get(0).getDetail(), result.getOperacion().get(0).getObservacion());
        assertEquals(input.getOperations().get(0).getOperationType(), result.getOperacion().get(0).getTipoOperacion().getCodigo());
        assertEquals(input.getOperations().get(0).getRequestedAmount().getCurrency(), result.getOperacion().get(0).getMoneda().getCodigo());
        assertEquals(input.getOperations().get(0).getRequestedAmount().getAmount(), result.getOperacion().get(0).getImporte());
        assertEquals(input.getOperations().get(0).getBank().getId(), result.getOperacion().get(0).getBanco().getId());
        assertEquals(input.getOperations().get(0).getBank().getBranch().getId(), result.getOperacion().get(0).getCentroContable().getCodigo());
        assertEquals(input.getOperations().get(0).getRelatedContract().getId(), result.getOperacion().get(0).getProductoSeguro().getCodigo());
        assertEquals(input.getOperations().get(0).getRelatedContract().getRequestedAmount().getAmount(), result.getOperacion().get(0).getProductoSeguro().getImporte());
        assertEquals(input.getOperations().get(0).getRelatedContract().getRequestedAmount().getCurrency(), result.getOperacion().get(0).getProductoSeguro().getMoneda().getCodigo());

        //TARJE
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getContractType(), result.getOperacion().get(0).getContratoOrigen().getTipo());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getNumber(), result.getOperacion().get(0).getContratoOrigen().getTarjeta().getNumero());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getNumberType().getId(), result.getOperacion().get(0).getContratoOrigen().getNumeroTipo());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getId(), result.getOperacion().get(0).getMovimiento().get(0).getNumero());
        assertEquals(FunctionUtils.dateToString(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getOperationDate()), result.getOperacion().get(0).getMovimiento().get(0).getFecha());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getConcept(), result.getOperacion().get(0).getMovimiento().get(0).getDescripcion());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getTransactionType().getId(), result.getOperacion().get(0).getMovimiento().get(0).getTipo());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount(), result.getOperacion().get(0).getMovimiento().get(0).getImporte());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency(), result.getOperacion().get(0).getMovimiento().get(0).getMoneda().getCodigo());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getId(), result.getOperacion().get(0).getMovimiento().get(0).getEstablecimiento().getCodigo());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getName(), result.getOperacion().get(0).getMovimiento().get(0).getEstablecimiento().getNombre());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getId(), result.getOperacion().get(0).getContratoOrigen().getNumero());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getSubProduct().getId(), result.getOperacion().get(0).getContratoOrigen().getSubTipo());

        //CUENT
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getContractType(), result.getOperacion().get(1).getContratoOrigen().getTipo());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getNumber(), result.getOperacion().get(1).getContratoOrigen().getNumero());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getNumberType().getId(), result.getOperacion().get(1).getContratoOrigen().getNumeroTipo());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getId(), result.getOperacion().get(1).getMovimiento().get(0).getNumero());
        assertEquals(FunctionUtils.dateToString(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getOperationDate()), result.getOperacion().get(1).getMovimiento().get(0).getFecha());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getConcept(), result.getOperacion().get(1).getMovimiento().get(0).getDescripcion());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getTransactionType().getId(), result.getOperacion().get(1).getMovimiento().get(0).getTipo());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount(), result.getOperacion().get(1).getMovimiento().get(0).getImporte());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency(), result.getOperacion().get(1).getMovimiento().get(0).getMoneda().getCodigo());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getId(), result.getOperacion().get(1).getMovimiento().get(0).getEstablecimiento().getCodigo());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getName(), result.getOperacion().get(1).getMovimiento().get(0).getEstablecimiento().getNombre());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getAccountType().getId(), result.getOperacion().get(1).getContratoOrigen().getSubTipo());

        //PREST
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getContractType(), result.getOperacion().get(2).getContratoOrigen().getTipo());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getNumber(), result.getOperacion().get(2).getContratoOrigen().getNumero());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getNumberType().getId(), result.getOperacion().get(2).getContratoOrigen().getNumeroTipo());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getId(), result.getOperacion().get(2).getMovimiento().get(0).getNumero());
        assertEquals(FunctionUtils.dateToString(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getOperationDate()), result.getOperacion().get(2).getMovimiento().get(0).getFecha());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getConcept(), result.getOperacion().get(2).getMovimiento().get(0).getDescripcion());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getTransactionType().getId(), result.getOperacion().get(2).getMovimiento().get(0).getTipo());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount(), result.getOperacion().get(2).getMovimiento().get(0).getImporte());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency(), result.getOperacion().get(2).getMovimiento().get(0).getMoneda().getCodigo());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getId(), result.getOperacion().get(2).getMovimiento().get(0).getEstablecimiento().getCodigo());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getName(), result.getOperacion().get(2).getMovimiento().get(0).getEstablecimiento().getNombre());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getLoanType().getId(), result.getOperacion().get(2).getContratoOrigen().getSubTipo());
    }


    @Test
    public void mapInHeaderFull() throws IOException {
        final DTOIntAutomaticRefundTransaction input = EntityMock.getInstance().getDTOIntAutomaticRefundTransaction();
        final Map<String, String> result = mapper.mapHeaderIn(input);

        assertFalse(result.isEmpty());

        assertNotNull(result.get("idCanal"));
        assertNotNull(result.get("callingChannel"));

        assertEquals(input.getApp(), result.get("idCanal"));
        assertEquals("1300001", result.get("callingChannel"));
    }


    @Test
    public void mapInHeaderEmpty() {
        final Map<String, String> result = mapper.mapHeaderIn(new DTOIntAutomaticRefundTransaction());

        assertFalse(result.isEmpty());
        assertNull(result.get("idCanal"));
        assertNotNull(result.get("callingChannel"));
    }

    @Test
    public void mapOutFull() throws IOException {
        ModelResultadoResponse input = EntityMock.getInstance().buildValidateAutomaticRefundTransactionModelResponse();

        AutomaticRefundTransaction result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getSubStatus());
        assertNotNull(result.getStatus().getSubStatus().getId());
        assertNotNull(result.getStatus().getSituation());
        assertNotNull(result.getStatus().getSituation().getId());
        assertNotNull(result.getStatus().getSituation().getName());


        assertEquals(input.getResultado().getNumero(), result.getId());
        assertEquals(APPROVED_STATUS_ID_ENUM_VALUE, result.getStatus().getId());
        assertEquals(input.getResultado().getEstado().getNombre(), result.getStatus().getName());
        assertEquals(input.getResultado().getEstadoAtencion().getCodigo(), result.getStatus().getSubStatus().getId());
        assertEquals(input.getResultado().getEstadoAtencion().getDescripcion(), result.getStatus().getSubStatus().getName());
        assertEquals(input.getResultado().getSituacion().getCodigo(), result.getStatus().getSituation().getId());
        assertEquals(input.getResultado().getSituacion().getDescripcion(), result.getStatus().getSituation().getName());
    }

    @Test
    public void mapOutEmpty() {
        final AutomaticRefundTransaction result = mapper.mapOut(new ModelResultadoResponse());
        assertNull(result);
    }
}