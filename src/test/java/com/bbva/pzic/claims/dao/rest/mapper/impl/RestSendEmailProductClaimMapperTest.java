package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputSendEmailProductClaim;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Map;

import static com.bbva.pzic.claims.EntityMock.CONTACTDETAILS_CONTACTTYPE_EMAIL_ENUM_VALUE;
import static com.bbva.pzic.claims.EntityMock.CONTACTDETAILS_CONTACTTYPE_EMAIL_FRONTEND;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 6/12/2019.
 *
 * @author Entelgy.
 */
@RunWith(MockitoJUnitRunner.class)
public class RestSendEmailProductClaimMapperTest {

    @InjectMocks
    private RestSendEmailProductClaimMapper mapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(translator.translateFrontendEnumValueStrictly("claim.contactDetails.contactType.id",
                CONTACTDETAILS_CONTACTTYPE_EMAIL_ENUM_VALUE)).thenReturn(CONTACTDETAILS_CONTACTTYPE_EMAIL_FRONTEND);
    }

    @Test
    public void mapInHeaderFullTest() throws IOException {
        InputSendEmailProductClaim input = EntityMock.getInstance().buildInputSendEmailProductClaim();
        Map<String, String> result = mapper.mapInHeader(input);

        assertNotNull(result);
        assertNotNull(result.get("canal"));

        assertEquals(input.getAap(), result.get("canal"));
    }

    @Test
    public void mapInFullTest() throws IOException {
        InputSendEmailProductClaim input = EntityMock.getInstance().buildInputSendEmailProductClaim();
        RequestType result = mapper.mapInBody(input);

        assertNotNull(result);
        assertNotNull(result.getNumero());
        assertNotNull(result.getTitular());
        assertNotNull(result.getTitular().getCodigoCentral());
        assertNotNull(result.getCorreos());
        assertEquals(1, result.getCorreos().size());
        assertNotNull(result.getCorreos().get(0).getId());
        assertNotNull(result.getCorreos().get(0).getCorreo());
        assertNotNull(result.getTipoContacto());
        assertNotNull(result.getAdjuntos());
        assertEquals(2, result.getAdjuntos().size());
        assertNotNull(result.getAdjuntos().get(0).getId());
        assertNotNull(result.getAdjuntos().get(1).getId());

        assertEquals(input.getProductClaimId(), result.getNumero());
        assertEquals(input.getClientId(), result.getTitular().getCodigoCentral());
        assertEquals(input.getContact().getId(), result.getCorreos().get(0).getId());
        assertEquals(input.getContact().getAddress(), result.getCorreos().get(0).getCorreo());
        assertEquals(CONTACTDETAILS_CONTACTTYPE_EMAIL_FRONTEND, result.getTipoContacto());
        assertEquals(input.getDocuments().get(0).getId(), result.getAdjuntos().get(0).getId());
        assertEquals(input.getDocuments().get(1).getId(), result.getAdjuntos().get(1).getId());
    }

    @Test
    public void mapInContactNullTest() throws IOException {
        InputSendEmailProductClaim input = EntityMock.getInstance().buildInputSendEmailProductClaim();
        input.setContact(null);
        RequestType result = mapper.mapInBody(input);

        assertNotNull(result);
        assertNotNull(result.getNumero());
        assertNotNull(result.getTitular());
        assertNotNull(result.getTitular().getCodigoCentral());
        assertNotNull(result.getCorreos());
        assertNull(result.getTipoContacto());
        assertNotNull(result.getAdjuntos());
        assertEquals(0, result.getCorreos().size());
        assertEquals(2, result.getAdjuntos().size());
        assertNotNull(result.getAdjuntos().get(0).getId());
        assertNotNull(result.getAdjuntos().get(1).getId());
    }

    @Test
    public void mapInDocumentsNullTest() throws IOException {
        InputSendEmailProductClaim input = EntityMock.getInstance().buildInputSendEmailProductClaim();
        input.setDocuments(null);
        RequestType result = mapper.mapInBody(input);

        assertNotNull(result);
        assertNotNull(result.getNumero());
        assertNotNull(result.getTitular());
        assertNotNull(result.getTitular().getCodigoCentral());
        assertNotNull(result.getCorreos());
        assertEquals(1, result.getCorreos().size());
        assertNotNull(result.getCorreos().get(0).getId());
        assertNotNull(result.getCorreos().get(0).getCorreo());
        assertNotNull(result.getTipoContacto());
        assertNotNull(result.getAdjuntos());
        assertEquals(0, result.getAdjuntos().size());
    }
}