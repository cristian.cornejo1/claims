package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputGetAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.ContactDetail;
import com.bbva.pzic.claims.canonic.Operation;
import com.bbva.pzic.claims.canonic.Petitioner;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelContacto;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelOperacion;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultado;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponse;
import com.bbva.pzic.claims.dao.rest.mock.stubs.ResultadoModelsStubs;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.claims.EntityMock.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestGetAutomaticRefundTransactionMapperTest {

    @InjectMocks
    private RestGetAutomaticRefundTransactionMapper mapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.status.id",
                APPROVED_STATUS_ID_BACKEND_VALUE)).thenReturn(APPROVED_STATUS_ID_ENUM_VALUE);
        when(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.businessAgents.id",
                AUTOMATICREFUNDTRANSACTION_BUSINESSAGENTS_ID_BACKEND_ASSIGNED)).
                thenReturn(AUTOMATICREFUNDTRANSACTION_BUSINESSAGENTS_ID_FRONTEND_R);
        when(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType",
                CLAIMS_PETITIONER_CONTACTDETAILTYPE_EMAIL_BACKEND)).thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_EMAIL_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType",
                CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_MOBILE_VALUE)).
                thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_MOBILE_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType",
                CLAIMS_PETITIONER_CONTACTDETAILTYPE_BACKEND_LANDLINE_VALUE)).
                thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_LANDLINE_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.petitioner.contact.phoneType",
                CLAIMS_PETITIONER_CONTACT_PHONETYPE_BACKEND_PHONE_NUMBER_VALUE)).
                thenReturn(CLAIMS_PETITIONER_CONTACT_PHONETYPE_FRONTEND_HOME_VALUE);
        when(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.operations.operationType",
                AUTOMATICREFUNDTRANSACTION_OPERATIONS_OPERATIONTYPE_ID_BACKEND_INTEREST)).
                thenReturn(AUTOMATICREFUNDTRANSACTION_OPERATIONS_OPERATIONTYPE_ID_FRONTEND_INT);
        when(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.operations.subProductRefundMode",
                AUTOMATICREFUNDTRANSACTION_OPERATIONS_SUBPRODUCTREFUNDMODE_BACKEND_CASHING)).
                thenReturn(AUTOMATICREFUNDTRANSACTION_OPERATIONS_SUBPRODUCTREFUNDMODE_FRONTEND_P);
        when(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.operations.exchangeRate",
                AUTOMATICREFUNDTRANSACTION_OPERATIONS_EXCHANGERATE_BACKEND_SALE)).
                thenReturn(AUTOMATICREFUNDTRANSACTION_OPERATIONS_EXCHANGERATE_FRONTEND_V);
        when(translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.contractType",
                CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_TARJE_VALUE)).
                thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_CARD_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.contractType",
                CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_CUENT_VALUE)).
                thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_ACCOUNT_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.contractType",
                CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_PREST_VALUE)).
                thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_LOAN_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.numberType",
                CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_PAN_VALUE)).
                thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_FRONTEND_PAN_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.automaticRefundTransactions.transactionType",
                CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_BACKEND_PURCHASE_VALUE)).
                thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_FRONTEND_PURCHASE_VALUE);
    }

    @Test
    public void mapPathInFull() {
        final InputGetAutomaticRefundTransaction input = EntityMock.getInstance().getInputGetAutomaticRefundTransaction();
        final Map<String, String> result = mapper.mapPathIn(input);
        assertFalse(result.isEmpty());
        assertEquals(input.getAutomaticRefundTransactionId(), result.get("numero"));
    }

    @Test
    public void mapHeaderInFull() {
        final InputGetAutomaticRefundTransaction input = EntityMock.getInstance().getInputGetAutomaticRefundTransaction();
        final Map<String, String> result = mapper.mapHeaderIn(input);
        assertFalse(result.isEmpty());
        assertEquals(input.getApp(), result.get("idCanal"));
    }

    @Test
    public void mapOutFull() throws IOException {
        final ModelResultadoResponse input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponse();
        final AutomaticRefundTransaction result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getName());
        assertNotNull(result.getCustomerConfirmation());
        assertNotNull(result.getRelatedClaim());
        assertNotNull(result.getRelatedClaim().getId());
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getSubStatus());
        assertNotNull(result.getStatus().getSubStatus().getId());
        assertNotNull(result.getStatus().getSubStatus().getName());
        assertNotNull(result.getSituation());
        assertNotNull(result.getSituation().getId());
        assertNotNull(result.getSituation().getName());
        assertNotNull(result.getChannel());
        assertNotNull(result.getChannel().getId());
        assertNotNull(result.getChannel().getName());
        assertNotNull(result.getFiles());
        assertNotNull(result.getFiles().get(0));
        assertNotNull(result.getFiles().get(0).getId());
        assertNotNull(result.getFiles().get(0).getName());
        assertNotNull(result.getFiles().get(0).getGroup());
        assertNotNull(result.getBusinessTeam());
        assertNotNull(result.getBusinessTeam().getId());
        assertNotNull(result.getBusinessTeam().getName());
        assertNotNull(result.getBusinessAgents());
        assertNotNull(result.getBusinessAgents().get(0));
        assertNotNull(result.getBusinessAgents().get(0).getId());
        assertNotNull(result.getBusinessAgents().get(0).getFullName());
        assertNotNull(result.getBusinessAgents().get(0).getAgentType());
        assertNotNull(result.getPetitioner());
        assertNotNull(result.getOperations());

        assertEquals(input.getResultado().getFecha(),result.getCreationDate());
        assertEquals(input.getResultado().getMotivo().getCodigo(),result.getReason().getId());
        assertEquals(input.getResultado().getMotivo().getNombre(),result.getReason().getName());
        assertEquals(FunctionUtils.convertTo(input.getResultado().getConformidadAtencion()),result.getCustomerConfirmation());
        assertEquals(input.getNumeroCaso(),result.getRelatedClaim().getId());
        assertEquals(APPROVED_STATUS_ID_ENUM_VALUE,result.getStatus().getId());
        assertEquals(input.getResultado().getEstado().getNombre(),result.getStatus().getName());
        assertEquals(input.getResultado().getEstadoAtencion().getCodigo(),result.getStatus().getSubStatus().getId());
        assertEquals(input.getResultado().getEstadoAtencion().getNombre(),result.getStatus().getSubStatus().getName());
        assertEquals(input.getResultado().getSituacion().getCodigo(),result.getSituation().getId());
        assertEquals(input.getResultado().getSituacion().getNombre(),result.getSituation().getName());
        assertEquals(input.getResultado().getCanal().getCodigo(),result.getChannel().getId());
        assertEquals(input.getResultado().getCanal().getNombre(),result.getChannel().getName());
        assertEquals(input.getResultado().getAdjunto().get(0).getId(),result.getFiles().get(0).getId());
        assertEquals(input.getResultado().getAdjunto().get(0).getNombre(),result.getFiles().get(0).getName());
        assertEquals(input.getResultado().getAdjunto().get(0).getGrupo(),result.getFiles().get(0).getGroup());
        assertEquals(input.getResultado().getEquipoResponsable().getCodigo(),result.getBusinessTeam().getId());
        assertEquals(input.getResultado().getEquipoResponsable().getNombre(),result.getBusinessTeam().getName());
        assertEquals(input.getResultado().getUsuarios().get(0).getCodigo(),result.getBusinessAgents().get(0).getId());
        assertEquals(input.getResultado().getUsuarios().get(0).getNombre(),result.getBusinessAgents().get(0).getFullName());
        assertEquals(AUTOMATICREFUNDTRANSACTION_BUSINESSAGENTS_ID_FRONTEND_R,result.getBusinessAgents().get(0).getAgentType());
    }

    @Test
    public void mapOutWithoutOperations() throws IOException {
        final ModelResultadoResponse input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponse();

        input.getResultado().setOperacion(null);

        final AutomaticRefundTransaction result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getPetitioner());
        assertNull(result.getOperations());
    }

    @Test
    public void mapOutPetitionerFull() throws IOException {
        final ModelResultadoResponse input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponse();

        final Petitioner result = mapper.mapOutPetitioner(input.getResultado().getTitular(),input.getResultado().getContactos());

        assertNotNull(result);
        assertNotNull(result.getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getLastName());
        assertNotNull(result.getSecondLastName());
        assertNotNull(result.getSegment());
        assertNotNull(result.getSegment().getName());
        assertNotNull(result.getSegment().getId());
        assertNotNull(result.getContactDetails());

        assertEquals(input.getResultado().getTitular().getTipoDoi(), result.getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getResultado().getTitular().getDoi(), result.getIdentityDocument().getDocumentNumber());
        assertEquals(input.getResultado().getTitular().getNombres(), result.getFirstName());
        assertEquals(input.getResultado().getTitular().getApellidoPaterno(), result.getLastName());
        assertEquals(input.getResultado().getTitular().getApellidoMaterno(), result.getSecondLastName());
        assertEquals(input.getResultado().getTitular().getSegmento().getCodigo(), result.getSegment().getId());
        assertEquals(input.getResultado().getTitular().getSegmento().getDescripcion(), result.getSegment().getName());
    }

    @Test
    public void mapOutContactDetailsFullWithContactDetailTypeLANDLINE() throws IOException{
        final ModelResultadoResponse input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponse();
        final Petitioner result = mapper.mapOutPetitioner(input.getResultado().getTitular(),input.getResultado().getContactos());
        ContactDetail contactDetail = result.getContactDetails().get(0);
        ModelContacto contacto = input.getResultado().getContactos().get(0);
        assertNotNull(contactDetail);
        assertNotNull(contactDetail.getContact());
        assertNotNull(contactDetail.getContact().getContactDetailType());
        assertNotNull(contactDetail.getContact().getNumber());
        assertNotNull(contactDetail.getContact().getPhoneType());
        assertNotNull(contactDetail.getContact().getCountry());
        assertNotNull(contactDetail.getContact().getCountry().getId());
        assertNotNull(contactDetail.getContact().getCountryCode());
        assertNotNull(contactDetail.getContact().getRegionalCode());
        assertNotNull(contactDetail.getContact().getExtension());

        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_LANDLINE_VALUE, contactDetail.getContact().getContactDetailType());
        assertEquals(contacto.getNumero(),contactDetail.getContact().getNumber());
        assertEquals(CLAIMS_PETITIONER_CONTACT_PHONETYPE_FRONTEND_HOME_VALUE, contactDetail.getContact().getPhoneType());
        assertEquals(contacto.getPaisFijo(),contactDetail.getContact().getCountry().getId());
        assertEquals(contacto.getCodigoPaisFijo(),contactDetail.getContact().getCountryCode());
        assertEquals(contacto.getCodigoRegionalFijo(),contactDetail.getContact().getRegionalCode());
        assertEquals(contacto.getAnexoFijo(),contactDetail.getContact().getExtension());
    }

    @Test
    public void mapOutContactDetailsFullWithContactDetailTypeMOBILE() throws IOException{
        final ModelResultadoResponse input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponse();
        final Petitioner result = mapper.mapOutPetitioner(input.getResultado().getTitular(),input.getResultado().getContactos());
        ContactDetail contactDetail = result.getContactDetails().get(1);
        ModelContacto contacto = input.getResultado().getContactos().get(1);
        assertNotNull(contacto);
        assertNotNull(contactDetail.getId());
        assertNotNull(contactDetail.getContact());
        assertNotNull(contactDetail.getContact().getContactDetailType());
        assertNotNull(contactDetail.getContact().getNumber());
        assertNotNull(contactDetail.getContact().getPhoneCompany());
        assertNotNull(contactDetail.getContact().getPhoneCompany().getId());
        assertNotNull(contactDetail.getContact().getPhoneCompany().getName());

        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_MOBILE_VALUE, contactDetail.getContact().getContactDetailType());
        assertEquals(contacto.getNumero(),contactDetail.getContact().getNumber());
        assertEquals(contacto.getOperadorCelular().getCodigo(),contactDetail.getContact().getPhoneCompany().getId());
        assertEquals(contacto.getOperadorCelular().getDescripcion(),contactDetail.getContact().getPhoneCompany().getName());
        assertEquals(contacto.getCodigoPaisCelular(),contactDetail.getContact().getCountryCode());
    }

    @Test
    public void mapOutContactDetailsFullWithContactDetailTypeEMAIL() throws IOException{
        final ModelResultadoResponse input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponse();
        final Petitioner result = mapper.mapOutPetitioner(input.getResultado().getTitular(),input.getResultado().getContactos());
        ContactDetail contactDetail = result.getContactDetails().get(2);
        ModelContacto contactos = input.getResultado().getContactos().get(2);
        assertNotNull(contactDetail.getContact().getContactDetailType());
        assertNotNull(contactDetail.getContact().getAddress());
        assertNotNull(contactDetail.getContact().getReceivesNotifications());

        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_EMAIL_VALUE, contactDetail.getContact().getContactDetailType());
        assertEquals(contactos.getCorreo(), contactDetail.getContact().getAddress());
        assertEquals(FunctionUtils.convertToBoolean(contactos.getNotificacionesCorreo()), contactDetail.getContact()
                .getReceivesNotifications());
    }

    @Test
    public void mapOutOperacionFull() throws IOException {
        final ModelResultadoResponse input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponse();

        final List<ModelOperacion> inputOperaciones = input.getResultado().getOperacion();

        final List<Operation> result = mapper.mapOutOperation(inputOperaciones);

        assertFalse(result.isEmpty());
        assertEquals(inputOperaciones.size(), result.size());
        ModelOperacion inputOperacion = inputOperaciones.get(0);
        Operation resultOperation = result.get(0);
        assertNotNull(resultOperation.getOperationType());
        assertNotNull(resultOperation.getOperationTypeName());
        assertNotNull(resultOperation.getBank());
        assertNotNull(resultOperation.getBank().getId());
        assertNotNull(resultOperation.getBank().getName());
        assertNotNull(resultOperation.getBank().getBranch());
        assertNotNull(resultOperation.getBank().getBranch().getId());
        assertNotNull(resultOperation.getBank().getBranch().getName());
        assertNotNull(resultOperation.getExchangeRate());
        assertNotNull(resultOperation.getExchangeRate().getValues());
        assertNotNull(resultOperation.getExchangeRate().getValues().getFactor());
        assertNotNull(resultOperation.getExchangeRate().getValues().getFactor().getValue());
        assertNotNull(resultOperation.getExchangeRate().getValues().getPriceType());
        assertNotNull(resultOperation.getRelatedContract());
        assertNotNull(resultOperation.getRelatedContract().getId());
        assertNotNull(resultOperation.getRelatedContract().getRequestedAmount());
        assertNotNull(resultOperation.getRelatedContract().getRequestedAmount().getAmount());
        assertNotNull(resultOperation.getRelatedContract().getRequestedAmount().getCurrency());
        assertNotNull(resultOperation.getRelatedContract().getProduct().getId());
        assertNotNull(resultOperation.getRelatedContract().getProduct().getName());
        assertNotNull(resultOperation.getClaimReason());
        assertNotNull(resultOperation.getClaimReason().getContract());
        assertNotNull(resultOperation.getClaimReason().getContract().getContractType());

        assertEquals(AUTOMATICREFUNDTRANSACTION_OPERATIONS_OPERATIONTYPE_ID_FRONTEND_INT,resultOperation.getOperationType());
        assertEquals(inputOperacion.getTipo().getDescripcion(),resultOperation.getOperationTypeName());
        assertEquals(inputOperacion.getBanco().getId(),resultOperation.getBank().getId());
        assertEquals(inputOperacion.getBanco().getDescripcion(),resultOperation.getBank().getName());
        assertEquals(inputOperacion.getCentroContable().getCodigo(),resultOperation.getBank().getBranch().getId());
        assertEquals(inputOperacion.getCentroContable().getDescripcion(),resultOperation.getBank().getBranch().getName());
        assertEquals(inputOperacion.getTipoCambio().getValor(),resultOperation.getExchangeRate().getValues().getFactor().getValue());
        assertEquals(AUTOMATICREFUNDTRANSACTION_OPERATIONS_EXCHANGERATE_FRONTEND_V,resultOperation.getExchangeRate().getValues()
                .getPriceType());
        assertEquals(inputOperacion.getProductoSeguro().getCodigo(),resultOperation.getRelatedContract().getId());
        assertEquals(inputOperacion.getProductoSeguro().getId(),resultOperation.getRelatedContract().getProduct().getId());
        assertEquals(inputOperacion.getProductoSeguro().getProducto(),resultOperation.getRelatedContract().getProduct().getName());
        assertEquals(inputOperacion.getProductoSeguro().getImporte(),resultOperation.getRelatedContract().getRequestedAmount().getAmount());
        assertEquals(inputOperacion.getProductoSeguro().getMoneda().getCodigo(),resultOperation.getRelatedContract()
                .getRequestedAmount().getCurrency());

        //operations[].claimReason.contract.contractType=="CARD"
        assertNotNull(resultOperation.getClaimReason().getContract().getId());
        assertNotNull(resultOperation.getClaimReason().getContract().getNumber());
        assertNotNull(resultOperation.getClaimReason().getContract().getNumberType());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0));
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getId());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getOperationDate());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getConcept());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getTransactionType());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getTransactionType().getId());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getTransactionType().getName());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getLocalAmount());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getStore());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getStore().getId());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getStore().getName());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getStatus());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getStatus().getId());
        assertNotNull(resultOperation.getClaimReason().getContract().getTransactions().get(0).getStatus().getName());
        assertNotNull(resultOperation.getClaimReason().getContract().getCardAgreement());
        assertNotNull(resultOperation.getClaimReason().getContract().getCardAgreement().getId());
        assertNotNull(resultOperation.getClaimReason().getContract().getSubProduct());
        assertNotNull(resultOperation.getClaimReason().getContract().getSubProduct().getId());

        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_CARD_VALUE,resultOperation.getClaimReason()
                .getContract().getContractType());
        assertEquals(CASHING_REFUND_MODE_ENUM_VALUE,resultOperation.getSubproductRefund().getRefundMode());
        assertEquals(CASHING_REFUND_MODE_ENUM_VALUE,resultOperation.getSubproductRefund().getRefundModeDescription());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_FRONTEND_PURCHASE_VALUE,resultOperation.getClaimReason()
                .getContract().getTransactions().get(0).getTransactionType().getId());

        //operations[].claimReason.contract.contractType=="ACCOUNT"
        resultOperation = result.get(1);
        assertNotNull(resultOperation.getClaimReason().getContract().getAccountType());
        assertNotNull(resultOperation.getClaimReason().getContract().getAccountType().getId());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_ACCOUNT_VALUE,resultOperation.getClaimReason()
                .getContract().getContractType());
        assertEquals(inputOperacion.getContrato().getNumero(),resultOperation.getSubproductRefund().getNumber());

        //operations[].claimReason.contract.contractType=="LOAN"
        resultOperation = result.get(2);
        inputOperacion = inputOperaciones.get(2);
        assertNotNull(resultOperation.getClaimReason().getContract().getLoanType());
        assertNotNull(resultOperation.getClaimReason().getContract().getLoanType().getId());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_LOAN_VALUE,resultOperation.getClaimReason()
                .getContract().getContractType());

        assertNotNull(resultOperation.getSubproductRefund().getId());
        assertNotNull(resultOperation.getSubproductRefund().getNumber());
        assertNotNull(resultOperation.getRequestedAmount().getCurrency());
        assertNotNull(resultOperation.getRequestedAmount().getAmount());

        assertEquals(inputOperacion.getContrato().getAliasProducto(),resultOperation.getSubproductRefund().getId());
        assertEquals(CASHING_REFUND_MODE_ENUM_VALUE,resultOperation.getSubproductRefund().getRefundMode());
        assertEquals(CASHING_REFUND_MODE_ENUM_VALUE,resultOperation.getSubproductRefund().getRefundModeDescription());
        assertEquals(inputOperacion.getContrato().getNumero(),resultOperation.getSubproductRefund().getNumber());
        assertEquals(inputOperacion.getMoneda().getCodigo(),resultOperation.getRequestedAmount().getCurrency());
        assertEquals(inputOperacion.getImporte(),resultOperation.getRequestedAmount().getAmount());
    }

    @Test
    public void mapOutEmpty() {
        ModelResultadoResponse modelResultadoResponse = new ModelResultadoResponse();
        modelResultadoResponse.setResultado(new ModelResultado());
        final AutomaticRefundTransaction result = mapper.mapOut(modelResultadoResponse);
        assertNotNull(result);
        assertNull(result.getFiles());
        assertNull(result.getPetitioner());
        assertNull(result.getOperations());
        assertNull(result.getCustomerConfirmation());
        assertNull(result.getSituation());
        assertNull(result.getCreationDate());
        assertNull(result.getBusinessAgents());
        assertNull(result.getChannel());
        assertNull(result.getRelatedClaim());
        assertNull(result.getReason());
        assertNull(result.getStatus());
        assertNull(result.getBusinessTeam());
        assertNull(result.getId());
    }

    @Test
    public void mapOutPetitionerEmpty() {
        final Petitioner result = mapper.mapOutPetitioner(null,null);
        assertNull(result);
    }
}

