package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.DTOIntUnknownPetitioner;
import com.bbva.pzic.claims.business.dto.InputSearchProductClaims;
import com.bbva.pzic.claims.canonic.ClaimType;
import com.bbva.pzic.claims.dao.rest.model.harec.*;
import com.bbva.pzic.claims.dao.rest.mapper.common.RestProductClaimMapper;
import com.bbva.pzic.claims.dao.rest.mock.stubs.ResultadoModelsStubs;
import com.bbva.pzic.claims.facade.v0.dto.PersonType;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.StatusSearch;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;


import static com.bbva.pzic.claims.EntityMock.*;
import static com.bbva.pzic.claims.util.Constants.DATE_LONG_FORMAT;
import static com.bbva.pzic.claims.util.Enums.*;
import static com.bbva.pzic.claims.util.FunctionUtils.buildDatetime;
import static com.bbva.pzic.claims.util.FunctionUtils.convertToBoolean;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Created on 11/26/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestSearchProductClaimsMapperTest {

    @InjectMocks
    private RestSearchProductClaimsMapper mapper;

    @Mock
    private RestProductClaimMapper restProductClaimMapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(restProductClaimMapper.mapOutPersonTypeEnum(any())).thenReturn(new PersonType());
        when(restProductClaimMapper.mapOutClaimTypeEnum(any())).thenReturn(new ClaimType());
        when(restProductClaimMapper.mapOutStatusEnum(any())).thenReturn(new StatusSearch());
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_CATEGORY_ID, CLAIMS_CATEGORY_ID_SIMPLE_BACKEND))
                .thenReturn(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_RESOLUTION_ID, CLAIMS_RESOLUTION_ID_NOPR_BACKEND))
                .thenReturn(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PETITIONER_CONTACTDETAILTYPE, CLAIMS_PETITIONER_CONTACTDETAILTYPE_EMAIL_BACKEND))
                .thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_EMAIL_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PETITIONER_CONTACTDETAILTYPE, CLAIMS_PETITIONER_CONTACTDETAILTYPE_LANDLINE_BACKEND))
                .thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_LANDLINE_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PETITIONER_CONTACTDETAILTYPE, CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_BACKEND))
                .thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(ENUM_CLAIMS_PETITIONER_CONTACT_PHONETYPE, CLAIMS_PETITIONER_CONTACT_PHONETYPE_MOBILE_BACKEND))
                .thenReturn(CLAIMS_PETITIONER_CONTACT_PHONETYPE_MOBILE_FRONTEND);
    }

    @Test
    public void mapInWithSessionedChannelFullTest() throws IOException {
        InputSearchProductClaims input = EntityMock.getInstance().buildInputSearchProductClaims();
        RequestType result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCanal());
        assertNotNull(result.getCodigoCentral());
        assertNull(result.getTipoDoi());
        assertNull(result.getNumeroDoi());

        assertEquals(input.getClientId(), result.getCodigoCentral());
    }

    @Test
    public void mapInWithoutSessionedChannelFullTest() throws IOException {
        InputSearchProductClaims input = EntityMock.getInstance().buildInputSearchProductClaims();
        input.setClientId(null);
        RequestType result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getCanal());
        assertNull(result.getCodigoCentral());
        assertNotNull(result.getTipoDoi());
        assertNotNull(result.getNumeroDoi());

        assertEquals(((DTOIntUnknownPetitioner) input.getPetitioner()).getIdentityDocument().getDocumentType().getId(),
                result.getTipoDoi());
        assertEquals(((DTOIntUnknownPetitioner) input.getPetitioner()).getIdentityDocument().getDocumentNumber(),
                result.getNumeroDoi());
    }

    @Test
    public void mapOutFullTestWithKnownPetitionerAndHolderAndEmail() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(0));

        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getCodigoCentral(), result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_EMAIL_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCorreo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertEquals(convertToBoolean(input.getRequerimientos().get(0).getContactos().get(0).getNotificacionesCorreo()),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        assertEquals("KNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutFullTestWithKnownPetitionerAndHolderAndLandline() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(1));

        input.getRequerimientos().get(0).getContactos().get(0).setTipoContacto("LANDLINE");
        input.getRequerimientos().get(0).getContactos().get(0).setId("id");
        input.getRequerimientos().get(0).getContactos().get(0).setNumero("numero");
        input.getRequerimientos().get(0).getContactos().get(0).setTipoFijo("MOBILE");
        input.getRequerimientos().get(0).getContactos().get(0).setPaisFijo("paisFijo");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoPaisFijo("codigoPaisFjo");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoRegionalFijo("codigoRegionalFijo");
        input.getRequerimientos().get(0).getContactos().get(0).setAnexoFijo("anexoFijo");
        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountry());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountry().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getRegionalCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getExtension());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getCodigoCentral(), result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_LANDLINE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getNumero(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertEquals(CLAIMS_PETITIONER_CONTACT_PHONETYPE_MOBILE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getPaisFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountry().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoPaisFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoRegionalFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getRegionalCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getAnexoFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getExtension());
        assertEquals("KNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutFullTestWithKnownPetitionerAndHolderAndMobile() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(2));

        input.getRequerimientos().get(0).getContactos().get(0).setTipoContacto("MOBILE");
        input.getRequerimientos().get(0).getContactos().get(0).setId("id");
        input.getRequerimientos().get(0).getContactos().get(0).setNumero("numero");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoPaisCelular("codigoPaisCelular");
        input.getRequerimientos().get(0).getContactos().get(0).setOperadorCelular(new OperadorCelularType());
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setCodigo("codigo");
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setDescripcion("descripcion");
        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getCodigoCentral(), result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getNumero(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoPaisCelular(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getDescripcion(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertEquals("KNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutFullTestWithUnknownPetitionerAndHolderAndEmail() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(4));

        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_EMAIL_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCorreo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertEquals(convertToBoolean(input.getRequerimientos().get(0).getContactos().get(0).getNotificacionesCorreo()),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        assertEquals("UNKNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutFullTestWithUnknownPetitionerAndHolderAndLandline() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(5));

        input.getRequerimientos().get(0).getContactos().get(0).setTipoContacto("LANDLINE");
        input.getRequerimientos().get(0).getContactos().get(0).setId("id");
        input.getRequerimientos().get(0).getContactos().get(0).setNumero("numero");
        input.getRequerimientos().get(0).getContactos().get(0).setTipoFijo("MOBILE");
        input.getRequerimientos().get(0).getContactos().get(0).setPaisFijo("paisFijo");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoPaisFijo("codigoPaisFjo");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoRegionalFijo("codigoRegionalFijo");
        input.getRequerimientos().get(0).getContactos().get(0).setAnexoFijo("anexoFijo");
        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountry());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountry().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getRegionalCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getExtension());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_LANDLINE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getNumero(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertEquals(CLAIMS_PETITIONER_CONTACT_PHONETYPE_MOBILE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getPaisFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountry().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoPaisFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoRegionalFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getRegionalCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getAnexoFijo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getExtension());
        assertEquals("UNKNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutFullTestWithUnknownPetitionerAndHolderAndMobile() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(6));

        input.getRequerimientos().get(0).getContactos().get(0).setTipoContacto("MOBILE");
        input.getRequerimientos().get(0).getContactos().get(0).setId("id");
        input.getRequerimientos().get(0).getContactos().get(0).setNumero("numero");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoPaisCelular("codigoPaisCelular");
        input.getRequerimientos().get(0).getContactos().get(0).setOperadorCelular(new OperadorCelularType());
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setCodigo("codigo");
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setDescripcion("descripcion");
        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getNumero(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoPaisCelular(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getDescripcion(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertEquals("UNKNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutFullTestWithKnownPetitionerAndAuthorized() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(6));

        RepresentanteType representanteType = input.getRequerimientos().get(0).getRepresentante();
        when(restProductClaimMapper.mapOutRepresentativePetitioner(representanteType))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(3));

        input.getRequerimientos().get(0).getContactos().get(0).setTipoContacto("MOBILE");
        input.getRequerimientos().get(0).getContactos().get(0).setId("id");
        input.getRequerimientos().get(0).getContactos().get(0).setNumero("numero");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoPaisCelular("codigoPaisCelular");
        input.getRequerimientos().get(0).getContactos().get(0).setOperadorCelular(new OperadorCelularType());
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setCodigo("codigo");
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setDescripcion("descripcion");
        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getNumero(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoPaisCelular(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getDescripcion(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertEquals("UNKNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
        assertEquals(input.getRequerimientos().get(0).getRepresentante().getCodigoCentral(), result.get(0).getPetitioners().get(1).getPetitioner().getId());
        assertEquals(input.getRequerimientos().get(0).getRepresentante().getNombres(), result.get(0).getPetitioners().get(1).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getRepresentante().getApellidoPaterno(), result.get(0).getPetitioners().get(1).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getRepresentante().getApellidoMaterno(), result.get(0).getPetitioners().get(1).getPetitioner().getSecondLastName());
        assertEquals("KNOWN", result.get(0).getPetitioners().get(1).getPetitioner().getBankRelationType());
        assertEquals("AUTHORIZED", result.get(0).getPetitioners().get(1).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutFullTestWithUnknownPetitionerAndAuthorized() throws IOException {
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();

        TitularType titularInput = input.getRequerimientos().get(0).getTitular();
        when(restProductClaimMapper.mapOutPetitioner(titularInput))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(6));

        RepresentanteType representanteType = input.getRequerimientos().get(0).getRepresentante();
        when(restProductClaimMapper.mapOutRepresentativePetitioner(representanteType))
                .thenReturn(EntityMock.getInstance().buildPetitionerSearch().get(7));

        input.getRequerimientos().get(0).getContactos().get(0).setTipoContacto("MOBILE");
        input.getRequerimientos().get(0).getContactos().get(0).setId("id");
        input.getRequerimientos().get(0).getContactos().get(0).setNumero("numero");
        input.getRequerimientos().get(0).getContactos().get(0).setCodigoPaisCelular("codigoPaisCelular");
        input.getRequerimientos().get(0).getContactos().get(0).setOperadorCelular(new OperadorCelularType());
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setCodigo("codigo");
        input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().setDescripcion("descripcion");

        input.getRequerimientos().get(0).getRepresentante().setCodigoCentral(null);
        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getReason().getSubReason());
        assertNotNull(result.get(0).getReason().getSubReason().getId());
        assertNotNull(result.get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.get(0).getCategory());
        assertNotNull(result.get(0).getCategory().getId());
        assertNotNull(result.get(0).getCategory().getName());
        assertNotNull(result.get(0).getPredecessorClaim());
        assertNotNull(result.get(0).getPredecessorClaim().getId());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getResolution().getId());
        assertNotNull(result.get(0).getResolution().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getPetitioners());
        assertNotNull(result.get(0).getPetitioners().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner());
        assertNull(result.get(0).getPetitioners().get(0).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
        assertNull(result.get(0).getPetitioners().get(1).getPetitioner().getId());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getSecondLastName());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getBankRelationType());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getPetitionerType());
        assertNotNull(result.get(0).getPetitioners().get(1).getPetitioner().getPetitionerType().getId());

        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getRequerimientos().get(0).getNumero(), result.get(0).getNumber());
        assertEquals(buildDatetime(input.getRequerimientos().get(0).getFechaRegistro(), DATE_LONG_FORMAT),
                result.get(0).getCreationDate());
        assertEquals(input.getRequerimientos().get(0).getProducto().getCodigo(), result.get(0).getProduct().getId());
        assertEquals(input.getRequerimientos().get(0).getProducto().getNombre(), result.get(0).getProduct().getName());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getRequerimientos().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getCodigo(), result.get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimientos().get(0).getSubmotivo().getNombre(), result.get(0).getReason().getSubReason().getDescription());
        assertEquals(CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND, result.get(0).getCategory().getId());
        assertEquals(input.getRequerimientos().get(0).getCategoria().getDescripcion(), result.get(0).getCategory().getName());
        assertEquals(input.getRequerimientos().get(0).getNumeroCasoReiterativo(), result.get(0).getPredecessorClaim().getId());
        assertEquals(CLAIMS_RESOLUTION_ID_NOPR_FRONTEND, result.get(0).getResolution().getId());
        assertEquals(input.getRequerimientos().get(0).getDictamen().getNombre(), result.get(0).getResolution().getName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getNombres(), result.get(0).getPetitioners().get(0).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getTitular().getApellidoMaterno(), result.get(0).getPetitioners().get(0).getPetitioner().getSecondLastName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getId(), result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getReferencia(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getNombre(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimientos().get(0).getDireccion().getComponentes().get(0).getTipoComponentes(),
                result.get(0).getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_FRONTEND,
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getId(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getNumero(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getNumber());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getCodigoPaisCelular(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getCountryCode());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getCodigo(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getId());
        assertEquals(input.getRequerimientos().get(0).getContactos().get(0).getOperadorCelular().getDescripcion(),
                result.get(0).getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getPhoneCompany().getName());
        assertEquals("UNKNOWN", result.get(0).getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals("HOLDER",result.get(0).getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
        assertEquals(input.getRequerimientos().get(0).getRepresentante().getNombres(), result.get(0).getPetitioners().get(1).getPetitioner().getFirstName());
        assertEquals(input.getRequerimientos().get(0).getRepresentante().getApellidoPaterno(), result.get(0).getPetitioners().get(1).getPetitioner().getLastName());
        assertEquals(input.getRequerimientos().get(0).getRepresentante().getApellidoMaterno(), result.get(0).getPetitioners().get(1).getPetitioner().getSecondLastName());
        assertEquals("UNKNOWN", result.get(0).getPetitioners().get(1).getPetitioner().getBankRelationType());
        assertEquals("AUTHORIZED", result.get(0).getPetitioners().get(1).getPetitioner().getPetitionerType().getId());
    }

    @Test
    public void mapOutPetitionerNullTest() throws IOException {
        when(restProductClaimMapper.mapOutPetitioner(any())).thenReturn(null);

        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        List<ProductClaims> result = mapper.mapOut(input);

        assertNotNull(result);
        assertEquals(3, result.size());
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getNumber());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getPersonType());
        assertNotNull(result.get(0).getClaimType());
        assertNotNull(result.get(0).getProduct());
        assertNotNull(result.get(0).getProduct().getId());
        assertNotNull(result.get(0).getProduct().getName());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getResolution());
        assertNotNull(result.get(0).getStatus());
        assertNull(result.get(0).getPetitioners());
        assertNotNull(result.get(1).getId());
        assertNotNull(result.get(1).getNumber());
        assertNotNull(result.get(1).getCreationDate());
        assertNotNull(result.get(1).getPersonType());
        assertNotNull(result.get(1).getClaimType());
        assertNotNull(result.get(1).getProduct());
        assertNotNull(result.get(1).getProduct().getId());
        assertNotNull(result.get(1).getProduct().getName());
        assertNotNull(result.get(1).getReason());
        assertNotNull(result.get(1).getReason().getId());
        assertNotNull(result.get(1).getReason().getName());
        assertNotNull(result.get(1).getResolution());
        assertNotNull(result.get(1).getStatus());
        assertNull(result.get(1).getPetitioners());
    }

    @Test
    public void mapOutEmptyTest() {
        List<ProductClaims> result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        List<ProductClaims> result = mapper.mapOut(new ResponseType());

        assertNull(result);
    }
}
