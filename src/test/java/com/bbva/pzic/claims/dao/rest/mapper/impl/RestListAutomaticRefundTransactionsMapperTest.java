package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputListAutomaticRefundTransactions;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultado;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponses;
import com.bbva.pzic.claims.dao.rest.mock.stubs.ResultadoModelsStubs;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.claims.EntityMock.*;
import static com.bbva.pzic.claims.util.Constants.CODIGO_CENTRAL;
import static com.bbva.pzic.claims.util.Constants.ID_CANAL;
import static com.bbva.pzic.claims.util.Enums.AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID;
import static com.bbva.pzic.claims.util.Enums.AUTOMATICREFUNDTRANSACTIONS_STATUS_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestListAutomaticRefundTransactionsMapperTest {

    @InjectMocks
    private RestListAutomaticRefundTransactionsMapper mapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(translator.translateBackendEnumValueStrictly(AUTOMATICREFUNDTRANSACTIONS_STATUS_ID,
                AUTOMATICREFUNDTRANSACTIONS_STATUS_ID_BACKEND))
                .thenReturn(AUTOMATICREFUNDTRANSACTIONS_STATUS_ID_FRONTEND);
        when(translator.translateBackendEnumValueStrictly(AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID,
                AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID_BACKEND))
                .thenReturn(AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID_FRONTEND);
    }

    @Test
    public void mapInQueryFullTest() {
        InputListAutomaticRefundTransactions input = EntityMock.getInstance().getInputListAutomaticRefundTransactions();
        Map<String, String> result = mapper.mapInQuery(input);

        assertNotNull(result);
        assertEquals(input.getCustomerId(), result.get(CODIGO_CENTRAL));
    }

    @Test
    public void mapInQueryEmptyTest() {
        Map<String, String> result = mapper.mapInQuery(new InputListAutomaticRefundTransactions());

        assertNotNull(result);
        assertNull(result.get(CODIGO_CENTRAL));
    }

    @Test
    public void mapInHeaderFullTest() {
        InputListAutomaticRefundTransactions input = EntityMock.getInstance().getInputListAutomaticRefundTransactions();
        Map<String, String> result = mapper.mapInHeader(input);

        assertNotNull(result);
        assertEquals(input.getApp(), result.get(ID_CANAL));
    }

    @Test
    public void mapOutFullTest() throws IOException {

        ModelResultadoResponses input = ResultadoModelsStubs.INSTANCE.getResultadoModelResponses();

        List<AutomaticRefundTransaction> result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.get(0));
        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getCreationDate());
        assertNotNull(result.get(0).getReason());
        assertNotNull(result.get(0).getReason().getId());
        assertNotNull(result.get(0).getReason().getName());
        assertNotNull(result.get(0).getStatus());
        assertNotNull(result.get(0).getStatus().getId());
        assertNotNull(result.get(0).getStatus().getName());
        assertNotNull(result.get(0).getStatus().getSubStatus());
        assertNotNull(result.get(0).getStatus().getSubStatus().getId());
        assertNotNull(result.get(0).getStatus().getSubStatus().getName());
        assertNotNull(result.get(0).getBusinessTeam());
        assertNotNull(result.get(0).getBusinessTeam().getId());
        assertNotNull(result.get(0).getBusinessTeam().getName());
        assertNotNull(result.get(0).getBusinessAgents().get(0));
        assertNotNull(result.get(0).getBusinessAgents().get(0).getId());
        assertNotNull(result.get(0).getBusinessAgents().get(0).getFullName());
        assertNotNull(result.get(0).getBusinessAgents().get(0).getAgentType());
        assertNotNull(result.get(0).getBusinessAgents().get(1));
        assertNotNull(result.get(0).getBusinessAgents().get(1).getId());
        assertNotNull(result.get(0).getBusinessAgents().get(1).getFullName());
        assertNotNull(result.get(0).getBusinessAgents().get(1).getAgentType());
        assertNotNull(result.get(0).getPetitioner());
        assertNotNull(result.get(0).getPetitioner().getFirstName());
        assertNotNull(result.get(0).getPetitioner().getLastName());
        assertNotNull(result.get(0).getPetitioner().getSegment());
        assertNotNull(result.get(0).getPetitioner().getSegment().getId());
        assertNotNull(result.get(0).getPetitioner().getSegment().getName());

        assertEquals(input.getResultado().get(0).getNumero(), result.get(0).getId());
        assertEquals(input.getResultado().get(0).getFecha(), result.get(0).getCreationDate());
        assertEquals(input.getResultado().get(0).getMotivo().getCodigo(), result.get(0).getReason().getId());
        assertEquals(input.getResultado().get(0).getMotivo().getNombre(), result.get(0).getReason().getName());
        assertEquals(AUTOMATICREFUNDTRANSACTIONS_STATUS_ID_FRONTEND, result.get(0).getStatus().getId());
        assertEquals(input.getResultado().get(0).getEstado().getNombre(), result.get(0).getStatus().getName());
        assertEquals(input.getResultado().get(0).getEstadoAtencion().getCodigo(), result.get(0).getStatus().getSubStatus().getId());
        assertEquals(input.getResultado().get(0).getEstadoAtencion().getNombre(), result.get(0).getStatus().getSubStatus().getName());
        assertEquals(input.getResultado().get(0).getEquipoResponsable().getCodigo(), result.get(0).getBusinessTeam().getId());
        assertEquals(input.getResultado().get(0).getEquipoResponsable().getNombre(), result.get(0).getBusinessTeam().getName());
        assertEquals(input.getResultado().get(0).getUsuarios().get(0).getCodigo(), result.get(0).getBusinessAgents().get(0).getId());
        assertEquals(input.getResultado().get(0).getUsuarios().get(0).getNombre(), result.get(0).getBusinessAgents().get(0).getFullName());
        assertEquals(AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID_FRONTEND, result.get(0).getBusinessAgents().get(0).getAgentType());
        assertEquals(input.getResultado().get(0).getUsuarios().get(1).getCodigo(), result.get(0).getBusinessAgents().get(1).getId());
        assertEquals(input.getResultado().get(0).getUsuarios().get(1).getNombre(), result.get(0).getBusinessAgents().get(1).getFullName());
        assertEquals(AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID_FRONTEND, result.get(0).getBusinessAgents().get(1).getAgentType());
        assertEquals(input.getResultado().get(0).getTitular().getNombres(), result.get(0).getPetitioner().getFirstName());
        assertEquals(input.getResultado().get(0).getTitular().getApellidoPaterno(), result.get(0).getPetitioner().getLastName());
        assertEquals(input.getResultado().get(0).getSegmento().getCodigo(), result.get(0).getPetitioner().getSegment().getId());
        assertEquals(input.getResultado().get(0).getSegmento().getNombre(), result.get(0).getPetitioner().getSegment().getName());
    }

    @Test
    public void mapOutEmptyTest() {

        ModelResultadoResponses modelResultadoResponse = new ModelResultadoResponses();
        modelResultadoResponse.setResultado(new ArrayList<>());
        modelResultadoResponse.getResultado().add(new ModelResultado());
        List<AutomaticRefundTransaction> result = mapper.mapOut(modelResultadoResponse);

        assertNotNull(result);
        assertNotNull(result.get(0));
        assertNull(result.get(0).getId());
        assertNull(result.get(0).getCreationDate());
        assertNull(result.get(0).getReason());
        assertNull(result.get(0).getStatus());
        assertNull(result.get(0).getBusinessTeam());
        assertNull(result.get(0).getBusinessAgents());
        assertNull(result.get(0).getPetitioner());
    }

}