package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputModifyAutomaticRefundTransaction;
import com.bbva.pzic.claims.dao.rest.model.conformidad.PeticionConformidad;
import com.bbva.pzic.claims.dao.rest.model.conformidad.ResultadoConformidad;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Map;

import static com.bbva.pzic.claims.EntityMock.AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_IN;
import static com.bbva.pzic.claims.EntityMock.AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_OUT;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestModifyPartialAutomaticRefundTransactionMapperTest {

    @InjectMocks
    private RestModifyPartialAutomaticRefundTransactionMapper mapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(translator.translateBackendEnumValueStrictly("automaticRefundTransactions.status.id",
                AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_IN)).thenReturn(AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_OUT);
    }

    @Test
    public void mapInHeaderFullTest(){
        InputModifyAutomaticRefundTransaction input = EntityMock.getInstance().buildInputModifyAutomaticRefundTransaction();
        Map<String, String> result = mapper.mapHeaderIn(input);
        assertNotNull(result);
        assertNotNull(result.get("user"));
        assertNotNull(result.get("idCanal"));

        assertEquals(result.get("user"),input.getUser());
        assertEquals(result.get("idCanal"),input.getIdCanal());
    }
    @Test
    public void mapInHeaderEmptyTest(){
        InputModifyAutomaticRefundTransaction input = EntityMock.getInstance().buildInputModifyAutomaticRefundTransaction();
        input.setUser(null);
        input.setIdCanal(null);
        Map<String, String> result = mapper.mapHeaderIn(input);
        assertNotNull(result);
        assertNull(result.get("user"));
        assertNull(result.get("idCanal"));
    }

    @Test
    public void mapInBodyFullTest(){
        InputModifyAutomaticRefundTransaction input = EntityMock.getInstance().buildInputModifyAutomaticRefundTransaction();
        PeticionConformidad result = mapper.mapBodyIn(input);
        assertNotNull(result);
        assertNotNull(result.getNumero());
        assertNotNull(result.getConformidadAtencion());
        assertNotNull(result.getEstadoAtencion());
        assertNotNull(result.getEstadoAtencion().getCodigo());
        assertNotNull(result.getSituacion());
        assertNotNull(result.getSituacion().getCodigo());

        assertEquals(result.getNumero(),input.getAutomaticRefundTransactionId());
        assertEquals(result.getConformidadAtencion(),
                FunctionUtils.convertBooleanToInteger(input.getPartialTransaction().getCustomerConfirmation()));
        assertEquals(result.getEstadoAtencion().getCodigo(),input.getPartialTransaction().getStatus().getSubStatus().getId());
        assertEquals(result.getSituacion().getCodigo(),input.getPartialTransaction().getSituation().getId());
    }

    @Test
    public void mapInBodyWithoutSingularsTest(){
        InputModifyAutomaticRefundTransaction input = EntityMock.getInstance().buildInputModifyAutomaticRefundTransaction();
        input.setAutomaticRefundTransactionId(null);
        input.getPartialTransaction().setCustomerConfirmation(null);
        PeticionConformidad result = mapper.mapBodyIn(input);
        assertNotNull(result);
        assertNull(result.getNumero());
        assertNull(result.getConformidadAtencion());
        assertNotNull(result.getEstadoAtencion());
        assertNotNull(result.getEstadoAtencion().getCodigo());
        assertNotNull(result.getSituacion());
        assertNotNull(result.getSituacion().getCodigo());

        assertEquals(result.getEstadoAtencion().getCodigo(),input.getPartialTransaction().getStatus().getSubStatus().getId());
        assertEquals(result.getSituacion().getCodigo(),input.getPartialTransaction().getSituation().getId());
    }

    @Test
    public void mapInBodyWithoutStatusTest(){
        InputModifyAutomaticRefundTransaction input = EntityMock.getInstance().buildInputModifyAutomaticRefundTransaction();
        input.getPartialTransaction().setStatus(null);
        PeticionConformidad result = mapper.mapBodyIn(input);
        assertNotNull(result);
        assertNotNull(result.getNumero());
        assertNotNull(result.getConformidadAtencion());
        assertNull(result.getEstadoAtencion());
        assertNotNull(result.getSituacion());
        assertNotNull(result.getSituacion().getCodigo());

        assertEquals(result.getNumero(),input.getAutomaticRefundTransactionId());
        assertEquals(result.getConformidadAtencion(),
                FunctionUtils.convertBooleanToInteger(input.getPartialTransaction().getCustomerConfirmation()));
        assertEquals(result.getSituacion().getCodigo(),input.getPartialTransaction().getSituation().getId());
    }

    @Test
    public void mapInBodyWithoutSubStatusTest(){
        InputModifyAutomaticRefundTransaction input = EntityMock.getInstance().buildInputModifyAutomaticRefundTransaction();
        input.getPartialTransaction().getStatus().setSubStatus(null);
        PeticionConformidad result = mapper.mapBodyIn(input);
        assertNotNull(result);
        assertNotNull(result.getNumero());
        assertNotNull(result.getConformidadAtencion());
        assertNull(result.getEstadoAtencion());
        assertNotNull(result.getSituacion());
        assertNotNull(result.getSituacion().getCodigo());

        assertEquals(result.getNumero(),input.getAutomaticRefundTransactionId());
        assertEquals(result.getConformidadAtencion(),
                FunctionUtils.convertBooleanToInteger(input.getPartialTransaction().getCustomerConfirmation()));
        assertEquals(result.getSituacion().getCodigo(),input.getPartialTransaction().getSituation().getId());
    }

    @Test
    public void mapInBodyWithoutSituationTest(){
        InputModifyAutomaticRefundTransaction input = EntityMock.getInstance().buildInputModifyAutomaticRefundTransaction();
        input.getPartialTransaction().setSituation(null);
        PeticionConformidad result = mapper.mapBodyIn(input);
        assertNotNull(result);
        assertNotNull(result.getNumero());
        assertNotNull(result.getConformidadAtencion());
        assertNotNull(result.getEstadoAtencion());
        assertNotNull(result.getEstadoAtencion().getCodigo());
        assertNull(result.getSituacion());

        assertEquals(result.getNumero(),input.getAutomaticRefundTransactionId());
        assertEquals(result.getConformidadAtencion(),
                FunctionUtils.convertBooleanToInteger(input.getPartialTransaction().getCustomerConfirmation()));
        assertEquals(result.getEstadoAtencion().getCodigo(),input.getPartialTransaction().getStatus().getSubStatus().getId());
    }

    @Test
    public void mapInBodyEmptyTest(){
        PeticionConformidad result = mapper.mapBodyIn(null);
        assertNull(result);
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ResultadoConformidad input = EntityMock.getInstance().buildResultadoConformidad();
        PartialTransaction result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getSubStatus());
        assertNotNull(result.getStatus().getSubStatus().getId());
        assertNotNull(result.getStatus().getSubStatus().getName());
        assertNotNull(result.getSituation());
        assertNotNull(result.getSituation().getId());
        assertNotNull(result.getSituation().getName());

        assertEquals(result.getStatus().getId(),AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_OUT);
        assertEquals(result.getStatus().getName(),input.getResultado().getEstado().getNombre());
        assertEquals(result.getStatus().getSubStatus().getId(),input.getResultado().getEstadoAtencion().getCodigo());
        assertEquals(result.getStatus().getSubStatus().getName(),input.getResultado().getEstadoAtencion().getNombre());
        assertEquals(result.getSituation().getId(),input.getResultado().getSituacion().getCodigo());
        assertEquals(result.getSituation().getName(),input.getResultado().getSituacion().getNombre());
    }

    @Test
    public void mapOutWithoutEstadoTest() throws IOException {
        ResultadoConformidad input = EntityMock.getInstance().buildResultadoConformidad();
        input.getResultado().setEstado(null);
        PartialTransaction result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getStatus());
        assertNull(result.getStatus().getId());
        assertNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getSubStatus());
        assertNotNull(result.getStatus().getSubStatus().getId());
        assertNotNull(result.getStatus().getSubStatus().getName());
        assertNotNull(result.getSituation());
        assertNotNull(result.getSituation().getId());
        assertNotNull(result.getSituation().getName());

        assertEquals(result.getStatus().getSubStatus().getId(),input.getResultado().getEstadoAtencion().getCodigo());
        assertEquals(result.getStatus().getSubStatus().getName(),input.getResultado().getEstadoAtencion().getNombre());
        assertEquals(result.getSituation().getId(),input.getResultado().getSituacion().getCodigo());
        assertEquals(result.getSituation().getName(),input.getResultado().getSituacion().getNombre());
    }

    @Test
    public void mapOutWithoutEstadoAtencionTest() throws IOException {
        ResultadoConformidad input = EntityMock.getInstance().buildResultadoConformidad();
        input.getResultado().setEstadoAtencion(null);
        PartialTransaction result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNull(result.getStatus().getSubStatus());
        assertNotNull(result.getSituation());
        assertNotNull(result.getSituation().getId());
        assertNotNull(result.getSituation().getName());

        assertEquals(result.getStatus().getId(),AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_OUT);
        assertEquals(result.getStatus().getName(),input.getResultado().getEstado().getNombre());
        assertEquals(result.getSituation().getId(),input.getResultado().getSituacion().getCodigo());
        assertEquals(result.getSituation().getName(),input.getResultado().getSituacion().getNombre());
    }

    @Test
    public void mapOutWithoutSituacionTest() throws IOException {
        ResultadoConformidad input = EntityMock.getInstance().buildResultadoConformidad();
        input.getResultado().setSituacion(null);
        PartialTransaction result = mapper.mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getStatus());
        assertNotNull(result.getStatus().getId());
        assertNotNull(result.getStatus().getName());
        assertNotNull(result.getStatus().getSubStatus());
        assertNotNull(result.getStatus().getSubStatus().getId());
        assertNotNull(result.getStatus().getSubStatus().getName());
        assertNull(result.getSituation());

        assertEquals(result.getStatus().getId(),AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_OUT);
        assertEquals(result.getStatus().getName(),input.getResultado().getEstado().getNombre());
        assertEquals(result.getStatus().getSubStatus().getId(),input.getResultado().getEstadoAtencion().getCodigo());
        assertEquals(result.getStatus().getSubStatus().getName(),input.getResultado().getEstadoAtencion().getNombre());
    }

    @Test
    public void mapOutWithoutResultadoTest() throws IOException {
        ResultadoConformidad input = EntityMock.getInstance().buildResultadoConformidad();
        input.setResultado(null);
        PartialTransaction result = mapper.mapOut(input);
        assertNull(result);
    }
}