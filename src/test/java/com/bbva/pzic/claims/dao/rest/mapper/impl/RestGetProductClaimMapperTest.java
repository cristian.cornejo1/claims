package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputGetProductClaim;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.mapper.common.RestProductClaimMapper;
import com.bbva.pzic.claims.dao.rest.mock.stubs.ResultadoModelsStubs;
import com.bbva.pzic.claims.facade.v0.dto.PersonType;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.StatusSearch;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.claims.EntityMock.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Created on 12/1/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestGetProductClaimMapperTest {

    @InjectMocks
    private RestGetProductClaimMapper mapper;

    @Mock
    private RestProductClaimMapper restProductClaimMapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(translator.translateBackendEnumValueStrictly("claims.status.id", STATUS_ID_REGISTER_FRONTEND_VALUE))
                .thenReturn(STATUS_ID_REGISTER_ENUM_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.status.id", STATUS_ID_PROCESS_FRONTEND_VALUE))
                .thenReturn(STATUS_ID_PROCESS_ENUM_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.resolution.id", RESOLUTION_ID_PETITION_FRONTEND_VALUE))
                .thenReturn(RESOLUTION_ID_PETITION_ENUM_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.resolution.deliveryType", RESOLUTION_DELIVERY_TYPE_ENUM_VALUE))
                .thenReturn(RESOLUTION_DELIVERY_TYPE_FRONTEND);
        when(translator.translateBackendEnumValueStrictly("claims.contract.numberType", CONTRACT_NUMBER_TYPE_ENUM_VALUE))
                .thenReturn(CONTRACT_NUMBER_TYPE_FRONTEND);
        when(translator.translateBackendEnumValueStrictly("claims.delivery.deliveryType", CLAIMS_DELIVERY_TYPE_ENUM_VALUE))
                .thenReturn(CLAIMS_DELIVERY_TYPE_FRONTEND);
        when(translator.translateBackendEnumValueStrictly("claims.claimType.id", CLAIM_TYPE_ID_PETITION_FRONTEND_VALUE))
                .thenReturn(CLAIM_TYPE_ID_PETITION_ENUM_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.resolution.resolutionType", CLAIMS_RESOLUTION_RESOLUTIONTYPE_BACKEND_DEFI))
                .thenReturn(CLAIMS_RESOLUTION_RESOLUTIONTYPE_FRONTEND_PERMANENT);
        when(translator.translateBackendEnumValueStrictly("claims.category.id", CLAIMS_CATEGORY_ID_FRONTEND_VALUE))
                .thenReturn(CLAIMS_CATEGORY_ID_ENUM_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType", CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_EMAIL_VALUE))
                .thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_EMAIL_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType", CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_MOBILE_VALUE))
                .thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_MOBILE_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.petitioner.contactDetailType", CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_LANDLINE_VALUE))
                .thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_LANDLINE_VALUE);
        when(translator.translateBackendEnumValue("claims.petitioner.contact.phoneType", CLAIMS_PETITIONER_CONTACT_PHONETYPE_FRONTEND_VALUE))
                .thenReturn(CLAIMS_PETITIONER_CONTACT_PHONETYPE_ENUM_VALUE);
        when(translator.translateBackendEnumValueStrictly("claims.petitioners.bankClassificationType", CLAIMS_PETITIONER_BANKCLASSIFICATIONTYPE_FRONTEND_VALUE))
                .thenReturn(CLAIMS_PETITIONER_BANKCLASSIFICATIONTYPE_ENUM_VALUE);

        when(restProductClaimMapper.mapOutPersonTypeEnum(any())).thenReturn(new PersonType());
        when(restProductClaimMapper.mapOutStatusEnum(any())).thenReturn(new StatusSearch());
    }

    @Test
    public void mapInFullTest() {
        InputGetProductClaim input = EntityMock.getInstance().buildInputGetProductClaim();
        RequestType result = mapper.mapInBody(input);

        assertNotNull(result);
        assertNotNull(input.getProductClaimId(), result.getNumeroRequerimiento());
    }

    @Test
    public void mapOutKnownHolderFullTest() throws IOException {
        when(restProductClaimMapper.mapOutPetitioner(any())).thenReturn(EntityMock.buildPetitioner("KNOWN", "HOLDER"));
        when(restProductClaimMapper.mapOutRepresentativePetitioner(any())).thenReturn(EntityMock.buildRepresentativePetitioner("KNOWN"));
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        ProductClaims result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getPersonType());
        assertNotNull(result.getClaimType());
        assertNotNull(result.getClaimType().getId());
        assertNotNull(result.getClaimType().getDescription());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getName());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getReason().getSubReason());
        assertNotNull(result.getReason().getSubReason().getId());
        assertNotNull(result.getReason().getSubReason().getDescription());
        assertNotNull(result.getResolution());
        assertNotNull(result.getResolution().getId());
        assertNotNull(result.getResolution().getName());
        assertNotNull(result.getResolution().getDelivery());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType().getId());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType().getDescription());
        assertNotNull(result.getResolution().getFundament());
        assertNotNull(result.getResolution().getFundament().getId());
        assertNotNull(result.getResolution().getFundament().getDescription());
        assertNotNull(result.getResolution().getResolutionType());
        assertNotNull(result.getResolution().getResolutionType().getId());
        assertNotNull(result.getResolution().getResolutionType().getDescription());
        assertNotNull(result.getResolution().getAdditionalInformation());
        assertNotNull(result.getResolution().getIsManualFlow());
        assertNotNull(result.getResolution().getReturnedAmounts());
        assertEquals(2, result.getResolution().getReturnedAmounts().size());
        assertNotNull(result.getResolution().getReturnedAmounts().get(0).getAmount());
        assertNotNull(result.getResolution().getReturnedAmounts().get(0).getCurrency());
        assertNotNull(result.getResolution().getReturnedAmounts().get(1).getAmount());
        assertNotNull(result.getResolution().getReturnedAmounts().get(1).getCurrency());
        assertNotNull(result.getResolution().getOperationalAmounts());
        assertEquals(2, result.getResolution().getOperationalAmounts().size());
        assertNotNull(result.getResolution().getOperationalAmounts().get(0).getAmount());
        assertNotNull(result.getResolution().getOperationalAmounts().get(0).getCurrency());
        assertNotNull(result.getResolution().getOperationalAmounts().get(1).getAmount());
        assertNotNull(result.getResolution().getOperationalAmounts().get(1).getCurrency());
        assertNotNull(result.getStatus());
        assertNotNull(result.getDocuments());
        assertEquals(2, result.getDocuments().size());
        assertNotNull(result.getDocuments().get(0).getId());
        assertNotNull(result.getDocuments().get(0).getGroup());
        assertNotNull(result.getDocuments().get(0).getName());
        assertNotNull(result.getDocuments().get(1).getId());
        assertNotNull(result.getDocuments().get(1).getGroup());
        assertNotNull(result.getDocuments().get(1).getName());

        assertNotNull(result.getContract());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getContract().getNumber());
        assertNotNull(result.getContract().getNumberType());
        assertNotNull(result.getContract().getNumberType().getId());
        assertNotNull(result.getContract().getProduct());
        assertNotNull(result.getContract().getProduct().getId());
        assertNotNull(result.getContract().getProduct().getName());

        assertNotNull(result.getClaimAmount());
        assertNotNull(result.getClaimAmount().getAmount());
        assertNotNull(result.getClaimAmount().getCurrency());
        assertNotNull(result.getDelivery());
        assertNotNull(result.getDelivery().getDeliveryType());
        assertNotNull(result.getDelivery().getDeliveryType().getId());
        assertNotNull(result.getDelivery().getDeliveryType().getDescription());
        assertNotNull(result.getChannel());
        assertNotNull(result.getChannel().getId());
        assertNotNull(result.getChannel().getDescription());
        assertNotNull(result.getGeolocation());
        assertNotNull(result.getGeolocation().getCode());
        assertNotNull(result.getGeolocation().getDescription());
        assertNotNull(result.getAtm());
        assertNotNull(result.getAtm().getId());
        assertNotNull(result.getBank());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getName());
        assertNotNull(result.getBank().getBranch());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getBank().getBranch().getName());
        assertNotNull(result.getPetition());
        assertNotNull(result.getPetition().getDescription());
        assertNotNull(result.getPriority());
        assertNotNull(result.getPriority().getIsUrgent());
        assertNotNull(result.getPriority().getBasis());
        assertNotNull(result.getEnrollmentDate());
        assertNotNull(result.getEndDate());
        assertNotNull(result.getCategory());
        assertNotNull(result.getCategory().getId());
        assertNotNull(result.getCategory().getName());
        assertNotNull(result.getCategory().getSubCategory());
        assertNotNull(result.getCategory().getSubCategory().getId());
        assertNotNull(result.getCategory().getSubCategory().getName());
        assertNotNull(result.getAdditionalInformation());
        assertNotNull(result.getOriginApplication());
        assertNotNull(result.getOriginApplication().getId());
        assertNotNull(result.getPredecessorClaim());
        assertNotNull(result.getPredecessorClaim().getId());
        assertNotNull(result.getIsOnlineRefundFlow());
        assertNotNull(result.getPreviousAttentionCode());
        assertNotNull(result.getAdditionalClaims());
        assertEquals(1, result.getAdditionalClaims().size());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType().getDescription());
        assertNotNull(result.getAdditionalClaims().get(0).getProduct());
        assertNotNull(result.getAdditionalClaims().get(0).getProduct().getId());

        assertNotNull(result.getAdditionalClaims().get(0).getProduct().getName());

        assertNotNull(result.getAdditionalClaims().get(0).getReason());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getName());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.getAdditionalClaims().get(0).getContract());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumber());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumberType());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumberType().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getProduct());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getProduct().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getProduct().getName());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount().getAmount());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount().getCurrency());
        assertNotNull(result.getAdditionalClaims().get(0).getAtm());
        assertNotNull(result.getAdditionalClaims().get(0).getAtm().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getName());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch().getName());
        assertNotNull(result.getAdditionalClaims().get(0).getIssueDate());

        assertNotNull(result.getIssueDate());

        assertNotNull(result.getPetitioners());
        assertEquals(2, result.getPetitioners().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertEquals(1, result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails());
        assertEquals(3, result.getPetitioners().get(0).getPetitioner().getContactDetails().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getNumber());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getPhoneType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountry());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountry().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountryCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getRegionalCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getExtension());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getContactDetailType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getNumber());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getCountryCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment().getDescription());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBankClassification());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBankClassification().getBankClassificationType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBankClassification().getBankClassificationType().getId());
        assertNotNull(result.getTracking());
        assertEquals(2, result.getTracking().size());
        assertNotNull(result.getTracking().get(0).getStatus());
        assertNotNull(result.getTracking().get(0).getStatus().getId());
        assertNotNull(result.getTracking().get(0).getStatus().getDescription());
        assertNotNull(result.getTracking().get(0).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(0).getLastUpdateDate());
        assertNotNull(result.getTracking().get(1).getStatus());
        assertNotNull(result.getTracking().get(1).getStatus().getId());
        assertNotNull(result.getTracking().get(1).getStatus().getDescription());
        assertNotNull(result.getTracking().get(1).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(1).getLastUpdateDate());

        assertEquals(input.getRequerimiento().getNumero(), result.getId());
        assertEquals(input.getRequerimiento().getNumero(), result.getNumber());
        assertEquals(input.getRequerimiento().getProducto().getCodigo(), result.getProduct().getId());
        assertEquals(input.getRequerimiento().getProducto().getNombre(), result.getProduct().getName());
        assertEquals(input.getRequerimiento().getMotivo().getCodigo(), result.getReason().getId());
        assertEquals(input.getRequerimiento().getMotivo().getNombre(), result.getReason().getName());
        assertEquals(input.getRequerimiento().getSubmotivo().getCodigo(), result.getReason().getSubReason().getId());
        assertEquals(input.getRequerimiento().getSubmotivo().getNombre(), result.getReason().getSubReason().getDescription());
        assertEquals(RESOLUTION_ID_PETITION_ENUM_VALUE, result.getResolution().getId());
        assertEquals(input.getRequerimiento().getDictamen().getNombre(), result.getResolution().getName());
        assertEquals(RESOLUTION_DELIVERY_TYPE_FRONTEND, result.getResolution().getDelivery().getDeliveryType().getId());
        assertEquals(input.getRequerimiento().getCartaRespuesta().getValue(), result.getResolution().getDelivery().getDeliveryType().getDescription());
        assertEquals(input.getRequerimiento().getDictamen().getFundamento().getCodigo(), result.getResolution().getFundament().getId());
        assertEquals(input.getRequerimiento().getDictamen().getFundamento().getDescripcion(), result.getResolution().getFundament().getDescription());
        assertEquals(CLAIMS_RESOLUTION_RESOLUTIONTYPE_FRONTEND_PERMANENT, result.getResolution().getResolutionType().getId());
        assertEquals(input.getRequerimiento().getDictamen().getClase().getDescripcion(), result.getResolution().getResolutionType().getDescription());
        assertEquals(input.getRequerimiento().getDictamen().getSustento(), result.getResolution().getAdditionalInformation());
        assertTrue(Boolean.parseBoolean(result.getResolution().getIsManualFlow()));
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(0).getValor(), result.getResolution().getReturnedAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(0).getValor(), result.getResolution().getReturnedAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(1).getValor(), result.getResolution().getReturnedAmounts().get(1).getAmount());
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(1).getValor(), result.getResolution().getReturnedAmounts().get(1).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(0).getValor(), result.getResolution().getOperationalAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(0).getValor(), result.getResolution().getOperationalAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(1).getValor(), result.getResolution().getOperationalAmounts().get(1).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(1).getValor(), result.getResolution().getOperationalAmounts().get(1).getAmount());

        assertEquals(input.getRequerimiento().getProducto().getContrato().getId(), result.getContract().getId());
        assertEquals(input.getRequerimiento().getProducto().getContrato().getNumero(), result.getContract().getNumber());
        assertEquals(CONTRACT_NUMBER_TYPE_FRONTEND, result.getContract().getNumberType().getId());
        assertEquals(input.getRequerimiento().getProducto().getContrato().getAliasProducto(), result.getContract().getProduct().getId());
        assertEquals(input.getRequerimiento().getProducto().getDescripcion(), result.getContract().getProduct().getName());

        assertEquals(input.getRequerimiento().getImporteReclamado(), result.getClaimAmount().getAmount());
        assertEquals(input.getRequerimiento().getDivisaReclamado(), result.getClaimAmount().getCurrency());
        assertEquals(CLAIMS_DELIVERY_TYPE_FRONTEND, result.getDelivery().getDeliveryType().getId());
        assertEquals(input.getRequerimiento().getEntregaHRCS().getValue(), result.getDelivery().getDeliveryType().getDescription());
        assertEquals(input.getRequerimiento().getInterposicion().getCodigo(), result.getChannel().getId());
        assertEquals(input.getRequerimiento().getInterposicion().getNombre(), result.getChannel().getDescription());
        assertEquals(input.getRequerimiento().getUbigeo().getCodigo(), result.getGeolocation().getCode());
        assertEquals(input.getRequerimiento().getUbigeo().getDescripcion(), result.getGeolocation().getDescription());
        assertEquals(input.getRequerimiento().getTaxonomia().getCajero(), result.getAtm().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getBancoOriginante().getCodigo(), result.getBank().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getBancoOriginante().getDescripcion(), result.getBank().getName());
        assertEquals(input.getRequerimiento().getTaxonomia().getOficinaOriginante().getCodigo(), result.getBank().getBranch().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getOficinaOriginante().getDescripcion(), result.getBank().getBranch().getName());
        assertEquals(input.getRequerimiento().getPedido(), result.getPetition().getDescription());
        assertTrue(result.getPriority().getIsUrgent());
        assertEquals(input.getRequerimiento().getSustento(), result.getPriority().getBasis());
        assertEquals(CLAIMS_CATEGORY_ID_ENUM_VALUE, result.getCategory().getId());
        assertEquals(input.getRequerimiento().getCategoria().getDescripcion(), result.getCategory().getName());
        assertEquals(input.getRequerimiento().getTaxonomia().getAgrupacion().getCodigo(), result.getCategory().getSubCategory().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getAgrupacion().getDescripcion(), result.getCategory().getSubCategory().getName());
        assertEquals(input.getRequerimiento().getDetalleComplementario(), result.getAdditionalInformation());
        assertEquals(input.getRequerimiento().getAppOrigenRegistro(), result.getOriginApplication().getId());
        assertEquals(input.getRequerimiento().getNumeroCasoReiterativo(), result.getPredecessorClaim().getId());
        assertFalse(Boolean.parseBoolean(result.getIsOnlineRefundFlow()));
        assertEquals(input.getNumeroAtencionLinea(), result.getPreviousAttentionCode());
        assertEquals(CLAIM_TYPE_ID_PETITION_ENUM_VALUE, result.getAdditionalClaims().get(0).getClaimType().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getTipo().getDescripcion(), result.getAdditionalClaims().get(0).getClaimType().getDescription());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getCodigo(), result.getAdditionalClaims().get(0).getProduct().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getNombre(), result.getAdditionalClaims().get(0).getProduct().getName());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getMotivo().getCodigo(), result.getAdditionalClaims().get(0).getReason().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getMotivo().getNombre(), result.getAdditionalClaims().get(0).getReason().getName());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getSubmotivo().getCodigo(), result.getAdditionalClaims().get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getSubmotivo().getNombre(), result.getAdditionalClaims().get(0).getReason().getSubReason().getDescription());

        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getContrato().getId(), result.getAdditionalClaims().get(0).getContract().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getContrato().getNumero(), result.getAdditionalClaims().get(0).getContract().getNumber());
        assertEquals(CONTRACT_NUMBER_TYPE_FRONTEND, result.getAdditionalClaims().get(0).getContract().getNumberType().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getContrato().getAliasProducto(), result.getAdditionalClaims().get(0).getContract().getProduct().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getContrato().getDescripcion(), result.getAdditionalClaims().get(0).getContract().getProduct().getName());


        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getImporteReclamado(), result.getAdditionalClaims().get(0).getClaimAmount().getAmount());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getDivisa(), result.getAdditionalClaims().get(0).getClaimAmount().getCurrency());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getCajero(), result.getAdditionalClaims().get(0).getAtm().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getBancoOriginante().getCodigo(), result.getAdditionalClaims().get(0).getBank().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getBancoOriginante().getDescripcion(), result.getAdditionalClaims().get(0).getBank().getName());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getOficinaOriginante().getCodigo(), result.getAdditionalClaims().get(0).getBank().getBranch().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getOficinaOriginante().getDescripcion(), result.getAdditionalClaims().get(0).getBank().getBranch().getName());

        //DOCUMENT FIRST INDEX
        assertEquals(input.getRequerimiento().getAdjunto().get(0).getId(), result.getDocuments().get(0).getId());
        assertEquals(input.getRequerimiento().getAdjunto().get(0).getGrupo(), result.getDocuments().get(0).getGroup());
        assertEquals(input.getRequerimiento().getAdjunto().get(0).getNombre(), result.getDocuments().get(0).getName());
        //SECOND FIRST INDEX
        assertEquals(input.getRequerimiento().getAdjunto().get(1).getId(), result.getDocuments().get(1).getId());
        assertEquals(input.getRequerimiento().getAdjunto().get(1).getGrupo(), result.getDocuments().get(1).getGroup());
        assertEquals(input.getRequerimiento().getAdjunto().get(1).getNombre(), result.getDocuments().get(1).getName());
        //PETITIONER
        assertEquals(input.getTitular().getDireccion().getNombre(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getTitular().getReferencia(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals("UBIGEO", result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(input.getRequerimiento().getTitular().getDireccion().getUbigeo().getCodigo(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimiento().getTitular().getDireccion().getUbigeo().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimiento().getTitular().getSegmento().getCodigo(), result.getPetitioners().get(0).getPetitioner().getSegment().getId());
        assertEquals(input.getRequerimiento().getTitular().getSegmento().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getSegment().getDescription());
        assertEquals(input.getRequerimiento().getTitular().getBanco().getCodigo(), result.getPetitioners().get(0).getPetitioner().getBank().getId());
        assertEquals(input.getRequerimiento().getTitular().getBanco().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getBank().getName());
        assertEquals(input.getRequerimiento().getTitular().getOficina().getCodigo(), result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getId());
        assertEquals(input.getRequerimiento().getTitular().getOficina().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getName());
        assertEquals(CLAIMS_PETITIONER_BANKCLASSIFICATIONTYPE_ENUM_VALUE, result.getPetitioners().get(0).getPetitioner().getBankClassification().getBankClassificationType().getId());
        //EMAIL CONTACTDETAILTYPE
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_EMAIL_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimiento().getContactos().get(0).getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimiento().getContactos().get(0).getCorreo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertTrue(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        //LANDLINE CONTACTDETAILTYPE
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_LANDLINE_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType());
        assertEquals(input.getRequerimiento().getContactos().get(1).getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertEquals(input.getRequerimiento().getContactos().get(1).getNumero(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getNumber());
        assertEquals(CLAIMS_PETITIONER_CONTACT_PHONETYPE_ENUM_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getPhoneType());
        assertEquals(input.getRequerimiento().getContactos().get(1).getPaisFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountry().getId());
        assertEquals(input.getRequerimiento().getContactos().get(1).getCodigoPaisFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountryCode());
        assertEquals(input.getRequerimiento().getContactos().get(1).getCodigoRegionalFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getRegionalCode());
        assertEquals(input.getRequerimiento().getContactos().get(1).getAnexoFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getExtension());
        //MOBILE CONTACTDETAILTYPE
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_MOBILE_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getContactDetailType());
        assertEquals(input.getRequerimiento().getContactos().get(2).getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getId());
        assertEquals(input.getRequerimiento().getContactos().get(2).getNumero(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getNumber());
        assertEquals(input.getRequerimiento().getContactos().get(2).getCodigoPaisCelular(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getCountryCode());
        assertEquals(input.getRequerimiento().getContactos().get(2).getOperadorCelular().getCodigo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getId());
        assertEquals(input.getRequerimiento().getContactos().get(2).getOperadorCelular().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getName());
        //TRACKING FIRST INDEX
        assertEquals(STATUS_ID_PROCESS_ENUM_VALUE, result.getTracking().get(0).getStatus().getId());
        assertEquals(input.getRequerimiento().getSeguimiento().get(0).getEstado().getNombre(), result.getTracking().get(0).getStatus().getDescription());
        assertTrue(result.getTracking().get(0).getStatus().getIsActive());
        //TRACKING SECOND INDEX
        assertEquals(STATUS_ID_REGISTER_ENUM_VALUE, result.getTracking().get(1).getStatus().getId());
        assertEquals(input.getRequerimiento().getSeguimiento().get(1).getEstado().getNombre(), result.getTracking().get(1).getStatus().getDescription());
        assertFalse(result.getTracking().get(1).getStatus().getIsActive());

        // IF(DTOdata.petitioners[0].petitioner.AUTHORIZED.id IS NOT NULL && DTORepresentante.nombres IS NOT NULL)
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getSegment());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getSegment().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getSegment().getDescription());

        assertEquals(input.getRepresentante().getDireccion().getNombre(), result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getRepresentante().getReferencia(), result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimiento().getRepresentante().getSegmento().getDescripcion(), result.getPetitioners().get(1).getPetitioner().getSegment().getDescription());
        assertEquals(input.getRequerimiento().getRepresentante().getSegmento().getCodigo(), result.getPetitioners().get(1).getPetitioner().getSegment().getId());
    }


    @Test
    public void mapOutUnknownHolderFullTest() throws IOException {
        when(restProductClaimMapper.mapOutPetitioner(any())).thenReturn(EntityMock.buildPetitioner("UNKNOWN", "HOLDER"));
        when(restProductClaimMapper.mapOutRepresentativePetitioner(any())).thenReturn(EntityMock.buildRepresentativePetitioner("UNKNOWN"));
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        ProductClaims result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getPersonType());
        assertNotNull(result.getClaimType());
        assertNotNull(result.getClaimType().getId());
        assertNotNull(result.getClaimType().getDescription());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getName());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getReason().getSubReason());
        assertNotNull(result.getReason().getSubReason().getId());
        assertNotNull(result.getReason().getSubReason().getDescription());
        assertNotNull(result.getResolution());
        assertNotNull(result.getResolution().getId());
        assertNotNull(result.getResolution().getName());
        assertNotNull(result.getResolution().getDelivery());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType().getId());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType().getDescription());
        assertNotNull(result.getResolution().getFundament());
        assertNotNull(result.getResolution().getFundament().getId());
        assertNotNull(result.getResolution().getFundament().getDescription());
        assertNotNull(result.getResolution().getResolutionType());
        assertNotNull(result.getResolution().getResolutionType().getId());
        assertNotNull(result.getResolution().getResolutionType().getDescription());
        assertNotNull(result.getResolution().getAdditionalInformation());
        assertNotNull(result.getResolution().getIsManualFlow());
        assertNotNull(result.getResolution().getReturnedAmounts());
        assertEquals(2, result.getResolution().getReturnedAmounts().size());
        assertNotNull(result.getResolution().getReturnedAmounts().get(0).getAmount());
        assertNotNull(result.getResolution().getReturnedAmounts().get(0).getCurrency());
        assertNotNull(result.getResolution().getReturnedAmounts().get(1).getAmount());
        assertNotNull(result.getResolution().getReturnedAmounts().get(1).getCurrency());
        assertNotNull(result.getResolution().getOperationalAmounts());
        assertEquals(2, result.getResolution().getOperationalAmounts().size());
        assertNotNull(result.getResolution().getOperationalAmounts().get(0).getAmount());
        assertNotNull(result.getResolution().getOperationalAmounts().get(0).getCurrency());
        assertNotNull(result.getResolution().getOperationalAmounts().get(1).getAmount());
        assertNotNull(result.getResolution().getOperationalAmounts().get(1).getCurrency());
        assertNotNull(result.getStatus());
        assertNotNull(result.getDocuments());
        assertEquals(2, result.getDocuments().size());
        assertNotNull(result.getDocuments().get(0).getId());
        assertNotNull(result.getDocuments().get(0).getGroup());
        assertNotNull(result.getDocuments().get(0).getName());
        assertNotNull(result.getDocuments().get(1).getId());
        assertNotNull(result.getDocuments().get(1).getGroup());
        assertNotNull(result.getDocuments().get(1).getName());
        assertNotNull(result.getContract());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getContract().getNumber());
        assertNotNull(result.getContract().getNumberType());
        assertNotNull(result.getContract().getNumberType().getId());
        assertNotNull(result.getContract().getProduct());
        assertNotNull(result.getContract().getProduct().getId());
        assertNotNull(result.getClaimAmount());
        assertNotNull(result.getClaimAmount().getAmount());
        assertNotNull(result.getClaimAmount().getCurrency());
        assertNotNull(result.getDelivery());
        assertNotNull(result.getDelivery().getDeliveryType());
        assertNotNull(result.getDelivery().getDeliveryType().getId());
        assertNotNull(result.getDelivery().getDeliveryType().getDescription());
        assertNotNull(result.getChannel());
        assertNotNull(result.getChannel().getId());
        assertNotNull(result.getChannel().getDescription());
        assertNotNull(result.getGeolocation());
        assertNotNull(result.getGeolocation().getCode());
        assertNotNull(result.getGeolocation().getDescription());
        assertNotNull(result.getAtm());
        assertNotNull(result.getAtm().getId());
        assertNotNull(result.getBank());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getName());
        assertNotNull(result.getBank().getBranch());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getBank().getBranch().getName());
        assertNotNull(result.getPetition());
        assertNotNull(result.getPetition().getDescription());
        assertNotNull(result.getPriority());
        assertNotNull(result.getPriority().getIsUrgent());
        assertNotNull(result.getPriority().getBasis());
        assertNotNull(result.getEnrollmentDate());
        assertNotNull(result.getEndDate());
        assertNotNull(result.getCategory());
        assertNotNull(result.getCategory().getId());
        assertNotNull(result.getCategory().getName());
        assertNotNull(result.getCategory().getSubCategory());
        assertNotNull(result.getCategory().getSubCategory().getId());
        assertNotNull(result.getCategory().getSubCategory().getName());
        assertNotNull(result.getAdditionalInformation());
        assertNotNull(result.getOriginApplication());
        assertNotNull(result.getOriginApplication().getId());
        assertNotNull(result.getPredecessorClaim());
        assertNotNull(result.getPredecessorClaim().getId());
        assertNotNull(result.getIsOnlineRefundFlow());
        assertNotNull(result.getPreviousAttentionCode());
        assertNotNull(result.getAdditionalClaims());
        assertEquals(1, result.getAdditionalClaims().size());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType().getDescription());
        assertNotNull(result.getAdditionalClaims().get(0).getProduct());
        assertNotNull(result.getAdditionalClaims().get(0).getProduct().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getProduct().getName());
        assertNotNull(result.getAdditionalClaims().get(0).getReason());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getName());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason().getDescription());
        assertNotNull(result.getAdditionalClaims().get(0).getContract());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumber());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumberType());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumberType().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getProduct());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getProduct().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount().getAmount());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount().getCurrency());
        assertNotNull(result.getAdditionalClaims().get(0).getAtm());
        assertNotNull(result.getAdditionalClaims().get(0).getAtm().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getName());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch().getName());
        assertNotNull(result.getPetitioners());
        assertEquals(2, result.getPetitioners().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertEquals(1, result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertEquals(1, result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails());
        assertEquals(3, result.getPetitioners().get(0).getPetitioner().getContactDetails().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getNumber());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getPhoneType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountry());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountry().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountryCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getRegionalCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getExtension());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getContactDetailType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getNumber());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getCountryCode());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment().getDescription());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBankClassification());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBankClassification().getBankClassificationType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBankClassification().getBankClassificationType().getId());

        assertNotNull(result.getTracking());
        assertEquals(2, result.getTracking().size());
        assertNotNull(result.getTracking().get(0).getStatus());
        assertNotNull(result.getTracking().get(0).getStatus().getId());
        assertNotNull(result.getTracking().get(0).getStatus().getDescription());
        assertNotNull(result.getTracking().get(0).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(0).getLastUpdateDate());
        assertNotNull(result.getTracking().get(1).getStatus());
        assertNotNull(result.getTracking().get(1).getStatus().getId());
        assertNotNull(result.getTracking().get(1).getStatus().getDescription());
        assertNotNull(result.getTracking().get(1).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(1).getLastUpdateDate());

        assertEquals(input.getRequerimiento().getNumero(), result.getId());
        assertEquals(input.getRequerimiento().getNumero(), result.getNumber());
        assertEquals(input.getRequerimiento().getProducto().getCodigo(), result.getProduct().getId());
        assertEquals(input.getRequerimiento().getProducto().getNombre(), result.getProduct().getName());
        assertEquals(input.getRequerimiento().getMotivo().getCodigo(), result.getReason().getId());
        assertEquals(input.getRequerimiento().getMotivo().getNombre(), result.getReason().getName());
        assertEquals(input.getRequerimiento().getSubmotivo().getCodigo(), result.getReason().getSubReason().getId());
        assertEquals(input.getRequerimiento().getSubmotivo().getNombre(), result.getReason().getSubReason().getDescription());
        assertEquals(RESOLUTION_ID_PETITION_ENUM_VALUE, result.getResolution().getId());
        assertEquals(input.getRequerimiento().getDictamen().getNombre(), result.getResolution().getName());
        assertEquals(RESOLUTION_DELIVERY_TYPE_FRONTEND, result.getResolution().getDelivery().getDeliveryType().getId());
        assertEquals(input.getRequerimiento().getCartaRespuesta().getValue(), result.getResolution().getDelivery().getDeliveryType().getDescription());
        assertEquals(input.getRequerimiento().getDictamen().getFundamento().getCodigo(), result.getResolution().getFundament().getId());
        assertEquals(input.getRequerimiento().getDictamen().getFundamento().getDescripcion(), result.getResolution().getFundament().getDescription());
        assertEquals(CLAIMS_RESOLUTION_RESOLUTIONTYPE_FRONTEND_PERMANENT, result.getResolution().getResolutionType().getId());
        assertEquals(input.getRequerimiento().getDictamen().getClase().getDescripcion(), result.getResolution().getResolutionType().getDescription());
        assertEquals(input.getRequerimiento().getDictamen().getSustento(), result.getResolution().getAdditionalInformation());
        assertTrue(Boolean.parseBoolean(result.getResolution().getIsManualFlow()));
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(0).getValor(), result.getResolution().getReturnedAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(0).getValor(), result.getResolution().getReturnedAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(1).getValor(), result.getResolution().getReturnedAmounts().get(1).getAmount());
        assertEquals(input.getRequerimiento().getImporteDevuelto().get(1).getValor(), result.getResolution().getReturnedAmounts().get(1).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(0).getValor(), result.getResolution().getOperationalAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(0).getValor(), result.getResolution().getOperationalAmounts().get(0).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(1).getValor(), result.getResolution().getOperationalAmounts().get(1).getAmount());
        assertEquals(input.getRequerimiento().getImporteOperacional().get(1).getValor(), result.getResolution().getOperationalAmounts().get(1).getAmount());
        assertEquals(input.getRequerimiento().getProducto().getContrato().getId(), result.getContract().getId());
        assertEquals(input.getRequerimiento().getProducto().getContrato().getNumero(), result.getContract().getNumber());
        assertEquals(CONTRACT_NUMBER_TYPE_FRONTEND, result.getContract().getNumberType().getId());
        assertEquals(input.getRequerimiento().getProducto().getContrato().getAliasProducto(), result.getContract().getProduct().getId());
        assertEquals(input.getRequerimiento().getImporteReclamado(), result.getClaimAmount().getAmount());
        assertEquals(input.getRequerimiento().getDivisaReclamado(), result.getClaimAmount().getCurrency());
        assertEquals(CLAIMS_DELIVERY_TYPE_FRONTEND, result.getDelivery().getDeliveryType().getId());
        assertEquals(input.getRequerimiento().getEntregaHRCS().getValue(), result.getDelivery().getDeliveryType().getDescription());
        assertEquals(input.getRequerimiento().getInterposicion().getCodigo(), result.getChannel().getId());
        assertEquals(input.getRequerimiento().getInterposicion().getNombre(), result.getChannel().getDescription());
        assertEquals(input.getRequerimiento().getUbigeo().getCodigo(), result.getGeolocation().getCode());
        assertEquals(input.getRequerimiento().getUbigeo().getDescripcion(), result.getGeolocation().getDescription());
        assertEquals(input.getRequerimiento().getTaxonomia().getCajero(), result.getAtm().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getBancoOriginante().getCodigo(), result.getBank().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getBancoOriginante().getDescripcion(), result.getBank().getName());
        assertEquals(input.getRequerimiento().getTaxonomia().getOficinaOriginante().getCodigo(), result.getBank().getBranch().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getOficinaOriginante().getDescripcion(), result.getBank().getBranch().getName());
        assertEquals(input.getRequerimiento().getPedido(), result.getPetition().getDescription());
        assertTrue(result.getPriority().getIsUrgent());
        assertEquals(input.getRequerimiento().getSustento(), result.getPriority().getBasis());
        assertEquals(CLAIMS_CATEGORY_ID_ENUM_VALUE, result.getCategory().getId());
        assertEquals(input.getRequerimiento().getCategoria().getDescripcion(), result.getCategory().getName());
        assertEquals(input.getRequerimiento().getTaxonomia().getAgrupacion().getCodigo(), result.getCategory().getSubCategory().getId());
        assertEquals(input.getRequerimiento().getTaxonomia().getAgrupacion().getDescripcion(), result.getCategory().getSubCategory().getName());
        assertEquals(input.getRequerimiento().getDetalleComplementario(), result.getAdditionalInformation());
        assertEquals(input.getRequerimiento().getAppOrigenRegistro(), result.getOriginApplication().getId());
        assertEquals(input.getRequerimiento().getNumeroCasoReiterativo(), result.getPredecessorClaim().getId());
        assertFalse(Boolean.parseBoolean(result.getIsOnlineRefundFlow()));
        assertEquals(input.getNumeroAtencionLinea(), result.getPreviousAttentionCode());
        assertEquals(CLAIM_TYPE_ID_PETITION_ENUM_VALUE, result.getAdditionalClaims().get(0).getClaimType().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getTipo().getDescripcion(), result.getAdditionalClaims().get(0).getClaimType().getDescription());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getCodigo(), result.getAdditionalClaims().get(0).getProduct().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getNombre(), result.getAdditionalClaims().get(0).getProduct().getName());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getMotivo().getCodigo(), result.getAdditionalClaims().get(0).getReason().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getMotivo().getNombre(), result.getAdditionalClaims().get(0).getReason().getName());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getSubmotivo().getCodigo(), result.getAdditionalClaims().get(0).getReason().getSubReason().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getSubmotivo().getNombre(), result.getAdditionalClaims().get(0).getReason().getSubReason().getDescription());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getContrato().getId(), result.getAdditionalClaims().get(0).getContract().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getContrato().getNumero(), result.getAdditionalClaims().get(0).getContract().getNumber());
        assertEquals(CONTRACT_NUMBER_TYPE_FRONTEND, result.getAdditionalClaims().get(0).getContract().getNumberType().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getProducto().getContrato().getAliasProducto(), result.getAdditionalClaims().get(0).getContract().getProduct().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getImporteReclamado(), result.getAdditionalClaims().get(0).getClaimAmount().getAmount());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getDivisa(), result.getAdditionalClaims().get(0).getClaimAmount().getCurrency());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getCajero(), result.getAdditionalClaims().get(0).getAtm().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getBancoOriginante().getCodigo(), result.getAdditionalClaims().get(0).getBank().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getBancoOriginante().getDescripcion(), result.getAdditionalClaims().get(0).getBank().getName());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getOficinaOriginante().getCodigo(), result.getAdditionalClaims().get(0).getBank().getBranch().getId());
        assertEquals(input.getRequerimiento().getTaxonomiasAdicionales().get(0).getOficinaOriginante().getDescripcion(), result.getAdditionalClaims().get(0).getBank().getBranch().getName());

        //DOCUMENT FIRST INDEX
        assertEquals(input.getRequerimiento().getAdjunto().get(0).getId(), result.getDocuments().get(0).getId());
        assertEquals(input.getRequerimiento().getAdjunto().get(0).getGrupo(), result.getDocuments().get(0).getGroup());
        assertEquals(input.getRequerimiento().getAdjunto().get(0).getNombre(), result.getDocuments().get(0).getName());
        //SECOND FIRST INDEX
        assertEquals(input.getRequerimiento().getAdjunto().get(1).getId(), result.getDocuments().get(1).getId());
        assertEquals(input.getRequerimiento().getAdjunto().get(1).getGrupo(), result.getDocuments().get(1).getGroup());
        assertEquals(input.getRequerimiento().getAdjunto().get(1).getNombre(), result.getDocuments().get(1).getName());
        //PETITIONER
        assertEquals(input.getTitular().getDireccion().getNombre(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getTitular().getReferencia(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals("UBIGEO", result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(input.getRequerimiento().getTitular().getDireccion().getUbigeo().getCodigo(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());
        assertEquals(input.getRequerimiento().getTitular().getDireccion().getUbigeo().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getName());
        assertEquals(input.getRequerimiento().getTitular().getSegmento().getCodigo(), result.getPetitioners().get(0).getPetitioner().getSegment().getId());
        assertEquals(input.getRequerimiento().getTitular().getSegmento().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getSegment().getDescription());
        assertEquals(input.getRequerimiento().getTitular().getBanco().getCodigo(), result.getPetitioners().get(0).getPetitioner().getBank().getId());
        assertEquals(input.getRequerimiento().getTitular().getBanco().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getBank().getName());
        assertEquals(input.getRequerimiento().getTitular().getOficina().getCodigo(), result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getId());
        assertEquals(input.getRequerimiento().getTitular().getOficina().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getName());
        assertEquals(CLAIMS_PETITIONER_BANKCLASSIFICATIONTYPE_ENUM_VALUE, result.getPetitioners().get(0).getPetitioner().getBankClassification().getBankClassificationType().getId());
        //EMAIL CONTACTDETAILTYPE
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_EMAIL_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertEquals(input.getRequerimiento().getContactos().get(0).getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(input.getRequerimiento().getContactos().get(0).getCorreo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getAddress());
        assertTrue(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getReceivesNotifications());
        //LANDLINE CONTACTDETAILTYPE
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_LANDLINE_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType());
        assertEquals(input.getRequerimiento().getContactos().get(1).getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertEquals(input.getRequerimiento().getContactos().get(1).getNumero(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getNumber());
        assertEquals(CLAIMS_PETITIONER_CONTACT_PHONETYPE_ENUM_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getPhoneType());
        assertEquals(input.getRequerimiento().getContactos().get(1).getPaisFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountry().getId());
        assertEquals(input.getRequerimiento().getContactos().get(1).getCodigoPaisFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getCountryCode());
        assertEquals(input.getRequerimiento().getContactos().get(1).getCodigoRegionalFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getRegionalCode());
        assertEquals(input.getRequerimiento().getContactos().get(1).getAnexoFijo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getExtension());
        //MOBILE CONTACTDETAILTYPE
        assertEquals(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_MOBILE_VALUE, result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getContactDetailType());
        assertEquals(input.getRequerimiento().getContactos().get(2).getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getId());
        assertEquals(input.getRequerimiento().getContactos().get(2).getNumero(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getNumber());
        assertEquals(input.getRequerimiento().getContactos().get(2).getCodigoPaisCelular(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getCountryCode());
        assertEquals(input.getRequerimiento().getContactos().get(2).getOperadorCelular().getCodigo(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getId());
        assertEquals(input.getRequerimiento().getContactos().get(2).getOperadorCelular().getDescripcion(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getPhoneCompany().getName());
        //TRACKING FIRST INDEX
        assertEquals(STATUS_ID_PROCESS_ENUM_VALUE, result.getTracking().get(0).getStatus().getId());
        assertEquals(input.getRequerimiento().getSeguimiento().get(0).getEstado().getNombre(), result.getTracking().get(0).getStatus().getDescription());
        assertTrue(result.getTracking().get(0).getStatus().getIsActive());
        //TRACKING SECOND INDEX
        assertEquals(STATUS_ID_REGISTER_ENUM_VALUE, result.getTracking().get(1).getStatus().getId());
        assertEquals(input.getRequerimiento().getSeguimiento().get(1).getEstado().getNombre(), result.getTracking().get(1).getStatus().getDescription());
        assertFalse(result.getTracking().get(1).getStatus().getIsActive());

        // IF(DTOdata.petitioners[0].petitioner.AUTHORIZED.id IS NOT NULL && DTORepresentante.nombres IS NOT NULL)
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getSegment());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getSegment().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getSegment().getDescription());

        assertEquals(input.getTitular().getDireccion().getNombre(), result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(input.getTitular().getReferencia(), result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertEquals(input.getRequerimiento().getRepresentante().getSegmento().getDescripcion(), result.getPetitioners().get(1).getPetitioner().getSegment().getDescription());
        assertEquals(input.getRequerimiento().getRepresentante().getSegmento().getCodigo(), result.getPetitioners().get(1).getPetitioner().getSegment().getId());
    }

    @Test
    public void mapOutPetitionerNullTest() throws IOException {
        when(restProductClaimMapper.mapOutPetitioner(any())).thenReturn(null);
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        ProductClaims result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getPersonType());
        assertNotNull(result.getClaimType());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getName());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getReason().getSubReason());
        assertNotNull(result.getReason().getSubReason().getId());
        assertNotNull(result.getReason().getSubReason().getDescription());
        assertNotNull(result.getResolution());
        assertNotNull(result.getResolution().getId());
        assertNotNull(result.getResolution().getName());
        assertNotNull(result.getResolution().getDelivery());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType().getId());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType().getDescription());
        assertNotNull(result.getResolution().getReturnedAmounts());
        assertEquals(2, result.getResolution().getReturnedAmounts().size());
        assertNotNull(result.getResolution().getReturnedAmounts().get(0).getAmount());
        assertNotNull(result.getResolution().getReturnedAmounts().get(0).getCurrency());
        assertNotNull(result.getResolution().getReturnedAmounts().get(1).getAmount());
        assertNotNull(result.getResolution().getReturnedAmounts().get(1).getCurrency());
        assertNotNull(result.getResolution().getOperationalAmounts());
        assertEquals(2, result.getResolution().getOperationalAmounts().size());
        assertNotNull(result.getResolution().getOperationalAmounts().get(0).getAmount());
        assertNotNull(result.getResolution().getOperationalAmounts().get(0).getCurrency());
        assertNotNull(result.getResolution().getOperationalAmounts().get(1).getAmount());
        assertNotNull(result.getResolution().getOperationalAmounts().get(1).getCurrency());
        assertNotNull(result.getDelivery());
        assertNotNull(result.getDelivery().getDeliveryType());
        assertNotNull(result.getDelivery().getDeliveryType().getId());
        assertNotNull(result.getDelivery().getDeliveryType().getDescription());
        assertNotNull(result.getChannel());
        assertNotNull(result.getChannel().getId());
        assertNotNull(result.getChannel().getDescription());
        assertNotNull(result.getStatus());
        assertNotNull(result.getDocuments());
        assertEquals(2, result.getDocuments().size());
        assertNotNull(result.getDocuments().get(0).getId());
        assertNotNull(result.getDocuments().get(0).getGroup());
        assertNotNull(result.getDocuments().get(0).getName());
        assertNotNull(result.getDocuments().get(1).getId());
        assertNotNull(result.getDocuments().get(1).getGroup());
        assertNotNull(result.getDocuments().get(1).getName());
        assertNotNull(result.getContract());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getContract().getNumber());
        assertNotNull(result.getContract().getNumberType());
        assertNotNull(result.getContract().getNumberType().getId());
        assertNotNull(result.getClaimAmount());
        assertNotNull(result.getClaimAmount().getAmount());
        assertNotNull(result.getClaimAmount().getCurrency());
        assertNull(result.getPetitioners());
        assertNotNull(result.getTracking());
        assertEquals(2, result.getTracking().size());
        assertNotNull(result.getTracking().get(0).getStatus());
        assertNotNull(result.getTracking().get(0).getStatus().getId());
        assertNotNull(result.getTracking().get(0).getStatus().getDescription());
        assertNotNull(result.getTracking().get(0).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(0).getLastUpdateDate());
        assertNotNull(result.getTracking().get(1).getStatus());
        assertNotNull(result.getTracking().get(1).getStatus().getId());
        assertNotNull(result.getTracking().get(1).getStatus().getDescription());
        assertNotNull(result.getTracking().get(1).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(1).getLastUpdateDate());
    }

    @Test
    public void mapOutDocumentsNullTest() throws IOException {
        when(restProductClaimMapper.mapOutPetitioner(any())).thenReturn(EntityMock.buildPetitioner("KNOWN", "HOLDER"));
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        input.getRequerimiento().setAdjunto(null);
        ProductClaims result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getPersonType());
        assertNotNull(result.getClaimType());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getName());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getResolution());
        assertNotNull(result.getStatus());
        assertNull(result.getDocuments());
        assertNotNull(result.getPetitioners());
        assertEquals(1, result.getPetitioners().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNotNull(result.getTracking());
        assertEquals(2, result.getTracking().size());
        assertNotNull(result.getTracking().get(0).getStatus());
        assertNotNull(result.getTracking().get(0).getStatus().getId());
        assertNotNull(result.getTracking().get(0).getStatus().getDescription());
        assertNotNull(result.getTracking().get(0).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(0).getLastUpdateDate());
        assertNotNull(result.getTracking().get(1).getStatus());
        assertNotNull(result.getTracking().get(1).getStatus().getId());
        assertNotNull(result.getTracking().get(1).getStatus().getDescription());
        assertNotNull(result.getTracking().get(1).getStatus().getIsActive());
        assertNotNull(result.getTracking().get(1).getLastUpdateDate());
    }

    @Test
    public void mapOutTrackingNullTest() throws IOException {
        when(restProductClaimMapper.mapOutPetitioner(any())).thenReturn(EntityMock.buildPetitioner("KNOWN", "HOLDER"));
        ResponseType input = ResultadoModelsStubs.INSTANCE.getResponseType();
        input.getRequerimiento().setSeguimiento(null);
        ProductClaims result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getNumber());
        assertNotNull(result.getCreationDate());
        assertNotNull(result.getPersonType());
        assertNotNull(result.getClaimType());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getName());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getName());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getResolution());
        assertNotNull(result.getStatus());
        assertNotNull(result.getDocuments());
        assertEquals(2, result.getDocuments().size());
        assertNotNull(result.getDocuments().get(0).getId());
        assertNotNull(result.getDocuments().get(0).getGroup());
        assertNotNull(result.getDocuments().get(0).getName());
        assertNotNull(result.getDocuments().get(1).getId());
        assertNotNull(result.getDocuments().get(1).getGroup());
        assertNotNull(result.getDocuments().get(1).getName());
        assertNotNull(result.getPetitioners());
        assertEquals(1, result.getPetitioners().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation());
        assertNull(result.getTracking());
    }

    @Test
    public void mapOutEmptyTest() {
        ProductClaims result = mapper.mapOut(null);

        assertNull(result);
    }

    @Test
    public void mapOutInitializedTest() {
        ProductClaims result = mapper.mapOut(new ResponseType());

        assertNull(result);
    }
}
