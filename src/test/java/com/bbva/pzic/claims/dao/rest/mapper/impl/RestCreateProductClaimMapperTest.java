package com.bbva.pzic.claims.dao.rest.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.DTOIntEmail;
import com.bbva.pzic.claims.business.dto.DTOIntMobile;
import com.bbva.pzic.claims.business.dto.DTOIntProductClaim;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.dao.rest.model.harec.RequestDataType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.util.FunctionUtils;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static com.bbva.pzic.claims.EntityMock.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 22/11/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class RestCreateProductClaimMapperTest {

    @InjectMocks
    private RestCreateProductClaimMapper mapper;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(translator.translateFrontendEnumValueStrictly("claims.personType.id",
                PERSON_TYPE_ID_LEGAL_ENUM_VALUE)).thenReturn(PERSON_TYPE_ID_LEGAL_FRONTEND_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.claimType.id",
                CLAIM_TYPE_ID_PETITION_ENUM_VALUE)).thenReturn(CLAIM_TYPE_ID_PETITION_FRONTEND_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.verdict.deliveryType.id",
                VERDICT_DELIVERY_TYPE_ID_EMAIL_ENUM_VALUE)).thenReturn(VERDICT_DELIVERY_TYPE_ID_EMAIL_FRONTEND);
        when(translator.translateFrontendEnumValueStrictly("claims.contactDetails.contact.contactDetailType",
                CLAIMS_CONTACT_DETAIL_TYPE_MOBILE_ENUM_VALUE)).thenReturn(CLAIMS_CONTACT_DETAIL_TYPE_MOBILE_FRONTEND);
        when(translator.translateFrontendEnumValueStrictly("claims.contactDetails.contact.contactDetailType",
                CLAIMS_CONTACT_DETAIL_TYPE_LANDLINE_ENUM_VALUE)).thenReturn(CLAIMS_CONTACT_DETAIL_TYPE_LANDLINE_FRONTEND);
        when(translator.translateFrontendEnumValueStrictly("claims.identityDocument.documentType.id",
                CLAIMS_IDENTITY_DOCUMENT_DOCUMENT_TYPE_ID_DNI_ENUM_VALUE)).thenReturn(CLAIMS_IDENTITY_DOCUMENT_DOCUMENT_TYPE_ID_DNI_FRONTEND);
        when(translator.translateFrontendEnumValueStrictly("claims.contract.numberTypeId",
                CLAIMS_NUMBER_TYPE_ID_PAN_ENUM_VALUE)).thenReturn(CLAIMS_NUMBER_TYPE_ID_PAN_FRONTEND);
        when(translator.translateFrontendEnumValueStrictly("claims.delivery.deliveryTypeId",
                CLAIMS_DELIVERY_DELIVERYTYPEID_BACKEND)).thenReturn(CLAIMS_DELIVERY_DELIVERYTYPEID_FRONTEND);
        when(translator.translateFrontendEnumValueStrictly("claims.contract.numberTypeId",
                CLAIMS_CONTRACT_NUMBERTYPE_BACKEND)).thenReturn(CLAIMS_CONTRACT_NUMBERTYPE_FRONTEND);
    }

    @Test
    public void mapInKnownHolderPetitionerTest() throws IOException {
        DTOIntProductClaim input = EntityMock.getInstance().getDTOOutProductClaimKnownTransaction();

        RequestDataType result = mapper.mapInBody(input);

        assertNotNull(result);
        assertNotNull(result.getAdjuntos());
        assertNotNull(result.getTipoPersona());
        assertNotNull(result.getEmpresa());
        assertNotNull(result.getEsMenor());
        assertNotNull(result.getTaxonomia());
        assertNotNull(result.getTaxonomia().getTipo());
        assertNotNull(result.getTaxonomia().getTipo().getCodigo());
        assertNotNull(result.getTaxonomia().getProducto());
        assertNotNull(result.getTaxonomia().getProducto().getCodigo());
        assertNotNull(result.getTaxonomia().getMotivo());
        assertNotNull(result.getTaxonomia().getMotivo().getCodigo());
        assertNotNull(result.getTaxonomia().getSubmotivo());
        assertNotNull(result.getTaxonomia().getSubmotivo().getCodigo());
        assertNotNull(result.getTaxonomia().getProducto());
        assertNotNull(result.getTaxonomia().getProducto().getContrato().getNumero());
        assertNotNull(result.getTaxonomia().getProducto().getContrato().getTipoNumero());
        assertNotNull(result.getTaxonomia().getProducto().getContrato().getId());
        assertNotNull(result.getTaxonomia().getProducto().getContrato().getAliasProducto());
        assertNotNull(result.getTaxonomia().getCajero());
        assertNotNull(result.getTaxonomia().getBancoOriginante().getCodigo());
        assertNotNull(result.getTaxonomia().getOficinaOriginante().getCodigo());
        assertNotNull(result.getTaxonomia().getFechaOperacion());
        assertNotNull(result.getContrato());
        assertNotNull(result.getInterposicion().getCodigo());
        assertNotNull(result.getUbicacion().getCodigo());
        assertNotNull(result.getUbicacion().getCodigo());
        assertNotNull(result.getEntregaHRCS());
        assertNotNull(result.getPedido());
        assertNotNull(result.getEsUrgente());
        assertNotNull(result.getSustento());
        assertNotNull(result.getFechaPresentacion());
        assertNotNull(result.getNumeroCasoReiterativo());
        assertNotNull(result.getIndicadorFCR());
        assertNotNull(result.getNumeroAtencionLinea());
        assertNotNull(result.getTaxonomiasAdicionales());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getTipo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getTipo().getCodigo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getMotivo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getMotivo().getCodigo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getSubmotivo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getSubmotivo().getCodigo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getProducto());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getProducto().getCodigo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getProducto().getContrato().getId());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getProducto().getContrato().getNumero());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getProducto().getContrato().getTipoNumero());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getProducto().getContrato().getAliasProducto());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getImporteReclamado());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getDivisa());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getCajero());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getBancoOriginante());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getBancoOriginante().getCodigo());
        assertNotNull(result.getTaxonomiasAdicionales().get(0).getOficinaOriginante().getCodigo());

        assertNotNull(result.getDivisa());
        assertNotNull(result.getDetalle());
        assertNotNull(result.getCartaRespuesta());
        assertNotNull(result.getTitular());
        assertNotNull(result.getTitular().getCodigoCentral());
        assertNull(result.getRepresentante());
        assertNotNull(result.getDireccion());
        assertNotNull(result.getDireccion().getId());
        assertNotNull(result.getDireccion().getNombre());
        assertNotNull(result.getReferencia());

        assertNotNull(result.getUbigeo());
        assertNotNull(result.getUbigeo().getCodUbigeo());

        assertNotNull(result.getCorreos());
        assertEquals(1, result.getCorreos().size());
        assertNotNull(result.getCorreos().get(0));
        assertNotNull(result.getCorreos().get(0).getId());
        assertNotNull(result.getCorreos().get(0).getCorreo());
        assertNotNull(result.getTelefonos());
        assertEquals(2, result.getTelefonos().size());
        assertNotNull(result.getTelefonos().get(0));
        assertNotNull(result.getTelefonos().get(0).getId());
        assertNotNull(result.getTelefonos().get(0).getNumero());
        assertNotNull(result.getTelefonos().get(0).getTipo());
        assertNotNull(result.getTelefonos().get(0).getOperador());
        assertNotNull(result.getTelefonos().get(1));
        assertNotNull(result.getTelefonos().get(1).getId());
        assertNotNull(result.getTelefonos().get(1).getNumero());
        assertNotNull(result.getTelefonos().get(1).getTipo());
        assertNotNull(result.getTelefonos().get(1).getOperador());

        assertEquals(FunctionUtils.convertBooleanToString(input.getPriority().getIsUrgent()),result.getEsUrgente());
        assertEquals(input.getFiles().get(0).getContent(), result.getAdjuntos().get(0).getBinario());
        assertEquals(input.getFiles().get(0).getName(), result.getAdjuntos().get(0).getNombre());
        assertEquals(PERSON_TYPE_ID_LEGAL_FRONTEND_VALUE, result.getTipoPersona());
        assertEquals(input.getCompany().getId(), result.getEmpresa());
        assertEquals(input.getIsUnderage(), result.getEsMenor());
        assertEquals(CLAIM_TYPE_ID_PETITION_FRONTEND_VALUE, result.getTaxonomia().getTipo().getCodigo());
        assertEquals(input.getProduct().getId(), result.getTaxonomia().getProducto().getCodigo());
        assertEquals(input.getReason().getId(), result.getTaxonomia().getMotivo().getCodigo());
        assertEquals(input.getReason().getSubReason().getId(), result.getTaxonomia().getSubmotivo().getCodigo());
        assertEquals(input.getContract().getNumber(), result.getTaxonomia().getProducto().getContrato().getNumero());
        assertEquals(CLAIMS_NUMBER_TYPE_ID_PAN_FRONTEND, result.getTaxonomia().getProducto().getContrato().getTipoNumero());
        assertEquals(input.getContract().getId(), result.getContrato());
        assertEquals(input.getClaimAmount().getAmount(), result.getImporteReclamado());
        assertEquals(input.getClaimAmount().getCurrency(), result.getDivisa());
        assertEquals(input.getReason().getComments(), result.getDetalle());
        assertEquals(VERDICT_DELIVERY_TYPE_ID_EMAIL_FRONTEND, result.getCartaRespuesta());
        assertEquals(input.getPetitioners().get(0).getPetitioner().getId(), result.getTitular().getCodigoCentral());
        assertEquals(input.getPetitioners().get(0).getPetitioner().getAddress().getId(), result.getDireccion().getId());
        assertEquals(input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress(), result.getDireccion().getNombre());
        assertEquals(input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAdditionalInformation(), result.getReferencia());

        assertEquals(input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode(),
                result.getUbigeo().getCodUbigeo());

        //CONTACT DETAILS - FIRST INDEX
        assertEquals(input.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId(),
                result.getCorreos().get(0).getId());
        assertEquals(((DTOIntEmail) input.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact()).getAddress(),
                result.getCorreos().get(0).getCorreo());


        //CONTACT DETAILS - SECOND INDEX
        assertEquals(input.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getId(),
                result.getTelefonos().get(0).getId());

        assertEquals(((DTOIntMobile) input.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact()).getNumber(),
                result.getTelefonos().get(0).getNumero());

        assertEquals(CLAIMS_CONTACT_DETAIL_TYPE_MOBILE_FRONTEND, result.getTelefonos().get(0).getTipo());

        assertEquals(((DTOIntMobile) input.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact()).getPhoneCompany().getId(),
                result.getTelefonos().get(0).getOperador());


        //CONTACT DETAILS - THIRD INDEX
        assertEquals(input.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getId(),
                result.getTelefonos().get(1).getId());

        assertEquals(((DTOIntMobile) input.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact()).getNumber(),
                result.getTelefonos().get(1).getNumero());

        assertEquals(CLAIMS_CONTACT_DETAIL_TYPE_LANDLINE_FRONTEND, result.getTelefonos().get(1).getTipo());
        assertEquals(((DTOIntMobile) input.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact()).getPhoneCompany().getId(),
                result.getTelefonos().get(1).getOperador());
    }

    @Test
    public void mapInKnownPetitionerWithoutUbigeoComponentTypeTest() throws IOException {
        DTOIntProductClaim input = EntityMock.getInstance().getDTOOutProductClaimKnownTransaction();

        input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0)
                .setComponentTypes(Collections.singletonList("ABCD"));

        RequestDataType result = mapper.mapInBody(input);
        assertNotNull(result);
        assertNull(result.getUbigeo());
    }

    @Test
    public void mapInUnknownPetitionerUbigeoComponentTypeTest() throws IOException {
        DTOIntProductClaim input = EntityMock.getInstance().getDTOOutProductClaimUnknownTransaction();

        RequestDataType result = mapper.mapInBody(input);
        assertNotNull(result);
        assertNotNull(result.getUbigeo());
        assertNotNull(result.getUbigeo().getCodUbigeo());

        assertEquals(input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode(),
                result.getUbigeo().getCodUbigeo());
    }

    @Test
    public void mapInUnknownPetitionerWithoutUbigeoComponentTypeTest() throws IOException {
        DTOIntProductClaim input = EntityMock.getInstance().getDTOOutProductClaimUnknownTransaction();

        input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0)
                .setComponentTypes(Collections.singletonList("ABCD"));

        RequestDataType result = mapper.mapInBody(input);
        assertNotNull(result);
        assertNull(result.getUbigeo());
    }

    @Test
    public void mapInNullTest() {
        RequestDataType result = mapper.mapInBody(null);

        assertNull(result);
    }

    @Test
    public void mapInHeaderFull() throws IOException {
        final DTOIntProductClaim input = EntityMock.getInstance().getDTOOutProductClaimKnownTransaction();
        final Map<String, String> result = mapper.mapHeaderIn(input);

        assertNotNull(result);
        assertNotNull(result.get("canal"));
        assertEquals(input.getAap(), result.get("canal"));
    }

    @Test
    public void mapInHeaderEmpty() {
        final Map<String, String> result = mapper.mapHeaderIn(new DTOIntProductClaim());

        assertNotNull(result);
        assertNull(result.get("canal"));
        assertNull(result.get("adjuntos"));
    }

    @Test
    public void mapOutFullTest() {
        final ResponseType input = EntityMock.getInstance().getResultadoResponseType();
        ProductClaimTransaction result = mapper.mapOut(input);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getNumber());

        assertEquals(input.getRequerimiento().getNumero(), result.getId());
        assertEquals(input.getRequerimiento().getNumero(), result.getNumber());
    }

    @Test
    public void mapOutEmptyTest() {
        ProductClaimTransaction result = mapper.mapOut(null);

        assertNull(result);
    }
}