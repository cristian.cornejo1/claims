package com.bbva.pzic.claims.facade.v0.dto;

import com.bbva.pzic.claims.util.FilterTestClasses;
import com.bbva.pzic.claims.util.WADLRequirements;
import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.affirm.Affirm;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author Entelgy
 */
public class ModelTest {

    @Test
    public void wadlRequirementsTest() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        final StringBuilder result = new StringBuilder();

        List<PojoClass> canonics = PojoClassFactory.getPojoClasses(this.getClass().getPackage().getName(), new FilterTestClasses());
        for (PojoClass canonic : canonics) {
            WADLRequirements.implementsSerializableCheck(result, canonic);
            WADLRequirements.fieldSerialVersionUIDCheck(result, canonic);
            WADLRequirements.annotationXmlRootElementCheck(result, canonic);
            WADLRequirements.annotationXmlTypeCheck(result, canonic);
            WADLRequirements.annotationXmlAccessorTypeCheck(result, canonic);
        }

        Assert.assertNotNull(result.toString());
        Affirm.affirmTrue(result.toString(), result.toString().isEmpty());
    }
}
