package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputGetProductClaim;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaimsData;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.bbva.pzic.claims.EntityMock.PRODUCT_CLAIM_ID;
import static org.junit.Assert.*;

/**
 * Created on 11/28/2019.
 *
 * @author Entelgy
 */
public class GetProductClaimMapperTest {

    private GetProductClaimMapper mapper;

    @Before
    public void setUp() {
        mapper = new GetProductClaimMapper();
    }

    @Test
    public void mapInFullTest() {
        InputGetProductClaim result = mapper.mapIn(PRODUCT_CLAIM_ID);

        assertNotNull(result);
        assertNotNull(result.getProductClaimId());

        assertEquals(PRODUCT_CLAIM_ID, result.getProductClaimId());
    }

    @Test
    public void testMapOutFull() throws IOException {
        ProductClaimsData productClaims = mapper.mapOut(EntityMock.getInstance().buildProductClaims());

        assertNotNull(productClaims);
        assertNotNull(productClaims.getData());
    }

    @Test
    public void testMapOutNull() {
        ProductClaimsData productClaims = mapper.mapOut(null);

        assertNull(productClaims);
    }
}
