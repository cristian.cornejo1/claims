package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.DTOIntAutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.claims.EntityMock.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ValidateAutomaticRefundTransactionMapperTest {

    @InjectMocks
    private ValidateAutomaticRefundTransactionMapper mapper;

    @Mock
    private Translator translator;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty(ServiceInvocationContext.AAP)).thenReturn(EntityMock.APP);
        when(translator.translateFrontendEnumValueStrictly("automaticRefundTransactions.refundMode", CASHING_REFUND_MODE_ENUM_VALUE)).thenReturn(CASHING_REFUND_MODE_BACKEND_VALUE);
        when(translator.translateFrontendEnumValueStrictly("automaticRefundTransactions.operationType", INTEREST_OPERATION_TYPE_ENUM_VALUE)).thenReturn(INTEREST_OPERATION_TYPE_BACKEND_VALUE);
        when(translator.translateFrontendEnumValueStrictly("automaticRefundTransactions.operationType", FEE_OPERATION_TYPE_ENUM_VALUE)).thenReturn(FEE_OPERATION_TYPE_BACKEND_VALUE);
        when(translator.translateFrontendEnumValueStrictly("automaticRefundTransactions.operationType", INSURANCE_PREMIUM_OPERATION_TYPE_FRONTEND_VALUE)).thenReturn(INSURANCE_PREMIUM_OPERATION_TYPE_BACKEND_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.petitioner.contactDetailType", CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_LANDLINE_VALUE)).thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_BACKEND_LANDLINE_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.petitioner.contactDetailType", CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_MOBILE_VALUE)).thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_MOBILE_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.petitioner.contactDetailType", CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_EMAIL_VALUE)).thenReturn(CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_EMAIL_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.contractType", CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_CARD_VALUE)).thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_TARJE_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.contractType", CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_ACCOUNT_VALUE)).thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_CUENT_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.contractType", CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_LOAN_VALUE)).thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_PREST_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.numberType", CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_FRONTEND_PAN_VALUE)).thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_PAN_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.numberType", CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_FRONTEND_LIC_VALUE)).thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_LIC_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.automaticRefundTransactions.transactionType", CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_FRONTEND_PURCHASE_VALUE)).thenReturn(CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_BACKEND_PURCHASE_VALUE);
        when(translator.translateFrontendEnumValueStrictly("claims.petitioner.contact.phoneType", CLAIMS_PETITIONER_CONTACT_PHONETYPE_FRONTEND_HOME_VALUE)).thenReturn(CLAIMS_PETITIONER_CONTACT_PHONETYPE_BACKEND_PHONE_NUMBER_VALUE);
    }

    @Test
    public void mapInFullTest() throws IOException {
        AutomaticRefundTransaction input = EntityMock.getInstance()
                .getAutomaticRefundTransaction();
        DTOIntAutomaticRefundTransaction result = mapper.mapIn(input);
        assertNotNull(result);
        assertNotNull(result.getApp());
        assertNotNull(result.getPetitioner().getId());
        assertNotNull(result.getPetitioner().getPetitioner().getIdentityDocument());
        assertNotNull(result.getPetitioner().getPetitioner().getIdentityDocument().getDocumentNumber());
        assertNotNull(result.getPetitioner().getPetitioner().getIdentityDocument().getDocumentType());
        assertNotNull(result.getPetitioner().getPetitioner().getIdentityDocument().getDocumentType().getId());
        assertNotNull(result.getPetitioner().getPetitioner().getSegment());
        assertNotNull(result.getPetitioner().getPetitioner().getSegment().getId());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails());

        //LANDLINE
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getContactDetailType());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getNumber());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getPhoneType());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getCountry());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getCountry().getId());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getCountryCode());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getRegionalCode());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getExtension());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getPhoneCompany());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getAddress());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getReceivesNotifications());

        //MOBILE
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getContactDetailType());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getNumber());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getPhoneCompany());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getPhoneCompany().getId());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getCountryCode());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getCountry());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getRegionalCode());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getExtension());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getAddress());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getReceivesNotifications());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getPhoneType());

        //EMAIL
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getContactDetailType());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getAddress());
        assertNotNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getReceivesNotifications());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getNumber());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getPhoneCompany());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getCountry());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getCountryCode());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getRegionalCode());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getExtension());
        assertNull(result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getPhoneType());

        assertNotNull(result.getRelatedClaim());
        assertNotNull(result.getRelatedClaim().getId());
        assertNotNull(result.getReason().getId());

        assertNotNull(result.getOperations());
        //MPTAR
        assertNotNull(result.getOperations().get(0).getSubproductRefund());
        assertNotNull(result.getOperations().get(0).getSubproductRefund().getId());
        assertNotNull(result.getOperations().get(0).getSubproductRefund().getNumber());
        assertNotNull(result.getOperations().get(0).getSubproductRefund().getRefundMode());

        assertNotNull(result.getOperations().get(0).getDetail());

        //CARD
        assertNotNull(result.getOperations().get(0).getOperationType());
        assertNotNull(result.getOperations().get(0).getRequestedAmount());
        assertNotNull(result.getOperations().get(0).getRequestedAmount().getCurrency());
        assertNotNull(result.getOperations().get(0).getRequestedAmount().getAmount());
        assertNotNull(result.getOperations().get(0).getBank());
        assertNotNull(result.getOperations().get(0).getBank().getId());
        assertNotNull(result.getOperations().get(0).getBank().getBranch());
        assertNotNull(result.getOperations().get(0).getBank().getBranch().getId());
        assertNotNull(result.getOperations().get(0).getRelatedContract());
        assertNotNull(result.getOperations().get(0).getRelatedContract().getId());
        assertNotNull(result.getOperations().get(0).getRelatedContract().getRequestedAmount());
        assertNotNull(result.getOperations().get(0).getRelatedContract().getRequestedAmount().getAmount());
        assertNotNull(result.getOperations().get(0).getRelatedContract().getRequestedAmount().getCurrency());
        assertNotNull(result.getOperations().get(0).getClaimReason());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getContractType());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getId());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getNumber());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getNumberType());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getNumberType().getId());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0));
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getId());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getOperationDate());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getConcept());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getTransactionType());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getId());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getName());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getCardAgreement());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getId());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getSubProduct());
        assertNotNull(result.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getSubProduct().getId());
        assertNull(result.getOperations().get(0).getClaimReason().getContract().getAccountType());
        assertNull(result.getOperations().get(0).getClaimReason().getContract().getLoanType());

        //No MPTAR
        assertNotNull(result.getOperations().get(1).getSubproductRefund());
        assertNotNull(result.getOperations().get(1).getSubproductRefund().getId());
        assertNotNull(result.getOperations().get(1).getSubproductRefund().getNumber());
        assertNull(result.getOperations().get(1).getSubproductRefund().getRefundMode());
        assertNotNull(result.getOperations().get(1).getDetail());
        assertNotNull(result.getOperations().get(1).getOperationType());
        assertNotNull(result.getOperations().get(1).getRequestedAmount());
        assertNotNull(result.getOperations().get(1).getRequestedAmount().getCurrency());
        assertNotNull(result.getOperations().get(1).getRequestedAmount().getAmount());
        assertNotNull(result.getOperations().get(1).getBank());
        assertNotNull(result.getOperations().get(1).getBank().getId());
        assertNotNull(result.getOperations().get(1).getBank().getBranch());
        assertNotNull(result.getOperations().get(1).getBank().getBranch().getId());
        assertNotNull(result.getOperations().get(1).getRelatedContract());
        assertNotNull(result.getOperations().get(1).getRelatedContract().getRequestedAmount());
        assertNotNull(result.getOperations().get(1).getRelatedContract().getRequestedAmount().getAmount());
        assertNotNull(result.getOperations().get(1).getRelatedContract().getRequestedAmount().getCurrency());
        assertNotNull(result.getOperations().get(1).getClaimReason());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract());

        //ACCOUNT
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getContractType());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getId());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getNumber());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getNumberType());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getNumberType().getId());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0));
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getId());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getOperationDate());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getConcept());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getTransactionType());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getId());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getName());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getAccountType());
        assertNotNull(result.getOperations().get(1).getClaimReason().getContract().getAccountType().getId());
        assertNull(result.getOperations().get(1).getClaimReason().getContract().getLoanType());
        assertNull(result.getOperations().get(1).getClaimReason().getContract().getCardAgreement());

        //LOAN
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getContractType());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getId());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getNumber());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getNumberType());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getNumberType().getId());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0));
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getId());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getOperationDate());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getConcept());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getTransactionType());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getId());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getName());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getLoanType());
        assertNotNull(result.getOperations().get(2).getClaimReason().getContract().getLoanType().getId());
        assertNull(result.getOperations().get(2).getClaimReason().getContract().getAccountType());
        assertNull(result.getOperations().get(2).getClaimReason().getContract().getCardAgreement());


        assertEquals(EntityMock.APP, result.getApp());
        assertEquals(input.getPetitioner().getId(), result.getPetitioner().getId());
        assertEquals(input.getPetitioner().getIdentityDocument().getDocumentType().getId(), result.getPetitioner().getPetitioner().getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getPetitioner().getIdentityDocument().getDocumentNumber(), result.getPetitioner().getPetitioner().getIdentityDocument().getDocumentNumber());
        assertEquals(input.getPetitioner().getSegment().getId(), result.getPetitioner().getPetitioner().getSegment().getId());

        //LANDLINE
        assertEquals("PHONE", result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getContactDetailType());
        assertEquals(input.getPetitioner().getContactDetails().get(0).getContact().getNumber(), result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getNumber());
        assertEquals(CLAIMS_PETITIONER_CONTACT_PHONETYPE_BACKEND_PHONE_NUMBER_VALUE, result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getPhoneType());
        assertEquals(input.getPetitioner().getContactDetails().get(0).getContact().getCountry().getId(), result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getCountry().getId());
        assertEquals(input.getPetitioner().getContactDetails().get(0).getContact().getCountryCode(), result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getCountryCode());
        assertEquals(input.getPetitioner().getContactDetails().get(0).getContact().getRegionalCode(), result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getRegionalCode());
        assertEquals(input.getPetitioner().getContactDetails().get(0).getContact().getExtension(), result.getPetitioner().getPetitioner().getContactDetails().get(0).getContacto().getExtension());

        //MOBILE
        assertEquals("MOBILE", result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getContactDetailType());
        assertEquals(input.getPetitioner().getContactDetails().get(1).getContact().getNumber(), result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getNumber());
        assertEquals(input.getPetitioner().getContactDetails().get(1).getContact().getPhoneCompany().getId(), result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getPhoneCompany().getId());
        assertEquals(input.getPetitioner().getContactDetails().get(1).getContact().getCountryCode(), result.getPetitioner().getPetitioner().getContactDetails().get(1).getContacto().getCountryCode());

        //EMAIL
        assertEquals("EMAIL", result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getContactDetailType());
        assertEquals(input.getPetitioner().getContactDetails().get(2).getContact().getAddress(), result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getAddress());
        assertEquals(input.getPetitioner().getContactDetails().get(2).getContact().getReceivesNotifications(), result.getPetitioner().getPetitioner().getContactDetails().get(2).getContacto().getReceivesNotifications());

        assertEquals(input.getRelatedClaim().getId(), result.getRelatedClaim().getId());
        assertEquals(input.getReason().getId(), result.getReason().getId());

        assertEquals(input.getOperations().size(), result.getOperations().size());
        //MPTAR
        assertEquals(input.getOperations().get(0).getSubproductRefund().getId(), result.getOperations().get(0).getSubproductRefund().getId());
        assertEquals(input.getOperations().get(0).getSubproductRefund().getNumber(), result.getOperations().get(0).getSubproductRefund().getNumber());
        assertEquals(CASHING_REFUND_MODE_BACKEND_VALUE, result.getOperations().get(0).getSubproductRefund().getRefundMode());
        assertEquals(input.getOperations().get(0).getDetail(), result.getOperations().get(0).getDetail());
        assertEquals(INTEREST_OPERATION_TYPE_BACKEND_VALUE, result.getOperations().get(0).getOperationType());
        assertEquals(input.getOperations().get(0).getRequestedAmount().getCurrency(), result.getOperations().get(0).getRequestedAmount().getCurrency());
        assertEquals(input.getOperations().get(0).getRequestedAmount().getAmount(), result.getOperations().get(0).getRequestedAmount().getAmount());
        assertEquals(input.getOperations().get(0).getBank().getId(), result.getOperations().get(0).getBank().getId());
        assertEquals(input.getOperations().get(0).getBank().getBranch().getId(), result.getOperations().get(0).getBank().getBranch().getId());
        assertEquals(input.getOperations().get(0).getRelatedContract().getId(), result.getOperations().get(0).getRelatedContract().getId());
        assertEquals(input.getOperations().get(0).getRelatedContract().getRequestedAmount().getCurrency(), result.getOperations().get(0).getRelatedContract().getRequestedAmount().getCurrency());
        assertEquals(input.getOperations().get(0).getRelatedContract().getRequestedAmount().getAmount(), result.getOperations().get(0).getRelatedContract().getRequestedAmount().getAmount());
        //CARD
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_TARJE_VALUE, result.getOperations().get(0).getClaimReason().getContract().getContractType());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getId(), result.getOperations().get(0).getClaimReason().getContract().getId());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getNumber(), result.getOperations().get(0).getClaimReason().getContract().getNumber());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_PAN_VALUE, result.getOperations().get(0).getClaimReason().getContract().getNumberType().getId());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getId(), result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getId());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getOperationDate(), result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getOperationDate());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getConcept(), result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getConcept());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_BACKEND_PURCHASE_VALUE, result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getTransactionType().getId());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount(), result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency(), result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getId(), result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getId());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getName(), result.getOperations().get(0).getClaimReason().getContract().getTransactions().get(0).getStore().getName());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getId(), result.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getId());
        assertEquals(input.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getSubProduct().getId(), result.getOperations().get(0).getClaimReason().getContract().getCardAgreement().getSubProduct().getId());

        //No MPTAR
        assertEquals(input.getOperations().get(1).getSubproductRefund().getId(), result.getOperations().get(1).getSubproductRefund().getId());
        assertEquals(input.getOperations().get(1).getSubproductRefund().getNumber(), result.getOperations().get(1).getSubproductRefund().getNumber());
        assertEquals(input.getOperations().get(1).getDetail(), result.getOperations().get(1).getDetail());
        assertEquals(FEE_OPERATION_TYPE_BACKEND_VALUE, result.getOperations().get(1).getOperationType());
        assertEquals(input.getOperations().get(1).getRequestedAmount().getCurrency(), result.getOperations().get(1).getRequestedAmount().getCurrency());
        assertEquals(input.getOperations().get(1).getRequestedAmount().getAmount(), result.getOperations().get(1).getRequestedAmount().getAmount());
        assertEquals(input.getOperations().get(1).getBank().getId(), result.getOperations().get(1).getBank().getId());
        assertEquals(input.getOperations().get(1).getBank().getBranch().getId(), result.getOperations().get(1).getBank().getBranch().getId());
        assertEquals(input.getOperations().get(1).getRelatedContract().getId(), result.getOperations().get(1).getRelatedContract().getId());
        assertEquals(input.getOperations().get(1).getRelatedContract().getRequestedAmount().getCurrency(), result.getOperations().get(1).getRelatedContract().getRequestedAmount().getCurrency());
        assertEquals(input.getOperations().get(1).getRelatedContract().getRequestedAmount().getAmount(), result.getOperations().get(1).getRelatedContract().getRequestedAmount().getAmount());

        //ACCOUNT
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_CUENT_VALUE, result.getOperations().get(1).getClaimReason().getContract().getContractType());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getId(), result.getOperations().get(1).getClaimReason().getContract().getId());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getNumber(), result.getOperations().get(1).getClaimReason().getContract().getNumber());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_LIC_VALUE, result.getOperations().get(1).getClaimReason().getContract().getNumberType().getId());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getId(), result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getId());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getOperationDate(), result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getOperationDate());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getConcept(), result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getConcept());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_BACKEND_PURCHASE_VALUE, result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getTransactionType().getId());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount(), result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency(), result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getId(), result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getId());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getName(), result.getOperations().get(1).getClaimReason().getContract().getTransactions().get(0).getStore().getName());
        assertEquals(input.getOperations().get(1).getClaimReason().getContract().getAccountType().getId(), result.getOperations().get(1).getClaimReason().getContract().getAccountType().getId());

        //LOAN
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_PREST_VALUE, result.getOperations().get(2).getClaimReason().getContract().getContractType());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getId(), result.getOperations().get(2).getClaimReason().getContract().getId());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getNumber(), result.getOperations().get(2).getClaimReason().getContract().getNumber());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_PAN_VALUE, result.getOperations().get(2).getClaimReason().getContract().getNumberType().getId());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getId(), result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getId());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getOperationDate(), result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getOperationDate());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getConcept(), result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getConcept());
        assertEquals(CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_BACKEND_PURCHASE_VALUE, result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getTransactionType().getId());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount(), result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getAmount());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency(), result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getLocalAmount().getCurrency());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getId(), result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getId());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getName(), result.getOperations().get(2).getClaimReason().getContract().getTransactions().get(0).getStore().getName());
        assertEquals(input.getOperations().get(2).getClaimReason().getContract().getLoanType().getId(), result.getOperations().get(2).getClaimReason().getContract().getLoanType().getId());
    }

    @Test
    public void mapInEmptyTest() {
        DTOIntAutomaticRefundTransaction result = mapper
                .mapIn(new AutomaticRefundTransaction());
        assertNotNull(result);
        assertNotNull(result.getApp());
        assertNull(result.getPetitioner());
        assertNull(result.getReason());
        assertNull(result.getOperations());
    }

    @Test
    public void mapOutFullTest() {
        final ServiceResponse<AutomaticRefundTransaction> result = mapper.mapOut(new AutomaticRefundTransaction());
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        final ServiceResponse<AutomaticRefundTransaction> result = mapper.mapOut(null);
        assertNull(result);
    }
}
