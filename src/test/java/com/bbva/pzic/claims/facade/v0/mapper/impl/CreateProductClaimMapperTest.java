package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.DTOIntEmail;
import com.bbva.pzic.claims.business.dto.DTOIntMobile;
import com.bbva.pzic.claims.business.dto.DTOIntProductClaim;
import com.bbva.pzic.claims.business.dto.DTOIntUnknownPetitioner;
import com.bbva.pzic.claims.canonic.BaseContract;
import com.bbva.pzic.claims.canonic.Petitioner;
import com.bbva.pzic.claims.canonic.ProductClaim;
import com.bbva.pzic.claims.canonic.ProductClaimTransaction;
import com.bbva.pzic.claims.facade.v0.dto.ProductClaimTransactionData;
import com.bbva.pzic.claims.util.mappers.ObjectMapperHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.activation.DataSource;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.claims.EntityMock.ASTA_MX_USER_ID_STUB;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 08/11/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class CreateProductClaimMapperTest {

    @InjectMocks
    private CreateProductClaimMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    private EntityMock entityMock = EntityMock.getInstance();

    @Test
    public void mapStringToProductClaimFullTest() throws IOException {
        ProductClaim result = mapper.mapStringToProductClaim(ObjectMapperHelper.getInstance().writeValueAsString(
                entityMock.getDTOIntProductClaimKnownTransaction()));

        assertNotNull(result);
    }

    @Test(expected = BusinessServiceException.class)
    public void mapStringToProductClaimWrongFormatTest() {
        mapper.mapStringToProductClaim("{wrongformat");
    }

    @Test
    public void mapInFullTest() throws IOException, URISyntaxException {
        ProductClaim input = entityMock.getDTOIntProductClaimKnownTransaction();

        DTOIntProductClaim base64String = entityMock.getFileDataSourceEncodedBase64String();
        List<DataSource> attachments = entityMock.buildAttachments();
        DTOIntProductClaim result = mapper.mapIn(input, attachments);

        assertNotNull(result);
        assertNotNull(result.getFiles());
        assertEquals(1, result.getFiles().size());
        assertNotNull(result.getPersonType());
        assertNotNull(result.getPersonType().getId());
        assertNotNull(result.getCompany());
        assertNotNull(result.getCompany().getId());
        assertNotNull(result.getIsUnderage());
        assertNotNull(result.getClaimType());
        assertNotNull(result.getClaimType().getId());
        assertNotNull(result.getProduct());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getReason());
        assertNotNull(result.getReason().getId());
        assertNotNull(result.getReason().getComments());
        assertNotNull(result.getReason().getSubReason());
        assertNotNull(result.getReason().getSubReason().getId());
        assertNotNull(result.getContract());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getContract().getNumber());
        assertNotNull(result.getContract().getNumberType());
        assertNotNull(result.getContract().getNumberType().getId());
        assertNotNull(result.getClaimAmount());
        assertNotNull(result.getClaimAmount().getAmount());
        assertNotNull(result.getClaimAmount().getCurrency());
        assertNotNull(result.getAtm());
        assertNotNull(result.getAtm().getId());
        assertNotNull(result.getBank());
        assertNotNull(result.getBank().getId());
        assertNotNull(result.getBank().getBranch());
        assertNotNull(result.getBank().getBranch().getId());
        assertNotNull(result.getChannel());
        assertNotNull(result.getChannel().getId());
        assertNotNull(result.getGeolocation());
        assertNotNull(result.getGeolocation().getCode());
        assertNotNull(result.getDelivery());
        assertNotNull(result.getDelivery().getDeliveryType());
        assertNotNull(result.getDelivery().getDeliveryType().getId());
        assertNotNull(result.getPetition());
        assertNotNull(result.getPetition().getDescription());
        assertNotNull(result.getPriority());
        assertNotNull(result.getPriority().getIsUrgent());
        assertNotNull(result.getPriority().getBasis());
        assertNotNull(result.getEnrollmentDate());
        assertNotNull(result.getPredecessorClaim());
        assertNotNull(result.getPredecessorClaim().getId());
        assertNotNull(result.getIsOnlineRefundFlow());
        assertNotNull(result.getPreviousAttentionCode());
        assertNotNull(result.getAdditionalClaims());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimType().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getProduct());
        assertNotNull(result.getAdditionalClaims().get(0).getProduct().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getReason());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason());
        assertNotNull(result.getAdditionalClaims().get(0).getReason().getSubReason().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumber());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumberType());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getNumberType().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getProduct());
        assertNotNull(result.getAdditionalClaims().get(0).getContract().getProduct().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount().getAmount());
        assertNotNull(result.getAdditionalClaims().get(0).getClaimAmount().getCurrency());
        assertNotNull(result.getAdditionalClaims().get(0).getAtm());
        assertNotNull(result.getAdditionalClaims().get(0).getAtm().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch());
        assertNotNull(result.getAdditionalClaims().get(0).getBank().getBranch().getId());
        assertNotNull(result.getAdditionalClaims().get(0).getIssueDate());
        assertNotNull(result.getIssueDate());

        assertNotNull(result.getResolution());
        assertNotNull(result.getResolution().getDelivery());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType());
        assertNotNull(result.getResolution().getDelivery().getDeliveryType().getId());
        assertNotNull(result.getPetitioners());
        assertEquals(2, result.getPetitioners().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getPetitionerType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getId());

        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getIdentityDocument());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getIdentityDocument().getDocumentType());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getIdentityDocument().getDocumentType().getId());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getIdentityDocument().getDocumentNumber());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getFirstName());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getLastName());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getSecondLastName());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getSegment().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents());

        assertEquals(3, result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().size());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());

        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(1));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getCode());

        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(2));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getComponentTypes());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getCode());

        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(((DTOIntEmail) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact()).getAddress());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact()).getNumber());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact()).getPhoneCompany());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact()).getPhoneCompany().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2));
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getId());
        assertNotNull(result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getContactDetailType());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact()).getNumber());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact()).getPhoneCompany());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact()).getPhoneCompany().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getBankRelationType());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getPetitionerType());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getPetitionerType().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getFormattedAddress());

        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());

        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(1));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getCode());

        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(2));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getComponentTypes());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getCode());

        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(3));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(3).getComponentTypes());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(3).getComponentTypes().get(0));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(3).getCode());

        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact()).getNumber());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact()).getPhoneCompany());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact()).getPhoneCompany().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1));
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact()).getNumber());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact()).getPhoneCompany());
        assertNotNull(((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact()).getPhoneCompany().getId());

        assertEquals(base64String.getFiles().get(0).getContent(), result.getFiles().get(0).getContent());
        assertEquals(base64String.getFiles().get(0).getName(), result.getFiles().get(0).getName());
        assertEquals(input.getPersonType().getId(), result.getPersonType().getId());
        assertEquals(input.getCompany().getId(), result.getCompany().getId());
        assertEquals("0", result.getIsUnderage());
        assertEquals(input.getClaimType().getId(), result.getClaimType().getId());
        assertEquals(input.getProduct().getId(), result.getProduct().getId());
        assertEquals(input.getReason().getId(), result.getReason().getId());
        assertEquals(input.getReason().getComments(), result.getReason().getComments());
        assertEquals(input.getReason().getSubReason().getId(), result.getReason().getSubReason().getId());
        assertEquals(input.getResolution().getDelivery().getDeliveryType().getId(), result.getResolution().getDelivery().getDeliveryType().getId());
        assertEquals(input.getContract().getId(), result.getContract().getId());
        assertEquals(input.getContract().getNumber(), result.getContract().getNumber());
        assertEquals(input.getContract().getNumberType().getId(), result.getContract().getNumberType().getId());
        assertEquals(input.getContract().getProduct().getId(), result.getContract().getProduct().getId());
        assertEquals(input.getClaimAmount().getAmount(), result.getClaimAmount().getAmount());
        assertEquals(input.getClaimAmount().getCurrency(), result.getClaimAmount().getCurrency());
        assertEquals(input.getAtm().getId(), result.getAtm().getId());
        assertEquals(input.getBank().getId(), result.getBank().getId());
        assertEquals(input.getBank().getBranch().getId(), result.getBank().getBranch().getId());
        assertEquals(input.getChannel().getId(), result.getChannel().getId());
        assertEquals(input.getGeolocation().getCode(), result.getGeolocation().getCode());
        assertEquals(input.getDelivery().getDeliveryType().getId(), result.getDelivery().getDeliveryType().getId());
        assertEquals(input.getPetition().getDescription(), result.getPetition().getDescription());
        assertEquals(input.getPriority().getIsUrgent(), result.getPriority().getIsUrgent());
        assertEquals(input.getPriority().getBasis(), result.getPriority().getBasis());
        assertEquals(input.getPredecessorClaim().getId(), result.getPredecessorClaim().getId());
        assertEquals(input.getOnlineRefundFlow(), result.getIsOnlineRefundFlow());
        assertEquals(input.getPreviousAttentionCode(), result.getPreviousAttentionCode());
        assertEquals(input.getAdditionalClaims().get(0).getClaimType().getId(), result.getAdditionalClaims().get(0).getClaimType().getId());
        assertEquals(input.getAdditionalClaims().get(0).getProduct().getId(), result.getAdditionalClaims().get(0).getProduct().getId());
        assertEquals(input.getAdditionalClaims().get(0).getReason().getId(), result.getAdditionalClaims().get(0).getReason().getId());
        assertEquals(input.getAdditionalClaims().get(0).getReason().getSubReason().getId(), result.getAdditionalClaims().get(0).getReason().getSubReason().getId());
        assertEquals(input.getAdditionalClaims().get(0).getContract().getId(), result.getAdditionalClaims().get(0).getContract().getId());
        assertEquals(input.getAdditionalClaims().get(0).getContract().getNumber(), result.getAdditionalClaims().get(0).getContract().getNumber());
        assertEquals(input.getAdditionalClaims().get(0).getContract().getNumberType().getId(), result.getAdditionalClaims().get(0).getContract().getNumberType().getId());
        assertEquals(input.getAdditionalClaims().get(0).getContract().getProduct().getId(), result.getAdditionalClaims().get(0).getContract().getProduct().getId());
        assertEquals(input.getAdditionalClaims().get(0).getClaimAmount().getAmount(), result.getAdditionalClaims().get(0).getClaimAmount().getAmount());
        assertEquals(input.getAdditionalClaims().get(0).getClaimAmount().getCurrency(), result.getAdditionalClaims().get(0).getClaimAmount().getCurrency());
        assertEquals(input.getAdditionalClaims().get(0).getAtm().getId(), result.getAdditionalClaims().get(0).getAtm().getId());
        assertEquals(input.getAdditionalClaims().get(0).getBank().getId(), result.getAdditionalClaims().get(0).getBank().getId());
        assertEquals(input.getAdditionalClaims().get(0).getBank().getBranch().getId(), result.getAdditionalClaims().get(0).getBank().getBranch().getId());

        //PETITIONER TYPE - UNKNOWN
        Petitioner inputUnknownPetitioner = input.getPetitioners().get(0).getPetitioner();
        assertEquals(inputUnknownPetitioner.getBankRelationType(), result.getPetitioners().get(0).getPetitioner().getBankRelationType());
        assertEquals(inputUnknownPetitioner.getPetitionerType().getId(), result.getPetitioners().get(0).getPetitioner().getPetitionerType().getId());
        assertEquals(inputUnknownPetitioner.getIdentityDocument().getDocumentType().getId(), ((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getIdentityDocument().getDocumentType().getId());
        assertEquals(inputUnknownPetitioner.getIdentityDocument().getDocumentNumber(), ((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getIdentityDocument().getDocumentNumber());
        assertEquals(inputUnknownPetitioner.getFirstName(), ((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getFirstName());
        assertEquals(inputUnknownPetitioner.getLastName(), ((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getLastName());
        assertEquals(inputUnknownPetitioner.getSecondLastName(), ((DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner()).getSecondLastName());
        assertEquals(inputUnknownPetitioner.getSegment().getId(), (result.getPetitioners().get(0).getPetitioner()).getSegment().getId());
        assertEquals(inputUnknownPetitioner.getBank().getId(), result.getPetitioners().get(0).getPetitioner().getBank().getId());
        assertEquals(inputUnknownPetitioner.getBank().getBranch().getId(), result.getPetitioners().get(0).getPetitioner().getBank().getBranch().getId());
        assertEquals(inputUnknownPetitioner.getAddress().getId(), result.getPetitioners().get(0).getPetitioner().getAddress().getId());
        assertEquals(inputUnknownPetitioner.getAddress().getLocation().getFormattedAddress(), result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getFormattedAddress());

        assertEquals(inputUnknownPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0),
                result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(inputUnknownPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode(),
                result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());

        assertEquals(inputUnknownPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0),
                result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertEquals(inputUnknownPetitioner.getAddress().getLocation().getAddressComponents().get(1).getCode(),
                result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getCode());

        assertEquals(inputUnknownPetitioner.getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0),
                result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0));
        assertEquals(inputUnknownPetitioner.getAddress().getLocation().getAddressComponents().get(2).getCode(),
                result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getCode());

        // CONTACT DETAILS - UNKNOWN
        BaseContract inputEmail = inputUnknownPetitioner.getContactDetails().get(0).getContact();
        assertEquals(inputEmail.getContactDetailType(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType().getId());
        assertEquals(inputEmail.getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(inputEmail.getAddress(), ((DTOIntEmail) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(0).getContact()).getAddress());

        BaseContract inputMobile = inputUnknownPetitioner.getContactDetails().get(1).getContact();
        assertEquals(inputMobile.getContactDetailType(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType().getId());
        assertEquals(inputMobile.getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertEquals(inputMobile.getNumber(), ((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact()).getNumber());
        assertEquals(inputMobile.getPhoneCompany().getId(), ((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(1).getContact()).getPhoneCompany().getId());

        BaseContract inputLandline = inputUnknownPetitioner.getContactDetails().get(2).getContact();
        assertEquals(inputLandline.getContactDetailType(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getContactDetailType().getId());
        assertEquals(inputLandline.getId(), result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact().getId());
        assertEquals(inputLandline.getNumber(), ((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact()).getNumber());
        assertEquals(inputLandline.getPhoneCompany().getId(), ((DTOIntMobile) result.getPetitioners().get(0).getPetitioner().getContactDetails().get(2).getContact()).getPhoneCompany().getId());

        //PETITIONER TYPE - KNOWN
        Petitioner inputKnownPetitioner = input.getPetitioners().get(1).getPetitioner();
        assertEquals(inputKnownPetitioner.getBankRelationType(), result.getPetitioners().get(1).getPetitioner().getBankRelationType());
        assertEquals(inputKnownPetitioner.getPetitionerType().getId(), result.getPetitioners().get(1).getPetitioner().getPetitionerType().getId());
        assertEquals(inputKnownPetitioner.getId(), result.getPetitioners().get(1).getPetitioner().getId());
        assertEquals(inputKnownPetitioner.getAddress().getId(), result.getPetitioners().get(1).getPetitioner().getAddress().getId());
        assertEquals(inputKnownPetitioner.getAddress().getLocation().getFormattedAddress(), result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getFormattedAddress());
        assertEquals(inputKnownPetitioner.getSegment().getId(), result.getPetitioners().get(1).getPetitioner().getSegment().getId());

        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode(),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(0).getCode());

        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(1).getCode(),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).getCode());

        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getComponentTypes().get(0));
        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(2).getCode(),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(2).getCode());

        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(3).getComponentTypes().get(0),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(3).getComponentTypes().get(0));
        assertEquals(inputKnownPetitioner.getAddress().getLocation().getAddressComponents().get(3).getCode(),
                result.getPetitioners().get(1).getPetitioner().getAddress().getLocation().getAddressComponents().get(3).getCode());

        // CONTACT DETAILS - KNOWN
        BaseContract inputKnownMobile = inputKnownPetitioner.getContactDetails().get(0).getContact();
        assertEquals(inputKnownMobile.getContactDetailType(), result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact().getContactDetailType().getId());
        assertEquals(inputKnownMobile.getId(), result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact().getId());
        assertEquals(inputKnownMobile.getNumber(), ((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact()).getNumber());
        assertEquals(inputKnownMobile.getPhoneCompany().getId(), ((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(0).getContact()).getPhoneCompany().getId());

        BaseContract inputKnownMobile2 = inputKnownPetitioner.getContactDetails().get(1).getContact();
        assertEquals(inputKnownMobile2.getContactDetailType(), result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact().getContactDetailType().getId());
        assertEquals(inputKnownMobile2.getId(), result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact().getId());
        assertEquals(inputKnownMobile2.getNumber(), ((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact()).getNumber());
        assertEquals(inputKnownMobile2.getPhoneCompany().getId(), ((DTOIntMobile) result.getPetitioners().get(1).getPetitioner().getContactDetails().get(1).getContact()).getPhoneCompany().getId());
    }

    @Test
    public void mapInFullPetitionerBankRelationTypeKnownPetitionerTypeIdHolderSessionedChannelTest() throws IOException, URISyntaxException {
        when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(ASTA_MX_USER_ID_STUB);
        ProductClaim input = entityMock.getDTOIntProductClaimKnownTransaction();
        input.getPetitioners().get(1).getPetitioner().getPetitionerType().setId("HOLDER");

        List<DataSource> attachments = entityMock.buildAttachments();
        DTOIntProductClaim result = mapper.mapIn(input, attachments);

        assertNotNull(result);
        assertEquals(2, result.getPetitioners().size());
        assertNotNull(result.getPetitioners().get(1).getPetitioner().getId());

        assertEquals(ASTA_MX_USER_ID_STUB, result.getPetitioners().get(1).getPetitioner().getId());
    }

    @Test
    public void mapInFullKnownAddressComponentTypeTwoDepartmentTest() throws IOException, URISyntaxException {
        ProductClaim input = entityMock.getDTOIntProductClaimKnownTransaction();
        input.getPetitioners().get(0).getPetitioner().getPetitionerType().setId("AUTHORIZED");

        // AddressComponents [DISTRICT, DEPARTMENT, DEPARTMENT]
        input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().get(1).setComponentTypes(
                Collections.singletonList("DEPARTMENT"));

        List<DataSource> attachments = entityMock.buildAttachments();
        DTOIntProductClaim result = mapper.mapIn(input, attachments);

        assertNotNull(result);
        assertNotNull(result.getPetitioners());
        assertEquals(2, result.getPetitioners().size());
        DTOIntUnknownPetitioner resultUnknowPetitioner = (DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner();
        assertNotNull(resultUnknowPetitioner);

        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents());
        assertEquals(2, resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().size());
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode());

        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1));
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes());
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getCode());

        Petitioner inputUnknowPetitioner = input.getPetitioners().get(0).getPetitioner();
        assertEquals(inputUnknowPetitioner.getAddress().getId(), resultUnknowPetitioner.getAddress().getId());
        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getFormattedAddress(),
                resultUnknowPetitioner.getAddress().getLocation().getFormattedAddress());

        assertEquals(3, input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().size());
        assertEquals(2, result.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().size());

        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode(),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode());

        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getCode(),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getCode());
    }

    @Test
    public void mapInFullKnownAddressComponentTypeOnlyDistrictProvinceTest() throws IOException, URISyntaxException {
        ProductClaim input = entityMock.getDTOIntProductClaimKnownTransaction();
        input.getPetitioners().get(0).getPetitioner().getPetitionerType().setId("AUTHORIZED");

        // Remove 1 element of AddressComponents (Department), only 2 elements: AddressComponents[DISTRICT, PROVINCE]
        input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().remove(2);

        List<DataSource> attachments = entityMock.buildAttachments();
        DTOIntProductClaim result = mapper.mapIn(input, attachments);

        assertNotNull(result);
        assertNotNull(result.getPetitioners());
        assertEquals(2, result.getPetitioners().size());
        DTOIntUnknownPetitioner resultUnknowPetitioner = (DTOIntUnknownPetitioner) result.getPetitioners().get(0).getPetitioner();
        assertNotNull(resultUnknowPetitioner);
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents());

        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0));
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes());
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertNotNull(resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode());

        Petitioner inputUnknowPetitioner = input.getPetitioners().get(0).getPetitioner();
        assertEquals(inputUnknowPetitioner.getAddress().getId(), resultUnknowPetitioner.getAddress().getId());
        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getFormattedAddress(),
                resultUnknowPetitioner.getAddress().getLocation().getFormattedAddress());

        assertEquals(2, input.getPetitioners().get(0).getPetitioner().getAddress().getLocation().getAddressComponents().size());

        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getComponentTypes().get(0));
        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode(),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(0).getCode());

        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getComponentTypes().get(0));
        assertEquals(inputUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getCode(),
                resultUnknowPetitioner.getAddress().getLocation().getAddressComponents().get(1).getCode());
    }

    @Test
    public void mapInWithEmptyDataTest() {
        DTOIntProductClaim result = mapper.mapIn(new ProductClaim(), Collections.emptyList());

        assertNotNull(result);
        assertNull(result.getFiles());
        assertNull(result.getPersonType());
        assertNull(result.getCompany());
        assertNull(result.getIsUnderage());
        assertNull(result.getClaimType());
        assertNull(result.getProduct());
        assertNull(result.getReason());
        assertNull(result.getResolution());
        assertNull(result.getPetitioners());
        assertNull(result.getContract());
        assertNull(result.getClaimAmount());
        assertNull(result.getAtm());
        assertNull(result.getBank());
        assertNull(result.getChannel());
        assertNull(result.getGeolocation());
        assertNull(result.getDelivery());
        assertNull(result.getPetition());
        assertNull(result.getPriority());
        assertNull(result.getEnrollmentDate());
        assertNull(result.getPredecessorClaim());
        assertNull(result.getIsOnlineRefundFlow());
        assertNull(result.getPreviousAttentionCode());
        assertNull(result.getAdditionalClaims());
    }

    @Test
    public void mapOutFullTest() {
        ProductClaimTransactionData response = mapper.mapOut(new ProductClaimTransaction());

        assertNotNull(response);
        assertNotNull(response.getData());
    }

    @Test
    public void mapOutNullTest() {
        ProductClaimTransactionData response = mapper.mapOut(null);

        assertNull(response);
    }
}
