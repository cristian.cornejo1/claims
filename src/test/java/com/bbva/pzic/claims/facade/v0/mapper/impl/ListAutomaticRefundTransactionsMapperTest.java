package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.business.dto.InputListAutomaticRefundTransactions;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.bbva.pzic.claims.EntityMock.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 16/09/2018.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class ListAutomaticRefundTransactionsMapperTest {

    @InjectMocks
    private ListAutomaticRefundTransactionsMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp(){
    }

    @Test
    public void mapInFullTest() {

        when(serviceInvocationContext.getProperty(BackendContext.AAP)).thenReturn(APP);

        InputListAutomaticRefundTransactions result = mapper.mapIn(CUSTOMER_ID);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());

        assertEquals(APP, result.getApp());
        assertEquals(CUSTOMER_ID, result.getCustomerId());
    }

    @Test
    public void mapInWithoutCustomerIdTest() {

        when(serviceInvocationContext.getProperty(BackendContext.AAP)).thenReturn(APP);
        when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_USER_ID)).thenReturn(ASTA_MX_USER_ID_STUB);

        InputListAutomaticRefundTransactions result = mapper.mapIn(null);

        assertNotNull(result);
        assertNotNull(result.getCustomerId());

        assertEquals(APP, result.getApp());
        assertEquals(ASTA_MX_USER_ID_STUB, result.getCustomerId());
    }

    @Test
    public void mapInEmptyTest() {

        InputListAutomaticRefundTransactions result = mapper.mapIn(null);

        assertNotNull(result);

        assertNull(result.getApp());
        assertNull(result.getCustomerId());
    }

    @Test
    public void mapOutFullTest() {

        ServiceResponse<List<AutomaticRefundTransaction>> result =
                mapper.mapOut(Collections.singletonList(new AutomaticRefundTransaction()));

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {

        ServiceResponse<List<AutomaticRefundTransaction>> result = mapper.mapOut(Collections.emptyList());

        assertNull(result);
    }
}