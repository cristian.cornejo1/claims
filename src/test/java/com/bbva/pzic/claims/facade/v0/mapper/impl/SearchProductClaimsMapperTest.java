package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.DTOIntUnknownPetitioner;
import com.bbva.pzic.claims.business.dto.InputSearchProductClaims;
import com.bbva.pzic.claims.facade.v0.dto.ListProductClaimsData;
import com.bbva.pzic.claims.facade.v0.dto.Search;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;

import static com.bbva.pzic.claims.EntityMock.*;
import static com.bbva.pzic.claims.util.Enums.ENUM_CLAIMS_IDENTITYDOCUMENT_DOCUMENTYPE_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 11/26/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchProductClaimsMapperTest {

    @InjectMocks
    private SearchProductClaimsMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty(BackendContext.AAP)).thenReturn(AAP_VALUE);
        when(translator.translateFrontendEnumValue(ENUM_CLAIMS_IDENTITYDOCUMENT_DOCUMENTYPE_ID,
                CLAIMS_IDENTITYDOCUMENT_DOCUMENTTYPE_ID_DNI_FRONTEND)).thenReturn(CLAIMS_IDENTITYDOCUMENT_DOCUMENTTYPE_ID_DNI_BACKEND);
    }

    @Test
    public void mapInFullWithSessionChannelTest() throws IOException {
        when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(CUSTOMER_ID);
        Search input = EntityMock.getInstance().buildSearch();
        InputSearchProductClaims result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getAap());
        assertNotNull(result.getClientId());
        assertNull(result.getPetitioner());

        assertEquals(CUSTOMER_ID, result.getClientId());
        assertEquals(AAP_VALUE, result.getAap());
    }

    @Test
    public void mapInFullWithoutSessionedChannelTest() throws IOException {
        Search input = EntityMock.getInstance().buildSearch();
        InputSearchProductClaims result = mapper.mapIn(input);

        assertNotNull(result);
        assertNotNull(result.getAap());
        assertNull(result.getClientId());
        assertNotNull(result.getPetitioner());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioner()).getIdentityDocument());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioner()).getIdentityDocument().getDocumentType());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioner()).getIdentityDocument().getDocumentType().getId());
        assertNotNull(((DTOIntUnknownPetitioner) result.getPetitioner()).getIdentityDocument().getDocumentNumber());

        assertEquals(AAP_VALUE, result.getAap());
        assertEquals(CLAIMS_IDENTITYDOCUMENT_DOCUMENTTYPE_ID_DNI_BACKEND, ((DTOIntUnknownPetitioner) result.getPetitioner()).getIdentityDocument().getDocumentType().getId());
        assertEquals(input.getPetitioner().getIdentityDocument().getDocumentNumber(), ((DTOIntUnknownPetitioner) result.getPetitioner()).getIdentityDocument().getDocumentNumber());
    }

    @Test
    public void mapInEmptyTest() {
        InputSearchProductClaims result = mapper.mapIn(new Search());

        assertNotNull(result);
        assertNotNull(result.getAap());
        assertNull(result.getClientId());
        assertNotNull(result.getPetitioner());
        assertNull(((DTOIntUnknownPetitioner) result.getPetitioner()).getIdentityDocument());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ListProductClaimsData productClaims = mapper.mapOut(Collections.singletonList(EntityMock.getInstance().buildProductClaims()));

        assertNotNull(productClaims);
        assertNotNull(productClaims.getData());
    }

    @Test
    public void mapOutNullTest() {
        ListProductClaimsData productClaims = mapper.mapOut(null);

        assertNull(productClaims);
    }

    @Test
    public void mapOutInitializedTest() {
        ListProductClaimsData productClaims = mapper.mapOut(Collections.emptyList());

        assertNull(productClaims);
    }
}
