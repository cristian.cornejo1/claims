package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputSendEmailProductClaim;
import com.bbva.pzic.claims.facade.v0.dto.SendEmail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static com.bbva.pzic.claims.EntityMock.AAP_VALUE;
import static com.bbva.pzic.claims.EntityMock.CUSTOMER_ID;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 6/12/2019.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class SendEmailProductClaimMapperTest {

    @InjectMocks
    private SendEmailProductClaimMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getProperty(BackendContext.AAP)).thenReturn(AAP_VALUE);
        when(serviceInvocationContext.getProperty(BackendContext.ASTA_MX_CLIENT_ID)).thenReturn(CUSTOMER_ID);
    }

    @Test
    public void mapInFullTest() throws IOException {
        SendEmail input = EntityMock.getInstance().buildSendEmail();
        InputSendEmailProductClaim result = mapper.mapIn(EntityMock.PRODUCT_CLAIM_ID, input);

        assertNotNull(result);
        assertNotNull(result.getProductClaimId());
        assertNotNull(result.getAap());
        assertNotNull(result.getClientId());
        assertNotNull(result.getContact());
        assertNotNull(result.getContact().getId());
        assertNotNull(result.getContact().getAddress());
        assertNotNull(result.getContact().getContactDetailType());
        assertNotNull(result.getDocuments());
        assertEquals(2, result.getDocuments().size());
        assertNotNull(result.getDocuments().get(0).getId());
        assertNotNull(result.getDocuments().get(0).getId());

        assertEquals(EntityMock.PRODUCT_CLAIM_ID, result.getProductClaimId());
        assertEquals(AAP_VALUE, result.getAap());
        assertEquals(CUSTOMER_ID, result.getClientId());
        assertEquals(input.getContact().getId(), result.getContact().getId());
        assertEquals(input.getContact().getAddress(), result.getContact().getAddress());
        assertEquals(input.getContact().getContactDetailType(), result.getContact().getContactDetailType());
        assertEquals(input.getDocuments().get(0).getId(), result.getDocuments().get(0).getId());
        assertEquals(input.getDocuments().get(1).getId(), result.getDocuments().get(1).getId());
    }

    @Test
    public void mapInEmptyTest() {
        InputSendEmailProductClaim result = mapper.mapIn(EntityMock.PRODUCT_CLAIM_ID, new SendEmail());

        assertNotNull(result);
        assertNotNull(result.getProductClaimId());
        assertNotNull(result.getAap());
        assertNotNull(result.getClientId());
        assertNull(result.getContact());
        assertNull(result.getDocuments());
    }
}