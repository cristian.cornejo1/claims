package com.bbva.pzic.claims.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.claims.EntityMock;
import com.bbva.pzic.claims.business.dto.InputModifyAutomaticRefundTransaction;
import com.bbva.pzic.claims.facade.v0.dto.AutomaticRefundTransactionF;
import com.bbva.pzic.claims.facade.v0.dto.PartialTransaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ModifyPartialAutomaticRefundTransactionMapperTest {

    @InjectMocks
    private ModifyPartialAutomaticRefundTransactionMapper mapper;

    @Mock
    private ServiceInvocationContext serviceInvocationContext;

    @Before
    public void setUp() {
        when(serviceInvocationContext.getUser()).thenReturn(EntityMock.USER);
        when(serviceInvocationContext.getProperty("aap")).thenReturn(EntityMock.APP);
    }


    @Test
    public void mapInFullTest() throws IOException {
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID,
                                                                EntityMock.getInstance().buildAutomaticRefundTransactionF());
        assertNotNull(result);
        assertNotNull(result.getAutomaticRefundTransactionId());
        assertNotNull(result.getUser());
        assertNotNull(result.getIdCanal());
        assertNotNull(result.getPartialTransaction());
        assertNotNull(result.getPartialTransaction().getCustomerConfirmation());
        assertNotNull(result.getPartialTransaction().getStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus().getId());
        assertNotNull(result.getPartialTransaction().getSituation());
        assertNotNull(result.getPartialTransaction().getSituation().getId());

        assertEquals(result.getAutomaticRefundTransactionId(),EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID);
        assertEquals(result.getUser(),EntityMock.USER);
        assertEquals(result.getIdCanal(),EntityMock.APP);
        assertEquals(result.getPartialTransaction().getCustomerConfirmation(),EntityMock.CUSTOMER_CONFIRMATION);
        assertEquals(result.getPartialTransaction().getStatus().getSubStatus().getId(),EntityMock.SUB_STATUS_ID);
        assertEquals(result.getPartialTransaction().getSituation().getId(),EntityMock.SITUATION_ID);
    }

    @Test
    public void mapInWithoutSituationTest() throws IOException {
        AutomaticRefundTransactionF automaticRefundTransactionF = EntityMock.getInstance().buildAutomaticRefundTransactionF();
        automaticRefundTransactionF.setSituation(null);
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID,
                automaticRefundTransactionF);
        assertNotNull(result);
        assertNotNull(result.getAutomaticRefundTransactionId());
        assertNotNull(result.getUser());
        assertNotNull(result.getIdCanal());
        assertNotNull(result.getPartialTransaction());
        assertNotNull(result.getPartialTransaction().getCustomerConfirmation());
        assertNotNull(result.getPartialTransaction().getStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus().getId());
        assertNull(result.getPartialTransaction().getSituation());

        assertEquals(result.getAutomaticRefundTransactionId(),EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID);
        assertEquals(result.getUser(),EntityMock.USER);
        assertEquals(result.getIdCanal(),EntityMock.APP);
        assertEquals(result.getPartialTransaction().getCustomerConfirmation(),EntityMock.CUSTOMER_CONFIRMATION);
        assertEquals(result.getPartialTransaction().getStatus().getSubStatus().getId(),EntityMock.SUB_STATUS_ID);
    }

    @Test
    public void mapInWithoutStatusTest() throws IOException {
        AutomaticRefundTransactionF automaticRefundTransactionF = EntityMock.getInstance().buildAutomaticRefundTransactionF();
        automaticRefundTransactionF.setStatus(null);
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID,
                                                                automaticRefundTransactionF);
        assertNotNull(result);
        assertNotNull(result.getAutomaticRefundTransactionId());
        assertNotNull(result.getUser());
        assertNotNull(result.getIdCanal());
        assertNotNull(result.getPartialTransaction());
        assertNotNull(result.getPartialTransaction().getCustomerConfirmation());
        assertNull(result.getPartialTransaction().getStatus());
        assertNotNull(result.getPartialTransaction().getSituation());
        assertNotNull(result.getPartialTransaction().getSituation().getId());

        assertEquals(result.getAutomaticRefundTransactionId(),EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID);
        assertEquals(result.getUser(),EntityMock.USER);
        assertEquals(result.getIdCanal(),EntityMock.APP);
        assertEquals(result.getPartialTransaction().getCustomerConfirmation(),EntityMock.CUSTOMER_CONFIRMATION);
        assertEquals(result.getPartialTransaction().getSituation().getId(),EntityMock.SITUATION_ID);
    }

    @Test
    public void mapInWithoutCustomerConfirmationTest() throws IOException {
        AutomaticRefundTransactionF automaticRefundTransactionF = EntityMock.getInstance().buildAutomaticRefundTransactionF();
        automaticRefundTransactionF.setCustomerConfirmation(null);
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID,
                                                                    automaticRefundTransactionF);
        assertNotNull(result);
        assertNotNull(result.getAutomaticRefundTransactionId());
        assertNotNull(result.getUser());
        assertNotNull(result.getIdCanal());
        assertNotNull(result.getPartialTransaction());
        assertNull(result.getPartialTransaction().getCustomerConfirmation());
        assertNotNull(result.getPartialTransaction().getStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus().getId());
        assertNotNull(result.getPartialTransaction().getSituation());
        assertNotNull(result.getPartialTransaction().getSituation().getId());

        assertEquals(result.getAutomaticRefundTransactionId(),EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID);
        assertEquals(result.getUser(),EntityMock.USER);
        assertEquals(result.getIdCanal(),EntityMock.APP);
        assertEquals(result.getPartialTransaction().getStatus().getSubStatus().getId(),EntityMock.SUB_STATUS_ID);
        assertEquals(result.getPartialTransaction().getSituation().getId(),EntityMock.SITUATION_ID);
    }

    @Test
    public void mapInWithoutAutomaticRefundTransactionIdTest() throws IOException {
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(null,
                EntityMock.getInstance().buildAutomaticRefundTransactionF());
        assertNotNull(result);
        assertNull(result.getAutomaticRefundTransactionId());
        assertNotNull(result.getUser());
        assertNotNull(result.getIdCanal());
        assertNotNull(result.getPartialTransaction());
        assertNotNull(result.getPartialTransaction().getCustomerConfirmation());
        assertNotNull(result.getPartialTransaction().getStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus().getId());
        assertNotNull(result.getPartialTransaction().getSituation());
        assertNotNull(result.getPartialTransaction().getSituation().getId());

        assertEquals(result.getUser(),EntityMock.USER);
        assertEquals(result.getIdCanal(),EntityMock.APP);
        assertEquals(result.getPartialTransaction().getCustomerConfirmation(),EntityMock.CUSTOMER_CONFIRMATION);
        assertEquals(result.getPartialTransaction().getStatus().getSubStatus().getId(),EntityMock.SUB_STATUS_ID);
        assertEquals(result.getPartialTransaction().getSituation().getId(),EntityMock.SITUATION_ID);
    }

    @Test
    public void mapInWithoutUserCanalTest() throws IOException {
        when(serviceInvocationContext.getUser()).thenReturn(null);
        when(serviceInvocationContext.getProperty("aap")).thenReturn(null);
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID,
                EntityMock.getInstance().buildAutomaticRefundTransactionF());
        assertNotNull(result);
        assertNotNull(result.getAutomaticRefundTransactionId());
        assertNull(result.getUser());
        assertNull(result.getIdCanal());
        assertNotNull(result.getPartialTransaction());
        assertNotNull(result.getPartialTransaction().getCustomerConfirmation());
        assertNotNull(result.getPartialTransaction().getStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus());
        assertNotNull(result.getPartialTransaction().getStatus().getSubStatus().getId());
        assertNotNull(result.getPartialTransaction().getSituation());
        assertNotNull(result.getPartialTransaction().getSituation().getId());

        assertEquals(result.getAutomaticRefundTransactionId(),EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID);
        assertEquals(result.getPartialTransaction().getCustomerConfirmation(),EntityMock.CUSTOMER_CONFIRMATION);
        assertEquals(result.getPartialTransaction().getStatus().getSubStatus().getId(),EntityMock.SUB_STATUS_ID);
        assertEquals(result.getPartialTransaction().getSituation().getId(),EntityMock.SITUATION_ID);
    }

    @Test
    public void mapInWithoutPartialTransactionTest() {
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID,null);
        assertNotNull(result);
        assertNotNull(result.getAutomaticRefundTransactionId());
        assertNotNull(result.getUser());
        assertNotNull(result.getIdCanal());
        assertNull(result.getPartialTransaction());

        assertEquals(result.getAutomaticRefundTransactionId(),EntityMock.AUTOMATIC_REFUND_TRANSACTION_ID);
        assertEquals(result.getUser(),EntityMock.USER);
        assertEquals(result.getIdCanal(),EntityMock.APP);
    }

    @Test
    public void mapInEmptyTest() {
        when(serviceInvocationContext.getUser()).thenReturn(null);
        when(serviceInvocationContext.getProperty("aap")).thenReturn(null);
        InputModifyAutomaticRefundTransaction result = mapper.mapIn(null, null);
        assertNotNull(result);
        assertNull(result.getAutomaticRefundTransactionId());
        assertNull(result.getUser());
        assertNull(result.getIdCanal());
        assertNull(result.getPartialTransaction());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<PartialTransaction> result = mapper.mapOut(new PartialTransaction());

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<PartialTransaction> result = mapper.mapOut(null);

        assertNull(result);
    }
}
