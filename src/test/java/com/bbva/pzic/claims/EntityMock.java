package com.bbva.pzic.claims;

import com.bbva.pzic.claims.business.dto.*;
import com.bbva.pzic.claims.canonic.AutomaticRefundTransaction;
import com.bbva.pzic.claims.canonic.PetitionerType;
import com.bbva.pzic.claims.canonic.ProductClaim;
import com.bbva.pzic.claims.dao.rest.model.conformidad.ResultadoConformidad;
import com.bbva.pzic.claims.dao.rest.model.harec.RequerimientoType;
import com.bbva.pzic.claims.dao.rest.model.harec.ResponseType;
import com.bbva.pzic.claims.dao.rest.model.reclamos.ModelResultadoResponse;
import com.bbva.pzic.claims.facade.v0.dto.*;
import com.bbva.pzic.claims.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.activation.DataSource;
import javax.activation.FileDataSource;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Entelgy
 */
public final class EntityMock {

    public static final String USER = "USER";
    public static final Boolean CUSTOMER_CONFIRMATION = true;
    public static final String SUB_STATUS_ID = "ID_PRUEBA";
    public static final String SITUATION_ID = "SITUATION_PRUEBA";
    public static final String APP = "app";
    public static final String ASTA_MX_USER_ID_STUB = "ASTA_MX_USER_ID";
    public final static String CUSTOMER_ID = "u";
    public final static String AUTOMATIC_REFUND_TRANSACTION_ID = "Z";
    public static final String APPROVED_STATUS_ID_BACKEND_VALUE = "A";
    public static final String APPROVED_STATUS_ID_ENUM_VALUE = "APPROVED";
    public static final String CASHING_REFUND_MODE_BACKEND_VALUE = "P";
    public static final String CASHING_REFUND_MODE_ENUM_VALUE = "CASHING";
    public static final String INTEREST_OPERATION_TYPE_BACKEND_VALUE = "INT";
    public static final String INTEREST_OPERATION_TYPE_ENUM_VALUE = "INTEREST";
    public static final String FEE_OPERATION_TYPE_BACKEND_VALUE = "COM";
    public static final String FEE_OPERATION_TYPE_ENUM_VALUE = "FEE";
    public static final String AAP_VALUE = "AAP";

    public static final String PERSON_TYPE_ID_LEGAL_ENUM_VALUE = "LEGAL_PERSON";
    public static final String PERSON_TYPE_ID_LEGAL_FRONTEND_VALUE = "JUR";

    public static final String CLAIM_TYPE_ID_PETITION_ENUM_VALUE = "PETITION";
    public static final String CLAIM_TYPE_ID_PETITION_FRONTEND_VALUE = "REQ";

    public static final String VERDICT_DELIVERY_TYPE_ID_EMAIL_ENUM_VALUE = "EMAIL";
    public static final String VERDICT_DELIVERY_TYPE_ID_EMAIL_FRONTEND = "EMA";

    public static final String CLAIMS_CONTACT_DETAIL_TYPE_MOBILE_ENUM_VALUE = "MOBILE";
    public static final String CLAIMS_CONTACT_DETAIL_TYPE_MOBILE_FRONTEND = "M";

    public static final String CLAIMS_CONTACT_DETAIL_TYPE_LANDLINE_ENUM_VALUE = "LANDLINE";
    public static final String CLAIMS_CONTACT_DETAIL_TYPE_LANDLINE_FRONTEND = "C";

    public static final String CLAIMS_IDENTITY_DOCUMENT_DOCUMENT_TYPE_ID_DNI_ENUM_VALUE = "RUC";
    public static final String CLAIMS_IDENTITYDOCUMENT_DOCUMENTTYPE_ID_DNI_FRONTEND = "DNI";
    public static final String CLAIMS_IDENTITY_DOCUMENT_DOCUMENT_TYPE_ID_DNI_FRONTEND = "R";
    public static final String CLAIMS_IDENTITYDOCUMENT_DOCUMENTTYPE_ID_DNI_BACKEND = "L";

    public static final String RESOLUTION_ID_PETITION_ENUM_VALUE = "NOT_APPLY";
    public static final String RESOLUTION_ID_PETITION_FRONTEND_VALUE = "NOPR";
    public static final String CLAIMS_RESOLUTION_ID_NOPR_FRONTEND = "NOT_APPLY";
    public static final String CLAIMS_RESOLUTION_ID_NOPR_BACKEND = "NOPR";

    public static final String CLAIMS_NUMBER_TYPE_ID_PAN_ENUM_VALUE = "PAN";
    public static final String CLAIMS_NUMBER_TYPE_ID_PAN_FRONTEND = "PAN";

    public static final String STATUS_ID_PETITION_ENUM_VALUE = "FINISHED";
    public static final String STATUS_ID_PETITION_FRONTEND_VALUE = "TERMINADO";
    public static final String STATUS_ID_PROCESS_ENUM_VALUE = "ANALYSIS";
    public static final String STATUS_ID_PROCESS_FRONTEND_VALUE = "EN_PROCESO";
    public static final String STATUS_ID_REGISTER_ENUM_VALUE = "REGISTERED";
    public static final String STATUS_ID_REGISTER_FRONTEND_VALUE = "EN_REGISTRO";
    public static final String CONTACTDETAILS_CONTACTTYPE_EMAIL_ENUM_VALUE = "EMAIL";
    public static final String CONTACTDETAILS_CONTACTTYPE_EMAIL_FRONTEND = "EMA";
    public static final String PRODUCT_CLAIM_ID = "123";
    public static final String RESOLUTION_DELIVERY_TYPE_ENUM_VALUE = "EMA";
    public static final String RESOLUTION_DELIVERY_TYPE_FRONTEND = "EMAIL";
    public static final String CONTRACT_NUMBER_TYPE_ENUM_VALUE = "PAN";
    public static final String CONTRACT_NUMBER_TYPE_FRONTEND = "PAN";
    public static final String CLAIMS_DELIVERY_TYPE_ENUM_VALUE = "PR_EMAIL";
    public static final String CLAIMS_DELIVERY_TYPE_FRONTEND = "EMAIL";

    public static final String CLAIMS_CATEGORY_ID_SIMPLE_FRONTEND = "SIMPLE";
    public static final String CLAIMS_CATEGORY_ID_SIMPLE_BACKEND = "REQC_UNICO";

    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_EMAIL_FRONTEND = "EMAIL";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_EMAIL_BACKEND = "EMAIL";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_LANDLINE_FRONTEND = "LANDLINE";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_LANDLINE_BACKEND = "LANDLINE";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_FRONTEND = "MOBILE";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_MOBILE_BACKEND = "MOBILE";

    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_MOBILE_FRONTEND = "MOBILE";
    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_MOBILE_BACKEND = "MOBILE";

    public static final String CLAIMS_DELIVERY_DELIVERYTYPEID_BACKEND = "PR_EMAIL";
    public static final String CLAIMS_DELIVERY_DELIVERYTYPEID_FRONTEND = "EMAIL";
    public static final String CLAIMS_CONTRACT_NUMBERTYPE_BACKEND = "LIC";
    public static final String CLAIMS_CONTRACT_NUMBERTYPE_FRONTEND = "LOCAL";
    public static final String CLAIMS_RESOLUTION_RESOLUTIONTYPE_FRONTEND_VALUE = "EMAIL";
    public static final String CLAIMS_RESOLUTION_RESOLUTIONTYPE_ENUM_VALUE = "EMAIL";
    public static final String CLAIMS_CATEGORY_ID_FRONTEND_VALUE = "REQC_UNICO";
    public static final String CLAIMS_CATEGORY_ID_ENUM_VALUE = "SIMPLE";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_EMAIL_VALUE = "EMAIL";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_EMAIL_VALUE = "EMAIL";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_LANDLINE_VALUE = "LANDLINE";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_LANDLINE_VALUE = "LANDLINE";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_FRONTEND_MOBILE_VALUE = "MOBILE";
    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_ENUM_MOBILE_VALUE = "MOBILE";
    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_FRONTEND_VALUE = "PHONE_NUMBER";
    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_ENUM_VALUE = "PHONE";
    public static final String CLAIMS_PETITIONER_BANKCLASSIFICATIONTYPE_FRONTEND_VALUE = "0";
    public static final String CLAIMS_PETITIONER_BANKCLASSIFICATIONTYPE_ENUM_VALUE = "NORMAL";
    public static final String CLAIMS_RESOLUTION_RESOLUTIONTYPE_BACKEND_DEFI="DEFI";
    public static final String CLAIMS_RESOLUTION_RESOLUTIONTYPE_FRONTEND_PERMANENT="PERMANENT";

    public static final String AUTOMATICREFUNDTRANSACTIONS_STATUS_ID_FRONTEND = "APPROVED";
    public static final String AUTOMATICREFUNDTRANSACTIONS_STATUS_ID_BACKEND = "A";
    public static final String AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID_FRONTEND = "ASSIGNED";
    public static final String AUTOMATICREFUNDTRANSACTIONS_BUSINESSAGENTS_ID_BACKEND = "R";

    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_FRONTEND_OFFICE_VALUE = "OFFICE";
    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_BACKEND_OFFICE_NUMBER_VALUE = "OFFICE_NUMBER";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_CARD_VALUE = "CARD";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_TARJE_VALUE = "TARJE";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_ACCOUNT_VALUE = "ACCOUNT";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_CUENT_VALUE = "CUENT";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_FRONTEND_LOAN_VALUE = "LOAN";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_CONTRACTTYPE_BACKEND_PREST_VALUE = "PREST";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_FRONTEND_PAN_VALUE = "PAN";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_PAN_VALUE = "PAN";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_FRONTEND_LIC_VALUE = "LIC";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_NUMBERTYPE_BACKEND_LIC_VALUE = "LIC";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_FRONTEND_PURCHASE_VALUE = "PURCHASE";
    public static final String CLAIMS_AUTOMATICREFUNDTRANSACTION_TRANSACTIONTYPE_BACKEND_PURCHASE_VALUE = "PURCHASE";
    public static final String AUTOMATICREFUNDTRANSACTION_BUSINESSAGENTS_ID_BACKEND_ASSIGNED = "R";
    public static final String AUTOMATICREFUNDTRANSACTION_BUSINESSAGENTS_ID_FRONTEND_R = "ASSIGNED";
    public static final String AUTOMATICREFUNDTRANSACTION_OPERATIONS_OPERATIONTYPE_ID_BACKEND_INTEREST = "INT";
    public static final String AUTOMATICREFUNDTRANSACTION_OPERATIONS_OPERATIONTYPE_ID_FRONTEND_INT = "INTEREST";
    public static final String AUTOMATICREFUNDTRANSACTION_OPERATIONS_SUBPRODUCTREFUNDMODE_BACKEND_CASHING = "P";
    public static final String AUTOMATICREFUNDTRANSACTION_OPERATIONS_SUBPRODUCTREFUNDMODE_FRONTEND_P = "CASHING";
    public static final String AUTOMATICREFUNDTRANSACTION_OPERATIONS_EXCHANGERATE_BACKEND_SALE="V";
    public static final String AUTOMATICREFUNDTRANSACTION_OPERATIONS_EXCHANGERATE_FRONTEND_V="SALE";

    public static final String CLAIMS_PETITIONER_CONTACTDETAILTYPE_BACKEND_LANDLINE_VALUE = "PHONE";
    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_BACKEND_PHONE_NUMBER_VALUE = "PHONE_NUMBER";
    public static final String CLAIMS_PETITIONER_CONTACT_PHONETYPE_FRONTEND_HOME_VALUE = "HOME";

    public static final String REJECTED_STATUS_ID_FRONTEND_VALUE = "REJECTED";
    public static final String REJECTED_STATUS_ID_BACKEND_VALUE = "R";
    public static final String INSURANCE_PREMIUM_OPERATION_TYPE_BACKEND_VALUE = "CAP_ASEG";
    public static final String INSURANCE_PREMIUM_OPERATION_TYPE_FRONTEND_VALUE = "INSURANCE_PREMIUM";

    public static final String AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_IN = "A";
    public static final String AUTOMATIC_REFUND_TRANSACTIONS_APPROVED_OUT = "APPROVED";
    public static final String AUTOMATIC_REFUND_TRANSACTIONS_REJECTED_IN = "R";
    public static final String AUTOMATIC_REFUND_TRANSACTIONS_REJECTED_OUT = "REJECTED";

    private static final EntityMock INSTANCE = new EntityMock();
    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private EntityMock() {
    }

    public static EntityMock getInstance() {
        return INSTANCE;
    }

    public static PetitionerSearch buildPetitioner(String bankRelationType, String petitionerType) {
        PetitionerSearch petitionerSearch = new PetitionerSearch();
        petitionerSearch.setBankRelationType(bankRelationType);
        petitionerSearch.setPetitionerType(new PetitionerType());
        petitionerSearch.getPetitionerType().setId(petitionerType);
        return petitionerSearch;
    }

    public DTOIntProductClaim getDTOOutProductClaimKnownTransaction() throws IOException {
        DTOIntProductClaim dtoProductClaim = objectMapper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "mock/dtoIntProductClaimTransaction.json"), DTOIntProductClaim.class);

        DTOIntKnownPetitioner dtoIntKnowPetitioner = new DTOIntKnownPetitioner();
        dtoIntKnowPetitioner.setId("55");
        dtoIntKnowPetitioner.setBankRelationType("KNOWN");
        DTOIntPetitionerType dtoIntPetitionerType = new DTOIntPetitionerType();
        dtoIntPetitionerType.setId("HOLDER");
        dtoIntKnowPetitioner.setPetitionerType(dtoIntPetitionerType);
        dtoIntKnowPetitioner.setAddress(getDTOIntAddress());
        dtoIntKnowPetitioner.setContactDetails(getDTOIntContact());

        List<DTOIntPetitioners> dtoIntPetitioners = new ArrayList<>();
        DTOIntPetitioners dtoIntPetitioner = new DTOIntPetitioners();
        dtoIntPetitioner.setPetitioner(dtoIntKnowPetitioner);
        dtoIntPetitioners.add(dtoIntPetitioner);

        dtoProductClaim.setPetitioners(dtoIntPetitioners);
        return dtoProductClaim;
    }

    private DTOIntAddress getDTOIntAddress() {
        DTOIntAddress dtoIntAddress = new DTOIntAddress();
        dtoIntAddress.setId("678");
        dtoIntAddress.setLocation(getDTOIntLocation());
        return dtoIntAddress;
    }

    private DTOIntLocation getDTOIntLocation() {
        DTOIntLocation dtoIntLocation = new DTOIntLocation();
        dtoIntLocation.setFormattedAddress("calle acuario 200");
        dtoIntLocation.setAdditionalInformation("block 2");

        List<DTOIntAddressComponent> dtoIntAddressComponents = new ArrayList<>();

        DTOIntAddressComponent dtoIntAddressComponent = new DTOIntAddressComponent();
        dtoIntAddressComponent.setComponentTypes(Collections.singletonList("UBIGEO"));
        dtoIntAddressComponent.setCode("101001");
        dtoIntAddressComponents.add(dtoIntAddressComponent);

        dtoIntLocation.setAddressComponents(dtoIntAddressComponents);
        return dtoIntLocation;
    }

    private List<DTOIntContact> getDTOIntContact() {
        List<DTOIntContact> dtoIntContacts = new ArrayList<>();

        DTOIntEmail dtoIntEmail = new DTOIntEmail();
        dtoIntEmail.setId("91");
        DTOContactDetailType dtoContactDetailType = new DTOContactDetailType();
        dtoContactDetailType.setId("EMAIL");
        dtoIntEmail.setContactDetailType(dtoContactDetailType);
        dtoIntEmail.setAddress("cynthia.gw@gmail.com");
        DTOIntContact dtoIntContact = new DTOIntContact();
        dtoIntContact.setContact(dtoIntEmail);
        dtoIntContacts.add(dtoIntContact);

        DTOIntMobile dtoIntMobile = new DTOIntMobile();
        dtoIntMobile.setId("92");
        dtoContactDetailType = new DTOContactDetailType();
        dtoContactDetailType.setId("MOBILE");
        dtoIntMobile.setContactDetailType(dtoContactDetailType);
        dtoIntMobile.setNumber("987612346543");
        DTOIntPhoneCompany dtoIntPhoneCompany = new DTOIntPhoneCompany();
        dtoIntPhoneCompany.setId("01");
        dtoIntMobile.setPhoneCompany(dtoIntPhoneCompany);
        dtoIntContact = new DTOIntContact();
        dtoIntContact.setContact(dtoIntMobile);
        dtoIntContacts.add(dtoIntContact);

        DTOIntMobile dtoIntLandline = new DTOIntMobile();
        dtoIntLandline.setId("93");
        dtoContactDetailType = new DTOContactDetailType();
        dtoContactDetailType.setId("LANDLINE");
        dtoIntLandline.setContactDetailType(dtoContactDetailType);
        dtoIntLandline.setNumber("015325678");
        dtoIntPhoneCompany = new DTOIntPhoneCompany();
        dtoIntPhoneCompany.setId("02");
        dtoIntLandline.setPhoneCompany(dtoIntPhoneCompany);
        dtoIntContact = new DTOIntContact();
        dtoIntContact.setContact(dtoIntLandline);
        dtoIntContacts.add(dtoIntContact);

        return dtoIntContacts;
    }

    public DTOIntProductClaim getDTOOutProductClaimUnknownTransaction() throws IOException {
        DTOIntProductClaim dtoProductClaim = objectMapper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "mock/dtoIntProductClaimTransaction.json"), DTOIntProductClaim.class);

        // Set Unknown (Holder by default)
        DTOIntUnknownPetitioner dtoIntUnknowPetitioner = new DTOIntUnknownPetitioner();
        dtoIntUnknowPetitioner.setId("01");
        dtoIntUnknowPetitioner.setBankRelationType("UNKNOWN");
        DTOIntPetitionerType dtoIntPetitionerType = new DTOIntPetitionerType();
        dtoIntPetitionerType.setId("AUTHORIZED");

        dtoIntUnknowPetitioner.setPetitionerType(dtoIntPetitionerType);
        dtoIntUnknowPetitioner.setAddress(getDTOIntAddress());
        dtoIntUnknowPetitioner.setContactDetails(getDTOIntContact());

        dtoIntUnknowPetitioner.setIdentityDocument(getDTOIntIdentityDocument());
        dtoIntUnknowPetitioner.setFirstName("LUIGI");
        dtoIntUnknowPetitioner.setLastName("RIVEROS");
        dtoIntUnknowPetitioner.setSecondLastName("CACERES");

        List<DTOIntPetitioners> dtoIntPetitioners = new ArrayList<>();
        DTOIntPetitioners dtoIntPetitioner = new DTOIntPetitioners();
        dtoIntPetitioner.setPetitioner(dtoIntUnknowPetitioner);
        dtoIntPetitioners.add(dtoIntPetitioner);

        dtoProductClaim.setPetitioners(dtoIntPetitioners);
        return dtoProductClaim;
    }

    private DTOIntIdentityDocument getDTOIntIdentityDocument() {
        DTOIntIdentityDocument dtoIntIdentityDocument = new DTOIntIdentityDocument();
        DTOIntDocumentType dtoIntDocumentType = new DTOIntDocumentType();
        dtoIntDocumentType.setId("RUC");
        dtoIntIdentityDocument.setDocumentType(dtoIntDocumentType);
        dtoIntIdentityDocument.setDocumentNumber("12345678");
        return dtoIntIdentityDocument;
    }

    public ResponseType getResultadoResponseType() {
        ResponseType output = new ResponseType();
        RequerimientoType requerimientoType = new RequerimientoType();
        requerimientoType.setNumero("11223344");
        output.setRequerimiento(requerimientoType);
        return output;
    }

    public ProductClaim getDTOIntProductClaimUnknownTransaction() throws IOException {
        return objectMapper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "mock/productClaimTransaction.json"), ProductClaim.class);
    }

    public ProductClaim getDTOIntProductClaimKnownTransaction() throws IOException {
        return objectMapper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "mock/productClaimTransaction.json"), ProductClaim.class);
    }

    public ProductClaims buildProductClaims() throws IOException {
        return objectMapper.readValue(
                Thread.currentThread().getContextClassLoader().getResourceAsStream(
                        "mock/productClaims.json"), ProductClaims.class);
    }

    public List<DataSource> buildAttachments() throws URISyntaxException {
        URL resource = Thread.currentThread().getContextClassLoader().getResource("mock/bbva-logo.png");
        if (resource == null) {
            return null;
        }
        return Collections.singletonList(new FileDataSource(Paths.get(resource.toURI()).toFile()));
    }

    public DTOIntProductClaim getFileDataSourceEncodedBase64String() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/bbva-logo.json"), DTOIntProductClaim.class);
    }

    public Search buildSearch() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/search.json"), Search.class);
    }

    public InputSearchProductClaims buildInputSearchProductClaims() throws IOException {
        InputSearchProductClaims input = objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/inputSearchProductClaims.json"), InputSearchProductClaims.class);

        DTOIntUnknownPetitioner dtoIntUnknownPetitioner = new DTOIntUnknownPetitioner();
        dtoIntUnknownPetitioner.setIdentityDocument(getDTOIntIdentityDocument());
        input.setPetitioner(dtoIntUnknownPetitioner);
        return input;
    }

    public InputGetProductClaim buildInputGetProductClaim() {
        InputGetProductClaim inputGetProductClaim = new InputGetProductClaim();
        inputGetProductClaim.setProductClaimId(PRODUCT_CLAIM_ID);
        return inputGetProductClaim;
    }

    public SendEmail buildSendEmail() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/sendEmail.json"), SendEmail.class);
    }

    public InputSendEmailProductClaim buildInputSendEmailProductClaim() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/inputSendEmailProductClaim.json"), InputSendEmailProductClaim.class);
    }

    public List<PetitionerSearch> buildPetitionerSearch() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/petitionerSearch.json"), new TypeReference<List<PetitionerSearch>>() {
        });
    }
    public static PetitionerSearch buildRepresentativePetitioner(String bankRelationType) {
        PetitionerSearch petitionerSearch = new PetitionerSearch();
        petitionerSearch.setBankRelationType(bankRelationType);
        petitionerSearch.setPetitionerType(new PetitionerType());
        petitionerSearch.getPetitionerType().setId("AUTHORIZED");
        return petitionerSearch;
    }
    public InputListAutomaticRefundTransactions getInputListAutomaticRefundTransactions() {
        final InputListAutomaticRefundTransactions input = new InputListAutomaticRefundTransactions();
        input.setApp(APP);
        input.setCustomerId(CUSTOMER_ID);
        return input;
    }
    public InputGetAutomaticRefundTransaction getInputGetAutomaticRefundTransaction() {
        final InputGetAutomaticRefundTransaction input = new InputGetAutomaticRefundTransaction();
        input.setApp(APP);
        input.setAutomaticRefundTransactionId(AUTOMATIC_REFUND_TRANSACTION_ID);
        return input;
    }

    public AutomaticRefundTransactionF buildAutomaticRefundTransactionF() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/inputAutomaticRefundTransactionF.json"), AutomaticRefundTransactionF.class);
    }

    public InputModifyAutomaticRefundTransaction buildInputModifyAutomaticRefundTransaction() {
        InputModifyAutomaticRefundTransaction dtoIn = new InputModifyAutomaticRefundTransaction();
        DTOIntPartialTransaction dtoPTransaction = new DTOIntPartialTransaction();
        DTOIntSituation dtoSituation = new DTOIntSituation();
        DTOIntStatus dtoStatus = new DTOIntStatus();
        DTOIntSubStatus dtoSubStatus = new DTOIntSubStatus();
        dtoSubStatus.setId(SUB_STATUS_ID);
        dtoStatus.setSubStatus(dtoSubStatus);
        dtoSituation.setId(SITUATION_ID);
        dtoPTransaction.setSituation(dtoSituation);
        dtoPTransaction.setStatus(dtoStatus);
        dtoPTransaction.setCustomerConfirmation(CUSTOMER_CONFIRMATION);
        dtoIn.setPartialTransaction(dtoPTransaction);
        dtoIn.setUser(USER);
        dtoIn.setIdCanal(APP);
        dtoIn.setAutomaticRefundTransactionId(AUTOMATIC_REFUND_TRANSACTION_ID);
        return dtoIn;
    }

    public ResultadoConformidad buildResultadoConformidad() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("mock/resultadoConformidad.json"), ResultadoConformidad.class);

    }

    public ModelResultadoResponse buildValidateAutomaticRefundTransactionModelResponse() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/validateAutomaticRefundTransaction/modelResponse.json"), ModelResultadoResponse.class);
    }
    public DTOIntAutomaticRefundTransaction getDTOIntAutomaticRefundTransaction() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/dtoIntAutomaticRefundTransaction.json"), DTOIntAutomaticRefundTransaction.class);
    }
    public AutomaticRefundTransaction getAutomaticRefundTransaction() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "mock/automaticRefundTransaction.json"), AutomaticRefundTransaction.class);
    }
}
