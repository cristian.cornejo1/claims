package com.bbva.pzic.claims.util;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoField;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;

/**
 * Created on 31/12/2019.
 *
 * @author Cesardl
 */
public final class WADLRequirements {

    public static void implementsSerializableCheck(StringBuilder result, PojoClass canonic) {
        boolean discriminator = false;
        for (Class<?> i : canonic.getClazz().getInterfaces()) {
            if (i.getName().equalsIgnoreCase(Serializable.class.getName())) {
                discriminator = true;
                break;
            }
        }

        if (!discriminator) {
            result.append(String.format("\n\t%s not implement Serializable class", canonic.getClazz().getName()));
        }
    }

    public static void fieldSerialVersionUIDCheck(StringBuilder result, PojoClass canonic) {
        boolean discriminator = false;
        for (PojoField field : canonic.getPojoFields()) {
            if (field.getName().equalsIgnoreCase("serialVersionUID")) {
                discriminator = true;
                break;
            }
        }

        if (!discriminator) {
            result.append(String.format("\n\t%s not contain serialVersionUID property", canonic.getClazz().getName()));
        }
    }

    public static void annotationXmlRootElementCheck(StringBuilder result, PojoClass canonic) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        XmlRootElement xmlRootElement = canonic.getClazz().getAnnotation(XmlRootElement.class);
        if (xmlRootElement != null) {
            nameCheck(result, canonic, xmlRootElement);
            nameSpaceCheck(result, canonic, xmlRootElement);
        } else {
            result.append(String.format("\n\t%s not contain annotation @XmlRootElement", canonic.getClazz().getName()));
        }
    }

    public static void annotationXmlTypeCheck(StringBuilder result, PojoClass canonic) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        XmlType xmlType = canonic.getClazz().getAnnotation(XmlType.class);
        if (xmlType != null) {
            nameCheck(result, canonic, xmlType);
            nameSpaceCheck(result, canonic, xmlType);
        } else {
            result.append(String.format("\n\t%s not contain annotation @XmlType", canonic.getClazz().getName()));
        }
    }

    public static void annotationXmlAccessorTypeCheck(StringBuilder result, PojoClass canonic) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        XmlAccessorType xmlAccessorType = canonic.getClazz().getAnnotation(XmlAccessorType.class);
        if (xmlAccessorType != null) {
            valueCheck(result, canonic, xmlAccessorType);
        } else {
            result.append(String.format("\n\t%s not contain annotation @XmlAccessorType", canonic.getClazz().getName()));
        }
    }

    private static void valueCheck(StringBuilder result, PojoClass canonic, XmlAccessorType xmlAccessorType) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String expected = XmlAccessType.FIELD.name();
        String actual = ((XmlAccessType) xmlAccessorType.getClass().getMethod("value").invoke(xmlAccessorType)).name();
        if (!expected.equals(actual)) {
            System.out.println(xmlAccessorType.getClass().getName());
            result.append(
                    String.format("\n\t%s invalid value in the @%s annotation, expected %s, actual %s",
                            canonic.getClazz().getName(),
                            ((Annotation) xmlAccessorType).annotationType().getSimpleName(),
                            expected, actual));
        }
    }

    private static <T> void nameCheck(StringBuilder result, PojoClass canonic, T annotation) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String expect = canonic.getClazz().getSimpleName();
        String actual = (String) annotation.getClass().getMethod("name").invoke(annotation);
        if (!expect.equalsIgnoreCase(actual)) {
            System.out.println(annotation.getClass().getName());
            result.append(
                    String.format("\n\t%s invalid name in the @%s annotation",
                            canonic.getClazz().getName(),
                            ((Annotation) annotation).annotationType().getSimpleName()));
        }
    }

    private static <T> void nameSpaceCheck(StringBuilder result, PojoClass canonic, T annotation) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String actual = (String) annotation.getClass().getMethod("namespace").invoke(annotation);
        String expect = "urn:".concat(canonic.getClazz().getPackage().getName().replace(".", ":"));
        if (!expect.equalsIgnoreCase(actual)) {
            result.append(
                    String.format("\n\t%s invalid namespace in the @%s annotation",
                            canonic.getClazz().getName(),
                            ((Annotation) annotation).annotationType().getSimpleName()));
        }
    }
}
