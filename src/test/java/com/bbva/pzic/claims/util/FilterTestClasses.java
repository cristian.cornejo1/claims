package com.bbva.pzic.claims.util;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;

/**
 * @author Entelgy
 */
public class FilterTestClasses implements PojoClassFilter {

    public boolean include(PojoClass pojoClass) {
        return !pojoClass.getSourcePath().contains("/test-classes/");
    }
}
